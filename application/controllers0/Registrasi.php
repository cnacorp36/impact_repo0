<?php 
 
class Registrasi extends CI_Controller{
 
	function __construct(){
		parent::__construct();		
		 $this->load->model('m_login');
		 $this->load->model('M_models');
	}
 
	function index(){
		$this->load->view('admin/registrasi');
	}

	public function add(){
		$email = strtolower($this->input->post('email'));
		$password = md5(strtolower($this->input->post('password')));
		$nama = strtoupper($this->input->post('nama'));
		$pabrik = $this->input->post('pabrik');

		//die(json_encode(array('success' => 0, 'msg' => $email)));

		$cek_email = $this->M_models->get_data('id_user', 'user', null, array('email' => $email));
		if($cek_email->num_rows() > 0){
			die(json_encode(array('success' => 0, 'msg' => 'Email sudah ada')));
		}

		$id_user = $this->M_models->get_id('user');
		$data_user = array(
			'id_user' => $id_user,
			'password' => $password,
			'role' => '2',
			'email' => $email,
			'status' => '1'
		);
		$this->M_models->insertData('user', $data_user);

		$id_perusahaan = $this->M_models->get_id('u_perusahaan');
		$data_perusahaan = array(
			'id_perusahaan' => $id_perusahaan,
			'nama' => $nama,
			'id_kategori' => 'k_01',
			'id_user' => $id_user
		);

		$this->M_models->insertData('u_perusahaan', $data_perusahaan);

		for($a=0; $a<count($pabrik); $a++){
			$data_pabrik = array(
				'id_pabrik' => $this->M_models->get_id('pabrik'),
				'id_perusahaan' => $id_perusahaan,
				'status' => '1',
				'alamat' => $pabrik[$a]['alamat'],
				'luas' => $pabrik[$a]['luas'],
				'kapasitas' => $pabrik[$a]['kapasitas']
			);

			$this->M_models->insertData('pabrik', $data_pabrik);
		}

		$data_session = array(
			'email' => $email,
			'role' => '2'
		);

		$this->session->set_userdata('impact_sess', $data_session);

		die(json_encode(array('success' => 1)));
	}
}