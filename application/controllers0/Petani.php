<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function __construct(){
		parent::__construct();		
		 $this->load->model('M_models');
		 
	}
class Petani extends CI_Controller {
	public function index()
	{
		$data['menu'] = 'petani';

		$this->load->view('admin/petani', $data);
	}
}
