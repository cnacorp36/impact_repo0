<?php 
 
class Login extends CI_Controller{
 
	function __construct(){
		parent::__construct();		
		$this->load->model('m_login');
		$this->load->model('M_models');
	}
 
	function index(){
		$this->load->view('admin/login');
	}
	function aksi_login(){
		$email = strtolower($this->input->post('email'));
		$password = md5(strtolower($this->input->post('password')));

		//die(json_encode(array('success' => 0, 'msg' => $password)));
		$cek_email = $this->M_models->get_data('id_user', 'user', null, array('email' => $email));

		if($cek_email->num_rows() == 0){
			die(json_encode(array('success' => 0, 'msg' => 'Email belum terdaftar')));
		}

		$cek = $this->M_models->get_data('', 'user', null, array('email' => $email, 'password' => $password), '', '', 0, 0, array(0 => "role='2' or role='0' or role='1'"));

		if($cek->num_rows() > 0){

			if($cek->row()->status == '0'){
				die(json_encode(array('success' => 0, 'msg' => 'Akun Anda tidak aktif')));
			}
 
			$data_session = array(
				'email' => $email,
				'role' => $cek->row()->role
			);
 
			$this->session->set_userdata('impact_sess', $data_session);
 
			die(json_encode(array('success' => 1)));
 
		}else{
			die(json_encode(array('success' => 0, 'msg' => 'Email atau password salah')));
		}
	}
 
	function logout(){
		$this->session->sess_destroy();
		redirect(base_url('welcome'));
	}
}