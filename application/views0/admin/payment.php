<?php $this->load->view('backend/header');?>
<?php $this->load->view('backend/navbar');?>
  <div class="breadcomb-area">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="breadcomb-list">
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="breadcomb-wp">
                  <div class="breadcomb-icon">
                    <i class="notika-icon notika-dollar"></i>
                  </div>
                  <div class="breadcomb-ctn">
                    <h2>PAYMENT</h2>
                    <p>KELAPA SAWIT</span></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="normal-table-area">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="normal-table-list mg-t-30">
            <div class="basic-tb-hd">
              <h2 style="text-align: center;">TAGIHAN</h2>
              
            </div>
            <div class="bsc-tbl-st">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>ID PEMBAYARAN</th>
                    <th>TOTAL HARGA</th>
                    <th>REKENING TUJUAN</th>
                    <th>TANGGAL KADALUARSA</th>
                    <th>STATUS</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $z=1;
                  foreach ($payment->result() as $row_bid) {
                    ?>
                    <tr>
                      <td><?=$z?></td>
                      <td><?=$row_bid->id_pembayaran?></td>
                      <td>Rp. <?=number_format($row_bid->total_bayar + $row_bid->kode_unik, 2)?></td>
                      <td>
                        <?php
                          if($row_bid->id_bank_tujuan != ''){
                        ?>
                        <div class="row">
                          <div class="col-md-6">
                            Nama Bank
                          </div>
                          <div class="col-md-6">
                            : <b style="color: #E53956"><?=$row_bid->nama_bank?></b>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            No. Rekening
                          </div>
                          <div class="col-md-6">
                            : <b style="color: #E53956"><?=$row_bid->norek?></b>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            Atas Nama
                          </div>
                          <div class="col-md-6">
                            : <b style="color: #E53956"><?=$row_bid->atas_nama?></b>
                          </div>
                        </div>
                        <?php
                          }else{
                        ?>
                        <button type="button" class="btn btn-primary" onclick="update_bank_tujuan('<?=$row_bid->id_pembayaran?>')">Pilih Bank Tujuan</button>
                        <?php
                          }
                        ?>
                      </td>
                      <td>
                        <?php
                          echo date('d / M / Y (H:i:s)', strtotime($row_bid->tgl_expired));
                        ?>
                      </td>
                      <td>
                        <?php
                          if($row_bid->status_bayar == '0'){
                            echo '<p style="color:red">Menunggu Pembayaran</p>';
                          }else if($row_bid->status_bayar == '2'){
                            echo '<p style="color:green">LUNAS</p>';
                            echo '<a href="'.base_url('delivery').'" class="btn btn-primary">LIHAT PENGIRIMAN</a>';
                          }
                        ?>
                      </td>
                    </tr>
                      <?php
                      $z++;
                    }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
          
          <br>
        </div>
      </div>
    </div>
  </div>

  
  <script type="text/javascript">
    function change_check(index, val){
      //console.log(val);
      if(val == '0'){
        $('#check'+index).val('1');
        var harga = $('#harga'+index).val();
        var total_ = $('#total_').val();
        //$('#total').html('Rp. '+(parseInt(total_) + parseInt(harga)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
        $('#total_').val(parseInt(total_) + parseInt(harga));
      }else{
        $('#check'+index).val('0');
        var harga = $('#harga'+index).val();
        var total_ = $('#total_').val();
        //$('#total').html('Rp. '+(parseInt(total_) - parseInt(harga)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
        $('#total_').val(parseInt(total_) - parseInt(harga));
      }
    }

    function change_bank(val) {
      if(val != ''){
        $('#selected_bank').val(val);
      }
    }

    function update_bank_tujuan(id_pembayaran) {
      swal({
        title: 'Mohon Tunggu',
        showConfirmButton: false,
        onOpen: () => {
          $.ajax({
            type: "POST",
            url: '<?php echo base_url()?>payment/load_bank',
            dataType: "json",
            data: {
            },
            success: function (data) {
              if(data.success == '1'){
                swal({
                  html: '\n\
                  <div>\n\
                    <div class="row">\n\
                      <p>Pilih Rekening Tujuan</p>'+data.data_bank+'\n\
                      <input type="hidden" id="selected_bank">\n\
                    </div>\n\
                  </div>\n\
                  ',
                  showCancelButton: true,
                  cancelButtonColor: '#d33',
                  onOpen: () => {},
                  onClose: () => {}
                }).then((result) => {
                  if(result){
                    var id_bank_tujuan = $('#selected_bank').val();
                    if(id_bank_tujuan != ''){
                      $.ajax({
                        type: "POST",
                        url: '<?php echo base_url()?>payment/update_bank_tujuan',
                        dataType: "json",
                        data: {
                          id_bank_tujuan: id_bank_tujuan,
                          id_pembayaran: id_pembayaran
                        },
                        success: function (data) {
                          if(data.success == '1'){
                            //send email pembayaran
                            $.ajax({
                              type: "GET",
                              url: '<?php echo base_url()?>email_handler/send_email_pembayaran/'+id_pembayaran,
                              dataType: "json",
                              data: {
                              },
                              success: function (data) {
                              },
                              error: function () {
                              }
                            })
                            swal({
                                title: 'Kirim permintaan berhasil!',
                                type: 'success'
                            }).then((result) => {
                                window.location.href='<?php echo base_url()?>payment';
                            })
                          }else{
                            swal({
                                title: 'Oops..',
                                text: data.msg,
                                type: 'error'
                            }).then((result) => {

                            })
                          }
                          //swal.close();
                        },
                        error: function () {
                          swal({
                              title: 'Oops.. Server sedang error.',
                              type: 'error'
                          }).then((result) => {

                          })
                        }
                      })
                    }
                  }
                })
              }else{
                swal({
                    title: 'Oops..',
                    text: data.msg,
                    type: 'error'
                }).then((result) => {

                })
              }
            },
            error: function () {
              swal({
                  title: 'Oops.. Server sedang error.',
                  type: 'error'
              }).then((result) => {

              })
            }
          })
        },
        onClose: () => {

        }
      })
    }
  </script>
<?php $this->load->view('backend/footer');?>    