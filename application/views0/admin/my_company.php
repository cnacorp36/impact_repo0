 <?php $this->load->view('backend/header')?>
<?php $this->load->view('backend/navbar')?>
 <div class="breadcomb-area">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="breadcomb-list">
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="breadcomb-wp">
                  <div class="breadcomb-icon">
                    <i class="notika-icon notika-map"></i>
                  </div>
                  <div class="breadcomb-ctn">
                    <h2>MY COMPANY</h2>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
 <div class="form-element-area">
        <div class="container">
             <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-element-list mg-t-30">
                        <div class="cmp-tb-hd">
                            <h2>My Company</h2>
                            <!-- <p>An input mask helps the user with the input. This can be useful for dates, numerics, phone numbers etc...</p> -->
                        </div>
                        <div class="row">
                                <?php
                              
                                $join_profile = array(0 => 'u_perusahaan p-p.id_user=u.id_user', 
                                1 => 'ref_kategori k-k.id_kategori=p.id_kategori');
                                
                                $profil = $this->M_models->get_data('p.*, k.nama as k_nama', 'user u', $join_profile, array('u.email' => $sess['email']), '', '', 1);
                                //$profil = $this->M_models->get_from_query("select a.*,b.nama as nama_kate from u_perusahaan a left join ref_kategori b on b.id_kategori = a.id_kategori")->result();
                                $key = $profil->row();
                                
                                    ?>
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group ic-cmp-int">
                                    <div class="form-cmp">
                                        Klasifikasi
                                    </div>
                                    <div class="nk-int-st">
                                        <input readonly type="text" class="form-control" data-mask="999-99-999-9999-9" value="<?php echo $key->k_nama?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group ic-cmp-int">
                                    <div class="form-cmp">
                                        Nama Perusahaan
                                    </div>
                                    <div class="nk-int-st">
                                        <input readonly type="text" class="form-control" data-mask="999-99-999-9999-9" value="<?php echo $key->nama?>">
                                    </div>
                                </div>
                            </div>
                        </div><br><br>
                        <?php
                            $q_factory = $this->M_models->get_data('*', 'pabrik', null, array('id_perusahaan' => $key->id_perusahaan), '', 'id_pabrik asc');

                            $a = 1;
                            foreach ($q_factory->result() as $_key) {
                                $status_ = '';
                                if($_key->status == '1'){
                                    $status_ = '<p style="color: green; font-size: 12px">(Aktif)</p>';
                                }else{
                                    $status_ = '<p style="color: red; font-size: 12px">(Tidak Aktif)</p>';
                                }
                                ?>
                        <hr>
                        <div class="cmp-tb-hd">
                            <h2>My Factory <?php echo $a?> <?php echo $status_?></h2>
                            <!-- <p>An input mask helps the user with the input. This can be useful for dates, numerics, phone numbers etc...</p> -->
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <div class="nk-int-mk">
                                    <h2>Alamat</h2>
                                </div>
                                <div class="form-group ic-cmp-int">
                                    <div class="form-ic-cmp">
                                        <i class="notika-icon notika-map"></i>
                                    </div>
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" data-mask="99-9999999" placeholder="Tax ID" value="<?php echo $_key->alamat?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <div class="nk-int-mk">
                                    <h2>Luas  PKS</h2>
                                </div>
                                <div class="form-group ic-cmp-int">
                                    <div class="form-ic-cmp">
                                        <i class="notika-icon notika-phone"></i>
                                    </div>
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" data-mask="(999) 999-9999" placeholder="Phone" value="<?php echo $_key->luas?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <div class="nk-int-mk">
                                    <h2>Kapasitas</h2>
                                </div>
                                <div class="form-group ic-cmp-int">
                                    <div class="form-ic-cmp">
                                        <i class="notika-icon notika-dollar"></i>
                                    </div>
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" data-mask="$ 999,999,999.99" placeholder="Currency" value="<?php echo $_key->kapasitas?>">
                                    </div>
                                </div>
                            </div>
                                    
                           
                        </div>
                                <?php
                                $a++;
                            }
                        ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
     <div class="data-table-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="data-table-list">
                        <div class="basic-tb-hd">
                            <h2>PO History</h2>
                            <!-- <p>It's just that simple. Turn your simple table into a sophisticated data table and offer your users a nice experience and great features without any effort.</p> -->
                        </div>
                        <div class="table-responsive">
                            <table id="data-table-basic" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>ID PO</th>
                                        <th>Date</th>
                                        <th>QTY</th>
                                        <th>Price</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $q_po = $this->M_models->get_data(
                                            'pod.id_purchase_order, do.tgl_add, dod.qty_realisasi, pod.harga, k.satuan',
                                            'delivery_order_detail dod',
                                            array(
                                                0 => 'delivery_order do-do.id_do=dod.id_do',
                                                1 => 'purchase_order_detail pod-pod.id_purchase_order=do.id_po and pod.id_produk=dod.id_produk',
                                                2 => 'purchase_order po-po.id_po=do.id_po',
                                                3 => 'bid b-b.id_bid=po.id_bid',
                                                4 => 'u_perusahaan up-up.id_perusahaan=b.id_perusahaan',
                                                5 => 'user u-u.id_user=up.id_user',
                                                6 => 'produk pr-pr.id_produk=dod.id_produk',
                                                7 => 'kebun k-k.id_kebun=pr.id_kebun'
                                            ),
                                            array(
                                                'u.email' => $this->cek['email']
                                            ), '', 'do.tgl_add desc'
                                        );

                                        foreach($q_po->result() as $key){
                                            $satuan = $key->satuan == '0' ? ' Kg' : ($key->satuan == '1' ? ' Ton' : ' Janjang');
                                            ?>
                                    <tr>
                                        <td><?=$key->id_purchase_order?></td>
                                        <td><?=date('d M Y', strtotime($key->tgl_add))?></td>
                                        <td><?=$key->qty_realisasi.$satuan?></td>
                                        <td>Rp. <?=number_format(($key->qty_realisasi * $key->harga), 2)?></td>
                                    </tr>
                                            <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('backend/footer')?>
