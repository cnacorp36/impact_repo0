<?php $this->load->view('backend/header')?>

 <div class="login-content">
        <!-- Login -->

        <div class="nk-block toggled" id="l-login">
            <form class="form-horizontal m-t-20" id="registersubmit" action="<?php echo base_url('registrasi/add');?>" method="post">
            <div class="nk-form">
                <div class="input-group">
                    <span class="input-group-addon nk-ic-st-pro"><i class="notika-icon notika-support"></i></span>
                    <div class="nk-int-st">
                        <input type="email" id="email" class="form-control" placeholder="example@domain.com" required>
                    </div>
                </div>
                <div class="input-group mg-t-15">
                    <span class="input-group-addon nk-ic-st-pro"><i class="notika-icon notika-edit"></i></span>
                    <div class="nk-int-st">
                        <input type="password" id="password"  class="form-control" placeholder="Password" required>
                    </div>
                </div><br><br>
                <div class="mg-t-15" style="text-align: left">
                    <span >Nama Perusahaan</span>
                    <div class="nk-int-st">
                        <input type="text" id="nama"  class="form-control" placeholder=" Nama Perusahaan" required>
                    </div>
                </div>
                <hr>
                <div id="pabrik_0">
                    <span class="input-group-addon nk-ic-st-pro">Pabrik 1</span>
                    <button type="button" onclick="removePabrik(0)" class="btn btn-danger btn-link" style="color: red" ><i class="notika-icon notika-minus-symbol"></i> Remove</button>
                    <div class="mg-t-15" style="text-align: left">
                        <span >Alamat</span>
                        <div class="nk-int-st">
                            <input id="alamat0" type="text" name="alamat[]"  class="form-control" placeholder=" Alamat Pabrik 1" required>
                        </div>
                    </div><br><br>
                    <div class="mg-t-15" style="text-align: left">
                        <span >Luas (Hektar)</span>
                        <div class="nk-int-st">
                            <input id="luas0" type="number" name="luas[]"  class="form-control" placeholder=" Luas Pabrik 1" required>
                        </div>
                    </div><br><br>
                    <div class="mg-t-15" style="text-align: left">
                        <span >Kapasitas (Ton)</span>
                        <div class="nk-int-st">
                            <input id="kapasitas0" type="number" name="kapasitas[]"  class="form-control" placeholder=" Kapasitas Pabrik 1" required>
                        </div>
                    </div>
                    <hr>
                </div>

                <input type="hidden" id="count_pabrik" value="1">
                <button type="button" data-ma-action="nk-login-switch" onclick="addPabrik()" class="btn btn-success btn-link"><i class="notika-icon notika-plus-symbol"></i> Tambah Pabrik</span></button>

                <div class="fm-checkbox">
                    <label><input type="checkbox" class="i-checks"> <i></i> Keep me signed in</label>
                </div>
                <div>
                    <button href="#l-register" data-ma-action="nk-login-switch" type="submit" data-ma-block="#l-register" class="btn btn-info"><i class="notika-icon notika-right-arrow right-arrow-ant"></i> Registrasi</button>
                </div>
            </div>
        </form>

            <div class="nk-navigation nk-lg-ic">
                <a href="<?php echo base_url('login')?>" data-ma-action="nk-login-switch" data-ma-block="#l-register"><i class="notika-icon notika-right-arrow"></i> <span>Login</span></a>
            </div>
        </div>

    </div>

    <script type="text/javascript">

        function addPabrik(){
            var count_pabrik = $('#count_pabrik').val();
            $('#count_pabrik').before('\n\
                <div id="pabrik_'+count_pabrik+'">\n\
                    <span class="input-group-addon nk-ic-st-pro">Pabrik '+(parseInt(count_pabrik) + 1)+'</span>\n\
                    <button type="button" onclick="removePabrik('+count_pabrik+')" class="btn btn-danger btn-link"><i class="notika-icon notika-minus-symbol"></i> Remove</button>\n\
                    <div class="mg-t-15" style="text-align: left">\n\
                        <span >Alamat</span>\n\
                        <div class="nk-int-st">\n\
                            <input id="alamat'+count_pabrik+'" type="text" name="alamat[]"  class="form-control" placeholder=" Alamat Pabrik '+(parseInt(count_pabrik) + 1)+'" required>\n\
                        </div>\n\
                    </div><br><br>\n\
                    <div class="mg-t-15" style="text-align: left">\n\
                        <span >Luas (Hektar)</span>\n\
                        <div class="nk-int-st">\n\
                            <input id="luas'+count_pabrik+'" type="number" name="luas[]"  class="form-control" placeholder=" Luas Pabrik '+(parseInt(count_pabrik) + 1)+'" required>\n\
                        </div>\n\
                    </div><br><br>\n\
                    <div class="mg-t-15" style="text-align: left">\n\
                        <span >Kapasitas (Ton)</span>\n\
                        <div class="nk-int-st">\n\
                            <input id="kapasitas'+count_pabrik+'" type="number" name="kapasitas[]"  class="form-control" placeholder=" Kapasitas Pabrik '+(parseInt(count_pabrik) + 1)+'" required>\n\
                        </div>\n\
                    </div>\n\
                    <hr>\n\
                </div>\n\
            ');
            $('#count_pabrik').val(parseInt(count_pabrik) + 1);
        }

        function removePabrik(val){
            var count_pabrik = $('#count_pabrik').val();
            if(count_pabrik == 1){
                swal({
                  type: 'error',
                  title: 'Oops...',
                  text: 'Data pabrik minimal 1'
                });
            }else{
                var alamat = Array();
                var luas = Array();
                var kapasitas = Array();
                for(var a=0; a<parseInt(count_pabrik); a++){
                    if(val != a){
                        alamat[alamat.length] = $('#alamat'+a).val();
                        luas[luas.length] = $('#luas'+a).val();
                        kapasitas[kapasitas.length] = $('#kapasitas'+a).val();
                    }
                    $('#pabrik_'+a).remove();
                }
                $('#count_pabrik').val(0);

                for(var b=0; b<alamat.length; b++){
                    addPabrik();
                    $('#alamat'+b).val(alamat[b]);
                    $('#luas'+b).val(luas[b]);
                    $('#kapasitas'+b).val(kapasitas[b]);
                }
            }
        }

        var url = '<?php echo base_url()?>registrasi/add';
        var url_redirect = '<?php echo base_url()?>admin';
    </script>
    <script src="<?php echo base_url('assets/js/package/dist/sweetalert2.all.min.js')?>"></script>
<?php $this->load->view('backend/footer_sweet_alert')?>