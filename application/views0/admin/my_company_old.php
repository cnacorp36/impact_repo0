<?php $this->load->view('backend/header')?>
<?php $this->load->view('backend/navbar')?>
<div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">My Company</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Library</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                            <form class="form-horizontal">
                                
                                    <?php
                                    $profil = $this->M_models->get_from_query("select a.*,b.nama as nama_kate from u_perusahaan a
                                        left join ref_kategori b on b.id_kategori = a.id_kategori")->result();
                                    foreach ($profil as $key) {
                                        ?>
                                        <div class="card-body">
                                    <h4 class="card-title"><b>My Company </b></h4>
                                        <div class="form-group row">
                                        <label for="fname" class="col-sm-3 text-right control-label col-form-label">Klasifikasi</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="fname" value="<?php echo $key->nama_kate?>" readonly="">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Kebun</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="lname" value="<?php echo $key->nama?>" readonly="">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Alamat</label>
                                        <div class="col-sm-9">
                                            <textarea class="form-control" readonly=""><?php echo $key->alamat?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="email1" class="col-sm-3 text-right control-label col-form-label">Luas PKS</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="email1" value="<?php echo $key->luas.' m2'?>" readonly="">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Kapasitas</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="cono1" value="<?php echo $key->kapasitas .' ton/hari'?>" readonly="">
                                        </div>
                                    </div>
                                    </div>    
                                        <?php
                                    }
                                    ?>
                            </form>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title"><b>PO HISTORY</b></h5>
                                <div class="table-responsive">
                                    <table id="zero_config" class="table table-striped table-bordered">
                                        <thead>
                                            <tr style="background-color: blue">
                                                <th style="color:white">DATE</th>
                                                <th style="color:white">QTY</th>
                                                <th style="color:white">COMPANY</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Tiger Nixon</td>
                                                <td>System Architect</td>
                                                <td>Edinburgh</td>
                                                
                                            </tr>
                                            <tr>
                                                <td>Garrett Winters</td>
                                                <td>Accountant</td>
                                                <td>Tokyo</td>
                                                
                                            </tr>
                                            <tr>
                                                <td>Ashton Cox</td>
                                                <td>Junior Technical Author</td>
                                                <td>San Francisco</td>
                                            </tr>
                                            <tr>
                                                <td>Cedric Kelly</td>
                                                <td>Senior Javascript Developer</td>
                                                <td>Edinburgh</td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Name</th>
                                                <th>Position</th>
                                                <th>Office</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

<?php $this->load->view('backend/footer')?>