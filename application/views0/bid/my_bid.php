<?php $this->load->view('backend/header');?>
<?php $this->load->view('backend/navbar');?>
<?php
    $bid = $this->M_models->get_from_query("
        select p0.nama, p.qty, p.gambar, p.tgl_edit, k.alamat, bd.id_produk, bd.qty as bd_qty, bd.harga, b.id_bid, bd.status, bd.qty1, bd.harga1, up.id_perusahaan
        from bid b
        left join bid_detail bd on bd.id_bid=b.id_bid
        left join produk p on p.id_produk=bd.id_produk
        left join kebun k on k.id_kebun=p.id_kebun
        left join u_petani p0 on p0.id_petani=k.id_petani
        left join user u on u.id_user=p0.id_user
        left join u_perusahaan up on up.id_perusahaan=b.id_perusahaan
        left join user u1 on u1.id_user=up.id_user
        where u.status='1' and k.status='1' and p.status='1' and b.status!='1' and u1.email='".$sess['email']."'
        order by bd.tgl_edit desc
    ");

    $pending_bid = array();
    $accepted_bid = array();
    $canceled = array();
    foreach ($bid->result() as $a) {
        if($a->status == '1'){
            $pending_bid[count($pending_bid)] = $a;
        }else if($a->status == '2' || $a->status == '4'){
            $canceled[count($canceled)] = $a;
        }else if($a->status != '0'){
            $accepted_bid[count($accepted_bid)] = $a;
        }
    }

    if($bid->num_rows() > 0){
        $q_pabrik = $this->M_models->get_data('', 'pabrik', null, array('id_perusahaan' => $bid->row()->id_perusahaan), '', 'alamat asc');
    }
?>
<div class="breadcomb-area">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="breadcomb-list">
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="breadcomb-wp">
                  <div class="breadcomb-icon">
                    <i class="notika-icon notika-dollar"></i>
                  </div>
                  <div class="breadcomb-ctn">
                    <h2>My Bid</h2>
                    <p>KEPALA SAWIT</span></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
    <div class="normal-table-area" style="background-color: green;" id="bid-form">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="normal-table-list mg-t-30">
                        <div class="basic-tb-hd">
                            <h2>Penawaran (Diterima)</h2>

                            <button type="button" class="btn btn-success" onclick="proses_po()">Proses Ke Pembayaran</button>
                            <!-- <p>Add Classes (<code>.table-striped</code>) to any table row within the tbody</p> -->
                        </div>
                        <div class="bsc-tbl-st">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Petani</th>
                                        <th>Penawaran Anda</th>
                                        <th>Penawaran Petani</th>
                                        <th>Alamat Pengiriman</th>
                                        <th>Gambar</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 1;
                                    for($i=1; $i<=count($accepted_bid); $i++){

                                    ?>
                                    <tr>
                                        <td><?php echo $i?></td>
                                        <td><p><?php echo $accepted_bid[$i-1]->nama.'</p>('.$accepted_bid[$i-1]->alamat.')'?></td>
                                        <td>
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Qty</th>
                                                        <th>Harga</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td><?php echo $accepted_bid[$i-1]->bd_qty.' Kg'; ?></td>
                                                        <td><?php echo 'Rp. '.number_format($accepted_bid[$i-1]->harga, 2) ?></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                        <td>
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Qty</th>
                                                        <th>Harga</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td><?php echo $accepted_bid[$i-1]->qty1.' Kg'; ?></td>
                                                        <td><?php echo 'Rp. '.number_format($accepted_bid[$i-1]->harga1, 2)?></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                        <td>
                                            <select class="form-control" id="pabrik_<?=($i-1)?>">
                                                <?php
                                                foreach ($q_pabrik->result() as $key_pabrik) {
                                                    echo '<option value="'.$key_pabrik->id_pabrik.'">'.$key_pabrik->alamat.'</option>';
                                                }
                                                ?>
                                            </select>
                                            <input type="hidden" id="id_produk_accept<?=($i-1)?>" value="<?=$accepted_bid[$i-1]->id_produk?>">
                                        </td>
                                        <td><img class="thumbnail" src="<?php echo $accepted_bid[$i-1]->gambar?>" style="width: 100px"/></td>
                                        <td id="btn_tr<?php echo $i?>">
                                            <button class="btn btn-danger" onclick="cancel_bid('<?php echo $accepted_bid[$i-1]->id_produk?>', '<?php echo $accepted_bid[$i-1]->id_bid?>', 2)" type="button"><i class="fa fa-trash"></i></button>
                                        </td>
                                    </tr>
                                    <?php
                                    }

                                    ?>                                    
                                    <tr id="tr_1"></tr>
                                </tbody>
                            </table>
                            <input type="hidden" id="count" value="<?=count($accepted_bid)?>"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="normal-table-area" style="background-color: yellow;" id="bid-form1">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="normal-table-list mg-t-30">
                        <div class="basic-tb-hd">
                            <h2>Penawaran (Pending)</h2>

                            <!-- <p>Add Classes (<code>.table-striped</code>) to any table row within the tbody</p> -->
                        </div>
                        <div class="bsc-tbl-st">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Petani</th>
                                        <th>Tersedia</th>
                                        <th>Bid</th>
                                        <th>Harga</th>
                                        <th>Gambar</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 1;
                                    for($i=1; $i<=count($pending_bid); $i++){

                                    ?>
                                    <tr>
                                        <td><?php echo $i?></td>
                                        <td><p><?php echo $pending_bid[$i-1]->nama.'</p>('.$pending_bid[$i-1]->alamat.')'?></td>
                                        <td><?php echo $pending_bid[$i-1]->qty?> Kg</td>
                                        <td id="qty-bid-<?php echo $pending_bid[$i-1]->id_produk?>"><?php echo $pending_bid[$i-1]->bd_qty?> Kg</td>
                                        <td id="qty-harga-<?php echo $pending_bid[$i-1]->id_produk?>">Rp. <?php echo number_format($pending_bid[$i-1]->harga, 2)?></td>
                                        <td><img class="thumbnail" src="<?php echo $pending_bid[$i-1]->gambar?>" style="width: 100px"/></td>
                                        <td>
                                            <button class="btn btn-danger" onclick="cancel_bid('<?php echo $pending_bid[$i-1]->id_produk?>', '<?php echo $pending_bid[$i-1]->id_bid?>', 2)" type="button"><i class="fa fa-trash"></i></button>
                                        </td>
                                    </tr>
                                    <?php
                                    }

                                    ?>                                    
                                    <tr id="tr_1"></tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="normal-table-area" style="background-color: #E53956;" id="bid-form1">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="normal-table-list mg-t-30">
                        <div class="basic-tb-hd">
                            <h2>Penawaran (Ditolak)</h2>

                            <!-- <p>Add Classes (<code>.table-striped</code>) to any table row within the tbody</p> -->
                        </div>
                        <div class="bsc-tbl-st">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Petani</th>
                                        <th>Tersedia</th>
                                        <th>Bid</th>
                                        <th>Harga</th>
                                        <th>Gambar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 1;

                                    for($i=1; $i<=count($canceled); $i++){

                                    ?>
                                    <tr>
                                        <td><?php echo $i?></td>
                                        <td><p><?php echo $canceled[$i-1]->nama.'</p>('.$canceled[$i-1]->alamat.')'?></td>
                                        <td><?php echo $canceled[$i-1]->qty?> Kg</td>
                                        <td id="qty-bid-<?php echo $canceled[$i-1]->id_produk?>"><?php echo $canceled[$i-1]->bd_qty?> Kg</td>
                                        <td id="qty-harga-<?php echo $canceled[$i-1]->id_produk?>">Rp. <?php echo number_format($canceled[$i-1]->harga, 2)?></td>
                                        <td><img class="thumbnail" src="<?php echo $canceled[$i-1]->gambar?>" style="width: 100px"/></td>
                                    </tr>
                                    <?php
                                    }

                                    ?>                                    
                                    <tr id="tr_1"></tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var email = '<?php echo $sess["email"]?>';

        function proses_po() {
            swal({
                title: 'Deal Proses',
                text: 'Lanjut ke pembayaran?',
                showCancelButton: true,
                cancelButtonColor: '#d33',
                onOpen: () => {
                    
                },
                onClose: () => {

                }
            }).then((result) => {
                if(result){
                    var id_pabrik = '';
                    var id_produk_accept = '';
                    var count_data_accept = $('#count').val();

                    for(var cnt=0; cnt<parseInt(count_data_accept); cnt++){
                        id_pabrik += $('#pabrik_'+cnt).val()+';';
                        id_produk_accept += $('#id_produk_accept'+cnt).val()+';';
                    }
                    // console.log(id_pabrik);
                    // console.log(id_produk_accept);
                    $.ajax({
                        type: "POST",
                        url: '<?php echo base_url()?>bid/proses_po',
                        dataType: "json",
                        data: {
                            id_pabrik: id_pabrik,
                            id_produk_accept: id_produk_accept
                        },
                        success: function (data) {
                            if(data.success == '1'){
                                for(var a=0; a<data.token.length; a++){
                                    send_notif(data.token[a], data.nama_perusahaan+" menyetujui penawaran pada produk Anda", "Deal3");
                                }
                                
                                swal({
                                    title: 'Permintaan Berhasil Diproses',
                                    type: 'success'
                                }).then((result) => {
                                    window.location.href='<?php echo base_url()?>payment';
                                })
                            }else{
                                swal({
                                    title: 'Oops..',
                                    text: data.msg,
                                    type: 'error'
                                }).then((result) => {

                                })
                            }
                        },
                        error: function () {
                            swal({
                                title: 'Oops.. Server sedang error.',
                                type: 'error'
                            }).then((result) => {

                            })
                        }
                    })
                }
            });
        }
        
        function cancel_bid(id_produk, id_bid, status){
            swal({
              title: 'Anda yakin ingin cancel penawaran ini?',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes!'
            }).then((result) => {
              if (result) {
                $.ajax({
                    type: "POST",
                    url: '<?php echo base_url()?>bid/cancel_bid',
                    dataType: "json",
                    data: {
                        id_bid: id_bid,
                        id_produk: id_produk,
                        status: status
                    },
                    success: function (data) {
                        if(data.success == '1'){
                            send_notif(data.token, data.perusahaan+" membatalkan penawaran pada produk Anda", "Deal2");
                            swal({
                                title: 'Cancel bid berhasil',
                                type: 'success'
                            }).then((result) => {
                                window.location.href='<?php echo base_url()?>bid';
                            })
                        }else{
                            swal({
                                title: 'Oops..',
                                text: data.msg,
                                type: 'error'
                            }).then((result) => {

                            })
                        }
                    },
                    error: function () {
                        swal({
                            title: 'Oops.. Server sedang error.',
                            type: 'error'
                        }).then((result) => {

                        })
                    }
                })
              }
            });
        }

        function send_notif(token, msg, target){
            let body;
            body = {
              to: token,
              data: {
                custom_notification: {
                  title: "Penawaran Produk",
                  body: msg,
                  sound: "default",
                  priority: "high",
                  show_in_foreground: true,
                  targetScreen: target,
                  large_icon: 'https://image.freepik.com/free-icon/small-boy-cartoon_318-38077.jpg'
                }
              },
              priority: 10
            };
            $.ajax({
                type: "POST",
                url: 'https://fcm.googleapis.com/fcm/send',
                dataType: "json",
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'key=AAAAysvf2c4:APA91bGQyw1452mF0GIxGcb9Rg5yvVXAQ8sIXfR6KEQAiTEopi3Uz0REWQJ8fdv_B4xyi-lNbA74toVEEiMzaUK4NO-hxYCI9rwEkOXG9ptCcd9ZFDb5QkMgsy0g5gAoq1zz_e4E0veW'
                },
                data: JSON.stringify(body),
                success: function (data) {
                },
                error: function () {
                }
            })
        }
    </script>
<script src="<?php echo base_url('assets/js/package/dist/sweetalert2.all.min.js')?>"></script>
<?php $this->load->view('backend/footer');?>