<div class="culmn">
            <nav class="navbar navbar-default bootsnav navbar-fixed no-background white">
                <div class="top-search">
                    <div class="container">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-search"></i></span>
                            <input type="text" class="form-control" placeholder="Search">
                            <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
                        </div>
                    </div>
                </div>
                <div class="container"> 
                    <div class="attr-nav">
                        <ul>
                            <li class="search"><a href="#"><i class="fa fa-search"></i></a></li>
                        </ul>
                    </div> 
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                            <i class="fa fa-bars"></i>
                        </button>
                        <a class="navbar-brand" href="#brand">
                            <div class="home_text ">
                            <h4 class="text-yellow"><img style="margin-top: -50px" src="<?php echo base_url()?>assets/images/agrinesia_logo.png" width="120" ></h4>
                        </div>
                            <!--<img src="assets/images/footer-logo.png" class="logo logo-scrolled" alt="">-->
                        </a>

                    </div>
                    <!-- End Header Navigation -->

                    <!-- navbar menu -->
                    <div class="collapse navbar-collapse" id="navbar-menu">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="#home">Home</a></li>                    
                            <li><a href="#features">About</a></li>
                            <li><a href="#service">Bisnis</a></li>
                            <!-- <li><a href="#who">Team</a></li> -->
                            <li><a href="#contact">Contact</a></li>
                            <li><a href="<?php echo base_url('login')?>">login</a></li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div> 
            </nav>  