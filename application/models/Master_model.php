<?php
class Master_model extends CI_Model {
	public function categori_sawit()
	{
		$this->db->select();
		// $this->db->where($where);
		$query = $this->db->get('ref_kategori');
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

	public function get_user(){
		$this->db->select();
		// $this->db->where($where);
		$query = $this->db->get('user');
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

	public function company(){
		$this->db->select('a.tgl_edit,a.qty,c.nama as nama_petani');
		$this->db->join('produk b','b.id_produk = a.id_produk','left');
		$this->db->join('u_petani c','b.id_petani = b.id_petani','left');
		$this->db->group_by('a.id_produk');
		$query = $this->db->get('bid_detail a');
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
}