<?php
class Market_model extends CI_Model {
	public function market()
	{
		$this->db->select('a.*,b.nama as n_petani,b.alamat,c.kelas ');
		// $this->db->where($where);
		$this->db->join('u_petani b','b.id_petani = a.id_petani','left');
		$query = $this->db->get('produk a');
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

	public function get_bid(){
		$this->db->select('a.id_produk,c.id_bid,p.qty as tersedia, b.nama,a.qty,a.harga, sum(a.qty*a.harga) as total');
		$this->db->join('bid c','c.id_bid = a.id_bid','left');
		$this->db->join('u_perusahaan b','b.id_perusahaan = c.id_perusahaan','left');
		$this->db->join('produk p','p.id_produk = a.id_produk','left');
		$this->db->group_by('a.id_produk');
		$query = $this->db->get('bid_detail a');
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

	public function bid_payment(){
		$this->db->select('c.nama as nama_petani , a.qty, a.harga, sum(a.qty*a.harga)as total_bid');
		$this->db->join('produk b','b.id_produk = a.id_produk','left');
		$this->db->join('u_petani c','b.id_petani = b.id_petani','left');
		$this->db->group_by('a.id_produk');
		$query = $this->db->get('bid_detail a');
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}	
	}

}