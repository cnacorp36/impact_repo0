<?php $this->load->view('backend/header');?>
<?php $this->load->view('backend/navbar');?>
<div class="breadcomb-area">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="breadcomb-list">
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="breadcomb-wp">
                  <div class="breadcomb-icon">
                    <i class="notika-icon notika-credit-card"></i>
                  </div>
                  <div class="breadcomb-ctn">
                    <h2>BID HISTORY</h2>
                    <p>KELAPA SAWIT</span></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="data-table-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="data-table-list">
                    <div class="basic-tb-hd">
                        <h2>Bid History</h2>
                        <!-- <p>It's just that simple. Turn your simple table into a sophisticated data table and offer your users a nice experience and great features without any effort.</p> -->
                    </div>
                    <div class="table-responsive">
                        <table id="data-table-basic" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>ID Bid</th>
                                    <th>ID Produk</th>
                                    <th>Terakhir Diubah</th>
                                    <th>Status</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $q_bid = $this->M_models->get_data(
                                        'bd.id_bid, bd.id_produk, bd.tgl_edit, bd.status, po.id_po',
                                        'bid_detail bd',
                                        array(
                                            0 => 'bid b-b.id_bid=bd.id_bid',
                                            1 => 'u_perusahaan cmp-cmp.id_perusahaan=b.id_perusahaan',
                                            2 => 'user u-u.id_user=cmp.id_user',
                                            3 => 'purchase_order po-po.id_bid=bd.id_bid'
                                        ),
                                        array(
                                            'u.email' => $this->cek['email']
                                        ),
                                        '', 'bd.tgl_edit desc'
                                    );

                                    foreach($q_bid->result() as $key){
                                        if($key->status == '0'){
                                            $status = 'Draft';
                                        }else if($key->status == '1'){
                                            $status = 'Pending';
                                        }else if($key->status == '2'){
                                            $status = 'Anda Membatalkan Penawaran Ini';
                                        }else if($key->status == '3' || $key->status == '5'){
                                            $status = 'Penawaran Disepakati';
                                        }else if($key->status == '4'){
                                            $status = 'Dibatalkan Oleh Penjual';
                                        }else if($key->status == '6'){
                                            $status = 'Purchase Order Sudah Masuk';
                                        }
                                        ?>
                                <tr>
                                    <td><?=$key->id_bid?></td>
                                    <td><?=$key->id_produk?></td>
                                    <td><?=date('d/M/Y H:i', strtotime($key->tgl_edit))?></td>
                                    <td><?=$status?></td>
                                    <td>
                                        <?php
                                            if($key->status != '6'){
                                                ?>
                                        <a class="btn btn-default" href="<?=base_url('Bid_history/detail/'.$key->id_bid.'/'.$key->id_produk.'/'.$key->status)?>">Detail Bid</a>
                                                <?php
                                            }else{
                                                ?>
                                        <a class="btn btn-default" href="<?=base_url('PO_history/detail/'.$key->id_po.'/'.$key->id_produk)?>">Detail PO</a>
                                                <?php
                                            }
                                        ?>
                                    </td>
                                </tr>
                                        <?php
                                    }
                                ?>
                            </tbody>
                        </table>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
    
<?php $this->load->view('backend/footer');?>