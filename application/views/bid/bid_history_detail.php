<?php $this->load->view('backend/header')?>
<?php $this->load->view('backend/navbar')?>
 <div class="breadcomb-area">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="breadcomb-list">
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="breadcomb-wp">
                  <div class="breadcomb-icon">
                    <i class="notika-icon notika-map"></i>
                  </div>
                  <div class="breadcomb-ctn">
                    <h2>BID HISTORY DETAIL</h2>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
 <div class="form-element-area">
        <div class="container">
             <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-element-list mg-t-30">
                        <div class="cmp-tb-hd">
                            <h2>Data Bid</h2>
                            <!-- <p>An input mask helps the user with the input. This can be useful for dates, numerics, phone numbers etc...</p> -->
                        </div>
                        <div class="row">
                            <div class="col-sm-4 col-xs-12">
                                <div class="nk-int-mk">
                                    <div class="form-cmp">
                                        <p style="color: grey; font-size: 11px">ID Bid</p>
                                    </div>
                                    <div class="nk-int-st">
                                        <p style="font-size: 14px; margin-top: -8px"><?=$data_bid['id_bid']?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 col-xs-12">
                                <div class="nk-int-mk">
                                    <div class="form-cmp">
                                        <p style="color: grey; font-size: 11px">ID Produk</p>
                                    </div>
                                    <div class="nk-int-st">
                                        <p style="font-size: 14px; margin-top: -8px"><?=$data_bid['id_produk']?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 col-xs-12">
                                <div class="nk-int-mk">
                                    <div class="form-cmp">
                                        <p style="color: grey; font-size: 11px">Terakhir Diubah</p>
                                    </div>
                                    <div class="nk-int-st">
                                        <p style="font-size: 14px; margin-top: -8px"><?=$data_bid['tgl_edit']?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 col-xs-12">
                                <div class="nk-int-mk">
                                    <div class="form-cmp">
                                        <p style="color: grey; font-size: 11px">Status</p>
                                    </div>
                                    <div class="nk-int-st">
                                        <p style="font-size: 14px; margin-top: -8px"><?=$data_bid['status']?></p>
                                    </div>
                                </div>
                            </div>
                        </div><br><br><hr>


                        <div class="cmp-tb-hd">
                            <h2>Data Kebun</h2>

                        </div>
                        <div class="row" id="form_update_password" >
                            
                            <div class="col-sm-4 col-xs-12">
                                <div class="nk-int-mk" >
                                    <div class="form-cmp">
                                        <p style="color: grey; font-size: 11px">ID Kebun</p>
                                    </div>
                                    <div class="nk-int-st">
                                        <p style="font-size: 14px; margin-top: -8px"><?=$data_kebun['id_kebun']?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4 col-xs-12">
                                <div class="nk-int-mk" >
                                    <div class="form-cmp">
                                        <p style="color: grey; font-size: 11px">Alamat</p>
                                    </div>
                                    <div class="nk-int-st">
                                        <p style="font-size: 14px; margin-top: -8px"><?=$data_kebun['alamat']?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4 col-xs-12">
                                <div class="nk-int-mk" >
                                    <div class="form-cmp">
                                        <p style="color: grey; font-size: 11px">Luas</p>
                                    </div>
                                    <div class="nk-int-st">
                                        <p style="font-size: 14px; margin-top: -8px"><?=$data_kebun['luas']?> Hektar</p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4 col-xs-12">
                                <div class="nk-int-mk" >
                                    <div class="form-cmp">
                                        <p style="color: grey; font-size: 11px">Kapasitas</p>
                                    </div>
                                    <div class="nk-int-st">
                                        <p style="font-size: 14px; margin-top: -8px"><?=$data_kebun['kapasitas']?> <?=$data_kebun['satuan']?></p>
                                    </div>
                                </div>
                            </div>
                        </div><br><br><hr>

                        <div class="cmp-tb-hd">
                            <h2>Data Petani</h2>

                        </div>
                        <div class="row" id="form_update_password" >
                            
                            <div class="col-sm-4 col-xs-12">
                                <div class="nk-int-mk" >
                                    <div class="form-cmp">
                                        <p style="color: grey; font-size: 11px">ID Petani</p>
                                    </div>
                                    <div class="nk-int-st">
                                        <p style="font-size: 14px; margin-top: -8px"><?=$data_petani['id_petani']?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4 col-xs-12">
                                <div class="nk-int-mk" >
                                    <div class="form-cmp">
                                        <p style="color: grey; font-size: 11px">Nama Petani</p>
                                    </div>
                                    <div class="nk-int-st">
                                        <p style="font-size: 14px; margin-top: -8px"><?=$data_petani['nama']?></p>
                                    </div>
                                </div>
                            </div>
                        </div><br><br><hr>

                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('backend/footer')?>
