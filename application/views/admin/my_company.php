<?php $this->load->view('backend/header')?>
<?php $this->load->view('backend/navbar')?>
 <div class="breadcomb-area">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="breadcomb-list">
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="breadcomb-wp">
                  <div class="breadcomb-icon">
                    <i class="notika-icon notika-map"></i>
                  </div>
                  <div class="breadcomb-ctn">
                    <h2>MY COMPANY</h2>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
 <div class="form-element-area">
        <div class="container">
             <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-element-list mg-t-30">
                        <div class="cmp-tb-hd">
                            <h2>My Company</h2>
                            <!-- <p>An input mask helps the user with the input. This can be useful for dates, numerics, phone numbers etc...</p> -->
                        </div>
                        <div class="row">
                                <?php
                                $join_profile = array(
                                    0 => 'u_perusahaan p-p.id_user=u.id_user',
                                    1 => 'ref_kategori k-k.id_kategori=p.id_kategori'
                                );
                                $profil = $this->M_models->get_data('p.*, k.nama as k_nama, u.id_user, u.hp', 'user u', $join_profile, array('u.email' => $sess['email']), '', '', 1);
                                //$profil = $this->M_models->get_from_query("select a.*,b.nama as nama_kate from u_perusahaan a left join ref_kategori b on b.id_kategori = a.id_kategori")->result();
                                $key = $profil->row();
                                
                                    ?>
                            <div class="col-sm-4 col-xs-12">
                                <div class="nk-int-mk">
                                    <div class="form-cmp">
                                        Klasifikasi
                                    </div>
                                    <div class="nk-int-st">
                                        <input disabled type="text" class="form-control" data-mask="999-99-999-9999-9" value="Kelapa Sawit">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 col-xs-12">
                                <div class="nk-int-mk">
                                    <div class="form-cmp">
                                        ID Perusahaan
                                    </div>
                                    <div class="nk-int-st">
                                        <input disabled type="text" class="form-control" data-mask="999-99-999-9999-9" value="<?php echo $key->id_user?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 col-xs-12">
                                <div class="nk-int-mk">
                                    <div class="form-cmp">
                                        Nama Perusahaan
                                    </div>
                                    <div class="nk-int-st">
                                        <input type="text" onkeyup="change_name()" class="form-control" id="nama" value="<?php echo $key->nama?>">
                                        <input type="hidden" id="nama1" value="<?=$key->nama?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <div class="nk-int-mk" style="margin-top: 18px">
                                    <div class="form-cmp">
                                        Email
                                    </div>
                                    <div class="nk-int-st">
                                        <input type="email" disabled onkeyup="change_name()" class="form-control" id="email" value="<?php echo $sess['email']?>">
                                        <input type="hidden" id="email1" value="<?=$sess['email']?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <div class="nk-int-mk" style="margin-top: 18px">
                                    <div class="form-cmp">
                                        Telp
                                    </div>
                                    <div class="nk-int-st" style="width: 100%">
                                        <input type="text" onkeyup="change_name()" class="form-control" id="hp" value="<?php echo $key->hp?>">
                                        <input type="hidden" id="hp1" value="<?=$key->hp?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <button class="btn btn-info" id="btn_save_nama" type="button" style="margin-top: 8px; display: none" onclick="save_nama_perusahaan()">simpan</button>
                            </div>
                        <script type="text/javascript">
                            var id_perusahaan = '<?=$key->id_perusahaan?>';
                            function change_name() {
                                var nama = $('#nama').val();
                                var nama1 = $('#nama1').val();
                                var email = $('#email').val();
                                var email1 = $('#email1').val();
                                var hp = $('#hp').val();
                                var hp1 = $('#hp1').val();

                                if(nama != nama1 || email != email1 || hp != hp1){
                                    $('#btn_save_nama').show();
                                }else{
                                    $('#btn_save_nama').hide();
                                }
                            }

                            function save_nama_perusahaan() {
                                var nama = $('#nama').val();
                                var email = $('#email').val();
                                var hp = $('#hp').val();
                                var email1 = $('#email1').val();

                                swal({
                                    title: 'Konfirmasi',
                                    text: 'Anda yakin ingin mengubah data perusahaan?',
                                    showCancelButton: true,
                                    cancelButtonColor: '#d33',
                                    onOpen: () => {
                                        
                                    },
                                    onClose: () => {

                                    }
                                }).then((result) => {
                                    if(result){
                                        $.ajax({
                                            type: "POST",
                                            url: '<?php echo base_url()?>admin/update_profile',
                                            dataType: "json",
                                            data: {
                                                nama: nama,
                                                email: email,
                                                email1: email1,
                                                hp: hp,
                                                id_perusahaan: id_perusahaan
                                            },
                                            success: function (data) {
                                                if(data.success == '1'){
                                                    swal({
                                                        title: 'Ubah Data Berhasil',
                                                        type: 'success'
                                                    }).then((result) => {
                                                        window.location.href='<?php echo base_url('admin/my_company')?>';
                                                    })
                                                }else{
                                                    swal({
                                                        title: 'Oops..',
                                                        text: data.msg,
                                                        type: 'error'
                                                    }).then((result) => {

                                                    })
                                                }
                                            },
                                            error: function () {
                                                swal({
                                                    title: 'Oops.. Server sedang error.',
                                                    type: 'error'
                                                }).then((result) => {

                                                })
                                            }
                                        })
                                    }
                                });
                            }
                        </script>
                        </div><br><br><hr>


                        <div class="cmp-tb-hd">
                            <h2>Ubah Password</h2>

                            <button class="btn btn-info" onclick="ubah_pass()" id="ubah_pass_">Ubah Password</button>
                        </div>
                        <div class="row" id="form_update_password" style="display: none">
                            
                            <div class="col-xs-12">
                                <div class="nk-int-mk" >
                                    <div class="form-cmp">
                                        Password Lama
                                    </div>
                                    <div class="nk-int-st">
                                        <input type="password" class="form-control" id="password0" placeholder="Password">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="nk-int-mk" style="margin-top: 18px">
                                    <div class="form-cmp">
                                        Password Baru
                                    </div>
                                    <div class="nk-int-st">
                                        <input type="password" class="form-control" id="password1" placeholder="Password Baru">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="nk-int-mk" style="margin-top: 18px">
                                    <div class="form-cmp">
                                        Ulangi Password Baru
                                    </div>
                                    <div class="nk-int-st">
                                        <input type="password" class="form-control" id="password2" placeholder="Re-Password Baru">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12" style="padding-top: 18px">
                                <button type="button" onclick="cancel_pass()" class="btn btn-default"><i class="fa fa-add"></i>Batal</button>
                                <button type="button" onclick="save_pass()" class="btn btn-success"><i class="fa fa-add"></i>Simpan</button>
                            </div>

                            <script type="text/javascript">
                                function ubah_pass() {
                                    $('#form_update_password').show();
                                    $('#ubah_pass_').hide();
                                }

                                function cancel_pass() {
                                    $('#form_update_password').hide();
                                    $('#ubah_pass_').show();
                                }

                                function save_pass() {
                                    let pass0 = $('#password0').val();
                                    let pass1 = $('#password1').val();
                                    let pass2 = $('#password2').val();

                                    swal({
                                        title: 'Konfirmasi',
                                        text: 'Anda yakin ingin mengubah password Anda?',
                                        showCancelButton: true,
                                        cancelButtonColor: '#d33',
                                        onOpen: () => {
                                            
                                        },
                                        onClose: () => {

                                        }
                                    }).then((result) => {
                                        if(result){
                                            $.ajax({
                                                type: "POST",
                                                url: '<?php echo base_url()?>admin/update_pass',
                                                dataType: "json",
                                                data: {
                                                    pass0: pass0,
                                                    pass1: pass1,
                                                    pass2: pass2,
                                                    email: $('#email1').val()
                                                },
                                                success: function (data) {
                                                    if(data.success == '1'){
                                                        swal({
                                                            title: 'Ubah Password Berhasil',
                                                            type: 'success'
                                                        }).then((result) => {
                                                            window.location.href='<?php echo base_url('admin/my_company')?>';
                                                        })
                                                    }else{
                                                        swal({
                                                            title: 'Oops..',
                                                            text: data.msg,
                                                            type: 'error'
                                                        }).then((result) => {

                                                        })
                                                    }
                                                },
                                                error: function () {
                                                    swal({
                                                        title: 'Oops.. Server sedang error.',
                                                        type: 'error'
                                                    }).then((result) => {

                                                    })
                                                }
                                            })
                                        }
                                    });
                                }
                            </script>
                        </div><br><br><hr>


                        <div class="cmp-tb-hd">
                            <h2>Pabrik</h2>

                            <button class="btn btn-info" onclick="add_pabrik()" id="add_pabrik_">Tambah Pabrik</button>
                            <!-- <p>An input mask helps the user with the input. This can be useful for dates, numerics, phone numbers etc...</p> -->
                        </div>
                        <div class="row" id="form_add_pabrik" style="display: none">
                            
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="nk-int-mk">
                                    <h2>Alamat</h2>
                                </div>
                                <div class="form-group ic-cmp-int">
                                    <div class="form-ic-cmp">
                                        <i class="notika-icon notika-map"></i>
                                    </div>
                                    <div class="nk-int-st">
                                        <input type="text" id="alamat" class="form-control" placeholder="Alamat" value="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="nk-int-mk">
                                    <h2>Kapasitas (Ton)</h2>
                                </div>
                                <div class="form-group ic-cmp-int">
                                    <div class="form-ic-cmp">
                                        <i class="notika-icon notika-dollar"></i>
                                    </div>
                                    <div class="nk-int-st">
                                        <input type="number" id="kapasitas" class="form-control" placeholder="Kapasitas" value="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <button type="button" onclick="cancel()" class="btn btn-default"><i class="fa fa-add"></i>Batal</button>
                                <button type="button" onclick="save()" class="btn btn-success"><i class="fa fa-add"></i>Simpan</button>
                            </div>
                        </div>

                        <script type="text/javascript">
                            function add_pabrik() {
                                $('#add_pabrik_').hide();
                                $('#form_add_pabrik').show();
                            }

                            function cancel() {
                                $('#add_pabrik_').show();
                                $('#form_add_pabrik').hide();
                            }

                            function save() {
                                $.ajax({
                                type: "POST",
                                url: '<?php echo base_url()?>admin/add_pabrik',
                                dataType: "json",
                                data: {
                                    alamat: $('#alamat').val(),
                                    kapasitas: $('#kapasitas').val()
                                },
                                success: function (data) {
                                    if(data.success == 1){
                                        swal({
                                            title: 'Permintaan Berhasil Diproses',
                                            type: 'success'
                                        }).then((result) => {
                                            window.location.href='<?php echo base_url()?>admin/my_company';
                                        })
                                    }else{
                                        swal({
                                            title: 'Oops..',
                                            text: data.msg,
                                            type: 'error'
                                        }).then((result) => {

                                        })
                                    }
                                },
                                error: function () {
                                    swal({
                                        title: 'Oops.. Server sedang error.',
                                        type: 'error'
                                    }).then((result) => {

                                    })
                                }
                            })
                            }
                        </script>
                        <?php
                            $q_factory = $this->M_models->get_data('*', 'pabrik', null, array('id_perusahaan' => $key->id_perusahaan), '', 'id_pabrik asc');

                            $a = 1;
                            foreach ($q_factory->result() as $_key) {
                                $status_ = '';
                                if($_key->status == '1'){
                                    $status_ = '<p style="color: green; font-size: 12px">(Aktif)</p>';
                                }else{
                                    $status_ = '<p style="color: red; font-size: 12px">(Tidak Aktif)</p>';
                                }
                                ?>
                        <hr>
                        <div class="cmp-tb-hd">
                            <h2>My Factory <?php echo $a?> <button type="button" class="btn btn-danger" onclick="delete_pabrik(<?=$a?>)"><i class="fa fa-trash"></i> Non-aktifkan</button></h2>
                            <?php echo $status_?>

                            <!-- <p>An input mask helps the user with the input. This can be useful for dates, numerics, phone numbers etc...</p> -->
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="nk-int-mk">
                                    <h2>ID Pabrik</h2>
                                </div>
                                <div class="form-group ic-cmp-int">
                                    <div class="form-ic-cmp">
                                        <i class="notika-icon notika-dollar"></i>
                                    </div>
                                    <div class="nk-int-st">
                                        <input id="id_pabrik_<?=$a?>" type="text" class="form-control" disabled placeholder="Kapasitas" value="<?php echo $_key->id_pabrik?>">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="nk-int-mk">
                                    <h2>Kapasitas (Ton)</h2>
                                </div>
                                <div class="form-group ic-cmp-int">
                                    <div class="form-ic-cmp">
                                        <i class="notika-icon notika-dollar"></i>
                                    </div>
                                    <div class="nk-int-st">
                                        <input onkeyup="change_data_pabrik(<?=$a?>)" id="kapasitas_<?=$a?>" type="number" class="form-control" placeholder="Kapasitas" value="<?php echo $_key->kapasitas?>">
                                        <input type="hidden" id="kapasitas1_<?=$a?>" value="<?=$_key->kapasitas?>">
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12">
                                <div class="nk-int-mk" style="margin-top: 12px">
                                    <h2>Alamat</h2>
                                </div>
                                <div class="form-group ic-cmp-int">
                                    <div class="form-ic-cmp">
                                        <i class="notika-icon notika-map"></i>
                                    </div>
                                    <div class="nk-int-st">
                                        <input onkeyup="change_data_pabrik(<?=$a?>)" type="text" id="alamat_<?=$a?>" class="form-control" placeholder="Alamat" value="<?php echo $_key->alamat?>">
                                        <input type="hidden" id="alamat1_<?=$a?>" value="<?=$_key->alamat?>">
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12">
                                <button type="submit" id="btn_save_pabrik_<?=$a?>" class="btn btn-info" onclick="save_pabrik(<?=$a?>)" style="display: none;">simpan</button>
                            </div>
                           
                        </div>
                                <?php
                                $a++;
                            }
                        ?>

                        <script type="text/javascript">
                            function change_data_pabrik(index) {
                                var kapasitas = $('#kapasitas_'+index).val();
                                var kapasitas1 = $('#kapasitas1_'+index).val();
                                var alamat = $('#alamat_'+index).val();
                                var alamat1 = $('#alamat1_'+index).val();

                                if(kapasitas1 != kapasitas || alamat1 != alamat){
                                    $('#btn_save_pabrik_'+index).show();
                                }else{
                                    $('#btn_save_pabrik_'+index).hide();
                                }
                            }

                            function save_pabrik(index) {
                                var kapasitas = $('#kapasitas_'+index).val();
                                var alamat = $('#alamat_'+index).val();
                                var id_pabrik = $('#id_pabrik_'+index).val();

                                swal({
                                    title: 'Konfirmasi',
                                    text: 'Anda yakin ingin mengubah data pabrik '+id_pabrik+'?',
                                    showCancelButton: true,
                                    cancelButtonColor: '#d33',
                                    onOpen: () => {
                                        
                                    },
                                    onClose: () => {

                                    }
                                }).then((result) => {
                                    if(result){
                                        $.ajax({
                                            type: "POST",
                                            url: '<?php echo base_url()?>admin/update_pabrik',
                                            dataType: "json",
                                            data: {
                                                kapasitas: kapasitas,
                                                alamat: alamat,
                                                id_pabrik: id_pabrik
                                            },
                                            success: function (data) {
                                                if(data.success == '1'){
                                                    swal({
                                                        title: 'Ubah Data Berhasil',
                                                        type: 'success'
                                                    }).then((result) => {
                                                        window.location.href='<?php echo base_url('admin/my_company')?>';
                                                    })
                                                }else{
                                                    swal({
                                                        title: 'Oops..',
                                                        text: data.msg,
                                                        type: 'error'
                                                    }).then((result) => {

                                                    })
                                                }
                                            },
                                            error: function () {
                                                swal({
                                                    title: 'Oops.. Server sedang error.',
                                                    type: 'error'
                                                }).then((result) => {

                                                })
                                            }
                                        })
                                    }
                                });
                            }

                            function delete_pabrik(index) {
                                var id_pabrik = $('#id_pabrik_'+index).val();

                                swal({
                                    title: 'Konfirmasi',
                                    text: 'Anda yakin ingin menon-aktifkan data pabrik '+id_pabrik+'?',
                                    showCancelButton: true,
                                    cancelButtonColor: '#d33',
                                    onOpen: () => {
                                        
                                    },
                                    onClose: () => {

                                    }
                                }).then((result) => {
                                    if(result){
                                        $.ajax({
                                            type: "POST",
                                            url: '<?php echo base_url()?>admin/delete_pabrik',
                                            dataType: "json",
                                            data: {
                                                id_pabrik: id_pabrik
                                            },
                                            success: function (data) {
                                                if(data.success == '1'){
                                                    swal({
                                                        title: 'Non-aktifkan Pabrik Berhasil',
                                                        type: 'success'
                                                    }).then((result) => {
                                                        window.location.href='<?php echo base_url('admin/my_company')?>';
                                                    })
                                                }else{
                                                    swal({
                                                        title: 'Oops..',
                                                        text: data.msg,
                                                        type: 'error'
                                                    }).then((result) => {

                                                    })
                                                }
                                            },
                                            error: function () {
                                                swal({
                                                    title: 'Oops.. Server sedang error.',
                                                    type: 'error'
                                                }).then((result) => {

                                                })
                                            }
                                        })
                                    }
                                });
                            }
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('backend/footer')?>
