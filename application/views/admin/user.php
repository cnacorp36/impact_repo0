<?php $this->load->view('backend/header')?>
<?php $this->load->view('backend/navbar')?>
<div class="notika-status-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="normal-table-list mg-t-30">
                        <div class="basic-tb-hd">
                            <h2 style="text-align: center;">USER</h2><br>
                            <!-- <p>Tables with borders on all possible sides of the Table and Cells (<code>.table-bordered</code>).</p> -->
                            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModalone"><i class="fa fa-plus"> Add New</i></button>
                        </div>
                        <div class="bsc-tbl-bdr">
                            <?php
                            $i  = 1;
                            if($user != false){
                            ?>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>ID User</th>
                                        <th>Email User</th>
                                        <th>Passwrod</th>
                                        <th>Role</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        foreach ($user as $key) {
                                            ?>
                                            <tr>
                                        <td><?php echo $i?></td>
                                        <td><?php echo $key->id_user?></td>
                                        <td><?php echo $key->email?></td>
                                        <td><?php echo $key->password?></td>
                                        <td> <?php if($key->role == 1) echo 'Admin'; 
                                                    elseif($key->role == 2) echo 'Perusahaan';
                                                    elseif ($key->role == 0) echo 'Super Admin';
                                                    elseif ($key->role == 3) echo 'Petani';?></td>
                                        <td><?php if($key->status == 1) echo 'aktif'; elseif($key->status == 0) echo 'tidak aktif';?></td>
                                        <td>
                                            <button class="btn btn-default"><i class="fa fa-edit"> edit</i></button>
                                            <button class="btn btn-default"><i class="fa fa-trash"> hapus</i></button>
                                        </td>
                                    </tr
                                            <?php
                                            $i++;
                                        }
                                    ?>
                                    >
                                    
                                </tbody>
                            </table>
                            <?php
                            }
                            ?>
                            
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal -->
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="modals-single">
                            <div class="modals-default-cl">
                                <div class="modal fade" id="myModalone" role="dialog">
                                    <div class="modal-dialog modals-default">
                                        <div class="modal-content">
                                            <form class="form-horizontal" action="<?php echo base_url('master/add_user')?>" method="post">
                                    <div class="row">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <div class="form-example-wrap mg-t-30">
                                                                <div class="cmp-tb-hd cmp-int-hd">
                                                                    <h2>ADD NEW USER</h2>
                                                                </div>
                                                                <div class="form-example-int form-horizental">
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                                                                                <label class="hrzn-fm">Email</label>
                                                                            </div>
                                                                            <div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
                                                                                <div class="nk-int-st">
                                                                                    <input type="text" name="email" class="form-control input-sm" placeholder="Enter Email">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-example-int form-horizental mg-t-15">
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                                                                                <label class="hrzn-fm">Password</label>
                                                                            </div>
                                                                            <div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
                                                                                <div class="nk-int-st">
                                                                                    <input type="password" name="password" class="form-control input-sm" placeholder="Password">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-example-int form-horizental mg-t-15">
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                                                                                <label class="hrzn-fm">Role</label>
                                                                            </div>
                                                                            <div class="col-lg-4 col-md-3 col-sm-4 col-xs-12">
                                                                                <div class="nk-int-st">
                                                                                    <select class="selectpicker" name="role">
                                                                                    <option>== Pilih Role == </option>                                                
                                                                                    <option value="0">Super Admin</option>
                                                                                    <option value="1">Admin</option>
                                                                                    <option value="2">Perusahaan</option>
                                                                                    <option value="3">Petani</option>
                                                                                </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-example-int form-horizental mg-t-15">
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                                                                                <label class="hrzn-fm">Status</label>
                                                                            </div>
                                                                            <div class="col-lg-4 col-md-3 col-sm-4 col-xs-12">
                                                                                <div class="nk-int-st">
                                                                                    <select class="selectpicker" name="status">
                                                                                    <option>== Pilih Status == </option>       
                                                                                    <option value="1">Aktif</option>
                                                                                    <option value="0">Tidak Aktif</option>
                                                                                </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                <button type="submit" class="btn btn-default">Save</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <!-- End Modal -->
        </div>
    </div>
<?php $this->load->view('backend/footer')?>
<!-- <script>
        $(function(){
            // Success Type
            $('#ts-success').on('click', function() {
                toastr.success('Have fun storming the castle!', 'Miracle Max Says');
            });

            // Success Type
            $('#ts-info').on('click', function() {
                toastr.info('We do have the Kapua suite available.', 'Turtle Bay Resort');
            });

            // Success Type
            $('#ts-warning').on('click', function() {
                toastr.warning('My name is Inigo Montoya. You killed my father, prepare to die!');
            });

            // Success Type
            $('#ts-error').on('click', function() {
                toastr.error('I do not think that word means what you think it means.', 'Inconceivable!');
            });
        });
    </script> -->