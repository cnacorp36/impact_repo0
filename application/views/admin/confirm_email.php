<?php $this->load->view('backend/header')?>

    <div class="login-content">
        <!-- Login -->

        <div class="nk-block toggled" id="l-login">
            <form class="form-horizontal m-t-20" id="registersubmit" action="<?php echo base_url('registrasi/add');?>" method="post">
            <div class="nk-form">
                <h3>Konfirmasi Email</h3>
                <p>Link Verifikasi Belum Diterima ?</p>
                <a href="#" onclick="send_code()" class="btn btn-info">Kirim Link Verifikasi</a>
                
            </div>
        </form>

    </div>

    <script type="text/javascript">
        var email = '<?=$this->session->userdata('impact_sess_reg')['email']?>';
        function send_code() {
            $.ajax({
                type: "POST",
                url: '<?=base_url('registrasi/send_code')?>',
                dataType: "json",
                data: {
                },
                success: function (data) {
                    if(data.success == '1'){
                        console.log('<?=base_url('email_library/confirm_email.php?email=')?>'+email+'&kode_v'+kode);
                        var kode = data.kode;

                        $.ajax({
                            type: "POST",
                            //url: 'http://192.168.1.12/kelapa_sawit/email_library/confirm_email.php?email='+email+'&kode_v='+kode,
                            url: 'https://agrinesia.id/email_library/confirm_email.php?email='+email+'&kode_v='+kode,
                            dataType: "json",
                            data: {
                            },
                            success: function (data) {
                            }
                        })
                        swal({
                            title: 'Registrasi Berhasil',
                            text: 'Welcome On Board !',
                            type: 'success'
                        }).then((result) => {
                            window.location.href = url_redirect;
                        });
                    }else{
                        swal({
                            title: 'Oops..',
                            text: data.msg,
                            type: 'error'
                        });
                    }
                },
                error: function () {
                    swal({
                        title: 'Oops.. Server sedang error.',
                        type: 'error'
                    }).then((result) => {

                    })
                }
            })
        }
        
    </script>
    <script src="<?php echo base_url('assets/js/package/dist/sweetalert2.all.min.js')?>"></script>
<?php $this->load->view('backend/footer_sweet_alert')?>