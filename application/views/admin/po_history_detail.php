<?php $this->load->view('backend/header')?>
<?php $this->load->view('backend/navbar')?>
 <div class="breadcomb-area">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="breadcomb-list">
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="breadcomb-wp">
                  <div class="breadcomb-icon">
                    <i class="notika-icon notika-map"></i>
                  </div>
                  <div class="breadcomb-ctn">
                    <h2>PURCHASE ORDER HISTORY DETAIL</h2>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
 <div class="form-element-area">
        <div class="container">
             <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-element-list mg-t-30">
                        <div class="cmp-tb-hd">
                            <h2>Data Purchase Order</h2>
                            <!-- <p>An input mask helps the user with the input. This can be useful for dates, numerics, phone numbers etc...</p> -->
                        </div>
                        <div class="row">
                            <div class="col-sm-4 col-xs-12">
                                <div class="nk-int-mk">
                                    <div class="form-cmp">
                                        <p style="color: grey; font-size: 11px">ID Purchase Order</p>
                                    </div>
                                    <div class="nk-int-st">
                                        <p style="font-size: 14px; margin-top: -8px"><?=$data_po['id_po']?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 col-xs-12">
                                <div class="nk-int-mk">
                                    <div class="form-cmp">
                                        <p style="color: grey; font-size: 11px">ID Produk</p>
                                    </div>
                                    <div class="nk-int-st">
                                        <p style="font-size: 14px; margin-top: -8px"><?=$data_po['id_produk']?></p>
                                    </div>
                                </div>
                            </div>
                        </div><br><br><hr>


                        <div class="cmp-tb-hd">
                            <h2>Data Delivery Order</h2>

                        </div>

<?php
                        if($data_do['id_sj'] == null || $data_do['id_sj'] == ''){
?>
                        <div class="row" id="form_update_password" >
                            
                            <div class="col-sm-4 col-xs-12">
                                <div class="nk-int-mk" >
                                    <div class="form-cmp">
                                        <p style="color: red; font-size: 14px">Data Tidak Ada</p>
                                    </div>
                                </div>
                            </div>
                        </div><br><br><hr>
<?php
                        }else{
?>
                        <div class="row" id="form_update_password" >
                            
                            <div class="col-sm-4 col-xs-12">
                                <div class="nk-int-mk" >
                                    <div class="form-cmp">
                                        <p style="color: grey; font-size: 11px">ID Surat Jalan</p>
                                    </div>
                                    <div class="nk-int-st">
                                        <p style="font-size: 14px; margin-top: -8px"><?=$data_do['id_sj']?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4 col-xs-12">
                                <div class="nk-int-mk" >
                                    <div class="form-cmp">
                                        <p style="color: grey; font-size: 11px">Mobil</p>
                                    </div>
                                    <div class="nk-int-st">
                                        <p style="font-size: 14px; margin-top: -8px"><?=$data_do['mobil']?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4 col-xs-12">
                                <div class="nk-int-mk" >
                                    <div class="form-cmp">
                                        <p style="color: grey; font-size: 11px">No. Plat</p>
                                    </div>
                                    <div class="nk-int-st">
                                        <p style="font-size: 14px; margin-top: -8px"><?=$data_do['no_plat']?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4 col-xs-12">
                                <div class="nk-int-mk" >
                                    <div class="form-cmp">
                                        <p style="color: grey; font-size: 11px">Sopir</p>
                                    </div>
                                    <div class="nk-int-st">
                                        <p style="font-size: 14px; margin-top: -8px"><?=$data_do['sopir']?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4 col-xs-12">
                                <div class="nk-int-mk" >
                                    <div class="form-cmp">
                                        <p style="color: grey; font-size: 11px">No. Hp</p>
                                    </div>
                                    <div class="nk-int-st">
                                        <p style="font-size: 14px; margin-top: -8px"><?=$data_do['hp']?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4 col-xs-12">
                                <div class="nk-int-mk" >
                                    <div class="form-cmp">
                                        <p style="color: grey; font-size: 11px">Qty</p>
                                    </div>
                                    <div class="nk-int-st">
                                        <p style="font-size: 14px; margin-top: -8px"><?=$data_do['qty_realisasi']?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4 col-xs-12">
                                <div class="nk-int-mk" >
                                    <div class="form-cmp">
                                        <p style="color: grey; font-size: 11px">Jarak</p>
                                    </div>
                                    <div class="nk-int-st">
                                        <p style="font-size: 14px; margin-top: -8px"><?=$data_do['jarak']?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4 col-xs-12">
                                <div class="nk-int-mk" >
                                    <div class="form-cmp">
                                        <p style="color: grey; font-size: 11px">Estimasi</p>
                                    </div>
                                    <div class="nk-int-st">
                                        <p style="font-size: 14px; margin-top: -8px"><?=$data_do['estimasi']?> Jam</p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4 col-xs-12">
                                <div class="nk-int-mk" >
                                    <div class="form-cmp">
                                        <p style="color: grey; font-size: 11px">Keterangan</p>
                                    </div>
                                    <div class="nk-int-st">
                                        <p style="font-size: 14px; margin-top: -8px"><?=$data_do['ket_company']?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4 col-xs-12">
                                <div class="nk-int-mk" >
                                    <div class="form-cmp">
                                        <p style="color: grey; font-size: 11px">Tgl Pengantaran</p>
                                    </div>
                                    <div class="nk-int-st">
                                        <p style="font-size: 14px; margin-top: -8px"><?=$data_do['tgl_delivery']?></p>
                                    </div>
                                </div>
                            </div>
                        </div><br><br><hr>
<?php
                        }

                        if($pay != false){
?>

                        <div class="cmp-tb-hd">
                            <h2>Data Pembayaran</h2>
                        </div>
                        <div class="row" id="form_update_password" >
                            
                            <div class="col-sm-4 col-xs-12">
                                <div class="nk-int-mk" >
                                    <div class="form-cmp">
                                        <p style="color: grey; font-size: 11px">ID Pembayaran</p>
                                    </div>
                                    <div class="nk-int-st">
                                        <p style="font-size: 14px; margin-top: -8px"><?=$pay->id_pembayaran?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4 col-xs-12">
                                <div class="nk-int-mk" >
                                    <div class="form-cmp">
                                        <p style="color: grey; font-size: 11px">Total Bayar</p>
                                    </div>
                                    <div class="nk-int-st">
                                        <p style="font-size: 14px; margin-top: -8px"><?=$pay->total_bayar?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4 col-xs-12">
                                <div class="nk-int-mk" >
                                    <div class="form-cmp">
                                        <p style="color: grey; font-size: 11px">Tgl Konfirmasi</p>
                                    </div>
                                    <div class="nk-int-st">
                                        <p style="font-size: 14px; margin-top: -8px"><?=$pay->tgl_trf?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4 col-xs-12">
                                <div class="nk-int-mk" >
                                    <div class="form-cmp">
                                        <p style="color: grey; font-size: 11px">Status Bayar</p>
                                    </div>
                                    <div class="nk-int-st">
                                        <p style="font-size: 14px; margin-top: -8px"><?=$pay->status_bayar?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12">
                                <div class="nk-int-mk" >
                                    <div class="form-cmp">
                                        <p style=""><b>Pengirim</b></p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4 col-xs-12">
                                <div class="nk-int-mk" >
                                    <div class="form-cmp">
                                        <p style="color: grey; font-size: 11px">Nama Bank</p>
                                    </div>
                                    <div class="nk-int-st">
                                        <p style="font-size: 14px; margin-top: -8px"><?=$pay->nb1?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4 col-xs-12">
                                <div class="nk-int-mk" >
                                    <div class="form-cmp">
                                        <p style="color: grey; font-size: 11px">Atas Nama</p>
                                    </div>
                                    <div class="nk-int-st">
                                        <p style="font-size: 14px; margin-top: -8px"><?=$pay->an1?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12">
                                <div class="nk-int-mk" >
                                    <div class="form-cmp">
                                        <p style=""><b>Penerima</b></p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4 col-xs-12">
                                <div class="nk-int-mk" >
                                    <div class="form-cmp">
                                        <p style="color: grey; font-size: 11px">Nama Bank</p>
                                    </div>
                                    <div class="nk-int-st">
                                        <p style="font-size: 14px; margin-top: -8px"><?=$pay->nb0?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4 col-xs-12">
                                <div class="nk-int-mk" >
                                    <div class="form-cmp">
                                        <p style="color: grey; font-size: 11px">Atas Nama</p>
                                    </div>
                                    <div class="nk-int-st">
                                        <p style="font-size: 14px; margin-top: -8px"><?=$pay->an0?></p>
                                    </div>
                                </div>
                            </div>

                        </div><br><br><hr>

<?php
                        }
                        
                        if($pay1 != null){
                            ?>
                        <div class="cmp-tb-hd">
                            <h2>Data Refund</h2>
                        </div>
                        <div class="row" id="form_update_password" >
                            
                            <div class="col-sm-4 col-xs-12">
                                <div class="nk-int-mk" >
                                    <div class="form-cmp">
                                        <p style="color: grey; font-size: 11px">ID Pembayaran</p>
                                    </div>
                                    <div class="nk-int-st">
                                        <p style="font-size: 14px; margin-top: -8px"><?=$pay1->id_pembayaran?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4 col-xs-12">
                                <div class="nk-int-mk" >
                                    <div class="form-cmp">
                                        <p style="color: grey; font-size: 11px">Total Bayar</p>
                                    </div>
                                    <div class="nk-int-st">
                                        <p style="font-size: 14px; margin-top: -8px"><?=$pay1->total_bayar?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4 col-xs-12">
                                <div class="nk-int-mk" >
                                    <div class="form-cmp">
                                        <p style="color: grey; font-size: 11px">Tgl Konfirmasi</p>
                                    </div>
                                    <div class="nk-int-st">
                                        <p style="font-size: 14px; margin-top: -8px"><?=$pay1->tgl_trf?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4 col-xs-12">
                                <div class="nk-int-mk" >
                                    <div class="form-cmp">
                                        <p style="color: grey; font-size: 11px">Status Bayar</p>
                                    </div>
                                    <div class="nk-int-st">
                                        <p style="font-size: 14px; margin-top: -8px"><?=$pay1->status_bayar?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12">
                                <div class="nk-int-mk" >
                                    <div class="form-cmp">
                                        <p style=""><b>Pengirim</b></p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4 col-xs-12">
                                <div class="nk-int-mk" >
                                    <div class="form-cmp">
                                        <p style="color: grey; font-size: 11px">Nama Bank</p>
                                    </div>
                                    <div class="nk-int-st">
                                        <p style="font-size: 14px; margin-top: -8px"><?=$pay1->nb1?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4 col-xs-12">
                                <div class="nk-int-mk" >
                                    <div class="form-cmp">
                                        <p style="color: grey; font-size: 11px">Atas Nama</p>
                                    </div>
                                    <div class="nk-int-st">
                                        <p style="font-size: 14px; margin-top: -8px"><?=$pay1->an1?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12">
                                <div class="nk-int-mk" >
                                    <div class="form-cmp">
                                        <p style=""><b>Penerima</b></p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4 col-xs-12">
                                <div class="nk-int-mk" >
                                    <div class="form-cmp">
                                        <p style="color: grey; font-size: 11px">Nama Bank</p>
                                    </div>
                                    <div class="nk-int-st">
                                        <p style="font-size: 14px; margin-top: -8px"><?=$pay1->nb0?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4 col-xs-12">
                                <div class="nk-int-mk" >
                                    <div class="form-cmp">
                                        <p style="color: grey; font-size: 11px">Atas Nama</p>
                                    </div>
                                    <div class="nk-int-st">
                                        <p style="font-size: 14px; margin-top: -8px"><?=$pay1->an0?></p>
                                    </div>
                                </div>
                            </div>

                        </div>
                            <?php
                        }
                    ?>
                        <br><br><hr>

                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('backend/footer')?>
