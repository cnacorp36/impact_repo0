 <?php $this->load->view('backend/header')?>
<?php $this->load->view('backend/navbar')?>


<div class="breadcomb-area">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="breadcomb-list">
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="breadcomb-wp">
                  <div class="breadcomb-icon">
                    <i class="notika-icon notika-map"></i>
                  </div>
                  <div class="breadcomb-ctn">
                    <h2>HELP</h2>
                    <p>KEPALA SAWIT</span></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>

<?php
    $join = array(0 => 'u_perusahaan up-up.id_user=u.id_user');
    $where = array('u.email' => $sess['email']);
    $q = $m->get_data('', 'user u', $join, $where)->row();
?>
 <div class="contact-area">
        <div class="container">
            <div class="row">   
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <h3 style="text-align: center;">MY COMPANY</h3>
                    <div class="contact-list">
                        <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                                        <!-- <label class="hrzn-fm">Nama Kebun/PT :</label> -->
                                    </div>
                                    <div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
                                        <div class="nk-int-st">
                                            <input type="text" name="nama" class="form-control" disabled placeholder="Nama Kebun/PT" value="<?=$q->id_perusahaan?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                                        <!-- <label class="hrzn-fm">Alamat :</label> -->
                                    </div>
                                    <div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
                                        <div class="nk-int-st">
                                            <input type="text" name="alamat" readonly="readonly" class="form-control" placeholder="Alamat" value="<?=$q->nama?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                                        <!-- <label class="hrzn-fm">Alamat :</label> -->
                                    </div>
                                    <div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
                                        <div class="nk-int-st">
                                            <input type="text" name="alamat" readonly="readonly" class="form-control" placeholder="Alamat" value="<?=$q->email?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                                        <!-- <label class="hrzn-fm">Alamat :</label> -->
                                    </div>
                                    <div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
                                        <div class="nk-int-st">
                                            <input type="text" name="alamat" readonly="readonly" class="form-control" placeholder="Alamat" value="<?=$q->hp?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div><!-- end from -->
                </div>
                <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12">
                    <h3 style="text-align: center;">KONTAK KAMI</h3>
                    <div class="contact-list">
                        <form class="form-horizontal" action="<?php echo base_url('petani/add_new_kontak')?>" method="post">
                        <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                                        <!-- <label class="hrzn-fm">Nama Kebun/PT :</label> -->
                                    </div>
                                    <div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
                                        <textarea class="form-control" id="pesan" placeholder="Pesan" style="height: 240px"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                            <button class="btn btn-info" type="button" onclick="send_message()">Kirim Pesan</button>
                        </div>
                        </form>
                    </div><!-- end from -->
                </div>

                <script type="text/javascript">
                    function send_message() {
                        var pesan = $('#pesan').val();

                        $.ajax({
                            type: "POST",
                            url: '<?php echo base_url()?>admin/upload_message',
                            dataType: "json",
                            data: {
                                pesan: pesan
                            },
                            success: function (data) {
                                if(data.success == '1'){
                                    swal({
                                        title: 'Kirim Pesan Berhasil',
                                        type: 'success'
                                    }).then((result) => {
                                        window.location.href='<?php echo base_url('admin/help')?>';
                                    })
                                }else{
                                    swal({
                                        title: 'Oops..',
                                        text: data.msg,
                                        type: 'error'
                                    }).then((result) => {

                                    })
                                }
                            },
                            error: function () {
                                swal({
                                    title: 'Oops.. Server sedang error.',
                                    type: 'error'
                                }).then((result) => {

                                })
                            }
                        })
                    }
                </script>
            </div>
        </div>                
    </div>
    <br>
<?php $this->load->view('backend/footer')?>
