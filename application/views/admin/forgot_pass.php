<?php $this->load->view('backend/header')?>

    <div class="login-content">
        <!-- Login -->

        <div class="nk-block toggled" id="l-login">
            <form class="form-horizontal m-t-20" id="registersubmit" action="<?php echo base_url('registrasi/add');?>" method="post">
            <div class="nk-form">
                <h3>Lupa Password</h3>
                <input class="form-control" placeholder="Email" id="email"><br>
                <button type="button" onclick="send_code()" class="btn btn-info">Reset Password</button>
                <a href="<?=base_url('login')?>" class="btn btn-default">Kembali ke login</a>
                
            </div>
        </form>

    </div>

    <script type="text/javascript">
        var email = '<?=$this->session->userdata('impact_sess_reg')['email']?>';
        function send_code() {
            let email = $('#email').val();
            $.ajax({
                type: "POST",
                url: '<?=base_url('login/generate_kode')?>',
                dataType: "json",
                data: {
                    email: email
                },
                success: function (data) {
                    if(data.success == '1'){
                        var kode = data.kode;
                        var pass = data.pass;
                        $.ajax({
                            type: "POST",
                            //url: 'http://192.168.1.12/kelapa_sawit/email_library/change_pass.php?email='+email+'&kode_v='+kode+'&pass='+pass,
                            url: 'https://agrinesia.id/email_library/change_pass.php?email='+email+'&kode_v='+kode+'&pass='+pass,
                            dataType: "json",
                            data: {
                            },
                            success: function (data) {
                            }
                        })
                        swal({
                            title: 'Email Berhasil Diverifikasi',
                            text: 'Silahkan cek email Anda',
                            type: 'success'
                        }).then((result) => {
                            window.location.href = url_redirect;
                        });
                    }else{
                        swal({
                            title: 'Oops..',
                            text: data.msg,
                            type: 'error'
                        });
                    }
                },
                error: function () {
                    swal({
                        title: 'Oops.. Server sedang error.',
                        type: 'error'
                    }).then((result) => {

                    })
                }
            })
        }
        
    </script>
    <script src="<?php echo base_url('assets/js/package/dist/sweetalert2.all.min.js')?>"></script>
<?php $this->load->view('backend/footer_sweet_alert')?>