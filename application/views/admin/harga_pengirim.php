<?php $this->load->view('backend/header')?>
<?php $this->load->view('backend/navbar')?>
<div class="notika-status-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="normal-table-list mg-t-30">
                        <div class="basic-tb-hd">
                            <h2 style="text-align: center;">HARGA PENGIRIMAN</h2>
                            <br>
                            <!-- <p>Tables with borders on all possible sides of the Table and Cells (<code>.table-bordered</code>).</p> -->
                            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModalone1"><i class="fa fa-plus"> Add New</i></button>
                        </div>
                        <div class="bsc-tbl-bdr">
                            <?php
                            ?>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>ID Mobil</th>
                                        <th>Harga</th>
                                        <th style="text-align: center;">Kapatias Min</th>
                                        <th style="text-align: center;">Kapasitas Max</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 1;
                                        $kategori = $this->M_models->get_from_query("select * from ref_mobil");
                                        foreach ($kategori->result() as $key ) {
                                            ?>
                                            <tr>
                                        <td><?php echo $i?></td>
                                        <td><?php echo $key->id_mobil?></td>
                                        <!-- <td><?php echo $key->mobil?></td> -->
                                        <td><?php echo $key->harga?></td>
                                        <td style="text-align: center;"><?php echo $key->kapasitas_min?></td>
                                        <td style="text-align: center;"><?php echo $key->kapasitas_max?></td>
                                        <td>
                                            <button class="btn btn-default" data-toggle="modal" data-target="#edit"><i class="fa fa-edit"> edit</i></button>
                                            <button class="btn btn-default"><i class="fa fa-trash"> hapus</i></button>
                                        </td>
                                    </tr>
                                            <?php
                                            $i++;
                                        }
                                    ?>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal -->
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="modals-single">
                            <div class="modals-default-cl">
                                <div class="modal fade" id="myModalone1" role="dialog">
                                    <div class="modal-dialog modals-default">
                                        <div class="modal-content">
                                    <div class="modal-body">
                                    <div class="cmp-tb-hd cmp-int-hd">
                                        <h2><u> Harga Pengiriman</u></h2>
                                        <br>
                                    </div>
                                    <form class="form-horizontal" action="<?php echo base_url('harga_pengirim/add_harga')?>" method="post">
                                    <div class="form-example-int form-horizental">
                                       <!--  <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-12">
                                                    <label class="hrzn-fm">Nama Mobil</label>
                                                </div>
                                                <div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
                                                    <div class="nk-int-st">
                                                        <input type="text" name="mobil" class="form-control input-sm" placeholder="Name Mobil">
                                                    </div>
                                                </div>
                                                </div>
                                            </div> -->
                                            <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-12">
                                                    <label class="hrzn-fm">Harga </label>
                                                </div>
                                                <div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
                                                    <div class="nk-int-st">
                                                        <input type="text" name="harga" class="form-control input-sm" placeholder="Harga">
                                                    </div>
                                                </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-12">
                                                    <label class="hrzn-fm">Kapasitas Min </label>
                                                </div>
                                                <div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
                                                    <div class="nk-int-st">
                                                        <input type="text" name="kapasitas_min" class="form-control input-sm" placeholder="min">
                                                    </div>
                                                </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-12">
                                                    <label class="hrzn-fm">Kapasitas Max </label>
                                                </div>
                                                <div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
                                                    <div class="nk-int-st">
                                                        <input type="text" name="kapasitas_max" class="form-control input-sm" placeholder="max">
                                                    </div>
                                                </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <div class="modal-footer">
                                                <button type="submit" class="btn btn-default" >Save</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                    </form>
                                        
                                    </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- MODAL EDIT -->
                <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="modals-single">
                            <div class="modals-default-cl">
                                <div class="modal fade" id="edit" role="dialog">
                                    <div class="modal-dialog modals-default">
                                        <div class="modal-content">
                                    <div class="modal-body">
                                    <div class="cmp-tb-hd cmp-int-hd">
                                        <h2><u>Harga Pengiriman</u></h2>
                                    </div>
                                    <form class="form-horizontal" action="<?php echo base_url('harga_pengirim/edit')?>" method="post">
                                    <div class="form-example-int form-horizental">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-12">
                                                    <label class="hrzn-fm">Nama Mobil</label>
                                                </div>
                                                <div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
                                                    <div class="nk-int-st">
                                                        <input type="text" name="mobil" class="form-control input-sm" placeholder="Name Mobil">
                                                    </div>
                                                </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-12">
                                                    <label class="hrzn-fm">Harga </label>
                                                </div>
                                                <div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
                                                    <div class="nk-int-st">
                                                        <input type="text" name="harga" class="form-control input-sm" placeholder="Harga">
                                                    </div>
                                                </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-12">
                                                    <label class="hrzn-fm">Min </label>
                                                </div>
                                                <div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
                                                    <div class="nk-int-st">
                                                        <input type="text" name="min" class="form-control input-sm" placeholder="Harga">
                                                    </div>
                                                </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-12">
                                                    <label class="hrzn-fm">Max </label>
                                                </div>
                                                <div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
                                                    <div class="nk-int-st">
                                                        <input type="text" name="max" class="form-control input-sm" placeholder="Harga">
                                                    </div>
                                                </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <div class="modal-footer">
                                                <button type="submit" class="btn btn-default" >Save</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                    </form>
                                        
                                    </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <!-- End Modal -->
        </div>
    </div>
<?php $this->load->view('backend/footer')?>