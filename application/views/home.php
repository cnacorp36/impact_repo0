 <?php $this->load->view('frontend/header')?>
 <?php $this->load->view('frontend/navbar')?>
 <section id="home" class="home bg-black fix">
                <div class="overlay"></div>
                <div class="container">
                    <div class="row">
                        <div class="main_home text-center">
                            <div class="col-md-12">
                                <div class="hello">
                                    <div class="slid_item">
                                        <div class="home_text ">
                                            <h1 class="text-yellow">Agrinesia</h1>
                                            <!-- <h3 class="text-white text-uppercase">We Create a Concept into The Market </h3> -->
                                        </div>
                                    </div><!-- End off slid item -->

                                </div>
                            </div>

                        </div>
                        <a class="mouse-scroll" href="#about"> 
                            <span class="mouse">
                                <span class="mouse-movement"></span>
                            </span>
                            <span class="mouse-message fadeIn">Explore</span> <br />
                            <!--<i class="fa fa-angle-down m-top-10 fadeIn mouse-message"></i>--> 
                        </a>

                    </div><!--End off row-->
                </div><!--End off container -->
            </section> <!--End off Home Sections-->
            <!--About Section-->
            <section id="about" class="about bg-yellow roomy-80">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="main_about text-center">
                                <h1 class="text-black">Agrinesia</h1>
                                <h2 class="text-white">- & -</h2>
                                <h3 class="text-black text-uppercase">Organized Agricultural E-market</h3>

                                <a href="" class="btn btn-primary m-top-100">Download</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--Featured Section-->
            <section id="features" class="features bg-white">
                <div class="container">
                    <div class="row">
                        <div class="main_features fix roomy-70">

                            <div class="col-md-6">
                                <div class="features_item">
                                    <div class="head_title">
                                        <h2 class="text-uppercase">ABOUT<strong> Agrinesia ?</strong></h2>
                                    </div>
                                    <div class="featured_content">
                                        <p style="text-align: justify;">Incorporated in 2018, Agrinesia envisioned to evolve local agricultural market into wider competition for global open trade. Based in Jakarta, Agrinesia applies technological aspect into this long-established industry, bringing opportunities for small farmers to be known nationally, encourage people to develop their own products as market demand’s growth is rapid.</p>

                                        <!-- <a href="" class="btn btn-default m-top-40">Read More</a> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- End off row -->
                </div><!-- End off container -->
            </section><!-- End off Featured Section-->
            <!--Business Section-->
            <section id="service" class="service bg-grey roomy-70">
                <div class="container">
                    <div class="row">
                        <div class="main_service">
                            <div class="col-md-6">
                                <div class="service_slid">
                                    <div class="slid_shap bg-yellow"></div>
                                    <div class="service_slid_item text-center">
                                        <div class="service_slid_text">
                                            <center><img src="<?php echo base_url()?>assets/images/sawit/sawit.png" widht="20" hight="20"></center>
                                            <!-- <span class="icon icon icon-list text-black"></span> -->
                                            <h5 class="text-black m-top-20">UI/UX Design</h5>
                                        </div>
                                        <div class="service_slid_text">
                                            <span class="icon icon icon-money-13 text-black"></span>
                                            <h5 class="text-black m-top-20">UI/UX Design</h5>
                                        </div>
                                        <div class="service_slid_text">
                                            <span class="icon icon icon-shopping text-black"></span>
                                            <h5 class="text-black m-top-20">UI/UX Design</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5 col-md-offset-1">
                                <div class="service_item sm-m-top-50">
                                    <div class="head_title">
                                        <h2 class="text-uppercase">WHAT<strong> IS Agrinesia ?</strong></h2>
                                    </div>
                                    <div class="service_content">
                                        <p>Agrinesia eliminates multi-transactional and trading between factories to the farmers without compromising quality and quantity by using web-based system and our cutting-edge AI technology to distinguish differentiation between class of the agricultural quality. Deals alongside two parties can be happen anytime anywhere!</p>

                                        <!-- <a href="" class="btn btn-default m-top-40">Read More</a> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section><!-- End off Business section -->
                 <!--Featured Section-->
            <section id="features" class="features bg-white">
                <div class="container">
                    <div class="row">
                        <div class="main_features fix roomy-70">

                            <div class="col-md-5">
                                <div class="features_item">
                                    <div class="head_title">
                                        <h2 class="text-uppercase">HOW Agrinesia<strong> WORKS FOR FACTORIES?</strong></h2>
                                    </div>
                                    <div class="featured_content">
                                        <ul>
                                            
                                            <li><span class="fa fa-share"></span> Select agricultural section</li>
                                            
                                            <li><span class="fa fa-share"></span> Select location</li>
                                            
                                            <li><span class="fa fa-share"></span> Select certain products from various farmers within your location based on quality and quantity</li>
                                            
                                            <li><span class="fa fa-share"></span> Order's verification</li>
                                            
                                            <li><span class="fa fa-share"></span> Payment</li>
                                            
                                            <li><span class="fa fa-share"></span> Deliveries</li>
                                        </ul>

                                        <!-- <a href="" class="btn btn-default m-top-40">Read More</a> -->
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 col-md-offset-1 sm-m-top-50">
                                <div class="features_item2_area">
                                    <div class="features_item2 text-center">
                                        <div class="divider_horizontal"></div>
                                        <div class="divider_vertical"></div>
                                        <div class="col-xs-6">
                                            <div class="features_item_text">
                                                <img src="<?php echo base_url()?>assets/admin/img/sawit/sawit.png" width="50" height="50" alt="" class="img-circle" />
                                                <p class="m-top-20"></p>WHAT
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <div class="features_item_text">
                                                <img src="<?php echo base_url()?>assets/admin/img/sawit/ikan.png" width="50" height="50" alt="" class="img-circle" />
                                                <p class="m-top-20">WHY</p>
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <div class="features_item_text m-top-50">
                                                <img src="<?php echo base_url()?>assets/admin/img/sawit/ayam.png" width="50" height="50" alt="" class="img-circle" />
                                                <p class="m-top-20">HOW</p>
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <div class="features_item_text m-top-50">
                                                <img src="<?php echo base_url()?>assets/admin/img/sawit/udang.png" width="50" height="50" alt="" class="img-circle" />
                                                <p class="m-top-20">WHO</p>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- End off row -->
                </div><!-- End off container -->
            </section><!-- End off Featured Section-->
            <section id="who" class="service bg-grey roomy-70">
                <div class="container">
                    <div class="row">
                        <div class="main_service">
                            <div class="col-md-5 col-md-offset-0">
                                <div class="service_item sm-m-top-50">
                                    <div class="head_title">
                                        <h2 class="text-uppercase">WHO<strong> GET INVOLVED ON THE DEALS?</strong></h2>
                                    </div>
                                    <div class="service_content">
                                        <p>Agrinesia responsibles for all the verifications and payments, also we provide transport for deliveries if demanded!</p>

                                        <!-- <a href="" class="btn btn-default m-top-40">Read More</a> -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="service_slid">
                                    <div class="slid_shap bg-yellow"></div>
                                    <div class="service_slid_item text-center">
                                        <div class="service_slid_text">
                                            <span class="icon icon icon-list text-black"></span>
                                            <!-- <h5 class="text-black m-top-20">UI/UX Design</h5> -->
                                        </div>
                                        <div class="service_slid_text">
                                            <span class="icon icon icon-money-13 text-black"></span>
                                            <!-- <h5 class="text-black m-top-20">UI/UX Design</h5> -->
                                        </div>
                                        <div class="service_slid_text">
                                            <span class="icon icon icon-shopping text-black"></span>
                                            <!-- <h5 class="text-black m-top-20">UI/UX Design</h5> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </section><!-- End off Business section -->


            <!--Choose section-->
            <section id="teams" class="teams roomy-80">
                <div class="container">
                    <div class="row">
                        <div class="main_choose">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="choose_item">
                                        <img src="<?php echo base_url()?>assets/admin//img/sawit/jk.jpg" height="300" width="550" alt="" />
                                    </div>
                                </div>
                            </div><!-- End off col-md-6 -->

                            <div class="col-md-5 col-md-offset-1">
                                <div class="choose_content sm-m-top-40">
                                    <div class="head_title">
                                        <h2 class="text-uppercase">Why <strong>USE Agrinesia FOR BIG FARMERS AND RAM?</strong></h2>
                                    </div>
                                    <div class="choose_item_text fix">
                                        <h6><i class="fa fa-check-square-o"></i>NO NEED TO GO OUT FIND BUYER</h6>
                                        <!-- <p>Atque ducimus velit, earum quidem, iusto dolorem. </p> -->
                                    </div>
                                    <div class="choose_item_text fix m-top-20">
                                        <h6><i class="fa fa-check-square-o"></i>SUPER EASY TO MARKET THE PRODUCT</h6>
                                        <!-- <p>Atque ducimus velit, earum quidem, iusto dolorem. </p> -->
                                    </div>
                                    <div class="choose_item_text fix m-top-20">
                                        <h6><i class="fa fa-check-square-o"></i>PAYMEENT GUARANTEE AS THE DEALS HAPPEN</h6>
                                        <!-- <p>Atque ducimus velit, earum quidem, iusto dolorem. </p> -->
                                    </div>
                                    <div class="choose_item_text fix m-top-20">
                                        <h6><i class="fa fa-check-square-o"></i>OPEN OPPORTUNITIES FOR WIDER MARKET</h6>
                                        <!-- <p>Atque ducimus velit, earum quidem, iusto dolorem. </p> -->
                                    </div>
                                </div>
                            </div><!-- End off col-md-6 -->
                        </div>
                    </div><!-- End off row -->
                </div><!-- End off container -->
            </section><!-- End off choose section -->


            <!--Portfolio Section-->



                <!-- <section id="portfolio" class="portfolio margin-top-120">
                    <div class="container">
                        <div class="row">
                            <div class="main-portfolio roomy-80">

                                <div class="col-md-4">
                                    <div class="head_title text-left sm-text-center wow fadeInDown">
                                        <h2>Our Works</h2>
                                    </div>
                                </div>

                                <div class="col-md-8">
                                    <div class="filters-button-group text-right sm-text-center">
                                        <button class="btn button is-checked" data-filter="*">all</button>
                                        <button class="btn button" data-filter=".metal">Web Design</button>
                                        <button class="btn button" data-filter=".transition">Logo Design</button>
                                        <button class="btn button" data-filter=".alkali">Branding</button>
                                        <button class="btn button" data-filter=".ar">Branding</button>
                                    </div>
                                </div>



                                <div style="clear: both;"></div>

                                <div class="grid text-center">

                                    <div class="grid-item transition metal ium">
                                        <img alt="" src="assets/images/porfolio-1.jpg">
                                        <div class="grid_hover_area text-center">
                                            <div class="girid_hover_text m-top-110">
                                                <h4 class="text-white">Your Work Title</h4>
                                                <p class="text-white">- Business Card, Branding</p>
                                                <a href="assets/images/porfolio-1.jpg" class="btn btn-primary popup-img">View Project</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="grid-item metalloid " >
                                        <img alt="" src="assets/images/porfolio-2.jpg">
                                        <div class="grid_hover_area text-center">
                                            <div class="girid_hover_text m-top-180">
                                                <h4 class="text-white">Your Work Title</h4>
                                                <p class="text-white">- Business Card, Branding</p>
                                                <a href="assets/images/porfolio-2.jpg" class="btn btn-primary popup-img">View Project</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="grid-item post-transition metal numberGreaterThan50">
                                        <img alt="" src="assets/images/porfolio-3.jpg">
                                        <div class="grid_hover_area text-center">
                                            <div class="girid_hover_text m-top-50">
                                                <h4 class="text-white">Your Work Title</h4>
                                                <p class="text-white">- Business Card, Branding</p>
                                                <a href="assets/images/porfolio-3.jpg" class="btn btn-primary popup-img">View Project</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="grid-item post-transition metal ium" >
                                        <img alt="" src="assets/images/porfolio-4.jpg">
                                        <div class="grid_hover_area text-center">
                                            <div class="girid_hover_text m-top-180">
                                                <h4 class="text-white">Your Work Title</h4>
                                                <p class="text-white">- Business Card, Branding</p>
                                                <a href="assets/images/porfolio-4.jpg" class="btn btn-primary popup-img">View Project</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="grid-item metal ar" >
                                        <img alt="" src="assets/images/porfolio-5.jpg">
                                        <div class="grid_hover_area text-center">
                                            <div class="girid_hover_text m-top-110">
                                                <h4 class="text-white">Your Work Title</h4>
                                                <p class="text-white">- Business Card, Branding</p>
                                                <a href="assets/images/porfolio-5.jpg" class="btn btn-primary popup-img">View Project</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="grid-item alkali ar" >
                                        <img alt="" src="assets/images/porfolio-6.jpg">
                                        <div class="grid_hover_area text-center">
                                            <div class="girid_hover_text m-top-50">
                                                <h4 class="text-white">Your Work Title</h4>
                                                <p class="text-white">- Business Card, Branding</p>
                                                <a href="assets/images/porfolio-6.jpg" class="btn btn-primary popup-img">View Project</a>
                                            </div>
                                        </div>
                                    </div>

                                </div>



                                <div style="clear: both;"></div>


                            </div>
                        </div>
                    </div>
                </section> -->



            <!--Test section-->
           <!--  <section id="test" class="test bg-grey roomy-60 fix">
                <div class="container">
                    <div class="row">                        
                        <div class="main_test fix">
                            <div class="col-md-6 sm-m-top-40">
                                <div class="test_item1 fix ">
                                    <div class="head_title fix">
                                        <h2 class="text-uppercase">What <strong>Client Say</strong></h2>
                                    </div>
                                    <div class="item_img">
                                        <img class="img-circle" src="assets/images/sawit/al.jpg" alt="" />
                                        <i class="fa fa-quote-left"></i>
                                    </div>

                                    <div class="item_text">
                                        <h5>Iwen Martin</h5>
                                        <h6>DonSkap@yahoo.com</h6>

                                        <p>Natus voluptatum enim quod necessitatibus quis
                                            expedita harum provident eos obcaecati id culpa
                                            corporis molestias.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 sm-m-top-40">
                                <div class="test_item1 fix ">
                                    <div class="head_title fix">
                                        <h2 class="text-uppercase">What <strong>Client Say</strong></h2>
                                    </div>
                                    <div class="item_img">
                                        <img class="img-circle" src="assets/images/sawit/al.jpg" alt="" />
                                        <i class="fa fa-quote-left"></i>
                                    </div>

                                    <div class="item_text">
                                        <h5>Isser Griffin</h5>
                                        <h6>yanser@gmail.com</h6>

                                         <p>Natus voluptatum enim quod necessitatibus quis
                                            expedita harum provident eos obcaecati id culpa
                                            corporis molestias.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section> -->
<?php $this->load->view('frontend/footer')?>