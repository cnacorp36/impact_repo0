<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Delivery extends CI_Controller {
	public $cek = null;

	function __construct(){
		parent::__construct();		
		$this->load->model('M_models');
		$this->cek =  $this->session->userdata('impact_sess');
		if(!$this->cek){
			redirect(base_url('login'));
		}
	}

	public function index(){
		if($this->cek['role'] == '2'){
			$join = array(
				0 => 'delivery_order do-do.id_do=dod.id_do',
				1 => 'purchase_order po-po.id_po=do.id_po',
				2 => 'purchase_order_detail pod-pod.id_purchase_order=po.id_po and pod.id_produk=dod.id_produk',
				3 => 'bid b-b.id_bid=po.id_bid',
				4 => 'bid_detail bd-bd.id_bid=b.id_bid and bd.id_produk=dod.id_produk',
				5 => 'u_perusahaan up-up.id_perusahaan=b.id_perusahaan',
				6 => 'user u-u.id_user=up.id_user',
				7 => 'pabrik fct-fct.id_pabrik=bd.id_pabrik',
				8 => 'produk prod-prod.id_produk=bd.id_produk',
				9 => 'kebun k-k.id_kebun=prod.id_kebun',
				10 => 'u_petani up_-up_.id_petani=k.id_petani'
			);
			$where = array(
				'u.email' => $this->cek['email'],
				'bd.status' => '6'
			);
			$q_delivery = $this->M_models->get_data('dod.*, fct.*, pod.qty, up.nama, up_.nama as nama_p, bd.satuan', 'delivery_order_detail dod', $join, $where, '', 'do.tgl_add asc', 0, 0, array(0 => "dod.status_pengiriman != '4'"));
			$data['do'] = $q_delivery;
			$data['menu'] = 'delivery';
			$this->load->view('admin/delivery', $data);
		}else{
			$join = array(
				0 => 'delivery_order do-do.id_do=dod.id_do',
				1 => 'purchase_order po-po.id_po=do.id_po',
				2 => 'purchase_order_detail pod-pod.id_purchase_order=po.id_po and pod.id_produk=dod.id_produk',
				3 => 'bid b-b.id_bid=po.id_bid',
				4 => 'bid_detail bd-bd.id_bid=b.id_bid and bd.id_produk=dod.id_produk',
				5 => 'u_perusahaan up-up.id_perusahaan=b.id_perusahaan',
				6 => 'user u-u.id_user=up.id_user',
				7 => 'pabrik fct-fct.id_pabrik=bd.id_pabrik',
				8 => 'produk prod-prod.id_produk=bd.id_produk',
				9 => 'kebun k-k.id_kebun=prod.id_kebun',
				10 => 'u_petani up_-up_.id_petani=k.id_petani'
			);

			$where = array(
				'bd.status' => '6'
			);
			$q_delivery = $this->M_models->get_data('dod.*, fct.*, pod.qty, up.nama, up_.nama as nama_p, bd.satuan', 'delivery_order_detail dod', $join, $where, '', 'do.tgl_add asc', 0, 0, array(0 => "dod.status_pengiriman != '4'"));
			$data['do'] = $q_delivery;
			$data['menu'] = 'delivery';
			$this->load->view('real_admin/delivery', $data);
		}
	}

	public function history(){
		$join = array(
			0 => 'delivery_order do-do.id_do=dod.id_do',
			1 => 'purchase_order po-po.id_po=do.id_po',
			2 => 'purchase_order_detail pod-pod.id_purchase_order=po.id_po and pod.id_produk=dod.id_produk',
			3 => 'bid b-b.id_bid=po.id_bid',
			4 => 'bid_detail bd-bd.id_bid=b.id_bid and bd.id_produk=dod.id_produk',
			5 => 'u_perusahaan up-up.id_perusahaan=b.id_perusahaan',
			6 => 'user u-u.id_user=up.id_user',
			7 => 'pabrik fct-fct.id_pabrik=bd.id_pabrik',
			8 => 'produk prod-prod.id_produk=bd.id_produk',
			9 => 'kebun k-k.id_kebun=prod.id_kebun',
			10 => 'u_petani up_-up_.id_petani=k.id_petani'
		);
		$q_delivery = $this->M_models->get_data('dod.*, fct.*, pod.qty, up.nama, up_.nama as nama_p, bd.satuan', 'delivery_order_detail dod', $join, array('bd.status' => '6'), '', 'do.tgl_add asc', 0, 0, array(0 => "dod.status_pengiriman = '4'"));
		$data['do'] = $q_delivery;
		$data['menu'] = 'delivery';
		$this->load->view('real_admin/delivery_history', $data);
	}

	public function verifikasi_admin(){
		$qty_confirm = $this->input->post('qty_confirm');
		$id_do = $this->input->post('id_do');
		$id_produk = $this->input->post('id_produk');
        //die(json_encode(array('success' => 0, 'msg' => $qty_confirm.' '.$id_do.' '.$id_produk)));
		$join = array(
			0 => 'delivery_order do-do.id_do=dod.id_do',
			1 => 'purchase_order po-po.id_po=do.id_po',
			2 => 'bid_detail bd-bd.id_bid=po.id_bid and bd.id_produk=dod.id_produk',
			3 => 'pembayaran p-p.id_po=po.id_po',
			4 => 'u_perusahaan up_-up_.id_perusahaan=po.id_perusahaan',
			5 => 'produk prod-prod.id_produk=dod.id_produk',
			6 => 'kebun k-k.id_kebun=prod.id_kebun',
			7 => 'u_petani up_1-up_1.id_petani=k.id_petani',
			8 => 'user u1-u1.id_user=up_1.id_user'
		);

		$cek_confirm = $this->M_models->get_data('u1.token, up_.nama, p.id_bank_tujuan, p.id_bank_asal, bd.harga, do.id_po, bd.qty1 as qty, prod.qty as p_qty, bd.id_bid, dod.tgl_delivery', 'delivery_order_detail dod', $join, array('dod.id_do' => $id_do, 'dod.id_produk' => $id_produk));

		if($cek_confirm->num_rows() > 0){

			$data_dod = array(
				'qty_realisasi' => $qty_confirm,
				'status_pengiriman' => '4'
			);

			$where_dod = array(
				'id_do' => $id_do,
				'id_produk' => $id_produk
			);

			$this->M_models->updateData('delivery_order_detail', $data_dod, $where_dod);

			//kemungkinan tidak dipakai (harus verifikasi admin dulu)
			if($qty_confirm < $cek_confirm->row()->qty){
				//pembayaran ke perusahaan
				$data_pembayaran = array(
					'id_pembayaran' => $this->M_models->get_id('pembayaran'),
					'status_bayar' => '0',
					'total_bayar' => 0,
					'id_bank_tujuan' => $cek_confirm->row()->id_bank_asal,
					'id_bank_asal' => $cek_confirm->row()->id_bank_tujuan,
					'atas_nama' => $this->M_models->get_data('atas_nama', 'bank', null, array('id_bank' => $cek_confirm->row()->id_bank_tujuan))->row()->atas_nama,
					'tgl_trf' => date('Y/m/d H:i:s'),
					'keterangan' => '',
					'sisa_hutang' => ($cek_confirm->row()->harga * ($qty_confirm - $cek_confirm->row()->qty)),
					'kode_unik' => 0,
					'tgl_add' => date('Y/m/d H:i:s'),
					'id_po' => $cek_confirm->row()->id_po,
					'is_child' => '1'
				);

				$this->M_models->insertData('pembayaran', $data_pembayaran);

				//pembayaran ke petani
				$data_pembayaran1 = array(
					'id_pembayaran' => $this->M_models->get_id('pembayaran'),
					'status_bayar' => '0',
					'total_bayar' => 0,
					'id_bank_tujuan' => '',
					'id_bank_asal' => $cek_confirm->row()->id_bank_tujuan,
					'atas_nama' => $this->M_models->get_data('atas_nama', 'bank', null, array('id_bank' => $cek_confirm->row()->id_bank_tujuan))->row()->atas_nama,
					'tgl_trf' => date('Y/m/d H:i:s'),
					'keterangan' => '',
					'sisa_hutang' => ($cek_confirm->row()->harga * $qty_confirm),
					'kode_unik' => 0,
					'tgl_add' => date('Y/m/d H:i:s'),
					'id_po' => $cek_confirm->row()->id_po,
					'is_child' => '1'
				);

				$this->M_models->insertData('pembayaran', $data_pembayaran1);

				//update qty produk
				// $qty_baru = $cek_confirm->row()->p_qty - $cek_confirm->row()->qty;
				// $this->M_models->update_data('produk', array('qty' => $qty_baru), array('id_produk' => $id_produk));

				//insert produk terjual
				$data_terjual = array(
					'id_purchase_order' => $cek_confirm->row()->id_po,
					'id_produk' => $id_produk,
					'id_bid' => $cek_confirm->row()->id_bid,
					'qty' => $cek_confirm->row()->qty,
					'harga' => $cek_confirm->row()->harga,
					'tgl_approve' => date('Y/m/d H:i:s'),
					'qty_confirm' => $qty_confirm,
					'tgl_confirm' => date('Y/m/d H:i:s', strtotime($cek_confirm->row()->tgl_delivery))
				);

				$this->M_models->insertData('produk_terjual', $data_terjual);

				die(json_encode(array('success' => 1, 'token' => $cek_confirm->row()->token, 'perusahaan' => $cek_confirm->row()->nama)));
			}else{
				//pembayaran ke petani
				$data_pembayaran1 = array(
					'id_pembayaran' => $this->M_models->get_id('pembayaran'),
					'status_bayar' => '0',
					'total_bayar' => 0,
					'id_bank_tujuan' => '',
					'id_bank_asal' => $cek_confirm->row()->id_bank_tujuan,
					'atas_nama' => $this->M_models->get_data('atas_nama', 'bank', null, array('id_bank' => $cek_confirm->row()->id_bank_tujuan))->row()->atas_nama,
					'tgl_trf' => date('Y/m/d H:i:s'),
					'keterangan' => '',
					'sisa_hutang' => ($cek_confirm->row()->harga * $qty_confirm),
					'kode_unik' => 0,
					'tgl_add' => date('Y/m/d H:i:s'),
					'id_po' => $cek_confirm->row()->id_po,
					'is_child' => '1'
				);

				$this->M_models->insertData('pembayaran', $data_pembayaran1);

				//update qty produk
				// $qty_baru = $cek_confirm->row()->p_qty - $cek_confirm->row()->qty;
				// $this->M_models->update_data('produk', array('qty' => $qty_baru), array('id_produk' => $id_produk));

				//insert produk terjual
				$data_terjual = array(
					'id_purchase_order' => $cek_confirm->row()->id_po,
					'id_produk' => $id_produk,
					'id_bid' => $cek_confirm->row()->id_bid,
					'qty' => $cek_confirm->row()->qty,
					'harga' => $cek_confirm->row()->harga,
					'tgl_approve' => date('Y/m/d H:i:s'),
					'qty_confirm' => $qty_confirm,
					'tgl_confirm' => date('Y/m/d H:i:s', strtotime($cek_confirm->row()->tgl_delivery))
				);

				$this->M_models->insertData('produk_terjual', $data_terjual);
				die(json_encode(array('success' => 1, 'token' => $cek_confirm->row()->token, 'perusahaan' => $cek_confirm->row()->nama)));
			}
		}else{
			die(json_encode(array('success' => 0, 'msg' => 'Data DO tidak ada')));
		}
		$data = array(
			'qty_realisasi' => $qty_confirm,
			'status_pengiriman' => '3'
		);
		$where = array(
			'id_do' => $id_do,
			'id_produk' => $id_produk
		);

		$this->M_models->update_data('delivery_order_detail', $data, $where);

		die(json_encode(array('success' => 1)));
	}

	public function verifikasi(){
		$qty_confirm = $this->input->post('qty_confirm');
		$ket = $this->input->post('ket');
		$id_do = $this->input->post('id_do');
		$id_produk = $this->input->post('id_produk');
        //die(json_encode(array('success' => 0, 'msg' => $qty_confirm.' '.$id_do.' '.$id_produk)));
		$join = array(
			0 => 'delivery_order do-do.id_do=dod.id_do',
			1 => 'purchase_order po-po.id_po=do.id_po',
			2 => 'bid_detail bd-bd.id_bid=po.id_bid and bd.id_produk=dod.id_produk',
			3 => 'pembayaran p-p.id_po=po.id_po'
		);
		$cek_confirm = $this->M_models->get_data('p.id_bank_tujuan, p.id_bank_asal, bd.harga, do.id_po, bd.qty', 'delivery_order_detail dod', $join, array('dod.id_do' => $id_do, 'dod.id_produk' => $id_produk));

		if($cek_confirm->num_rows() > 0){
			$data_dod = array(
				'qty_realisasi' => $qty_confirm,
				'status_pengiriman' => '3',
				'ket_company' => $ket,
				'tgl_delivery' => date('Y/m/d H:i:s')
			);

			$where_dod = array(
				'id_do' => $id_do,
				'id_produk' => $id_produk
			);

			$this->M_models->updateData('delivery_order_detail', $data_dod, $where_dod);

			die(json_encode(array('success' => '1')));

			//kemungkinan tidak dipakai (harus verifikasi admin dulu)
			if($qty_confirm < $cek_confirm->row()->qty){
				//pembayaran ke perusahaan
				$data_pembayaran = array(
					'id_pembayaran' => $this->M_models->get_id('pembayaran'),
					'status_bayar' => '0',
					'total_bayar' => 0,
					'id_bank_tujuan' => $cek_confirm->row()->id_bank_asal,
					'id_bank_asal' => $cek_confirm->row()->id_bank_tujuan,
					'atas_nama' => $this->M_models->get_data('atas_nama', 'bank', null, array('id_bank' => $cek_confirm->row()->id_bank_tujuan))->row()->atas_nama,
					'tgl_trf' => date('Y/m/d H:i:s'),
					'keterangan' => '',
					'sisa_hutang' => ($cek_confirm->row()->harga * ($qty_confirm - $cek_confirm->row()->qty)),
					'kode_unik' => 0,
					'tgl_add' => date('Y/m/d H:i:s'),
					'id_po' => $cek_confirm->row()->id_po,
					'is_child' => '1'
				);

				$this->M_models->insertData('pembayaran', $data_pembayaran);

				//pembayaran ke petani
				$data_pembayaran1 = array(
					'id_pembayaran' => $this->M_models->get_id('pembayaran'),
					'status_bayar' => '0',
					'total_bayar' => 0,
					'id_bank_tujuan' => '',
					'id_bank_asal' => $cek_confirm->row()->id_bank_tujuan,
					'atas_nama' => $this->M_models->get_data('atas_nama', 'bank', null, array('id_bank' => $cek_confirm->row()->id_bank_tujuan))->row()->atas_nama,
					'tgl_trf' => date('Y/m/d H:i:s'),
					'keterangan' => '',
					'sisa_hutang' => ($cek_confirm->row()->harga * $qty_confirm),
					'kode_unik' => 0,
					'tgl_add' => date('Y/m/d H:i:s'),
					'id_po' => $cek_confirm->row()->id_po,
					'is_child' => '1'
				);

				$this->M_models->insertData('pembayaran', $data_pembayaran1);
				die(json_encode(array('success' => 1)));
			}else{
				//pembayaran ke petani
				$data_pembayaran1 = array(
					'id_pembayaran' => $this->M_models->get_id('pembayaran'),
					'status_bayar' => '0',
					'total_bayar' => 0,
					'id_bank_tujuan' => '',
					'id_bank_asal' => $cek_confirm->row()->id_bank_tujuan,
					'atas_nama' => $this->M_models->get_data('atas_nama', 'bank', null, array('id_bank' => $cek_confirm->row()->id_bank_tujuan))->row()->atas_nama,
					'tgl_trf' => date('Y/m/d H:i:s'),
					'keterangan' => '',
					'sisa_hutang' => ($cek_confirm->row()->harga * $qty_confirm),
					'kode_unik' => 0,
					'tgl_add' => date('Y/m/d H:i:s'),
					'id_po' => $cek_confirm->row()->id_po,
					'is_child' => '1'
				);

				$this->M_models->insertData('pembayaran', $data_pembayaran1);
				die(json_encode(array('success' => 1)));
			}
		}else{
			die(json_encode(array('success' => 0, 'msg' => 'Data DO tidak ada')));
		}
		$data = array(
			'qty_realisasi' => $qty_confirm,
			'status_pengiriman' => '3'
		);
		$where = array(
			'id_do' => $id_do,
			'id_produk' => $id_produk
		);

		$this->M_models->update_data('delivery_order_detail', $data, $where);

		die(json_encode(array('success' => 1)));
	}
}
