<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_Controller {
	public $cek = null;

	function __construct(){
		parent::__construct();		
		$this->load->model('M_models');
		$this->cek =  $this->session->userdata('impact_sess');
		if(!$this->cek){
			redirect(base_url('login'));
		}else if($this->cek['role'] != '0' && $this->cek['role'] != '1'){
			redirect(base_url('admin'));
		}
		$this->load->library('email');
	}
	public function index(){
		$data['menu'] = 'setting';
		$this->load->view('real_admin/setting', $data);
	}
}