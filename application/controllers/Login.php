<?php 
 
class Login extends CI_Controller{
 

 	public $host = 0;
	function __construct(){
		parent::__construct();		
		$this->load->model('m_login');
		$this->load->model('M_models');
		$this->host = 'localhost';
		// if($this->session->userdata('impact_sess')){
		// 	redirect(base_url('admin'));
		// }else if($this->session->userdata('impact_sess_reg')){
		// 	redirect(base_url('registrasi/confirm_email'));
		// }
	}
 
	function index(){
		$m = $this->M_models;
		//setcookie('remember_me_agrinesia_id', '1', time() + (86400 * 30), '/', 'localhost');
		if(isset($_COOKIE['remember_me_agrinesia_id'])){
			$data['sign_me'] = $_COOKIE['remember_me_agrinesia_id'];
			if($data['sign_me'] == 1){
				if(isset($_COOKIE['remember_me_agrinesia_code'])){
					$remember_me_agrinesia_code = $_COOKIE['remember_me_agrinesia_code'];

					if($remember_me_agrinesia_code != ''){
						$cek_code = $m->get_data('', 'user', null, array('remember_me_code' => $remember_me_agrinesia_code));

						if($cek_code->num_rows() > 0){
							if($cek_code->row()->status == '2'){
								$this->session->set_userdata('impact_sess_reg', array('email' => $cek_code->row()->email, 'role' => $cek_code->row()->role));

								redirect(base_url('registrasi/confirm_email'));
							}else{
								$this->session->set_userdata('impact_sess', array('email' => $cek_code->row()->email, 'role' => $cek_code->row()->role));

								redirect(base_url('admin'));
							}
						}	
					}
				}
			}
		}else{
			$data['sign_me'] = 0;
		}
		$this->load->view('admin/login', $data);
	}

	public function test_cookie(){
		//echo $_COOKIE['remember_me_agrinesia_id'];
	}
	function aksi_login(){
		
		$email = strtolower($this->input->post('email'));
		$password = md5($this->input->post('password'));
		$sign_me = $this->input->post('sign_me');

		//die(json_encode(array('success' => 0, 'msg' => $password)));
		$cek_email = $this->M_models->get_data('id_user', 'user', null, array('email' => $email));

		if($cek_email->num_rows() == 0){
			die(json_encode(array('success' => 0, 'msg' => 'Email belum terdaftar')));
		}

		$cek = $this->M_models->get_data('', 'user', null, array('email' => $email, 'password' => $password), '', '', 0, 0, array(0 => "role='2' or role='0' or role='1'"));

		if($cek->num_rows() > 0){

			if($cek->row()->status == '0'){
				die(json_encode(array('success' => 0, 'msg' => 'Akun Anda tidak aktif')));
			}

			$cookie_code = md5($email.date('YmdHis'));

			$this->M_models->update_data('user', array('remember_me_code' => $cookie_code), array('email' => $email));

			setcookie('remember_me_agrinesia_id', $sign_me, time() + (31536000 * 30), '/', $this->host);
			setcookie('remember_me_agrinesia_code', $cookie_code, time() + (31536000 * 30), '/', $this->host);

			if($cek->row()->status == '2'){
				$data_session = array(
					'email' => $email,
					'role' => $cek->row()->role
				);
	 
				$this->session->set_userdata('impact_sess_reg', $data_session);
	 
				die(json_encode(array('success' => 2)));
			}else{
				$data_session = array(
					'email' => $email,
					'role' => $cek->row()->role
				);
	 
				$this->session->set_userdata('impact_sess', $data_session);
	 
				die(json_encode(array('success' => 1)));
			}
 
		}else{
			die(json_encode(array('success' => 0, 'msg' => 'Email atau password salah')));
		}
	}
 	
 	public function forgot_password(){
 		$this->load->view('admin/forgot_pass');
 	}

 	public function generate_kode(){
 		$email = $this->input->post('email');

 		$m = $this->M_models;

 		$cek = $m->get_data('', 'user', null, array('email' => $email, 'role' => '2'));
 		if($cek->num_rows() == 0){
 			die(json_encode(array('success' => 0, 'msg' => 'Email Belum Terdaftar')));
 		}
 		$kode = md5($email.date('YmdHis'));
 		$pass = substr($kode, 0, 6);

 		$m->updateData('user', array('kode_verifikasi' => $kode, 'password' => md5($pass)), array('email' => $email));

 		die(json_encode(array('success' => 1, 'kode' => $kode, 'pass' => $pass)));
 	}

	function logout(){
		setcookie('remember_me_agrinesia_code', 'asd', time() + (31536000 * 30), '/', $this->host);
		$this->session->sess_destroy();
		$this->session->unset_userdata('impact_sess');
		$this->session->unset_userdata('impact_sess_reg');
		redirect(base_url('welcome'));
	}
}