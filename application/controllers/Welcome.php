<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		if($this->session->userdata('impact_sess')){
			redirect(base_url('admin'));
		}else if($this->session->userdata('impact_sess_reg')){
			redirect(base_url('registrasi/confirm_email'));
		}else{
			$this->load->view('home');
		}
	}

	public function delete_data_bid(){
		$this->load->model('M_models');
		$id_bid = $this->input->post('id_bid');
		$id_perusahaan = $this->input->post('id_perusahaan');
		$id_petani = $this->input->post('id_petani');

		$m = $this->M_models;

		if($id_perusahaan){
			$qbid = $m->get_data('', 'bid', null, array('id_perusahaan' => $id_perusahaan));

			foreach ($qbid->result() as $key) {
				$m->deleteData('bid_detail', array('id_bid' => $key->id_bid));
			}
			$m->deleteData('bid', array('id_perusahaan' => $id_perusahaan));

			$qpo = $m->get_data('po.id_po, do.id_do', 'purchase_order po', array(0 => 'delivery_order do-do.id_po=po.id_po'), array('po.id_perusahaan' => $id_perusahaan));

			foreach ($qpo->result() as $key) {
				$m->deleteData('purchase_order_detail', array('id_purchase_order' => $key->id_po));
				$m->deleteData('pembayaran', array('id_po' => $key->id_po));
				$m->deleteData('delivery_order_detail', array('id_do' => $key->id_do));
				$m->deleteData('delivery_order', array('id_do' => $key->id_do));
			}
			$m->deleteData('purchase_order', array('id_perusahaan' => $id_perusahaan));
		}

		if($id_bid){
			if($id_petani){
				$join = array(
					0 => 'produk p-p.id_produk=bd.id_produk',
					1 => 'kebun k-k.id_kebun=p.id_kebun'
				);
				$q = $m->get_data('bd.id_bid', 'bid_detail bd', $join, array('k.id_petani' => $id_petani, 'bd.id_bid' => $id_bid));

				foreach ($q->result() as $key) {
					$m->deleteData('bid_detail', array('id_bid' => $key->id_bid));
				}
			}else{
				$m->deleteData('bid_detail', array('id_bid' => $id_bid));
				$m->deleteData('bid', array('id_bid' => $id_bid));
			}
		}else{
			if($id_petani){
				$join = array(
					0 => 'produk p-p.id_produk=bd.id_produk',
					1 => 'kebun k-k.id_kebun=p.id_kebun'
				);
				$q = $m->get_data('bd.id_bid', 'bid_detail bd', $join, array('k.id_petani' => $id_petani));

				foreach ($q->result() as $key) {
					$m->deleteData('bid_detail', array('id_bid' => $key->id_bid));
				}
			}
		}
	}
}
