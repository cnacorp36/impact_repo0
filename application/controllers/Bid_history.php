<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bid_history extends CI_Controller {
    public $cek = null;

    function __construct(){
        parent::__construct();      
        $this->load->model('M_models');
        $this->cek =  $this->session->userdata('impact_sess');
        if(!$this->cek){
            redirect(base_url('login'));
        }
    }

    public function index(){
        $data['menu'] = 'Bid History';
        $this->load->view('bid/bid_history', $data);
    }

    public function detail($id_bid, $id_produk, $status){
        $m = $this->M_models;
        $data['m'] = $m;
        $data['menu'] = 'Bid History Detail';

        $q = $m->get_data(
        	'bd.status, bd.tgl_edit, k.id_kebun, k.alamat, k.luas, k.kapasitas, k.satuan, farm.id_petani, farm.nama',
        	'bid_detail bd',
        	array(
        		0 => 'produk p-p.id_produk=bd.id_produk',
        		1 => 'kebun k-k.id_kebun=p.id_kebun',
        		2 => 'u_petani farm-farm.id_petani=k.id_petani'
        	),
        	array(
        		'bd.id_bid' => $id_bid,
        		'bd.id_produk' => $id_produk,
        		'bd.status' => $status
        	)
        );
        
        if($q->num_rows() > 0){
            $r = $q->row();
            $data_bid = array(
                'id_bid' => $id_bid,
                'id_produk' => $id_produk,
                'tgl_edit' => date('d/M/Y H:i', strtotime($r->tgl_edit)),
                'status' => $r->status == '0' ? 'Draft' : ($r->status == '1' ? 'Pending' : ($r->status == '2' ? 'Anda Membatalkan Penawaran Ini' : ($r->status == '3' || $r->status == '5' ? 'Penawaran Disepakati' : ($r->status == '4' ? 'Dibatalkan Oleh Penjual' : 'Purchase Order Sudah Masuk'))))
            );

            $data_kebun = array(
            	'id_kebun' => $r->id_kebun,
            	'alamat' => $r->alamat,
            	'luas' => number_format($r->luas),
            	'kapasitas' => number_format($r->kapasitas),
            	'satuan' => $r->satuan == '0' ? 'Kg' : ($r->satuan == '1' ? 'Ton' : 'Janjang')
            );

            $data_petani = array(
            	'id_petani' => $r->id_petani,
            	'nama' => $r->nama
            );

            $data['data_bid'] = $data_bid;
            $data['data_kebun'] = $data_kebun;
            $data['data_petani'] = $data_petani;
            
            $this->load->view('bid/bid_history_detail', $data);
        }else{
            redirect(base_url('Bid_history'));
        }
        
    }
}