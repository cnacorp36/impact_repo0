<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Petani extends CI_Controller {
	public $cek = null;

	function __construct(){
		parent::__construct();		
		$this->load->model('M_models');
		
		$this->cek =  $this->session->userdata('impact_sess');
	}

	public function index()
	{
		$data['menu'] = 'petani';

		$this->load->view('admin/petani', $data);
	}
}
