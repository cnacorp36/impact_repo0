<?php 
 
class Registrasi extends CI_Controller{
 
	function __construct(){
		parent::__construct();		
		 $this->load->model('m_login');
		 $this->load->model('M_models');

		 if($this->session->userdata('impact_sess')){
		 	redirect(base_url('admin'));
		 }
	}
 
	function index(){
		if($this->session->userdata('impact_sess_reg')){
			redirect(base_url('registrasi/confirm_email'));
		}
		$this->load->view('admin/registrasi');
	}

	public function add(){
		$email = strtolower($this->input->post('email'));
		$password = md5($this->input->post('password'));
		$password1 = md5($this->input->post('password1'));
		$nama = strtoupper($this->input->post('nama'));
		$pabrik = $this->input->post('pabrik');

		//die(json_encode(array('success' => 0, 'msg' => $email)));

		if($password1 != $password){
			die(json_encode(array('success' => 0, 'msg' => 'Password tidak sama')));
		}

		$cek_email = $this->M_models->get_data('id_user', 'user', null, array('email' => $email));
		if($cek_email->num_rows() > 0){
			die(json_encode(array('success' => 0, 'msg' => 'Email sudah ada')));
		}

		$id_user = $this->M_models->get_id('user');
		$data_user = array(
			'id_user' => $id_user,
			'password' => $password,
			'role' => '2',
			'email' => $email,
			'status' => '2',
			'kode_verifikasi' => md5($email.date('YmdHis'))
		);
		$this->M_models->insertData('user', $data_user);

		$id_perusahaan = $this->M_models->get_id('u_perusahaan');
		$data_perusahaan = array(
			'id_perusahaan' => $id_perusahaan,
			'nama' => $nama,
			'id_kategori' => 'k_01',
			'id_user' => $id_user
		);

		$this->M_models->insertData('u_perusahaan', $data_perusahaan);

		for($a=0; $a<count($pabrik); $a++){
			$data_pabrik = array(
				'id_pabrik' => $this->M_models->get_id('pabrik'),
				'id_perusahaan' => $id_perusahaan,
				'status' => '1',
				'alamat' => $pabrik[$a]['alamat'],
				'luas' => 0,
				'kapasitas' => $pabrik[$a]['kapasitas']
			);

			$this->M_models->insertData('pabrik', $data_pabrik);
		}

		$data_session = array(
			'email' => $email,
			'role' => '2'
		);

		$this->session->set_userdata('impact_sess_reg', $data_session);

		die(json_encode(array('success' => 1, 'kode_verifikasi' => md5($email.date('YmdHis')))));
	}

	public function confirm_email(){
		$m = $this->M_models;

		$cek = $m->get_data('', 'user', null, array('email' => $this->session->userdata('impact_sess_reg')['email']));
		if($cek->num_rows() > 0){
			if($cek->row()->status == '1'){
				$c = $this->session->userdata('impact_sess_reg');
				$this->session->set_userdata('impact_sess', $c);
				$this->session->unset_userdata('impact_sess_reg');
				redirect(base_url('admin'));
			}
		}else{
			$this->session->unset_userdata('impact_sess_reg');
			redirect(base_url('login'));
		}
		$this->load->view('admin/confirm_email');
	}

	public function send_code(){
		$email = $this->session->userdata('impact_sess_reg')['email'];

		$kode = md5($email.date('YmdHis'));

		$this->M_models->updateData('user', array('kode_verifikasi' => $kode), array('email' => $email));

		die(json_encode(array('success' => 1, 'kode' => $kode)));
	}

	public function confirm($kode){
		$m = $this->M_models;

		$cek = $m->get_data('', 'user', null, array('kode_verifikasi' => $kode));
		if($cek->num_rows() > 0){
			$m->updateData('user', array('status' => '1', 'kode_verifikasi' => ''), array('kode_verifikasi' => $kode));

			$data_session = array(
				'email' => $cek->row()->email,
				'role' => $cek->row()->role
			);

			$this->session->set_userdata('impact_sess', $data_session);

			redirect(base_url('admin'));
		}else{
			echo 'Kode Verifikasi Tidak Valid';
		}
	}
}