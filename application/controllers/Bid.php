<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bid extends CI_Controller {
	public $cek = null;
	public $server_key = 'AAAAzN1XsPA:APA91bFISzUkyUJEULBnknhm2OuartYcUzxMzI2FRLJoGD04EvzsCtHJKemgkP-VITnN60wb4MNpjl3Vq_0r0XIPndvgTsNVe4jEPJuCMtuy2E8pZbP2NHK1BcZ4T_tTbH-TzIOy7rpR';

	function __construct(){
		parent::__construct();		
		 $this->load->model('M_models');
		 $this->cek =  $this->session->userdata('impact_sess');
		 if(!$this->cek){
		 	redirect(base_url('login'));
		 }
		 $this->load->library('email');
	}
	public function index()
	{
		$data['menu'] = 'market';
		$data['sess'] = $this->cek;
		$join = array(
			0 => 'u_perusahaan up-up.id_perusahaan=p.id_perusahaan',
			1 => 'user u-u.id_user=up.id_user'
		);
		$q_pabrik = $this->M_models->get_data('p.*', 'pabrik p', $join, array('u.email' => $data['sess']['email'], 'p.status' => '1'), '', 'p.alamat');

		$data['pabrik'] = '';

		foreach ($q_pabrik->result() as $key) {
			$data['pabrik'] .= '<option value="'.$key->id_pabrik.'">'.$key->alamat.'</option>';
		}
		$this->load->view('bid/my_bid', $data);
	}

	// public function addBank(){

	// 	for ($a=0; $a < 3; $a++) { 
	// 		$id_bank = $this->M_models->get_id('bank');
	// 		$data = array(
	// 			'id_bank' => $id_bank,
	// 			'nama_bank' => 'nama'.$a,
	// 			'norek' => 'norek'.$a,
	// 			'atas_nama' => 'atas_nama'.$a,
	// 			'status' => '0',
	// 			'id_user' => 'u_18_10_19_000002',
	// 			'tgl_add' => date('Y/m/d H:i:s')
	// 		);

	// 		$this->M_models->insertData('bank', $data);
	// 	}
	// }

	public function proses_po(){
		//$email = $this->input->post('email');
		//$email = 'a@gmail.com';
		$id_pabrik = $this->input->post('id_pabrik');
		$id_produk_accept = $this->input->post('id_produk_accept');
		$total_bayar = 0;
		//$id_pabrik = 'ftry_18_11_02_000004';

		//die(json_encode(array('success' => 0, 'msg' => $id_pabrik)));
		$join0 = array(
			0 => 'u_perusahaan up-up.id_perusahaan=p.id_perusahaan',
			1 => 'user u-u.id_user=up.id_user'
		);
		$cek_pabrik = $this->M_models->get_data('p.id_perusahaan', 'pabrik p', $join0, array('u.status' => '1', 'u.role' => '2', 'u.email' => $this->cek['email']));

		if($cek_pabrik->num_rows() == 0){
			die(json_encode(array('success' => 0, 'msg' => 'Invalid Request')));
		}
		$join = array(0 => 'bid_detail bd-bd.id_bid=b.id_bid');
		$where1[0] = "bd.status='3' or bd.status='5'";
		//die(json_encode(array('success' => 0, 'msg' => $cek_pabrik->row()->id_perusahaan)));
		$q_bid = $this->M_models->get_data('b.id_bid, b.use_deliveries', 'bid b', $join, array('b.id_perusahaan' => $cek_pabrik->row()->id_perusahaan, 'b.status' => '0'), '', '', 1, 0, $where1);

		if($q_bid->num_rows() == 0){
			die(json_encode(array('success' => 0, 'msg' => 'Data Tidak Ada')));
		}else{
			$q_bid_detail_ = $this->M_models->get_from_query("select count(id_bid) as a from bid_detail where id_bid='".$q_bid->row()->id_bid."' and (status='3' or status='5')");
			if($q_bid_detail_->num_rows() > 0){
				if($q_bid_detail_->row()->a == '0'){
					die(json_encode(array('success' => 0, 'msg' => 'Data Tidak Ada')));
				}
			}else{
				die(json_encode(array('success' => 0, 'msg' => 'Data Tidak Ada')));
			}
		}

		$exp_ = explode(';', $id_pabrik);
		$exp_1 = explode(';', $id_produk_accept);
		for($aa=0; $aa<count($exp_); $aa++){
			if($exp_[$aa] != ''){
				$cek_tersedia = $this->M_models->get_data('bd.qty, p.qty as tersedia, k.satuan', 'bid_detail bd', array(0 => 'produk p-p.id_produk=bd.id_produk', 1 => 'kebun k-k.id_kebun=p.id_kebun'), array('bd.id_bid' => $q_bid->row()->id_bid, 'bd.id_produk' => $exp_1[$aa]), '', '', 0, 0, array(0 => "(bd.status='3' or bd.status='5')"));

				if($cek_tersedia->num_rows() > 0){
					$r_tersedia = $cek_tersedia->row();
					$satuan = $r_tersedia->satuan == '0' ? 'Kg' : ($r_tersedia->satuan == '1' ? 'Ton' : 'Janjang');
					if($r_tersedia->tersedia == '0'){
						die(json_encode(array('success' => 0, 'msg' => 'Produk '.$exp_1[$aa].' Telah Habis')));
					}else if($r_tersedia->qty > $r_tersedia->tersedia){
						die(json_encode(array('success' => 0, 'msg' => 'Produk '.$exp_1[$aa].' hanya tersisa '.$r_tersedia->tersedia.' '.$satuan.'. Silahkan cek kembali penawaran Anda !')));
					}else{
						$data_edit_bid_detail['id_pabrik'] = $exp_[$aa];
						// $where_edit_bid_detail['id_produk'] = $exp_1[$aa];
						// $where_edit_bid_detail['id_bid'] = $q_bid->row()->id_bid;
						$where_edit_bid_detail = "id_produk='".$exp_1[$aa]."' and id_bid='".$q_bid->row()->id_bid."' and (status='3' or status='5')";

						$this->M_models->updateData('bid_detail', $data_edit_bid_detail, $where_edit_bid_detail);
					}
				}else{
					die(json_encode(array('success' => 0, 'msg' => 'Produk Telah Habis')));
				}
			}
		}

		$data_edit_bid['status'] = '1';
		$data_edit_bid['tgl_edit'] = date('Y/m/d H:i:s');
		$where_edit_bid['id_bid'] = $q_bid->row()->id_bid;
		$this->M_models->updateData('bid', $data_edit_bid, $where_edit_bid);

		$join__ = array(
			0 => 'produk p-p.id_produk=bd.id_produk',
			1 => 'kebun k-k.id_kebun=p.id_kebun',
			2 => 'u_petani up_-up_.id_petani=k.id_petani',
			3 => 'user u-u.id_user=up_.id_user'
		);
		$q_bid_detail = $this->M_models->get_data('bd.*, u.token, p.qty as p_qty', 'bid_detail bd', $join__, array('bd.id_bid' => $q_bid->row()->id_bid));

		$id_po = $this->M_models->get_id('purchase_order');

		$data_po = array(
			'id_po' => $id_po,
			'id_perusahaan' => $cek_pabrik->row()->id_perusahaan,
			'tgl_add' => date('Y/m/d'),
			'use_deliveries' => $q_bid->row()->use_deliveries,
			'id_bid' => $q_bid->row()->id_bid,
			'status_bayar' => '0'
		);

		$this->M_models->insertData('purchase_order', $data_po);

		$data_baru = array();

		$token = array();
		foreach ($q_bid_detail->result() as $key) {
			if($key->status == '0' || $key->status == '1'){
				//masukkan data bid_detail yang masih pending atau draft ke dalam array utk diinsert ulang ke id_bid yang baru
				$data_baru[count($data_baru)] = $key;
			}else if($key->status == '5'){
				//insert data selisih dari bid yang tolak sebagian ke bid_detail sebagai tolak oleh petani

				//update qty dan harga yang disetujui oleh petani dari bid_detail yg tolak sebagian
				$token[count($token)] = $key->token;
				$data_edit_selisih['qty'] = $key->qty1;
				$data_edit_selisih['harga'] = $key->harga1;
				$data_edit_selisih['status'] = '6';
				$where_edit_selisih['id_bid'] = $key->id_bid;
				$where_edit_selisih['id_produk'] = $key->id_produk;
				$where_edit_selisih['status'] = '5';

				$this->M_models->updateData('bid_detail', $data_edit_selisih, $where_edit_selisih);

				$qty_baru = $key->p_qty - $key->qty1;
				$this->M_models->updateData('produk', array('qty' => $qty_baru), array('id_produk' => $key->id_produk));

				if($key->qty > $key->qty1){

					$data_baru_selisih = array(
						'id_bid' => $key->id_bid,
						'id_produk' => $key->id_produk,
						'qty' => ($key->qty - $key->qty1),
						'harga' => $key->harga,
						'tgl_add' => $key->tgl_add,
						'tgl_edit' => date('Y/m/d H:i:s'),
						'status' => '4',
						'harga_delivery' => 0,
						'jarak' => 0,
						'qty1' => 0,
						'harga1' => 0
					);

					$this->M_models->insertData('bid_detail', $data_baru_selisih);
					
				}
				//insert po dari bid yang disetujui oleh petani
				$data_po_detail = array(
					'id_purchase_order' => $id_po,
					'id_produk' => $key->id_produk,
					'harga' => $key->harga1,
					'qty' => $key->qty1,
					'harga_delivery' => 0,
					'jarak' => 0
				);

				$this->M_models->insertData('purchase_order_detail', $data_po_detail);

				$total_bayar += $key->harga1 * $key->qty1;
			}else if($key->status == '3'){
				$qty_baru = $key->p_qty - $key->qty1;
				$this->M_models->updateData('produk', array('qty' => $qty_baru), array('id_produk' => $key->id_produk));
				
				$token[count($token)] = $key->token;
				$data_po_detail = array(
					'id_purchase_order' => $id_po,
					'id_produk' => $key->id_produk,
					'harga' => $key->harga1,
					'qty' => $key->qty1,
					'harga_delivery' => 0,
					'jarak' => 0
				);

				$this->M_models->insertData('purchase_order_detail', $data_po_detail);

				$total_bayar += $key->harga1 * $key->qty1;
			}
		}

		$q_kode_unik = $this->M_models->get_from_query("
			SELECT
			    kode_unik
			FROM
			    pembayaran
			WHERE
			    tgl_add BETWEEN DATE_SUB(NOW(), INTERVAL 3 DAY) AND NOW()
		    ORDER BY kode_unik ASC LIMIT 1
		");

		if($q_kode_unik->num_rows() > 0){
			$kode_unik = $q_kode_unik->row()->kode_unik + 1;
		}else{
			$kode_unik = 300;
		}

		$q_expired = $this->M_models->get_data('pembayaran', 'setting_expired');
		if($q_expired->num_rows() > 0){
			$jam = $q_expired->row()->pembayaran != '' && $q_expired->row()->pembayaran != null ? $q_expired->row()->pembayaran : 240;
		}else{
			$jam = 240;
		}
		$expired = date('Y-m-d H:i:s', strtotime('+'.$jam.' hour'));

		$data_pembayaran = array(
			'id_pembayaran' => $this->M_models->get_id('pembayaran'),
			'status_bayar' => '0',
			'total_bayar' => $total_bayar,
			'id_bank_tujuan' => '',
			'id_bank_asal' => '',
			'atas_nama' => '',
			'tgl_trf' => date('Y/m/d'),
			'keterangan' => '',
			'sisa_hutang' => $total_bayar,
			'kode_unik' => $kode_unik,
			'tgl_add' => date('Y/m/d H:i:s'),
			'id_po' => $id_po,
			'tgl_expired' => $expired
		);

		$this->M_models->insertData('pembayaran', $data_pembayaran);

		//update bid_detail yang statusnya 3 (setuju oleh petani) menjadi 6 (setuju oleh perusahaan)
		$data_edit_detail['status'] = '6';
		$where_edit_detail['status'] = '3';
		$this->M_models->updateData('bid_detail', $data_edit_detail, $where_edit_detail);

		//insert ulang data bid yg masih pending/draft dengan id_bid yang baru
		if(count($data_baru) > 0){

			$id_bid = $this->M_models->get_id('bid');
			$data_baru_bid = array(
				'id_bid' => $id_bid,
				'status' => '0',
				'tgl_add' => date('Y/m/d H:i:s'),
				'tgl_edit' => date('Y/m/d H:i:s'),
				'use_deliveries' => '0',
				'id_perusahaan' => $cek_pabrik->row()->id_perusahaan
			);

			$this->M_models->insertData('bid', $data_baru_bid);

			for($a=0; $a<count($data_baru); $a++){
				//delete data lama
				$this->M_models->deleteData('bid_detail', array('id_bid' => $data_baru[$a]->id_bid, 'id_produk' => $data_baru[$a]->id_produk));

				$data_baru_bid_detail = array(
					'id_bid' => $id_bid,
					'id_produk' => $data_baru[$a]->id_produk,
					'qty' => $data_baru[$a]->qty,
					'harga' => $data_baru[$a]->harga,
					'tgl_add' => date('Y/m/d H:i:s'),
					'tgl_edit' => date('Y/m/d H:i:s'),
					'status' => $data_baru[$a]->status,
					'harga_delivery' => 0,
					'jarak' => 0,
					'qty1' => 0,
					'harga1' => 0
				);

				$this->M_models->insertData('bid_detail', $data_baru_bid_detail);
			}
		}

		$perusahaan_ = $this->M_models->get_data('up.nama', 'user u', array(0 => 'u_perusahaan up-up.id_user=u.id_user'), array('u.email' => $this->cek['email']));

		$nama_perusahaan = $perusahaan_->num_rows() > 0 ? $perusahaan_->row()->nama : '';

		die(json_encode(array('success' => 1, 'token' => $token, 'nama_perusahaan' => $nama_perusahaan, 'server_key' => $this->server_key)));
	}

	public function cancel_bid($value='')
	{
		$id_bid = $this->input->post('id_bid');
		$id_produk = $this->input->post('id_produk');
		$status = $this->input->post('status');

		$data0['status'] = $status;
		$data0['tgl_edit'] = date('Y/m/d H:i:s');
		$where0['id_bid'] = $id_bid;
		$where0['id_produk'] = $id_produk;

		$this->M_models->updateData('bid_detail', $data0, $where0);

		$join_perusahaan = array(
			0 => 'bid b-b.id_bid=bd.id_bid',
			1 => 'u_perusahaan up_-up_.id_perusahaan=b.id_perusahaan'
		);

		$cek0 = $this->M_models->get_data('bd.id_bid, up_.nama', 'bid_detail bd', $join_perusahaan, array('bd.id_bid' => $id_bid, 'bd.status' => '1'));

		$perusahaan = '';
		if($cek0->num_rows() > 0){
			$perusahaan = $cek0->row()->nama;
			$data1['status'] = '0';
			$data1['tgl_edit'] = date('Y/m/d H:i:s');
			$where1['id_bid'] = $id_bid;
			$this->M_models->updateData('bid', $data1, $where1);
		}else{
			
			$cek1 = $this->M_models->get_from_query("select id_bid from bid_detail where id_bid='".$id_bid."' and (status='2' or status='4')");

			if($cek1->num_rows() > 0){
				$data1['status'] = '2';
				$data1['tgl_edit'] = date('Y/m/d H:i:s');
				$where1['id_bid'] = $id_bid;
				$this->M_models->updateData('bid', $data1, $where1);
			}else{
				$cek2 = $this->M_models->get_from_query("select id_bid from bid_detail where id_bid='".$id_bid."' and status='5'");

				if($cek2->num_rows() > 0){
					$data1['status'] = '3';
					$data1['tgl_edit'] = date('Y/m/d H:i:s');
					$where1['id_bid'] = $id_bid;
					$this->M_models->updateData('bid', $data1, $where1);
				}
			}
		}

		$q_token = $this->M_models->get_data('u.token', 'produk p', array(0 => 'kebun k-k.id_kebun=p.id_kebun', 1 => 'u_petani up_-up_.id_petani=k.id_petani', 2 => 'user u-u.id_user=up_.id_user'), array('p.id_produk' => $id_produk));

		$token = $q_token->num_rows() > 0 ? $q_token->row()->token : '';

		die(json_encode(array('success' => 1, 'perusahaan' => $perusahaan, 'token' => $token, 'server_key' => $this->server_key)));
	}
}
