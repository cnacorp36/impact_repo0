<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	public $cek = null;
	public $server_key = 'AAAAzN1XsPA:APA91bFISzUkyUJEULBnknhm2OuartYcUzxMzI2FRLJoGD04EvzsCtHJKemgkP-VITnN60wb4MNpjl3Vq_0r0XIPndvgTsNVe4jEPJuCMtuy2E8pZbP2NHK1BcZ4T_tTbH-TzIOy7rpR';

	function __construct(){
		parent::__construct();		
		 $this->load->model('M_models');
		 $this->cek =  $this->session->userdata('impact_sess');
		 if(!$this->cek){
		 	redirect(base_url('login'));
		 }
	}

	public function ggg()
	{
		print_r($this->session->userdata('impact_sess_reg'));
		echo 'ddd';
		print_r($this->session->userdata('impact_sess'));
	}

	public function generalisasi_id(){
		$m_ = $this->M_models;
		$q = $m_->get_data('', 'user1');

		$cnt = 1;
		$cnt1 = 1;
		$cnt2 = 1;
		$y = date('y');
		$m = date('m');
		foreach ($q->result() as $key) {

			if($cnt < 10){
				$id_user = 'u_'.$y.'-'.$m.'-00000'.$cnt;
			}else{
				$id_user = 'u_'.$y.'-'.$m.'-0000'.$cnt;
			}

			$m_->updateData('user', array('id_user' => $id_user), array('id_user' => $key->id_user));
			if($key->role == 0){
				$m_->updateData('u_admin', array('id_user' => $id_user), array('id_user' => $key->id_user));
			}else if($key->role == 2){
				if($cnt1 < 10){
					$id_perusahaan = 'cmp_'.$y.'-'.$m.'-00000'.$cnt1;
				}else{
					$id_perusahaan = 'cmp_'.$y.'-'.$m.'-0000'.$cnt1;
				}
				$m_->updateData('u_perusahaan', array('id_user' => $id_user, 'id_perusahaan' => $id_perusahaan), array('id_user' => $key->id_user));
				$cnt1++;
			}else{
				if($cnt2 < 10){
					$id_petani = 'p_'.$y.'-'.$m.'-00000'.$cnt2;
				}else{
					$id_petani = 'p_'.$y.'-'.$m.'-0000'.$cnt2;
				}
				$id_petani = $m_->get_id('u_petani');
				$m_->updateData('u_petani', array('id_user' => $id_user, 'id_petani' => $id_petani), array('id_user' => $key->id_user));
				$cnt2++;
			}
			$cnt++;
		}
	}
	public function help(){
		$data['menu'] = 'help';
		$data['m'] = $this->M_models;
		$data['sess'] = $this->cek;
		$this->load->view('admin/help', $data);
	}
	public function index()
	{
		$data['menu'] = 'home';
		$this->load->view('admin/home_admin', $data);
	}

	public function my_company(){
		//$this->load->model('master_model');
		//$data['company'] = $this->master_model->company();
		//$data['company'] = $this->M_models->get_data('*', 'produk');
		$data['menu'] = 'company';
		$data['sess'] = $this->cek;
		$this->load->view('admin/my_company',$data);
	}
	public function market(){
		//$this->load->model('market_model');
		//$data['market'] = $this->market_model->market();
		$data['menu'] = 'market';
		$data['sess'] = $this->cek;
		$this->load->view('admin/market',$data);
	}

	public function submit_bid(){
		$email = $this->input->post('email');

		$join0 = array(
			0 => 'u_perusahaan p-p.id_user=u.id_user'
		);
		$cek_email = $this->M_models->get_data('p.id_perusahaan, p.nama', 'user u', $join0, array('u.status' => '1', 'u.role' => '2', 'u.email' => $email));
		if($cek_email->num_rows() == 0){
			die(json_encode(array('success' => 0, 'msg' => 'Invalid Request')));
		}

		//die(json_encode(array('success' => 0, 'msg' => $cek_email->row()->id_perusahaan)));

		$get_bid = $this->M_models->get_from_query("
			select b.id_bid, bd.id_produk, u.token, bd.qty, p.qty as tersedia
			from bid b
			left join bid_detail bd on bd.id_bid=b.id_bid
			left join produk p on p.id_produk=bd.id_produk
			left join kebun k on k.id_kebun=p.id_kebun
			left join u_petani up_ on up_.id_petani=k.id_petani
			left join user u on u.id_user=up_.id_user
			where b.id_perusahaan='".$cek_email->row()->id_perusahaan."'
			and b.status='0' and bd.status='0'
		");

		// $id_bid_msg = '';

		// foreach ($get_bid->result() as $key) {
		// 	$id_bid_msg .= $id_bid_msg == '' ? $key->id_bid : ', '.$key->id_bid;
		// }

		// die(json_encode(array('success' => 0, 'msg' => $id_bid_msg)));

		$token = array();
		$data_ = array();
		foreach ($get_bid->result() as $key) {
			if($key->qty <= $key->tersedia){
				$token[count($token)] = $key->token;
				$data_[] = array(
					'data' => array(
						'status' => '1'
					),
					'where' => array(
						'id_bid' => $key->id_bid,
						'id_produk' => $key->id_produk,
						'status' => '0'
					)
				);
			}else{
				die(json_encode(array('success' => 0, 'msg' => 'Produk dalam penawaran Anda ada yang melebihi ketersediaan. Silahkan cek kembali penawaran Anda !')));
			}
		}

		for($a=0; $a<count($data_); $a++){
			$data = $data_[$a]['data'];
			$where = $data_[$a]['where'];

			$this->M_models->updateData('bid_detail', $data, $where);
		}

		die(json_encode(array('success' => 1, 'token' => $token, 'perusahaan' => $cek_email->row()->nama, 'server_key' => $this->server_key)));
	}

	public function delete_bid(){
		$id_bid = $this->input->post('id_bid');
		$id_produk = $this->input->post('id_produk');

		$cek_status_bid = $this->M_models->get_data('status', 'bid_detail', null, array('id_bid' => $id_bid, 'id_produk' => $id_produk));

		if($cek_status_bid->num_rows() == 0){
			die(json_encode(array('success' => 0, 'msg' => 'Invalid Request')));
		}

		if($cek_status_bid->row()->status == '0' || $cek_status_bid->row()->status == '5'){
			$data_where['id_bid'] = $id_bid;
			$data_where['id_produk'] = $id_produk;

			$this->M_models->deleteData('bid_detail', $data_where);

			if($this->M_models->get_data('id_bid', 'bid_detail', null, array('id_bid' => $id_bid))->num_rows() == 0){
				$data_where1['id_bid'] = $id_bid;
				$this->M_models->deleteData('bid', $data_where1);
			}
		}else if($cek_status_bid->row()->status == '1'){
			$data['status'] = '2';
			$data_where['id_bid'] = $id_bid;
			$data_where['id_produk'] = $id_produk;

			$this->M_models->updateData('bid_detail', $data, $data_where);
		}else{
			die(json_encode(array('success' => 0, 'msg' => 'Invalid Status')));
		}
		die(json_encode(array('success' => 1)));
	}

	public function update_pass(){
		$pass0 = $this->input->post('pass0');
		$pass1 = $this->input->post('pass1');
		$pass2 = $this->input->post('pass2');
		$email = $this->input->post('email');

		$m = $this->M_models;

		if($pass2 != $pass1){
			$this->error_return('Password Baru Tidak Sama');
		}

		$cek = $m->get_data('', 'user', null, array('role' => '2', 'email' => $email, 'password' => md5($pass0)));

		if($cek->num_rows() == 0){
			$this->error_return('Password Lama Salah');
		}

		$m->updateData('user', array('password' => md5($pass1)), array('email' => $email));

		$this->success_return();
	}

	public function error_return($msg){
		$return = array('success' => 0, 'msg' => $msg);
		$this->json_return($return);
	}

	public function success_return(){
		$this->json_return(array('success' => 1));
	}
	public function json_return($return){
		die(json_encode($return));
	}

	public function edit_bid(){
		$id_bid = $this->input->post('id_bid');
		$id_produk = $this->input->post('id_produk');
		$qty = $this->input->post('qty');
		$harga = $this->input->post('harga');

		$data = array(
			'qty' => $qty,
			'harga' => $harga
		);

		$where = array(
			'id_bid' => $id_bid,
			'id_produk' => $id_produk
		);

		$this->M_models->updateData('bid_detail', $data, $where);
		die(json_encode(array('success' => 1, 'harga' => number_format($harga, 2))));
	}

	public function add_bid(){
		//die(json_encode(array('success' => 0, 'msg' => $this->input->post('harga'))));
		$email = $this->input->post('email');

		$join0 = array(
			0 => 'u_perusahaan p-p.id_user=u.id_user'	
		);
		$cek_email = $this->M_models->get_data('p.id_perusahaan', 'user u', $join0, array('u.status' => '1', 'u.role' => '2', 'u.email' => $email));
		if($cek_email->num_rows() == 0){
			die(json_encode(array('success' => 0, 'msg' => 'Invalid Request')));
		}
		$id_produk = $this->input->post('id_produk');
		$qty = $this->input->post('qty');
		$harga = $this->input->post('harga');

		// $join2[0] = array(
		// 	0 => 'bid b',
		// 	1 => 'b.id_bid=bd.id_bid'
		// );
		//$cek_produk = $this->M_models->get_data('b.id_bid', 'bid_detail bd', $join2, array('bd.id_produk' => $id_produk, 'b.id_perusahaan' => $cek_email->row()->id_perusahaan, 'b.status !=' => '0'));
		$cek_produk = $this->M_models->get_from_query("
			select b.id_bid, bd.status from bid_detail bd left join bid b on b.id_bid=bd.id_bid where bd.id_produk='".$id_produk."' and b.id_perusahaan='".$cek_email->row()->id_perusahaan."' and b.status != '1'
		");

		if($cek_produk->num_rows() > 0){

			$data_edit1['status'] = '0';
			$data_edit1['tgl_edit'] = date('Y/m/d H:i:s');
			$where_edit1['id_bid'] = $cek_produk->row()->id_bid;
			$this->M_models->updateData('bid', $data_edit1, $where_edit1);

			if($cek_produk->row()->status == '0' || $cek_produk->row()->status == '2' || $cek_produk->row()->status == '4' || $cek_produk->row()->status == '5'){

				$satuan = $this->M_models->get_data('k.satuan', 'produk p', array('kebun k-k.id_kebun=p.id_kebun'), array('p.id_produk' => $id_produk))->row()->satuan;

				$data_bid_detail_baru = array(
					'id_bid' => $cek_produk->row()->id_bid,
					'id_produk' => $id_produk,
					'qty' => $qty,
					'harga' => $harga,
					'tgl_add' => date('Y/m/d H:i:s'),
					'tgl_edit' => date('Y/m/d H:i:s'),
					'status' => '0',
					'harga_delivery' => 0,
					'jarak' => 0,
					'satuan' => $satuan
				);

				$this->M_models->insertData('bid_detail', $data_bid_detail_baru);

				// $data_edit = array(
				// 	'qty' => $qty,
				// 	'harga' => $harga,
				// 	'tgl_edit' => date('Y/m/d H:i:s'),
				// 	'status' => '0'
				// );

				// $where_edit = array(
				// 	'id_bid' => $cek_produk->row()->id_bid,
				// 	'id_produk' => $id_produk
				// );

				// $this->M_models->updateData('bid_detail', $data_edit, $where_edit);
			}
			
			if($cek_produk->row()->status == '2' || $cek_produk->row()->status == '4' || $cek_produk->row()->status == '5'){
				$count_bid = $this->input->post('count_bid');

				$join1 = array(
					0 => 'kebun k-k.id_kebun=p.id_kebun',
					1 => 'u_petani up-up.id_petani=k.id_petani'
				);
				$q_return = $this->M_models->get_data('p.id_produk, up.nama, k.alamat, k.satuan, p.qty, p.gambar', 'produk p', $join1, array('p.id_produk' => $id_produk))->row();

				$q_return->satuan = $q_return->satuan == '0' ? 'Kg' : ($q_return->satuan == '1' ? 'Ton' : 'Janjang');
				$data['data'] = '
					<tr>
						<td>'.$count_bid.'</td>
						<td><p>'.$q_return->nama.'</p>('.$q_return->alamat.')</td>
						<td>'.number_format($q_return->qty).' '.$q_return->satuan.'</td>
						<td id="qty-bid-'.$q_return->id_produk.'">'.$qty.' '.$q_return->satuan.'</td>
						<td id="qty-harga-'.$q_return->id_produk.'">Rp. '.number_format($harga, 2).'</td>
						<td id="qty-sub_total-'.$q_return->id_produk.'">Rp. '.number_format($harga * $qty, 2).'</td>
						<td><img class="thumbnail" src="'.$q_return->gambar.'" style="width: 100px"/></td>
						<td id="btn_tr'.$count_bid.'">
		                    <button class="btn btn-primary" onclick="edit_bid('.$count_bid.', '.$q_return->qty.', '.$qty.', '.$harga.', \''.$id_produk.'\', \''.$cek_produk->row()->id_bid.'\')" type="button"><i class="fa fa-pencil"></i></button>
		                    <button class="btn btn-danger" onclick="delete_produk(\''.$id_produk.'\', \''.$cek_produk->row()->id_bid.'\')" type="button"><i class="fa fa-trash"></i></button>
		                </td>
					</tr>
				';
				$data['count_bid'] = $count_bid+1;

				die(json_encode(array('success' => 1, 'data' => $data)));
			}else if($cek_produk->row()->status == '0'){
				die(json_encode(array('success' => 2, 'qty' => $qty.' Kg', 'harga' => 'Rp. '.number_format($harga, 2))));
			}else{
				die(json_encode(array('success' => 0, 'msg' => 'Silahkan selesaikan transaksi Anda di menu "my_bid" sebelum bid lagi')));
			}
		}

		$cek_bid = $this->M_models->get_data('id_bid', 'bid', null, array('id_perusahaan' => $cek_email->row() ->id_perusahaan, 'status' => '0'));

		if($cek_bid->num_rows() > 0){
			$id_bid = $cek_bid->row()->id_bid;
		}else{
			$id_bid = $this->M_models->get_id('bid');
			$data_bid = array(
				'id_bid' => $id_bid,
				'status' => '0',
				'tgl_add' => date('Y/m/d H:i:s'),
				'tgl_edit' => date('Y/m/d H:i:s'),
				'use_deliveries' => '0',
				'id_perusahaan' => $cek_email->row()->id_perusahaan
			);

			$this->M_models->insertData('bid', $data_bid);
		}

		$satuan = $this->M_models->get_data('k.satuan', 'produk p', array('kebun k-k.id_kebun=p.id_kebun'), array('p.id_produk' => $id_produk))->row()->satuan;

		$data_bid_detail = array(
			'id_bid' => $id_bid,
			'id_produk' => $id_produk,
			'qty' => $qty,
			'harga' => $harga,
			'tgl_add' => date('Y/m/d H:i:s'),
			'tgl_edit' => date('Y/m/d H:i:s'),
			'status' => '0',
			'harga_delivery' => 0,
			'jarak' => 0,
			'satuan' => $satuan
		);

		$this->M_models->insertData('bid_detail', $data_bid_detail);

		//get data produk
		$count_bid = $this->input->post('count_bid');

		$join1 = array(
			0 => 'kebun k-k.id_kebun=p.id_kebun',
			1 => 'u_petani up-up.id_petani=k.id_petani'
		);
		$q_return = $this->M_models->get_data('p.id_produk, up.nama, k.satuan, k.alamat, p.qty, p.gambar', 'produk p', $join1, array('p.id_produk' => $id_produk))->row();

		$q_return->satuan = $q_return->satuan == '0' ? 'Kg' : ($q_return->satuan == '1' ? 'Ton' : 'Janjang');

		$data['data'] = '
			<tr>
				<td>'.$count_bid.'</td>
				<td><p>'.$q_return->nama.'</p>('.$q_return->alamat.')</td>
				<td>'.number_format($q_return->qty).' '.$q_return->satuan.'</td>
				<td id="qty-bid-'.$q_return->id_produk.'">'.$qty.' '.$q_return->satuan.'</td>
				<td id="qty-harga-'.$q_return->id_produk.'">Rp. '.number_format($harga, 2).'</td>
				<td id="qty-sub_total-'.$q_return->id_produk.'">Rp. '.number_format($harga * $qty, 2).'</td>
				<td><img class="thumbnail" src="'.$q_return->gambar.'" style="width: 100px"/></td>
				<td id="btn_tr'.$count_bid.'">
                    <button class="btn btn-primary" onclick="edit_bid('.$count_bid.', \''.number_format($q_return->qty).'\', '.$qty.', '.$harga.', \''.$id_produk.'\', \''.$id_bid.'\', \''.$q_return->satuan.'\')" type="button"><i class="fa fa-pencil"></i></button>
                    <button class="btn btn-danger" onclick="delete_produk(\''.$id_produk.'\', \''.$id_bid.'\')" type="button"><i class="fa fa-trash"></i></button>
                </td>
			</tr>
		';
		$data['count_bid'] = $count_bid+1;

		die(json_encode(array('success' => 1, 'data' => $data)));
	}

	public function load_more_produk(){
		$loaded = $this->input->post('loaded');

		$i = $loaded + 1;
        $join[0] = array(
            0 => 'kebun k',
            1 => 'k.id_kebun=p.id_kebun'
        );
        $join[1] = array(
            0 => 'u_petani p0',
            1 => 'p0.id_petani=k.id_petani'
        );
        $join[2] = array(
            0 => 'user u',
            1 => 'u.id_user=p0.id_user'
        );

        $count_market = $this->M_models->get_data('p0.nama', 'produk p', $join, array('u.status' => '1', 'k.status' => '1', 'p.status' => '1', 'p.qty >' => 0), '', 'p.id_produk')->num_rows();

        $market = $this->M_models->get_data('p.id_produk, p0.nama, p.qty, p.gambar, p.tgl_edit, k.alamat', 'produk p', $join, array('u.status' => '1', 'k.status' => '1', 'p.status' => '1', 'p.qty >' => 0), '', 'p.id_produk', 20, $loaded);

        $data = '';
        foreach ($market->result() as $a) {
        	$data .= '
        		<tr id="tr'.$i.'">
                    <td>'.$i.'</td>
                    <td>
                        <a href="'.base_url().'petani" ><p><'.$a->nama.'</p>('.$a->alamat.')
                        <input type="hidden" id="id_produk'.$i.'" value="'.$a->id_produk.'">
                    </td>
                    <td>'.$a->qty.' Kg<input type="hidden" id="qty_'.$i.'" value="'.$a->qty.'"/></td>
                    <td><img class="thumbnail" src="'.$a->gambar.'"/></td>
                    <td>
                        <button type="button" class="btn btn-info" onclick="bid('.$i.')">BID</button>
                    </td>
                </tr>
        	';
        	$i++;
        }

        die(json_encode(array('data' => $data, 'loaded' => ($i-1), 'now_loaded' => $market->num_rows(), 'param_load_more' => ($count_market > ($loaded + 20)))));
	}

	public function add_new_delivery($id_produk){
		$q = $this->M_models->get_from_query("select k.harga from produk p left join sawit_kelas k on k.id=p.id_kelas where p.id_produk='".$id_produk."'");
		$harga = $q->row()->harga;

		$email = $this->session->userdata('email');
		$abc = $this->M_models->get_from_query("select a.*,b.id_perusahaan from user a left join u_perusahaan b on b.id_user = a.id_user where a.email = '".$email."' ");
		$id_perusahaan = $abc->row()->id_perusahaan;
		$peru = $this->M_models->get_from_query("select b.* from bid a left join bid_detail b on b.id_bid=a.id_bid where id_perusahaan ='".$id_perusahaan."' and a.status = '0'  ");
		if($peru->num_rows()>0){
			$insert = true;

			foreach ($peru->result() as $key) {
				if($id_produk == $key->id_produk){
					$insert = false;
				}
			}

			if($insert){
				$id_bid = $peru->row()->id_bid;
				$bid_detail = array(
					'id_bid' => $id_bid,
					'id_produk' => $id_produk,
					'qty' => 0,
					'harga' => $harga,
					'tgl_add' => date('Y-m-d H:i:s'),
					'tgl_edit' => date('Y-m-d H:i:s'),
					'status' => '0'
				);
				$this->M_models->insertData('bid_detail',$bid_detail);
			}
			
			redirect(base_url("admin/delivery"));
		}else{
			$id_bid = $this->M_models->get_id('bid');
			$bid_array = array(
				'id_bid' => $id_bid,
				'id_perusahaan' => $id_perusahaan,
				'tgl_add' => date('Y-m-d H:i:s'),
				'tgl_edit' => date('Y-m-d H:i:s'),
				'status' => '0'
			);
			$this->M_models->insertData('bid',$bid_array);
			$bid_detail_array = array(
				'id_bid' => $id_bid,
				'id_produk' => $id_produk,
				'qty' => 0,
				'harga' => $harga,
				'tgl_add' => date('Y-m-d H:i:s'),
				'tgl_edit' => date('Y-m-d H:i:s'),
				'status' => '0'
			);
			$this->M_models->insertData('bid_detail',$bid_detail_array);
			redirect(base_url("admin/delivery"));
		}
	}
	public function update_delivery(){
		$qty = $this->input->post('qty');
		$id_bid = $this->input->post('id_bid');
		$id_produk = $this->input->post('id_produk');
		$data_edit['qty'] = $qty;
		$where_edit['id_bid'] = $id_bid;
		$where_edit['id_produk'] = $id_produk;
		$this->M_models->update_data('bid_detail',$data_edit,$where_edit);
		//redirect(base_url("admin/delivery"));
		die(json_encode(array('success' => 1)));
	}
	public function finis(){
		$this->load->view('admin/finis');
	}

	function tambah_data(){
		for($a=0; $a<10; $a++){
			$id_user = $this->M_models->get_id('user');
			$data_user = array(
				'id_user' => $id_user,
				'password' => md5('123'),
				'email' => $a.'@gmail.com',
				'role' => '3',
				'status' => '1'
			);

			$this->M_models->insertData('user', $data_user);

			$id_petani = $this->M_models->get_id('u_petani');
			$data_petani = array(
				'id_petani' => $id_petani,
				'nama' => $a.'nama',
				'id_user' => $id_user,
				'id_kategori' => ''
			);

			$this->M_models->insertData('u_petani', $data_petani);

			for($b=0; $b<10; $b++){
				$id_kebun = $this->M_models->get_id('kebun');

				$data_kebun = array(
					'id_kebun' => $id_kebun,
					'id_petani' => $id_petani,
					'alamat' => $a.'-'.$b.'alamat',
					'luas' => ($a+$b),
					'kapasitas' => ($a+$b),
					'status' => '1'
				);

				$this->M_models->insertData('kebun', $data_kebun);

				for($c=0; $c<10; $c++){
					$id_produk = $this->M_models->get_id('produk');

					$data_produk = array(
						'id_produk' => $id_produk,
						'gambar' => '',
						'qty' => ($a+$b+$c),
						'id_kebun' => $id_kebun,
						'status' => '1',
						'tgl_add' => date('Y/m/d H:i:s'),
						'tgl_edit' => date('Y/m/d H:i:s')
					);

					$this->M_models->insertData('produk', $data_produk);
				}
			}
		}
	}

	public function add_pabrik(){
		$m = $this->M_models;
		$s = $this->session->userdata('impact_sess');

		$join = array(
			0 => 'u_perusahaan up-up.id_user=u.id_user'
		);
		$where = array(
			'u.email' => $s['email'],
			'u.role' => '2',
			'u.status' => '1'
		);
		$cek_user = $m->get_data('up.id_perusahaan', 'user u', $join, $where);

		if($cek_user->num_rows() > 0){
			$data = array(
				'id_pabrik' => $m->get_id('pabrik'),
				'id_perusahaan' => $cek_user->row()->id_perusahaan,
				'status' => '1',
				'alamat' => $this->input->post('alamat'),
				'luas' => 0,
				'kapasitas' => $this->input->post('kapasitas')
			);
			$m->insertData('pabrik', $data);
			die(json_encode(array('success' => 1)));
		}else{
			die(json_encode(array('success' => 0, 'msg' => 'Invalid User')));
		}
	}

	public function update_profile(){
		$nama = $this->input->post('nama');
		$email = $this->input->post('email');
		$email1 = $this->input->post('email1');
		$hp = $this->input->post('hp');
		$id_perusahaan = $this->input->post('id_perusahaan');

		$m = $this->M_models;

		if($nama == ''){
			$this->err('Nama Tidak Valid');
		}
		if($email == ''){
			$this->err('Email Tidak Valid');
		}
		if($hp == ''){
			$this->err('Nomor Telepon Tidak Valid');
		}

		$m->update_data('user', array('email' => $email, 'hp' => $hp), array('email' => $email1));

		$data_sess = array(
			'email' => $email,
			'role' => '2'
		);

		$this->session->set_userdata('impact_sess', $data_sess);

		$this->M_models->update_data('u_perusahaan', array('nama' => $nama), array('id_perusahaan' => $id_perusahaan));

		$this->succ();
	}

	public function upload_message(){
		$pesan = $this->input->post('pesan');
		$email = $this->cek['email'];
		$m = $this->M_models;

		$cek_email = $m->get_data('', 'user', null, array('email' => $email));

		if($cek_email->num_rows() > 0){
			$m->insertData('faq', array('id_kategori' => 1, 'pesan' => $pesan, 'id_user' => $cek_email->row()->id_user, 'id_faq' => $m->get_id('faq')));

			$this->succ();
		}else{
			$this->err('Invalid Request');
		}
	}

	public function err($msg=''){
		die(json_encode(array('success' => 0, 'msg' => $msg)));
	}

	public function succ($key = 'data', $data=null){
		die(json_encode(array('success' => 1, $key => $data)));
	}

	public function update_pabrik(){
		$kapasitas = $this->input->post('kapasitas');
		$alamat = $this->input->post('alamat');
		$id_pabrik = $this->input->post('id_pabrik');

		if($kapasitas == '' || $kapasitas <= 0){
			die(json_encode(array('success' => 0, 'msg' => 'Kapasitas Tidak Valid')));
		}

		if($alamat == ''){
			die(json_encode(array('success' => 0, 'msg' => 'Alamat Tidak Valid')));
		}

		$this->M_models->update_data('pabrik', array('alamat' => $alamat, 'kapasitas' => $kapasitas), array('id_pabrik' => $id_pabrik));

		die(json_encode(array('success' => 1)));
	}

	public function delete_pabrik(){
		$id_pabrik = $this->input->post('id_pabrik');

		if($id_pabrik == ''){
			die(json_encode(array('success' => 0, 'msg' => 'ID Pabrik tidak valid')));
		}

		// $cek_bd = $this->M_models->get_data('', 'bid_detail', null, array('id_pabrik' => $id_pabrik));

		// if($cek_bd->num_rows() > 0){
		// 	die(json_encode(array('success' => 0, 'msg' => 'Ubah data gagal karena ID Pabrik telah dipakai untuk Bid')));
		// }


		//$this->M_models->deleteData('pabrik', array('id_pabrik' => $id_pabrik));
		$this->M_models->updateData('pabrik', array('status' => '0'), array('id_pabrik' => $id_pabrik));

		die(json_encode(array('success' => 1)));
	}
}
