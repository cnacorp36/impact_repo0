<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model extends CI_Model
{

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->database();
    }

    public function cek_petani($id_petani){
        return $this->get_from_query("
            select u.status from u_petani p left join user u on u.id_user=p.id_user where p.id_petani='".$id_petani."'
        ");
    }

    function get_data($select='*', $tabel, $join=null, $where=null, $groupby='', $orderby='', $limit=0, $offset=0, $where1=null){
        $this->db->select($select);
        $this->db->from($tabel);
        if($join != null){
            for($a=0; $a<count($join); $a++){
                if(count($join[$a]) == 2){
                    $this->db->join($join[$a][0], $join[$a][1], 'left');
                }else{
                    $exp = explode('-', $join[$a]);
                    $this->db->join($exp[0], $exp[1], 'left');
                }
            }
        }
        if($where != null){
            $this->db->where($where);
        }
        if($where1 != null){
            for($a=0; $a<count($where1); $a++){
                $this->db->where("(".$where1[$a].")");
            }
        }
        if($groupby != ''){
            $this->db->group_by($groupby);
        }
        if($orderby != ''){
            $this->db->order_by($orderby);
        }

        if($limit != 0){
            $this->db->limit($limit, $offset);
        }

        return $this->db->get();
    }

    function get_id($tabel){
        $y = date('y');
        $m = date('m');
        $d = date('d');
        if($tabel == 'user'){
            $first_id = 'u_';
            $id_user = $this->get_from_query("select id_user from ".$tabel." order by id_user desc limit 1");
            if($id_user->num_rows() > 0){
                $id_user = $id_user->row()->id_user;
            }else{
                return $first_id.$y.'_'.$m.'_'.$d.'_000001';
            }
            $exp = explode('_', $id_user);
            if($exp[1] == $y && $exp[2] == $m && $exp[3] == $d){
                $last_id = intval($exp[4])+1;
                $last_id = $last_id < 10 ? '00000'.$last_id : ($last_id < 100 ? '0000'.$last_id : ($last_id < 1000 ? '000'.$last_id : ($last_id < 10000 ? '00'.$last_id : ($last_id < 100000 ? '0'.$last_id : $last_id))));
                return $first_id.$y.'_'.$m.'_'.$d.'_'.$last_id;
            }else{
                return $first_id.$y.'_'.$m.'_'.$d.'_000001';
            }
        }else if($tabel == 'u_petani'){
            $first_id = 'p_';
            $id_petani = $this->get_from_query("select id_petani from ".$tabel." order by id_petani desc limit 1");
            if($id_petani->num_rows() > 0){
                $id_petani = $id_petani->row()->id_petani;
            }else{
                return $first_id.$y.'_'.$m.'_'.$d.'_000001';
            }
            $exp = explode('_', $id_petani);
            if($exp[1] == $y && $exp[2] == $m && $exp[3] == $d){
                $last_id = intval($exp[4])+1;
                $last_id = $last_id < 10 ? '00000'.$last_id : ($last_id < 100 ? '0000'.$last_id : ($last_id < 1000 ? '000'.$last_id : ($last_id < 10000 ? '00'.$last_id : ($last_id < 100000 ? '0'.$last_id : $last_id))));
                return $first_id.$y.'_'.$m.'_'.$d.'_'.$last_id;
            }else{
                return $first_id.$y.'_'.$m.'_'.$d.'_000001';
            }
        }else if($tabel == 'u_perusahaan'){
            $first_id = 'cmp_';
            $id_perusahaan = $this->get_from_query("select id_perusahaan from ".$tabel." order by id_perusahaan desc limit 1");
            if($id_perusahaan->num_rows() > 0){
                $id_perusahaan = $id_perusahaan->row()->id_perusahaan;
            }else{
                return $first_id.$y.'_'.$m.'_'.$d.'_000001';
            }
            $exp = explode('_', $id_perusahaan);
            if($exp[1] == $y && $exp[2] == $m && $exp[3] == $d){
                $last_id = intval($exp[4])+1;
                $last_id = $last_id < 10 ? '00000'.$last_id : ($last_id < 100 ? '0000'.$last_id : ($last_id < 1000 ? '000'.$last_id : ($last_id < 10000 ? '00'.$last_id : ($last_id < 100000 ? '0'.$last_id : $last_id))));
                return $first_id.$y.'_'.$m.'_'.$d.'_'.$last_id;
            }else{
                return $first_id.$y.'_'.$m.'_'.$d.'_000001';
            }
        }else if($tabel == 'pabrik'){
            $first_id = 'ftry_';
            $id_pabrik = $this->get_from_query("select id_pabrik from ".$tabel." order by id_pabrik desc limit 1");
            if($id_pabrik->num_rows() > 0){
                $id_pabrik = $id_pabrik->row()->id_pabrik;
            }else{
                return $first_id.$y.'_'.$m.'_'.$d.'_000001';
            }
            $exp = explode('_', $id_pabrik);
            if($exp[1] == $y && $exp[2] == $m && $exp[3] == $d){
                $last_id = intval($exp[4])+1;
                $last_id = $last_id < 10 ? '00000'.$last_id : ($last_id < 100 ? '0000'.$last_id : ($last_id < 1000 ? '000'.$last_id : ($last_id < 10000 ? '00'.$last_id : ($last_id < 100000 ? '0'.$last_id : $last_id))));
                return $first_id.$y.'_'.$m.'_'.$d.'_'.$last_id;
            }else{
                return $first_id.$y.'_'.$m.'_'.$d.'_000001';
            }
        }else if($tabel == 'kebun'){
            $first_id = 'farm_';
            $id_kebun = $this->get_from_query("select id_kebun from ".$tabel." order by id_kebun desc limit 1");
            if($id_kebun->num_rows() > 0){
                $id_kebun = $id_kebun->row()->id_kebun;
            }else{
                return $first_id.$y.'_'.$m.'_'.$d.'_000001';
            }
            $exp = explode('_', $id_kebun);
            if($exp[1] == $y && $exp[2] == $m && $exp[3] == $d){
                $last_id = intval($exp[4])+1;
                $last_id = $last_id < 10 ? '00000'.$last_id : ($last_id < 100 ? '0000'.$last_id : ($last_id < 1000 ? '000'.$last_id : ($last_id < 10000 ? '00'.$last_id : ($last_id < 100000 ? '0'.$last_id : $last_id))));
                return $first_id.$y.'_'.$m.'_'.$d.'_'.$last_id;
            }else{
                return $first_id.$y.'_'.$m.'_'.$d.'_000001';
            }
        }else if($tabel == 'produk'){
            $first_id = 'prod_';
            $id_produk = $this->get_from_query("select id_produk from ".$tabel." order by id_produk desc limit 1");
            if($id_produk->num_rows() > 0){
                $id_produk = $id_produk->row()->id_produk;
            }else{
                return $first_id.$y.'_'.$m.'_'.$d.'_000001';
            }
            $exp = explode('_', $id_produk);
            if($exp[1] == $y && $exp[2] == $m && $exp[3] == $d){
                $last_id = intval($exp[4])+1;
                $last_id = $last_id < 10 ? '00000'.$last_id : ($last_id < 100 ? '0000'.$last_id : ($last_id < 1000 ? '000'.$last_id : ($last_id < 10000 ? '00'.$last_id : ($last_id < 100000 ? '0'.$last_id : $last_id))));
                return $first_id.$y.'_'.$m.'_'.$d.'_'.$last_id;
            }else{
                return $first_id.$y.'_'.$m.'_'.$d.'_000001';
            }
        }else if($tabel == 'bid'){
            $first_id = 'bid_';
            $id_bid = $this->get_from_query("select id_bid from ".$tabel." order by id_bid desc limit 1");
            if($id_bid->num_rows() > 0){
                $id_bid = $id_bid->row()->id_bid;
            }else{
                return $first_id.$y.'_'.$m.'_'.$d.'_000001';
            }
            $exp = explode('_', $id_bid);
            if($exp[1] == $y && $exp[2] == $m && $exp[3] == $d){
                $last_id = intval($exp[4])+1;
                $last_id = $last_id < 10 ? '00000'.$last_id : ($last_id < 100 ? '0000'.$last_id : ($last_id < 1000 ? '000'.$last_id : ($last_id < 10000 ? '00'.$last_id : ($last_id < 100000 ? '0'.$last_id : $last_id))));
                return $first_id.$y.'_'.$m.'_'.$d.'_'.$last_id;
            }else{
                return $first_id.$y.'_'.$m.'_'.$d.'_000001';
            }
        }else if($tabel == 'purchase_order'){
            $first_id = 'po_';
            $id_po = $this->get_from_query("select id_po from ".$tabel." order by id_po desc limit 1");
            if($id_po->num_rows() > 0){
                $id_po = $id_po->row()->id_po;
            }else{
                return $first_id.$y.'_'.$m.'_'.$d.'_000001';
            }
            $exp = explode('_', $id_po);
            if($exp[1] == $y && $exp[2] == $m && $exp[3] == $d){
                $last_id = intval($exp[4])+1;
                $last_id = $last_id < 10 ? '00000'.$last_id : ($last_id < 100 ? '0000'.$last_id : ($last_id < 1000 ? '000'.$last_id : ($last_id < 10000 ? '00'.$last_id : ($last_id < 100000 ? '0'.$last_id : $last_id))));
                return $first_id.$y.'_'.$m.'_'.$d.'_'.$last_id;
            }else{
                return $first_id.$y.'_'.$m.'_'.$d.'_000001';
            }
        }else if($tabel == 'bank'){
            $first_id = 'bank_';
            $id_bank = $this->get_from_query("select id_bank from ".$tabel." order by id_bank desc limit 1");
            if($id_bank->num_rows() > 0){
                $id_bank = $id_bank->row()->id_bank;
            }else{
                return $first_id.$y.'_'.$m.'_'.$d.'_000001';
            }
            $exp = explode('_', $id_bank);
            if($exp[1] == $y && $exp[2] == $m && $exp[3] == $d){
                $last_id = intval($exp[4])+1;
                $last_id = $last_id < 10 ? '00000'.$last_id : ($last_id < 100 ? '0000'.$last_id : ($last_id < 1000 ? '000'.$last_id : ($last_id < 10000 ? '00'.$last_id : ($last_id < 100000 ? '0'.$last_id : $last_id))));
                return $first_id.$y.'_'.$m.'_'.$d.'_'.$last_id;
            }else{
                return $first_id.$y.'_'.$m.'_'.$d.'_000001';
            }
        }else if($tabel == 'pembayaran'){
            $first_id = 'pay_';
            $id_pembayaran = $this->get_from_query("select id_pembayaran from ".$tabel." order by id_pembayaran desc limit 1");
            if($id_pembayaran->num_rows() > 0){
                $id_pembayaran = $id_pembayaran->row()->id_pembayaran;
            }else{
                return $first_id.$y.'_'.$m.'_'.$d.'_000001';
            }
            $exp = explode('_', $id_pembayaran);
            if($exp[1] == $y && $exp[2] == $m && $exp[3] == $d){
                $last_id = intval($exp[4])+1;
                $last_id = $last_id < 10 ? '00000'.$last_id : ($last_id < 100 ? '0000'.$last_id : ($last_id < 1000 ? '000'.$last_id : ($last_id < 10000 ? '00'.$last_id : ($last_id < 100000 ? '0'.$last_id : $last_id))));
                return $first_id.$y.'_'.$m.'_'.$d.'_'.$last_id;
            }else{
                return $first_id.$y.'_'.$m.'_'.$d.'_000001';
            }
        }else if($tabel == 'delivery_order'){
            $first_id = '_';
            $id_do = $this->get_from_query("select id_do from ".$tabel." order by id_do desc limit 1");
            if($id_do->num_rows() > 0){
                $id_do = $id_do->row()->id_do;
            }else{
                return $first_id.$y.'_'.$m.'_'.$d.'_000001';
            }
            $exp = explode('_', $id_do);
            if($exp[1] == $y && $exp[2] == $m && $exp[3] == $d){
                $last_id = intval($exp[4])+1;
                $last_id = $last_id < 10 ? '00000'.$last_id : ($last_id < 100 ? '0000'.$last_id : ($last_id < 1000 ? '000'.$last_id : ($last_id < 10000 ? '00'.$last_id : ($last_id < 100000 ? '0'.$last_id : $last_id))));
                return $first_id.$y.'_'.$m.'_'.$d.'_'.$last_id;
            }else{
                return $first_id.$y.'_'.$m.'_'.$d.'_000001';
            }
        }
    }

    function get_from_query($query){
        return $this->db->query($query);
    }
    
    function get_data_all($tabel, $order='', $by='', $limit='', $p_count=0, $start='')
    {
        if($p_count == 0){
            $this->db->select('*');
            $this->db->from($tabel);
            if($order!=''){
                $this->db->order_by($order, $by);
            }
            if($limit!=''){
                $this->db->limit($limit, $start);
            }

            $query = $this->db->get();
        }else{
            $this->db->from($tabel);
            $query = $this->db->count_all_results();
        }
        
        return $query;
    }

    function insert_data($tabel, $data)
    {
        $query = $this->db->insert($tabel, $data);

        if($query)
            return true;
        else
            return false;
    }

    function update_data($tabel, $data, $id)
    {
         $query = $this->db->update($tabel, $data, $id);

         if($query)
            return true;
         else
            return false;
    }

    function delete_data($tabel, $where, $value)
    {
        $this->db->where($where, $value);

        $query = $this->db->delete($tabel);

        if($query)
            return true;
        else
            return false;
    }

    function delete_data1($tabel, $where)
    {
        $this->db->where($where);

        $query = $this->db->delete($tabel);

        if($query)
            return true;
        else
            return false;
    }

    function get_data_like($tabel, $like, $value, $order, $by)
    {
        $this->db->select('*');
        $this->db->from($tabel);
        $this->db->like($like, $value);
        $this->db->order_by($order, $by);
        $query = $this->db->get();
        return $query;
    }

    function get_data_where_like($tabel, $where1, $value1, $where2, $value2, $order, $by)
    {
        $this->db->select('*');
        $this->db->from($tabel);
        $this->db->where($where1, $value1);
        $this->db->like($where2, $value2);
        $this->db->order_by($order, $by);
        $query = $this->db->get();
        return $query;
    }


}