<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Delivery_order extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
        $this->load->model('Model');
    }

    public function edit_do_post(){
      header('Access-Control-Allow-Origin: http://localhost:3000');
      //header('Access-Control-Allow-Origin: https://tabunganasa.com');
      header('Access-Control-Allow-Headers: *');
      header('Access-Control-Allow-Methods: POST');
    
      $id_po = $this->post('id_po');
      $id_produk = $this->post('id_produk');
      $mobil = $this->post('mobil');
      $no_plat = $this->post('no_plat');
      $sopir = $this->post('sopir');
      $estimasi = $this->post('estimasi');

      $cek = $this->Model->get_data('dod.id_do', 'delivery_order_detail dod', array(0 => 'delivery_order do-do.id_do=dod.id_do'), array('do.id_po' => $id_po, 'dod.id_produk' => $id_produk));

      if($cek->num_rows() > 0){
        $data = array(
          'mobil' => $mobil,
          'no_plat' => $no_plat,
          'sopir' => $sopir,
          'estimasi' => $estimasi
        );

        $where = array('id_do' => $cek->row()->id_do, 'id_produk' => $id_produk);

        $this->Model->update_data('delivery_order_detail', $data, $where);

        die(json_encode(array('success' => 1)));
      }else{
        die(json_encode(array('success' => 0, 'msg' => 'Invalid Request')));
      }
    }
    
    public function upload_do_post(){
      header('Access-Control-Allow-Origin: http://localhost:3000');
      //header('Access-Control-Allow-Origin: https://tabunganasa.com');
      header('Access-Control-Allow-Headers: *');
      header('Access-Control-Allow-Methods: POST');
    
      $id_po = $this->post('id_po');
      $id_produk = $this->post('id_produk');
      $mobil = $this->post('mobil');
      $no_plat = $this->post('no_plat');
      $sopir = $this->post('sopir');
      $estimasi = $this->post('estimasi');

      $id = $this->Model->get_id('delivery_order');
      $id_do = 'do'.$id;
      $id_sj = 'sj'.$id;

      $join_po = array(
        0 => 'delivery_order do-do.id_po=p.id_po'
      );

      $where_po = array(
        'p.id_po' => $id_po
      );
      $cek_po = $this->Model->get_data('', 'pembayaran p', $join_po, $where_po, '', '', 0, 0, array(0 => 'do.id_po is not null'));

      if($cek_po->num_rows() == 0){
        $data = array(
          'id_do' => $id_do,
          'id_po' => $id_po,
          'tgl_add' => date('Y/m/d H:i:s')
        );

        $this->Model->insert_data('delivery_order', $data);

        $data1 = array(
          'id_do' => $id_do,
          'id_sj' => $id_sj,
          'id_produk' => $id_produk,
          'mobil' => $mobil,
          'no_plat' => $no_plat,
          'jarak' => 0,
          'qty_realisasi' => 0,
          'status_pengiriman' => '1',
          'sopir' => $sopir,
          'estimasi' => $estimasi
        );

        $this->Model->insert_data('delivery_order_detail', $data1);

        die(json_encode(array('success' => 1)));
      }else{
        die(json_encode(array('success' => 0, 'msg' => 'Data sudah ada')));
      }
    }

    public function accept_bid_post(){
      header('Access-Control-Allow-Origin: http://localhost:3000');
      //header('Access-Control-Allow-Origin: https://tabunganasa.com');
      header('Access-Control-Allow-Headers: *');
      header('Access-Control-Allow-Methods: POST');

      $id_bid = $this->post('id_bid');
      $id_produk = $this->post('id_produk');
      $qty = $this->post('qty');
      $harga = $this->post('harga');

      $cek_qty_harga = $this->Model->get_data('qty, harga', 'bid_detail', null, array('id_bid' => $id_bid, 'id_produk' => $id_produk));

      if($cek_qty_harga->num_rows() > 0){
        if($qty != $cek_qty_harga->row()->qty || $harga != $cek_qty_harga->row()->harga){
          $data = array(
            'status' => '5'
          );
        }else{
          $data = array(
            'status' => '3'
          );
        }
        $data['tgl_edit'] = date('Y/m/d H:i:s');
        $data['qty1'] = $qty;
        $data['harga1'] = $harga;

        $where = array('id_produk' => $id_produk, 'id_bid' => $id_bid);
        $this->Model->update_data('bid_detail', $data, $where);

        die(json_encode(array('success' => 1)));
        // $message = ['success' => 1];
            
        // $this->set_response($message, REST_Controller::HTTP_OK); // CREATED (201) being the HTTP response code   
      }else{
        die(json_encode(array('success' => 0, 'msg' => 'Invalid Request')));
        // $message = ['success' => 0, 'msg' => 'Invalid Request'];
          
        // $this->set_response($message, REST_Controller::HTTP_BAD_REQUEST);
      }
    }

    public function cancel_bid_post(){
      header('Access-Control-Allow-Origin: http://localhost:3000');
      //header('Access-Control-Allow-Origin: https://tabunganasa.com');
      header('Access-Control-Allow-Headers: *');
      header('Access-Control-Allow-Methods: POST');

      $id_bid = $this->post('id_bid');
      $id_produk = $this->post('id_produk');

      $data['status'] = '4';
      $where = array('id_bid' => $id_bid, 'id_produk' => $id_produk);
      $this->Model->update_data('bid_detail', $data, $where);
      die(json_encode(array('success' => 1)));
    }
}
