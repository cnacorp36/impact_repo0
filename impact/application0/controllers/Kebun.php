<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Kebun extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
        $this->load->model('Model');
    }

    public function get_kebun_post()
    {
        
        header('Access-Control-Allow-Origin: http://localhost:3000');
        //header('Access-Control-Allow-Origin: https://tabunganasa.com');
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Methods: POST');

        $username = $this->post('username');
        $token = $this->post('token');
        
        //die(json_encode(array('success' => $username)));
        //$token = $this->post('token');
        

        if($username == ''){// || $token == ''){
            die(json_encode(array('success' => 3, 'msg' => 'Invalid Request')));
            // $message = ['success' => 0, 'msg' => 'Invalid Request'];
            
            // $this->set_response($message, REST_Controller::HTTP_BAD_REQUEST);
        }else{

            $cek = $this->Model->get_from_query("select up.* from user u left join u_petani up on up.id_user=u.id_user where (u.email='".$username."' or u.hp='".$username."')");// and u.token='".$token."'");

            if($cek->num_rows() > 0){
                $data_token['token'] = $token;
                $where_token['id_user'] = $cek->row()->id_user;

                $this->Model->update_data('user', $data_token, $where_token);
                $id_petani = $cek->row()->id_petani;
                $q_kebun = $this->Model->get_data('', 'kebun', null, array('id_petani' => $id_petani), '', 'id_kebun desc');
                
                // $token = base64_encode($cek->row()->id_user.'-'.date('Y/m/d H:i:s'));
                // $this->Model->update_token($token, $cek->row()->id_user);

                if($q_kebun->num_rows() > 0){
                    $kebun_data = array();
                    foreach($q_kebun->result() as $key){
                        $key->total_produk = $this->Model->get_data('id_produk', 'produk', null, array('id_kebun' => $key->id_kebun))->num_rows();
                        $key->kapasitas = number_format(($key->kapasitas));
                        $kebun_data[count($kebun_data)] = $key;
                    }
                    die(json_encode(array('success' => 1, 'kebun' => $kebun_data)));
                    // $message = [
                    //     'success' => 1,
                    //     //'token' => $token,
                    //     'kebun' => $q_kebun->result()
                    // ];
                    // $this->set_response($message, REST_Controller::HTTP_OK); // CREATED (201) being the HTTP response code
                }else{
                    die(json_encode(array('success' => 2, 'msg' => 'Kebun Belum Ada')));
                    // $message = [
                    //     'success' => 2,
                    //     //'token' => $token,
                    //     'msg' => 'Kebun Belum Ada'
                    // ];
                    // $this->set_response($message, REST_Controller::HTTP_OK); // CREATED (201) being the HTTP response code 
                }
            }else{
                die(json_encode(array('success' => 4, 'msg' => 'Invalid Request')));
                // $message = ['success' => 0, 'msg' => 'Invalid Token'];
            
                // $this->set_response($message, REST_Controller::HTTP_BAD_REQUEST);
            }
        }
    }

    public function delete_kebun_post()
    {
        header('Access-Control-Allow-Origin: http://localhost:3000');
        //header('Access-Control-Allow-Origin: https://tabunganasa.com');
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Methods: POST');

        $id_kebun = $this->post('id_kebun');
        //$token = $this->post('token');

        if($id_kebun == ''){// || $token == ''){
            die(json_encode(array('success' => 0, 'msg' => 'Invalid Request')));
            // $message = ['success' => 0, 'msg' => 'Invalid Request'];
            
            // $this->set_response($message, REST_Controller::HTTP_BAD_REQUEST);
        }else{
            $join = array(
                0 => 'u_petani up-up.id_petani=k.id_petani',
                1 => 'user u-u.id_user=up.id_user'
            );
            $cek = $this->Model->get_data('u.id_user', 'kebun k', $join, array('k.id_kebun' => $id_kebun));//, 'u.token' => $token));

            if($cek->num_rows() > 0){
                $where['id_kebun'] = $id_kebun;

                $this->Model->delete_data1('produk', $where);

                $this->Model->delete_data1('kebun', $where);

                die(json_encode(array('success' => 1)));
                
                // $token = base64_encode($cek->row()->id_user.'-'.date('Y/m/d H:i:s'));
                // $this->Model->update_token($token, $cek->row()->id_user);

                // $message = [
                //     'success' => '1'
                // ];
                // $this->set_response($message, REST_Controller::HTTP_OK); // CREATED (201) being the HTTP response code
            }else{
                die(json_encode(array('success' => 0, 'msg' => 'Invalid Token')));
                // $message = ['success' => 0, 'msg' => 'Invalid Token'];
            
                // $this->set_response($message, REST_Controller::HTTP_BAD_REQUEST);
            }
        }

        $where['id_kebun'] = $id_kebun;

        $this->Model->delete_data('kebun', $where);

        die(json_encode(array('success' => 1)));
        // $message = [
        //     'success' => '1'
        // ];
        // $this->set_response($message, REST_Controller::HTTP_OK); // CREATED (201) being the HTTP response code
    }

    public function edit_kebun_post()
    {   
        header('Access-Control-Allow-Origin: http://localhost:3000');
        //header('Access-Control-Allow-Origin: https://tabunganasa.com');
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Methods: POST');

        $id_kebun = $this->post('id_kebun');
        $alamat = $this->post('alamat');
        $luas = $this->post('luas');
        $kapasitas = $this->post('kapasitas');
        $status = $this->post('status');
        $satuan = $this->post('satuan');
        //$token = $this->post('token');

        if($id_kebun == ''){// || $token == ''){
            die(json_encode(array('success' => 0, 'msg' => 'Invalid Request')));
        }else{
            $luas = str_replace(',', '', $luas);
            $kapasitas = str_replace(',', '', $kapasitas);
            $join = array(
                0 => 'u_petani up-up.id_petani=k.id_petani',
                1 => 'user u-u.id_user=up.id_user'
            );
            $cek = $this->Model->get_data('u.id_user', 'kebun k', $join, array('k.id_kebun' => $id_kebun));

            if($cek->num_rows() > 0){
                $data = array(
                    'alamat' => $alamat,
                    'luas' => $luas,
                    'kapasitas' => $kapasitas,
                    'status' => $status,
                    'satuan' => $satuan
                );

                $where['id_kebun'] = $id_kebun;

                $this->Model->update_data('kebun', $data, $where);

                // $token = base64_encode($cek->row()->id_user.'-'.date('Y/m/d H:i:s'));
                // $this->Model->update_token($token, $cek->row()->id_user);

                die(json_encode(array('success' => 1)));
                // $message = [
                //     'success' => '1'
                // ];
                // $this->set_response($message, REST_Controller::HTTP_OK); // CREATED (201) being the HTTP response code
            }else{
                die(json_encode(array('success' => 0, 'msg' => 'Invalid Token')));
                // $message = ['success' => 0, 'msg' => 'Invalid Token'];
            
                // $this->set_response($message, REST_Controller::HTTP_BAD_REQUEST);
            }
        }
    }

    public function add_kebun_post()
    {   
        header('Access-Control-Allow-Origin: http://localhost:3000');
        //header('Access-Control-Allow-Origin: https://tabunganasa.com');
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Methods: POST');

        $username = $this->post('username');
        $alamat = $this->post('alamat');
        $luas = $this->post('luas');
        $kapasitas = $this->post('kapasitas');
        $status = $this->post('status');
        $satuan = $this->post('satuan');
        //$token = $this->post('token');

        if($username == ''){// || $token == ''){
            die(json_encode(array('success' => 0, 'Invalid Request')));
            // $message = ['success' => 0, 'msg' => 'Invalid Request'];
            
            // $this->set_response($message, REST_Controller::HTTP_BAD_REQUEST);
        }else{
            $cek = $this->Model->get_from_query("select up.* from user u left join u_petani up on up.id_user=u.id_user where (u.email='".$username."' or u.hp='".$username."')");

            if($cek->num_rows() > 0){
                $luas = str_replace(',', '', $luas);
                $kapasitas = str_replace(',', '', $kapasitas);
                $id_petani = $cek->row()->id_petani;
                $id_kebun = $this->Model->get_id('kebun');

                // $token = base64_encode($cek->row()->id_user.'-'.date('Y/m/d H:i:s'));
                // $this->Model->update_token($token, $cek->row()->id_user);

                $data = array(
                    'id_kebun' => $id_kebun,
                    'id_petani' => $id_petani,
                    'alamat' => $alamat,
                    'luas' => $luas,
                    'kapasitas' => $kapasitas,
                    'status' => $status,
                    'satuan' => $satuan
                );

                $this->Model->insert_data('kebun', $data);

                die(json_encode(array('success' => 1)));
                // $message = [
                //     'success' => '1'
                //     //'token' => $token
                // ];
                // $this->set_response($message, REST_Controller::HTTP_OK); // CREATED (201) being the HTTP response code
            }else{
                die(json_encode(array('success' => 0, 'msg' => 'Invalid Request')));
                // $message = ['success' => 0, 'msg' => 'Invalid Token'];
            
                // $this->set_response($message, REST_Controller::HTTP_BAD_REQUEST);
            }
        }
    }

    public function users_delete(){
        $id = (int) $this->get('id');

        // Validate the id.
        if ($id <= 0)
        {
            // Set the response and exit
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        // $this->some_model->delete_something($id);
        $message = [
            'id' => $id,
            'message' => 'Deleted the resource'
        ];

        $this->set_response($message, REST_Controller::HTTP_NO_CONTENT); // NO_CONTENT (204) being the HTTP response code
    }

}
