<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Bid extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
        $this->load->model('Model');
    }

    public function cc_get(){
        $m = $this->Model;
        $join0 = array(0 => 'u_petani up-up.id_user=u.id_user');
        
        $q0 = $m->get_data('up.*', 'user u', $join0, array('u.email' => 'cnacorp37@gmail.com'))->row()->id_petani;
        
        $q1 = $m->get_data('', 'kebun', null, array('id_petani' => $q0))->row()->id_kebun;
        
        $q2 = $m->get_data('', 'produk', null, array('id_kebun' => $q1))->row();
        ?>
        <pre>
            <?php print_r($q2);?>
        </pre>
        <?php
        
    }
    
    public function get_bid_post(){
      header('Access-Control-Allow-Origin: http://localhost:3000');
      //header('Access-Control-Allow-Origin: https://tabunganasa.com');
      header('Access-Control-Allow-Headers: *');
      header('Access-Control-Allow-Methods: POST');
    
        $username = $this->post('username');
        $status = $this->post('status');

      if($username == ''){// || $token == ''){
        die(json_encode(array('success' => 0, 'msg' => 'Invalid Request')));
        // $message = ['success' => 0, 'msg' => 'Invalid Request'];
        
        // $this->set_response($message, REST_Controller::HTTP_BAD_REQUEST);
      }else{
        $cek = $this->Model->get_from_query("select up.* from user u left join u_petani up on up.id_user=u.id_user where (u.email='".$username."' or u.hp='".$username."')");// and u.token='".$token."'");

        if($cek->num_rows() > 0){
          $join = array(
            0 => 'produk p-p.id_produk=bd.id_produk',
            1 => 'kebun k-k.id_kebun=p.id_kebun',
            2 => 'bid b-b.id_bid=bd.id_bid',
            3 => 'u_perusahaan uc-uc.id_perusahaan=b.id_perusahaan',
            4 => 'user u-u.id_user=uc.id_user'
          );
          //$where1[0] = "bd.status!='0' && bd.status!='6'";
          if($status == '1'){
            //pending
            $where1[0] = "bd.status='1'";
          }else if($status == '2'){
            //accepted
            $where1[0] = "bd.status='3' or bd.status='5' or bd.status='6'";
          }else if($status == '3'){
            //canceled
            $where1[0] = "bd.status='2' or bd.status='4'";
          }else{
            $where1[0] = "bd.status!='0' or bd.status!='6'";
          }
          
          $q_bid = $this->Model->get_data('bd.*, p.gambar, p.qty as tersisa, k.satuan', 'bid_detail bd', $join, array('u.status' => '1', 'k.id_petani' => $cek->row()->id_petani, 'b.status' => '0'), '', 'bd.tgl_edit desc', 0, 0, $where1);

          $pending = array();
          $cancel = array();
          $accept = array();

          foreach ($q_bid->result() as $key) {
            $key->total = number_format(($key->harga * $key->qty), 2);
            $key->harga1 = $key->harga;
            $key->harga = number_format($key->harga, 2);
            $key->tgl_edit = date('d M Y', strtotime($key->tgl_edit));
            $key->status_accept = '0';
            $key->satuan = $key->satuan == '0' ? 'Kg' : ($key->satuan == '1' ? 'Ton' : 'Janjang');

            if($key->status == '1'){
              $pending[count($pending)] = $key;
            }else if($key->status == '2' || $key->status == '4'){
              $key->keterangan = $key->status == '2' ? 'Pembeli Membatalkan Pesanan Ini' : 'Anda Membatalkan Pesanan Ini';
              $cancel[count($cancel)] = $key;
            }else if($key->status != '0'){
              $key->total1 = number_format(($key->harga1 * $key->qty1), 2);
              $key->harga11 = $key->harga1;
              $key->hargaa = number_format($key->harga1, 2);
              $key->keterangan = 'Menunggu Konfirmasi Pembeli';
              $accept[count($accept)] = $key;
            }
          }

          $join_accept0 = array(
            0 => 'produk p-p.id_produk=pod.id_produk',
            1 => 'kebun k-k.id_kebun=p.id_kebun',
            2 => 'delivery_order do-do.id_po=pod.id_purchase_order',
            3 => 'delivery_order_detail dod-dod.id_do=do.id_do and dod.id_produk=pod.id_produk',
            4 => 'pembayaran inv-inv.id_po=pod.id_purchase_order'
          );

          $where_accept = array(
            'k.id_petani' => $cek->row()->id_petani,
            'inv.is_child' => '0'
          );
          $q_accept = $this->Model->get_data('
            dod.*,inv.is_child, inv.status_bayar, p.gambar, pod.*, inv.tgl_expired, inv.tgl_expired_proses_do, k.satuan',
            'purchase_order_detail pod', $join_accept0, $where_accept, '', '', 0, 0, array(0 => "dod.status_pengiriman='1' or dod.status_pengiriman='0' or dod.status_pengiriman='2' or dod.status_pengiriman='3' or dod.id_do is null"));
          foreach ($q_accept->result() as $key) {
            $key->hargaa = number_format($key->harga, 2);
            $key->tgl_expired = date('d M Y (H:i:s)', strtotime($key->tgl_expired));
            $key->tgl_expired_proses_do = date('d M Y (H:i:s)', strtotime($key->tgl_expired_proses_do));
            $key->qty1 = $key->qty;
            $key->total1 = number_format(($key->harga * $key->qty), 2);
            $key->satuan = $key->satuan == '0' ? 'Kg' : ($key->satuan == '1' ? 'Ton' : 'Janjang');
              
            if($key->id_do == null){
              if($key->status_bayar == '2'){
                  $key->status_accept = '2';
              }else if($key->status_bayar == '0'){
                $key->status_accept = '1';
              }
              
              //cek status_bayar
            }else{
              if($key->status_pengiriman == '3'){
                $key->status_accept = '4';
              }else{

                $key->status_accept = '3';
              }
              //cek status_pengiriman
            }

            $accept[count($accept)] = $key;
          }

          $join_accept = array(
            0 => 'purchase_order po-po.id_po=pod.id_purchase_order',
            1 => 'produk p-p.id_produk=pod.id_produk',
            2 => 'kebun k-k.id_kebun=p.id_kebun',
            3 => 'delivery_order do-do.id_po=po.id_po',
            4 => 'delivery_order_detail dod-dod.id_do=do.id_do and dod.id_produk=pod.id_produk',
            5 => 'pembayaran inv-inv.id_po=po.id_po'
          );

          // $where_accept = array(
          //   'k.id_petani' => $cek->row()->id_petani
          // );
          // $q_accept = $this->Model->get_data('
          //   do.id_do, inv.status_bayar, p.gambar, pod.*, inv.tgl_expired, inv.tgl_expired_proses_do',
          //   'purchase_order_detail pod', $join_accept, $where_accept, '', '', 0, 0, array(0 => 'dod.status_pengiriman < 2 or dod.status_pengiriman is null'));
          // foreach ($q_accept->result() as $key) {
          //   $key->hargaa = number_format($key->harga, 2);
          //   $key->tgl_expired = date('d M Y (H:i:s)', strtotime($key->tgl_expired));
          //   $key->tgl_expired_proses_do = date('d M Y (H:i:s)', strtotime($key->tgl_expired_proses_do));
          //   $key->qty1 = $key->qty;
          //   $key->total1 = number_format(($key->harga * $key->qty), 2);
              
          //   if($key->id_do == null){
          //     if($key->status_bayar == '2'){
          //         $key->status_accept = '2';
          //     }else if($key->status_bayar == '0'){
          //       $key->status_accept = '1';
          //     }
              
          //     //cek status_bayar
          //   }else{
          //       $key->status_accept = '3';
          //     //cek status_pengiriman
          //   }

          //   $accept[count($accept)] = $key;
          // }
          
          $where_accept['dod.status_pengiriman'] = '2';
          $where_accept['inv.status_bayar'] = '0';
          $q_accept1 = $this->Model->get_data('
            do.id_do, inv.status_bayar, p.gambar, pod.*, po.tgl_add, k.satuan',
            'purchase_order_detail pod', $join_accept, $where_accept, 'do.id_do, pod.id_produk');
            
          foreach ($q_accept1->result() as $key) {
            $key->hargaa = number_format($key->harga, 2);
            $key->tgl_add = date('d M Y', strtotime($key->tgl_add));
            $key->qty1 = $key->qty;
            $key->total1 = number_format(($key->harga * $key->qty), 2);
            $key->satuan = $key->satuan == '0' ? 'Kg' : ($key->satuan == '1' ? 'Ton' : 'Janjang');
              
            $key->status_accept = '5';

            $accept[count($accept)] = $key;
          }
            
            
          die(json_encode(array(
            'success' => 1,
            'pending' => $pending,
            'cancel' => $cancel,
            'accept' => $accept,
            'asd' => $q_accept->num_rows()
          )));

          // $message = [
          //   'success' => 1,
          //   'pending' => $pending,
          //   'cancel' => $cancel,
          //   'accept' => $accept
          // ];
          // $this->set_response($message, REST_Controller::HTTP_OK); // CREATED (201) being the HTTP response code   
        }else{
          die(json_encode(array('success' => 0, 'msg' => 'Invalid Token')));
          // $message = ['success' => 0, 'msg' => 'Invalid Token'];
            
          // $this->set_response($message, REST_Controller::HTTP_BAD_REQUEST);
        }
      }
    }

    public function accept_bid_post(){
      header('Access-Control-Allow-Origin: http://localhost:3000');
      //header('Access-Control-Allow-Origin: https://tabunganasa.com');
      header('Access-Control-Allow-Headers: *');
      header('Access-Control-Allow-Methods: POST');

      $id_bid = $this->post('id_bid');
      $id_produk = $this->post('id_produk');
      $qty = $this->post('qty');
      $harga = $this->post('harga');

      $cek_qty_harga = $this->Model->get_data('qty, harga', 'bid_detail', null, array('id_bid' => $id_bid, 'id_produk' => $id_produk));

      if($cek_qty_harga->num_rows() > 0){
        if($qty != $cek_qty_harga->row()->qty || $harga != $cek_qty_harga->row()->harga){
          $data = array(
            'status' => '5'
          );
        }else{
          $data = array(
            'status' => '3'
          );
        }
        $data['tgl_edit'] = date('Y/m/d H:i:s');
        $data['qty1'] = $qty;
        $data['harga1'] = $harga;

        $where = array('id_produk' => $id_produk, 'id_bid' => $id_bid);
        $this->Model->update_data('bid_detail', $data, $where);

        die(json_encode(array('success' => 1)));
        // $message = ['success' => 1];
            
        // $this->set_response($message, REST_Controller::HTTP_OK); // CREATED (201) being the HTTP response code   
      }else{
        die(json_encode(array('success' => 0, 'msg' => 'Invalid Request')));
        // $message = ['success' => 0, 'msg' => 'Invalid Request'];
          
        // $this->set_response($message, REST_Controller::HTTP_BAD_REQUEST);
      }
    }

    public function cancel_bid_post(){
      header('Access-Control-Allow-Origin: http://localhost:3000');
      //header('Access-Control-Allow-Origin: https://tabunganasa.com');
      header('Access-Control-Allow-Headers: *');
      header('Access-Control-Allow-Methods: POST');

      $id_bid = $this->post('id_bid');
      $id_produk = $this->post('id_produk');

      $data['status'] = '4';
      $where = array('id_bid' => $id_bid, 'id_produk' => $id_produk);
      $this->Model->update_data('bid_detail', $data, $where);
      die(json_encode(array('success' => 1)));
    }
}
