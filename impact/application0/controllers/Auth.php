<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Auth extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
        $this->load->model('Model');
    }

    public function login_get()
    {
        // Users from a data store e.g. database
        $users = [
            ['id' => 1, 'name' => 'John', 'email' => 'john@example.com', 'fact' => 'Loves coding'],
            ['id' => 2, 'name' => 'Jim', 'email' => 'jim@example.com', 'fact' => 'Developed on CodeIgniter'],
            ['id' => 3, 'name' => 'Jane', 'email' => 'jane@example.com', 'fact' => 'Lives in the USA', ['hobbies' => ['guitar', 'cycling']]],
        ];

        $id = $this->get('id');

        // If the id parameter doesn't exist return all the users

        if ($id === NULL)
        {
            // Check if the users data store contains users (in case the database result returns NULL)
            if ($users)
            {
                // Set the response and exit
                $this->response($users, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }
            else
            {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'No users were found'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }

        // Find and return a single record for a particular user.
        else {
            $id = (int) $id;

            // Validate the id.
            if ($id <= 0)
            {
                // Invalid id, set the response and exit.
                $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
            }

            // Get the user from the array, using the id as key for retrieval.
            // Usually a model is to be used for this.

            $user = NULL;

            if (!empty($users))
            {
                foreach ($users as $key => $value)
                {
                    if (isset($value['id']) && $value['id'] === $id)
                    {
                        $user = $value;
                    }
                }
            }

            if (!empty($user))
            {
                $this->set_response($user, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }
            else
            {
                $this->set_response([
                    'status' => FALSE,
                    'message' => 'User could not be found'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }
    }
    
    public function edit_profile_post()
    {
        header('Access-Control-Allow-Origin: http://localhost:8081');
        //header('Access-Control-Allow-Origin: https://tabunganasa.com');
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Methods: POST');
        
        //die(json_encode(array('asd' => 'ccd')));

        $username = $this->post('username');
        $email = $this->post('email');
        $hp = $this->post('hp');
        $nama = $this->post('nama');

        if($username == ''){
            // $message = ['success' => 0];
            
            // $this->set_response($message, REST_Controller::HTTP_BAD_REQUEST);
            die(json_encode(array('success' => '0', 'msg' => 'Invalid Request')));
        }else if($email == '' && $hp == ''){
            die(json_encode(array('success' => '0', 'msg' => 'Email atau No.Hp tidak boleh kosong')));
        }else{
            $join = array(
                0 => 'u_petani up-up.id_user=u.id_user'
            );

            $where = array(
                'u.role' => '3',
                'u.status' => '1'
            );
            $cek = $this->Model->get_data('', 'user u', $join, $where, '', '', 0, 0, array(0 => "u.email='".$username."' or u.hp='".$username."'"));

            $sess_username = $username;
            if($cek->num_rows() > 0){
                $r = $cek->row();
                if($username == $r->email){
                    if($email != '' && $username != $email){
                        $sess_username = $email;
                    }
                }else if($username == $r->hp){
                    if($hp != '' && $username != $hp){
                        $sess_username = $hp;
                    }
                }else{
                    die(json_encode(array('success' => 0, 'msg' => 'Invalid Result')));
                }
                if($email != ''){
                    $data['email'] = $email;
                }
                if($hp != ''){
                    $data['hp'] = $hp;
                }

                $where['id_user'] = $r->id_user;
                $this->Model->update_data('user', $data, $where);

                if($nama != ''){
                    $data1['nama'] = $nama;
                    $this->Model->update_data('u_petani', $data1, $where);
                }

                die(json_encode(array('success' => 1, 'username' => $sess_username)));
            }else{
                die(json_encode(array('success' => 0, 'msg' => 'Invalid Result')));
            }
        }
    }
    
    public function profile_post()
    {
        
        header('Access-Control-Allow-Origin: http://localhost:8081');
        //header('Access-Control-Allow-Origin: https://tabunganasa.com');
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Methods: POST');
        
        //die(json_encode(array('asd' => 'ccd')));

        $username = $this->post('username');

        if($username == ''){
            // $message = ['success' => 0];
            
            // $this->set_response($message, REST_Controller::HTTP_BAD_REQUEST);
            die(json_encode(array('success' => '0', 'msg' => 'Invalid Request')));
        }else{
            $join = array(
                0 => 'u_petani up-up.id_user=u.id_user'
            );

            $where = array(
                'u.role' => '3',
                'u.status' => '1'
            );
            $cek = $this->Model->get_data('', 'user u', $join, $where, '', '', 0, 0, array(0 => "u.email='".$username."' or u.hp='".$username."'"));

            if($cek->num_rows() > 0){
                $r = $cek->row();
                $data = array(
                    'email' => $r->email,
                    'status' => $r->status == '0' ? 'Tidak Aktif' : 'Aktif',
                    'hp' => $r->hp,
                    'id_petani' => $r->id_petani,
                    'nama' => $r->nama
                );

                $join_po = array(
                    0 => 'delivery_order do-do.id_do=dod.id_do',
                    1 => 'produk pr-pr.id_produk=dod.id_produk',
                    2 => 'kebun k-k.id_kebun=pr.id_kebun',
                    3 => 'purchase_order_detail pod-pod.id_purchase_order=do.id_po and pod.id_produk=dod.id_produk'
                );

                $where_po = array(
                    'k.id_petani' => $cek->row()->id_petani,
                    'dod.status_pengiriman' => '4'
                );
                $q_po_history = $this->Model->get_data('do.id_po, dod.id_produk, dod.qty_realisasi, dod.ket_company, dod.tgl_delivery,
                pod.harga, pod.qty, pod.harga_delivery, pod.jarak, k.satuan, dod.id',
                'delivery_order_detail dod', $join_po, $where_po, '', 'dod.tgl_delivery desc', 4);

                $data_po = array();

                foreach($q_po_history->result() as $key){
                    $data_po[count($data_po)] = array(
                        'id_po' => $key->id_po,
                        'harga' => number_format($key->harga, 2),
                        'qty' => number_format($key->qty),
                        'harga_delivery' => number_format($key->harga_delivery, 2),
                        'jarak' => $key->jarak,
                        'satuan' => ($key->satuan == '0' ? 'Kg' : ($key->satuan == '1' ? 'Ton' : 'Janjang')),
                        'id_produk' => $key->id_produk,
                        'qty_realisasi' => number_format($key->qty_realisasi),
                        'ket_company' => $key->ket_company,
                        'tgl_delivery' => date('d M Y', strtotime($key->tgl_delivery)),
                        'id' => $key->id
                    );
                }

                // $data_po1 = $data_po;
                // for($a=0; $a<count($data_po1); $a++){
                //     $data_po[count($data_po)] = $data_po1[$a];
                // }

                // $data_po1 = $data_po;
                // for($a=0; $a<count($data_po1); $a++){
                //     $data_po[count($data_po)] = $data_po1[$a];
                // }
                die(json_encode(array('success' => 1, 'profile' => $data, 'po' => $data_po)));
            }else{
                die(json_encode(array('success' => 0, 'msg' => 'Invalid Result')));
            }
        }
        
    }

    public function test_post(){
        $username = $this->post('username');
        $message = ['success' => 0, 'username' => $username];
            
        $this->set_response($message, REST_Controller::HTTP_BAD_REQUEST);
    }
    
    public function login_post()
    {
        
        header('Access-Control-Allow-Origin: http://localhost:8081');
        //header('Access-Control-Allow-Origin: https://tabunganasa.com');
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Methods: POST');
        
        //die(json_encode(array('asd' => 'ccd')));

        $username = $this->post('username');
        $password = md5($this->post('password'));

        if($username == '' || $password == ''){
            // $message = ['success' => 0];
            
            // $this->set_response($message, REST_Controller::HTTP_BAD_REQUEST);
            die(json_encode(array('success' => '0', 'msg' => 'Invalid Request')));
        }else{
            $cek = $this->Model->get_from_query("select u.id_user, u.email, u.status, p.nama, p.id_petani
                from user u
                left join u_petani p on p.id_user=u.id_user
                where (u.email='".$username."' or u.hp='".$username."') and u.password='".$password."' and u.role='3'
            ");
            if($cek->num_rows() > 0){
                $cek->row()->username = $username;
                // $token = base64_encode($cek->row()->id_user.'-'.date('Y/m/d H:i:s'));

                // $data['token'] = $token;
                // $where['id_user'] = $cek->row()->id_user;
                // $this->Model->update_data('user', $data, $where);

                // $message = [
                //     'success' => '1',
                //     //'token' => $token,
                //     'data_auth' => $cek->row()
                // ];
                // $this->set_response($message, REST_Controller::HTTP_OK); // CREATED (201) being the HTTP response code
                die(json_encode(array('success' => '1', 'data_auth' => $cek->row())));
            }else{
                // $message = [
                //     'success' => '0',
                //     'msg' => 'Username atau password salah'
                // ];
                // $this->set_response($message, REST_Controller::HTTP_OK); // CREATED (201) being the HTTP response code
                die(json_encode(array('success' => '0', 'msg' => 'Username atau password salah')));
            }    
        }
        
    }

    public function register_post()
    {
        
        header('Access-Control-Allow-Origin: http://localhost:3000');
        //header('Access-Control-Allow-Origin: https://tabunganasa.com');
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Methods: POST');

        $email = $this->post('email');
        $password = md5($this->post('password'));
        $nama = $this->post('nama');
        $telp = $this->post('telp');
        $id_kategori = 'k_01';

        if($email == '' || $password == '' || $nama == '' || $telp == ''){
            die(json_encode(array('success' => 0, 'msg' => 'Invalid Request')));
            // $message = ['success' => 0];
            
            // $this->set_response($message, REST_Controller::HTTP_BAD_REQUEST);
        }else{

            $cek_email = $this->Model->get_from_query("select email from user where email='".$email."'")->num_rows();
            if($cek_email > 0){
                die(json_encode(array('success' => 0, 'msg' => 'E-mail sudah terdaftar')));
                // $message = [
                //     'success' => '0',
                //     'msg' => 'E-mail sudah terdaftar'
                // ];
                // $this->set_response($message, REST_Controller::HTTP_OK); // CREATED (201) being the HTTP response code
            }else{
                $id_user = $this->Model->get_id('user');
                $id_petani = $this->Model->get_id('u_petani');

                $data_user = array(
                    'id_user' => $id_user,
                    'password' => $password,
                    'email' => $email,
                    'role' => '3',
                    'status' => '1',
                    'hp' => $telp
                );

                $this->Model->insert_data('user', $data_user);

                $data_petani = array(
                    'id_petani' => $id_petani,
                    'nama' => $nama,
                    'id_user' => $id_user,
                    'id_kategori' => $id_kategori
                    
                );

                $this->Model->insert_data('u_petani', $data_petani);
                $data_auth_ = array(
                    'id_user' => $id_user,
                    'id_petani' => $id_petani,
                    'username' => ($email == '' ? $telp : $email),
                    'nama' => $nama
                );
                die(json_encode(array('success' => 1, 'data_auth' => $data_auth_)));
                // $message = [
                //     'success' => '1'
                // ];
                // $this->set_response($message, REST_Controller::HTTP_OK); // CREATED (201) being the HTTP response code
            }
        }
        
    }

    public function users_delete(){
        $id = (int) $this->get('id');

        // Validate the id.
        if ($id <= 0)
        {
            // Set the response and exit
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        // $this->some_model->delete_something($id);
        $message = [
            'id' => $id,
            'message' => 'Deleted the resource'
        ];

        $this->set_response($message, REST_Controller::HTTP_NO_CONTENT); // NO_CONTENT (204) being the HTTP response code
    }

}
