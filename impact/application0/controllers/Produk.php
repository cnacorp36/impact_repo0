<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Produk extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
        $this->load->model('Model');
    }

    public function edit_produk_post(){
      header('Access-Control-Allow-Origin: http://localhost:3000');
      //header('Access-Control-Allow-Origin: https://tabunganasa.com');
      header('Access-Control-Allow-Headers: *');
      header('Access-Control-Allow-Methods: POST');

      $gambar = $this->post('gambar');
      $qty = $this->post('qty');
      $status = $this->post('status');
      $id_produk = $this->post('id_produk');
      $paramChangeGambar = $this->post('paramChangeGambar');
      
      $qty = str_replace(',', '', $qty);

      if($paramChangeGambar == '1'){
        $d = DIRECTORY_SEPARATOR;
        $file = $id_produk.date('Y-m-d H_i_s').'.png';
        file_put_contents(getcwd().$d.'assets'.$d.'images'.$d.'produk'.$d.$file, base64_decode($gambar));  
      }

      $data_produk = array(
        'qty' => $qty,
        'status' => $status,
        'tgl_edit' => date('Y-m-d H:i:s')
      );

      if($paramChangeGambar == '1'){
        $data_produk['gambar'] = 'http://impact1.tabunganasa.com/impact/assets/images/produk/'.$file;
      }

      $where['id_produk'] = $id_produk;
      $this->Model->update_data('produk', $data_produk, $where);

      die(json_encode(array('success' => 1)));
    }
    
    public function upload_produk_post(){
        header('Access-Control-Allow-Origin: http://localhost:3000');
        //header('Access-Control-Allow-Origin: https://tabunganasa.com');
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Methods: POST');

        $gambar = $this->post('gambar');
        $qty = $this->post('qty');
        $id_kebun = $this->post('id_kebun');
        $status = $this->post('status');
        $id_produk = $this->Model->get_id('produk');

        $d = DIRECTORY_SEPARATOR;
        $file = $id_produk.date('Y-m-d H_i_s').'.png';
        file_put_contents(getcwd().$d.'assets'.$d.'images'.$d.'produk'.$d.$file, base64_decode($gambar));  

        $data_produk = array(
          'id_produk' => $id_produk,
          'gambar' => 'http://impact1.tabunganasa.com/impact/assets/images/produk/'.$file,
          'qty' => $qty,
          'id_kebun' => $id_kebun,
          'status' => $status,
          'tgl_add' => date('Y-m-d H:i:s'),
          'tgl_edit' => date('Y-m-d H:i:s')
        );
        $this->Model->insert_data('produk', $data_produk);

        die(json_encode(array('success' => 1)));

        // $message = [
        //   'success' => '1'
        // ];
        // $this->set_response($message, REST_Controller::HTTP_OK); // CREATED (201) being the HTTP response code   
    }

    public function delete_produk_post()
    {
      header('Access-Control-Allow-Origin: http://localhost:3000');
      //header('Access-Control-Allow-Origin: https://tabunganasa.com');
      header('Access-Control-Allow-Headers: *');
      header('Access-Control-Allow-Methods: POST');

      $id_produk = $this->post('id_produk');

      if($id_produk != ''){
        $data_delete['id_produk'] = $id_produk;

        $this->Model->delete_data1('produk', $data_delete);

        die(json_encode(array('success' => 1)));
      }else{
        die(json_encode(array('success' => 0, 'msg' => 'Invalid Request')));
      }
    }

    public function get_all_my_produk_post()
    {
        
      header('Access-Control-Allow-Origin: http://localhost:3000');
      //header('Access-Control-Allow-Origin: https://tabunganasa.com');
      header('Access-Control-Allow-Headers: *');
      header('Access-Control-Allow-Methods: POST');
      
      $username = $this->post('username');
      $id_kebun = $this->post('id_kebun');
      //$token = $this->post('token');

      if($id_kebun != ''){
        $q_produk = $this->Model->get_data('', 'produk', null, array('id_kebun' => $id_kebun), '', 'id_produk desc');
        $data_produk = array();

        $id = 1;
        foreach ($q_produk->result() as $key) {
          $key->id = $id;
          $key->qty = number_format($key->qty);
          $key->tgl_edit = date('d M Y', strtotime($key->tgl_edit));
          $data_produk[count($data_produk)] = $key;
          $id++;
        }

        die(json_encode(array('success' => 1, 'produk' => $data_produk)));
      }else{
        if($username == ''){// || $token == ''){
          die(json_encode(array('success' => 0, 'msg' => 'Invalid Request')));
          // $message = ['success' => 0, 'msg' => 'Invalid Request'];
          
          // $this->set_response($message, REST_Controller::HTTP_BAD_REQUEST);
        }else{
          $join = array(
            0 => 'kebun k-k.id_kebun=p.id_kebun',
            1 => 'u_petani up-up.id_petani=k.id_petani',
            2 => 'user u-u.id_user=up.id_user'
          );
          $where[0] = "u.email='".$username."' or u.hp='".$username."'";

          $cek = $this->Model->get_data('p.*, k.id_kebun', 'produk p', $join, null, '', 'k.id_kebun asc, p.id_produk desc', 0, 0, $where);

          if($cek->num_rows() > 0){
            
            die(json_encode(array('success' => 1, 'produk' => $cek->result())));
            // $message = [
            //     'success' => '1',
            //     'produk' => $cek->result()
            // ];
            // $this->set_response($message, REST_Controller::HTTP_OK); // CREATED (201) being the HTTP response code
          }else{
            die(json_encode(array('success' => 2, 'msg' => 'Produk Tidak Ada')));
            // $message = ['success' => 2, 'msg' => 'Produk Tidak Ada'];
        
            // $this->set_response($message, REST_Controller::HTTP_OK);
          }
        }
      }
    }
}
