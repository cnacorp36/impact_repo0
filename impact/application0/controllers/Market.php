<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Market extends REST_Controller {

  function __construct()
  {
    // Construct the parent class
    parent::__construct();

    // Configure limits on our controller methods
    // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
    $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
    $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
    $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
    $this->load->model('Model');
  }

  public function get_produk_post()
  {
      
    header('Access-Control-Allow-Origin: http://localhost:3000');
    //header('Access-Control-Allow-Origin: https://tabunganasa.com');
    header('Access-Control-Allow-Headers: *');
    header('Access-Control-Allow-Methods: POST');

    $order = $this->post('order_by');
    $limit = $this->post('limit');
    $start = $this->post('start');
    $username = $this->post('username');

    if($username == ''){// || $token == ''){
      die(json_encode(array('success' => 0, 'msg' => 'Invalid Request')));
      // $message = ['success' => 0, 'msg' => 'Invalid Request'];
      
      // $this->set_response($message, REST_Controller::HTTP_BAD_REQUEST);
    }else{
      $cek = $this->Model->get_from_query("select up.* from user u left join u_petani up on up.id_user=u.id_user where (u.email='".$username."' or u.hp='".$username."')");// and u.token='".$token."'");

      if($cek->num_rows() > 0){
        //produk
        $join = array(
          0 => 'kebun k-k.id_kebun=p.id_kebun',
          1 => 'u_petani up-up.id_petani=k.id_petani',
          2 => 'user u-u.id_user=up.id_user'
        );
        $q_produk = $this->Model->get_data('p.*, k.alamat, up.nama as nama_petani', 'produk p', $join, array('p.status' => '1', 'k.status' => '1', 'u.status' => '1'), '', $order, $limit, $start);

        $count_produk = $this->Model->get_data('', 'produk p', $join, array('p.status' => '1', 'k.status' => '1', 'u.status' => '1'))->num_rows();

        $data_produk = array();
        $id = 1;
        foreach ($q_produk->result() as $key) {
          $key->id = $id;
          $data_produk[count($data_produk)] = $key;
          $id++;
        }
        //draft

        die(json_encode(array('success' => 1, 'produk' => $data_produk, 'total_produk_on_market' => $count_produk)));
        // $message = [
        //   'success' => 1,
        //   'produk' => $q_produk->result(),
        //   'total_produk_on_market' => $count_produk
        // ];
        // $this->set_response($message, REST_Controller::HTTP_OK); // CREATED (201) being the HTTP response code
      }else{
        die(json_encode(array('success' => 0, 'msg' => 'Invalid Token')));
        // $message = ['success' => 0, 'msg' => 'Invalid Token'];
            
        // $this->set_response($message, REST_Controller::HTTP_BAD_REQUEST);
      }
    }
  }
}
