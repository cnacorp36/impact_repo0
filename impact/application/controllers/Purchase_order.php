<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Purchase_order extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
        $this->load->model('Model');
    }

    public function get_po_post(){

      $username = $this->post('username');

      if($username == ''){// || $token == ''){
        die(json_encode(array('success' => 0, 'msg' => 'Invalid Request')));
        // $message = ['success' => 0, 'msg' => 'Invalid Request'];
        
        // $this->set_response($message, REST_Controller::HTTP_BAD_REQUEST);
      }else{
        $cek = $this->Model->get_from_query("select up.* from user u left join u_petani up on up.id_user=u.id_user where (u.email='".$username."' or u.hp='".$username."')");// and u.token='".$token."'");

        if($cek->num_rows() > 0){
          $join = array(
            0 => 'produk p-p.id_produk=pod.id_produk',
            1 => 'kebun k-k.id_kebun=p.id_kebun',
            2 => 'pembayaran pay-pay.id_po=pod.id_purchase_order',
            3 => 'purchase_order po-po.id_po=pod.id_purchase_order',
            4 => 'u_perusahaan up=up.id_perusahaan=po.id_perusahaan',
            5 => 'delivery_order do-do.id_po=po.id_po'
          );
          $q_po = $this->Model->get_data('pod.*, ', 'purchase_order_detail pod', $join, array('k.id_petani' => $cek->row()->id_petani, 'do.id_do' => ''), '', 'pod.id_purchase_order desc, pod.id_produk desc');
          
          die(json_encode(array('success' => 1, 'pending' => $pending, 'cancel' => $cancel, 'accept' => $accept)));
          // $message = [
          //   'success' => 1,
          //   'pending' => $pending,
          //   'cancel' => $cancel,
          //   'accept' => $accept
          // ];
          // $this->set_response($message, REST_Controller::HTTP_OK); // CREATED (201) being the HTTP response code   
        }else{
          die(json_encode(array('success' => 0, 'msg' => 'Invalid Token')));
          // $message = ['success' => 0, 'msg' => 'Invalid Token'];
            
          // $this->set_response($message, REST_Controller::HTTP_BAD_REQUEST);
        }
      }
    }

    public function accept_bid_post(){
      header('Access-Control-Allow-Origin: http://localhost:3000');
      //header('Access-Control-Allow-Origin: https://tabunganasa.com');
      header('Access-Control-Allow-Headers: *');
      header('Access-Control-Allow-Methods: POST');

      $id_bid = $this->post('id_bid');
      $id_produk = $this->post('id_kebun');
      $qty = $this->post('qty');
      $harga = $this->post('harga');

      $cek_qty_harga = $this->Model->get_data('qty, harga', 'bid_detail', null, array('id_bid' => $id_bid, 'id_produk' => $id_produk));

      if($cek_qty_harga->num_rows() > 0){
        if($qty != $cek_qty_harga->row()->qty || $harga != $cek_qty_harga->row()->harga){
          $data = array(
            'status' => '5'
          );
        }else{
          $data = array(
            'status' => '3'
          );
        }
        $data['tgl_edit'] = date('Y/m/d H:i:s');
        $data['qty1'] = $qty;
        $data['harga1'] = $harga;

        $where = array('id_produk' => $id_produk, 'id_bid' => $id_bid);
        $this->Model->update_data('bid_detail', $data, $where);

        die(json_encode(array('success' => 1)));
        // $message = ['success' => 1];
            
        // $this->set_response($message, REST_Controller::HTTP_OK); // CREATED (201) being the HTTP response code   
      }else{
        die(json_encode(array('success' => 0, 'msg' => 'Invalid Request')));
        // $message = ['success' => 0, 'msg' => 'Invalid Request'];
          
        // $this->set_response($message, REST_Controller::HTTP_BAD_REQUEST);
      }
    }

    public function cancel_bid_post(){
      header('Access-Control-Allow-Origin: http://localhost:3000');
      //header('Access-Control-Allow-Origin: https://tabunganasa.com');
      header('Access-Control-Allow-Headers: *');
      header('Access-Control-Allow-Methods: POST');

      $id_bid = $this->post('id_bid');
      $id_produk = $this->post('id_kebun');

      $this->Model->update_data('bid_detail', array('status' => '2'), array('id_bid' => $id_bid, 'id_produk' => $id_produk));
      die(json_encode(array('success' => 1)));
      // $message = ['success' => 1];
          
      // $this->set_response($message, REST_Controller::HTTP_OK); // CREATED (201) being the HTTP response code   
    }
}
