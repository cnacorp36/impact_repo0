<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Auth extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
        $this->load->model('Model');
    }

    public function login_get()
    {
        // Users from a data store e.g. database
        $users = [
            ['id' => 1, 'name' => 'John', 'email' => 'john@example.com', 'fact' => 'Loves coding'],
            ['id' => 2, 'name' => 'Jim', 'email' => 'jim@example.com', 'fact' => 'Developed on CodeIgniter'],
            ['id' => 3, 'name' => 'Jane', 'email' => 'jane@example.com', 'fact' => 'Lives in the USA', ['hobbies' => ['guitar', 'cycling']]],
        ];

        $id = $this->get('id');

        // If the id parameter doesn't exist return all the users

        if ($id === NULL)
        {
            // Check if the users data store contains users (in case the database result returns NULL)
            if ($users)
            {
                // Set the response and exit
                $this->response($users, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }
            else
            {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'No users were found'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }

        // Find and return a single record for a particular user.
        else {
            $id = (int) $id;

            // Validate the id.
            if ($id <= 0)
            {
                // Invalid id, set the response and exit.
                $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
            }

            // Get the user from the array, using the id as key for retrieval.
            // Usually a model is to be used for this.

            $user = NULL;

            if (!empty($users))
            {
                foreach ($users as $key => $value)
                {
                    if (isset($value['id']) && $value['id'] === $id)
                    {
                        $user = $value;
                    }
                }
            }

            if (!empty($user))
            {
                $this->set_response($user, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }
            else
            {
                $this->set_response([
                    'status' => FALSE,
                    'message' => 'User could not be found'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }
    }
    
    public function edit_password_post()
    {
        header('Access-Control-Allow-Origin: http://localhost:8081');
        //header('Access-Control-Allow-Origin: https://tabunganasa.com');
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Methods: POST');

        $username = $this->post('username');
        $password_lama = $this->post('password_lama');
        $password_baru = $this->post('password_baru');
        $password_baru1 = $this->post('password_baru1');

        if($password_baru1 != $password_baru){
            die(json_encode(array('success' => 0, 'msg' => 'Password Baru Tidak Sama')));
        }

        if($username == ''){
            die(json_encode(array('success' => '0', 'msg' => 'Invalid Request')));
        }

        $join = array(
            0 => 'u_petani up-up.id_user=u.id_user'
        );

        $where = array(
            'u.role' => '3',
            'u.status' => '1'
        );
        $cek = $this->Model->get_data('*, u.id_user as u_id', 'user u', $join, $where, '', '', 0, 0, array(0 => "u.email='".$username."' or u.hp='".$username."'"));

        if($cek->num_rows() == 0){
            die(json_encode(array('success' => '0', 'msg' => 'Invalid Request')));
        }

        $r = $cek->row();

        $cek_pass_lama = $this->Model->get_data('', 'user', null, array('id_user' => $r->u_id, 'password' => md5($password_lama)));

        if($cek_pass_lama->num_rows() == 0){
            die(json_encode(array('success' => 0, 'msg' => 'Password Lama Salah')));
        }

        $this->Model->update_data('user', array('password' => md5($password_baru)), array('id_user' => $r->u_id));

        die(json_encode(array('success' => 1)));
    }

    public function edit_profile_post()
    {
        header('Access-Control-Allow-Origin: http://localhost:8081');
        //header('Access-Control-Allow-Origin: https://tabunganasa.com');
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Methods: POST');
        
        //die(json_encode(array('asd' => 'ccd')));

        $username = $this->post('username');
        $email = $this->post('email');
        $hp = $this->post('hp');
        $nama = $this->post('nama');

        if($username == ''){
            // $message = ['success' => 0];
            
            // $this->set_response($message, REST_Controller::HTTP_BAD_REQUEST);
            die(json_encode(array('success' => '0', 'msg' => 'Invalid Request')));
        }else if($email == '' && $hp == ''){
            die(json_encode(array('success' => '0', 'msg' => 'Email atau No.Hp tidak boleh kosong')));
        }else{
            $join = array(
                0 => 'u_petani up-up.id_user=u.id_user'
            );

            $where = array(
                'u.role' => '3',
                'u.status' => '1'
            );
            $cek = $this->Model->get_data('*, u.id_user as u_id', 'user u', $join, $where, '', '', 0, 0, array(0 => "u.email='".$username."' or u.hp='".$username."'"));

            $sess_username = $username;
            if($cek->num_rows() > 0){
                $r = $cek->row();

                $cek_email_edit = $this->Model->get_data('', 'user', null, array('email' => $email, 'id_user !=' => $r->u_id));
                if($cek_email_edit->num_rows() > 0){
                    die(json_encode(array('success' => 0, 'msg' => 'Email sudah digunakan')));
                }
                if($username == $r->email){
                    if($email != '' && $username != $email){
                        $sess_username = $email;
                    }
                }else if($username == $r->hp){
                    if($hp != '' && $username != $hp){
                        $sess_username = $hp;
                    }
                }else{
                    die(json_encode(array('success' => 0, 'msg' => 'Invalid Request')));
                }
                if($hp != ''){
                    $data['hp'] = $hp;
                    $username = $hp;
                }
                if($email != ''){
                    $data['email'] = $email;
                    $username = $email;
                }

                $where1['id_user'] = $r->id_user;
                $this->Model->update_data('user', $data, $where1);

                if($nama != ''){
                    $data1['nama'] = $nama;
                    $this->Model->update_data('u_petani', $data1, $where1);
                }

                die(json_encode(array('success' => 1, 'id_user' => $r->u_id, 'id_petani' => $r->id_petani, 'username' => $username, 'nama' => $nama)));
            }else{
                die(json_encode(array('success' => 0, 'msg' => 'Invalid Request')));
            }
        }
    }
    
    public function profile_post()
    {
        
        header('Access-Control-Allow-Origin: http://localhost:8081');
        //header('Access-Control-Allow-Origin: https://tabunganasa.com');
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Methods: POST');
        
        //die(json_encode(array('asd' => 'ccd')));

        $username = $this->post('username');

        if($username == ''){
            // $message = ['success' => 0];
            
            // $this->set_response($message, REST_Controller::HTTP_BAD_REQUEST);
            die(json_encode(array('success' => '0', 'msg' => 'Invalid Request')));
        }else{
            $join = array(
                0 => 'u_petani up-up.id_user=u.id_user'
            );

            $where = array(
                'u.role' => '3',
                'u.status' => '1'
            );
            $cek = $this->Model->get_data('', 'user u', $join, $where, '', '', 0, 0, array(0 => "u.email='".$username."' or u.hp='".$username."'"));

            if($cek->num_rows() > 0){
                $r = $cek->row();
                $data = array(
                    'email' => $r->email,
                    'status' => $r->status == '0' ? 'Tidak Aktif' : 'Aktif',
                    'hp' => $r->hp,
                    'id_petani' => $r->id_petani,
                    'nama' => $r->nama
                );

                die(json_encode(array('success' => 1, 'profile' => $data)));//, 'po' => $data_po)));
            }else{
                die(json_encode(array('success' => 0, 'msg' => 'Invalid Result')));
            }
        }
        
    }

    public function get_po_post()
    {
        header('Access-Control-Allow-Origin: http://localhost:8081');
        //header('Access-Control-Allow-Origin: https://tabunganasa.com');
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Methods: POST');

        $username = $this->post('username');

        if($username == ''){
            // $message = ['success' => 0];
            
            // $this->set_response($message, REST_Controller::HTTP_BAD_REQUEST);
            die(json_encode(array('success' => '0', 'msg' => 'Invalid Request')));
        }else{
            $join = array(
                0 => 'u_petani up-up.id_user=u.id_user'
            );

            $where = array(
                'u.role' => '3',
                'u.status' => '1'
            );
            $cek = $this->Model->get_data('', 'user u', $join, $where, '', '', 0, 0, array(0 => "u.email='".$username."' or u.hp='".$username."'"));

            if($cek->num_rows() > 0){
                $r = $cek->row();

                $join_po = array(
                    0 => 'delivery_order do-do.id_do=dod.id_do',
                    1 => 'produk pr-pr.id_produk=dod.id_produk',
                    2 => 'kebun k-k.id_kebun=pr.id_kebun',
                    3 => 'purchase_order_detail pod-pod.id_purchase_order=do.id_po and pod.id_produk=dod.id_produk'
                );

                $where_po = array(
                    'k.id_petani' => $r->id_petani,
                    'dod.status_pengiriman' => '4'
                );
                $q_po_history = $this->Model->get_data('do.id_po, dod.id_produk, dod.qty_realisasi, dod.ket_company, dod.tgl_delivery,
                pod.harga, pod.qty, pod.harga_delivery, pod.jarak, k.satuan, dod.id',
                'delivery_order_detail dod', $join_po, $where_po, '', 'dod.tgl_delivery desc');

                $data_po = array();

                foreach($q_po_history->result() as $key){
                    $data_po[count($data_po)] = array(
                        'id_po' => $key->id_po,
                        'harga' => number_format($key->harga, 2),
                        'qty' => number_format($key->qty),
                        'harga_delivery' => number_format($key->harga_delivery, 2),
                        'jarak' => $key->jarak,
                        'satuan' => ($key->satuan == '0' ? 'Kg' : ($key->satuan == '1' ? 'Ton' : 'Janjang')),
                        'id_produk' => $key->id_produk,
                        'qty_realisasi' => number_format($key->qty_realisasi),
                        'ket_company' => $key->ket_company,
                        'tgl_delivery' => date('d M Y', strtotime($key->tgl_delivery)),
                        'id' => $key->id
                    );
                }

                die(json_encode(array('success' => 1, 'data_po' => $data_po)));
            }else{
                die(json_encode(array('success' => 0, 'msg' => 'Invalid Result')));
            }
        }
    }

    public function get_bid_post()
    {
        header('Access-Control-Allow-Origin: http://localhost:8081');
        //header('Access-Control-Allow-Origin: https://tabunganasa.com');
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Methods: POST');

        $username = $this->post('username');

        if($username == ''){
            // $message = ['success' => 0];
            
            // $this->set_response($message, REST_Controller::HTTP_BAD_REQUEST);
            die(json_encode(array('success' => '0', 'msg' => 'Invalid Request')));
        }else{
            $join = array(
                0 => 'u_petani up-up.id_user=u.id_user'
            );

            $where = array(
                'u.role' => '3',
                'u.status' => '1'
            );
            $cek = $this->Model->get_data('', 'user u', $join, $where, '', '', 0, 0, array(0 => "u.email='".$username."' or u.hp='".$username."'"));

            if($cek->num_rows() > 0){
                $r = $cek->row();

                $select = 'bd.id_bid, bd.id_produk, bd.tgl_edit, bd.status, po.id_po';
                $join = array(
                    0 => 'produk p-p.id_produk=bd.id_produk',
                    1 => 'kebun k-k.id_kebun=p.id_kebun',
                    2 => 'purchase_order po-po.id_bid=bd.id_bid'
                );
                $where = array(
                    'k.id_petani' => $r->id_petani,
                    'bd.status!=' => '0'
                );
                $q_bid_history = $this->Model->get_data($select, 'bid_detail bd', $join, $where, '', 'bd.tgl_edit desc');

                $data_bid = array();

                foreach($q_bid_history->result() as $key){
                    $key->tgl_edit = date('d/M/Y H:i', strtotime($key->tgl_edit));
                    $s = $key->status;
                    $key->status1 = $s == '1' ? 'Pending' : ($s == '2' ? 'Dibatalkan Oleh Pembeli' : ($s == '3' || $s == '5' ? 'Penawaran Disepakati' : ($s == '4' ? 'Anda Membatalkan Penawaran Ini' : ($s == '6' ? 'Purchase Order Sudah Masuk' : ''))));
                    $data_bid[] = $key;
                }

                die(json_encode(array('success' => 1, 'data_bid' => $data_bid)));
            }else{
                die(json_encode(array('success' => 0, 'msg' => 'Invalid Result')));
            }
        }
    }

    public function get_bid_detail_post()
    {
        header('Access-Control-Allow-Origin: http://localhost:8081');
        //header('Access-Control-Allow-Origin: https://tabunganasa.com');
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Methods: POST');

        $id_bid = $this->post('id_bid');
        $id_produk = $this->post('id_produk');
        $status = $this->post('status');

        $m = $this->Model;

        $select = 'bd.*, up.id_perusahaan, up.nama';
        $join = array(
            0 => 'bid b-b.id_bid=bd.id_bid',
            1 => 'u_perusahaan up-up.id_perusahaan=b.id_perusahaan'
        );
        $where = array(
            'bd.id_bid' => $id_bid,
            'bd.id_produk' => $id_produk,
            'bd.status' => $status
        );
        $q = $m->get_data($select, 'bid_detail bd', $join, $where);

        if($q->num_rows() > 0){
            $r = $q->row();

            $status1 = $status == '1' ? 'Pending' : ($status == '2' ? 'Dibatalkan Oleh Pembeli' : ($status == '3' || $status == '5' ? 'Penawaran Disepakati' : ($status == '4' ? 'Anda Membatalkan Penawaran Ini' : '')));

            $data_bid = array(
                'id_bid' => $id_bid,
                'id_produk' => $id_produk,
                'status' => $status1,
                'tgl_edit' => date('d/M/Y H:i', strtotime($r->tgl_edit))
            );

            $data_buyer = array(
                'id_perusahaan' => $r->id_perusahaan,
                'nama' => $r->nama
            );

            die(json_encode(array('success' => 1, 'data_bid' => $data_bid, 'data_buyer' => $data_buyer)));
        }else{
            die(json_encode(array('success' => 0, 'msg' => 'Invalid Request')));
        }
    }

    public function get_po_detail_post()
    {
        header('Access-Control-Allow-Origin: http://localhost:8081');
        //header('Access-Control-Allow-Origin: https://tabunganasa.com');
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Methods: POST');

        $id_po = $this->post('id_po');
        $id_produk = $this->post('id_produk');

        $m = $this->Model;

        $join = array(
            0 => 'delivery_order do_-do_.id_po=pod.id_purchase_order',
            1 => 'delivery_order_detail dod-dod.id_do=do_.id_do and dod.id_produk=pod.id_produk',
            2 => 'produk prod-prod.id_produk=pod.id_produk',
            3 => 'kebun k-k.id_kebun=prod.id_kebun',
            4 => 'u_petani up-up.id_petani=k.id_petani',
            5 => 'purchase_order po-po.id_po=pod.id_purchase_order',
            6 => 'bid_detail bd-bd.id_bid=po.id_bid and bd.id_produk=pod.id_produk',
            7 => 'pabrik pabrik-pabrik.id_pabrik=bd.id_pabrik',
            8 => 'u_perusahaan perusahaan-perusahaan.id_perusahaan=pabrik.id_perusahaan'
        );

        $where = array(
            'pod.id_purchase_order' => $id_po,
            'pod.id_produk' => $id_produk,
            //'dod.status_pengiriman' => '4',
            'bd.status' => '6'
        );

        $q = $m->get_data('pod.*, dod.*, up.id_user, bd.satuan, pod.id_produk as prod, perusahaan.id_perusahaan, perusahaan.nama, pabrik.id_pabrik, pabrik.alamat', 'purchase_order_detail pod', $join, $where);

        if($q->num_rows() > 0){
            $r = $q->row();
            $data_po = array(
                'id_po' => $r->id_purchase_order,
                'id_produk' => $r->prod
            );

            $data_buyer = array(
                'id_perusahaan' => $r->id_perusahaan,
                'nama' => $r->nama,
                'id_pabrik' => $r->id_pabrik,
                'alamat' => $r->alamat
            );

            if($r->id_do == null || $r->id_do == ''){
                $data_do = false;
            }else{
                $data_do = array(
                    'id_sj' => $r->id_sj,
                    'mobil' => $r->mobil,
                    'no_plat' => $r->no_plat,
                    'jarak' => $r->jarak,
                    'qty_realisasi' => number_format($r->qty_realisasi).($r->satuan == '0' ? ' Kg' : ($r->satuan == '1' ? ' Ton' : ' Janjang')),
                    'sopir' => $r->sopir,
                    'estimasi' => $r->estimasi,
                    'hp' => $r->hp,
                    'ket_company' => $r->ket_company,
                    'tgl_delivery' => date('d/M/Y H:i:s', strtotime($r->tgl_delivery))
                );
            }

            $q1 = $m->get_data(
                'p.*, b.nama_bank as nb0, b1.nama_bank as nb1, b.atas_nama as an0, b1.atas_nama as an1',
                'pembayaran p',
                array(
                    0 => 'bank b-b.id_bank=p.id_bank_tujuan',
                    1 => 'user u-u.id_user=b.id_user',
                    2 => 'bank b1-b1.id_bank=p.id_bank_asal',
                    3 => 'user u1-u1.id_user=b1.id_user'
                ),
                array(
                    'p.id_po' => $r->id_purchase_order,
                    'u.role' => '3'
                ),
                '',
                'p.tgl_add desc'
            );

            if($q1->num_rows() > 0){
                $q1->row()->total_bayar = 'Rp. '.number_format($q1->row()->total_bayar + $q1->row()->kode_unik);
                $q1->row()->tgl_trf = date('d/M/Y H:i:s', strtotime($q1->row()->tgl_trf));
                $q1->row()->status_bayar = $q1->row()->status_bayar == '0' ? 'Belum Bayar' : 'Sudah Bayar';
            }

            $pembayaran = $q1->num_rows() > 0 ? $q1->row() : null;

            
            die(json_encode(array('success' => 1, 'data_po' => $data_po, 'data_buyer' => $data_buyer, 'data_do' => $data_do, 'pay' => $pembayaran)));
        }else{
            die(json_encode(array('success' => 0, 'msg' => 'Invalid Request')));
        }
    }

    public function test_post(){
        $username = $this->post('username');
        $message = ['success' => 0, 'username' => $username];
            
        $this->set_response($message, REST_Controller::HTTP_BAD_REQUEST);
    }
    
    public function login_post()
    {
        
        header('Access-Control-Allow-Origin: http://localhost:8081');
        //header('Access-Control-Allow-Origin: https://tabunganasa.com');
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Methods: POST');
        
        //die(json_encode(array('asd' => 'ccd')));

        $username = $this->post('username');
        $password = md5($this->post('password'));

        if($username == '' || $password == ''){
            // $message = ['success' => 0];
            
            // $this->set_response($message, REST_Controller::HTTP_BAD_REQUEST);
            die(json_encode(array('success' => '0', 'msg' => 'Invalid Request')));
        }else{
            $cek = $this->Model->get_from_query("select u.id_user, u.email, u.status, p.nama, p.id_petani
                from user u
                left join u_petani p on p.id_user=u.id_user
                where (u.email='".$username."' or u.hp='".$username."') and u.password='".$password."' and u.role='3'
            ");
            if($cek->num_rows() > 0){
                $cek->row()->username = $username;
                // $token = base64_encode($cek->row()->id_user.'-'.date('Y/m/d H:i:s'));

                // $data['token'] = $token;
                // $where['id_user'] = $cek->row()->id_user;
                // $this->Model->update_data('user', $data, $where);

                // $message = [
                //     'success' => '1',
                //     //'token' => $token,
                //     'data_auth' => $cek->row()
                // ];
                // $this->set_response($message, REST_Controller::HTTP_OK); // CREATED (201) being the HTTP response code
                die(json_encode(array('success' => '1', 'data_auth' => $cek->row())));
            }else{
                // $message = [
                //     'success' => '0',
                //     'msg' => 'Username atau password salah'
                // ];
                // $this->set_response($message, REST_Controller::HTTP_OK); // CREATED (201) being the HTTP response code
                die(json_encode(array('success' => '0', 'msg' => 'Username atau password salah')));
            }    
        }
        
    }

    public function register_post()
    {
        
        header('Access-Control-Allow-Origin: http://localhost:3000');
        //header('Access-Control-Allow-Origin: https://tabunganasa.com');
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Methods: POST');

        $email = $this->post('email');
        $password = md5($this->post('password'));
        $nama = $this->post('nama');
        $telp = $this->post('telp');
        $id_kategori = 'k_01';

        if($email == '' || $password == '' || $nama == '' || $telp == ''){
            die(json_encode(array('success' => 0, 'msg' => 'Invalid Request')));
            // $message = ['success' => 0];
            
            // $this->set_response($message, REST_Controller::HTTP_BAD_REQUEST);
        }else{

            $cek_email = $this->Model->get_from_query("select email from user where email='".$email."'")->num_rows();
            if($cek_email > 0){
                die(json_encode(array('success' => 0, 'msg' => 'E-mail sudah terdaftar')));
                // $message = [
                //     'success' => '0',
                //     'msg' => 'E-mail sudah terdaftar'
                // ];
                // $this->set_response($message, REST_Controller::HTTP_OK); // CREATED (201) being the HTTP response code
            }else{
                $id_user = $this->Model->get_id('user');
                $id_petani = $this->Model->get_id('u_petani');

                $data_user = array(
                    'id_user' => $id_user,
                    'password' => $password,
                    'email' => $email,
                    'role' => '3',
                    'status' => '1',
                    'hp' => $telp
                );

                $this->Model->insert_data('user', $data_user);

                $data_petani = array(
                    'id_petani' => $id_petani,
                    'nama' => $nama,
                    'id_user' => $id_user,
                    'id_kategori' => $id_kategori
                    
                );

                $this->Model->insert_data('u_petani', $data_petani);
                $data_auth_ = array(
                    'id_user' => $id_user,
                    'id_petani' => $id_petani,
                    'username' => ($email == '' ? $telp : $email),
                    'nama' => $nama
                );
                die(json_encode(array('success' => 1, 'data_auth' => $data_auth_)));
                // $message = [
                //     'success' => '1'
                // ];
                // $this->set_response($message, REST_Controller::HTTP_OK); // CREATED (201) being the HTTP response code
            }
        }
        
    }

    public function reset_pass_post()
    {
        header('Access-Control-Allow-Origin: http://localhost:3000');
        //header('Access-Control-Allow-Origin: https://tabunganasa.com');
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Methods: POST');

        $username = $this->post('username');

        $m = $this->Model;

        $cek_email = $m->get_data('', 'user', null, array('email' => $username, 'role' => '3'));

        if($cek_email->num_rows() == 0){
            die(json_encode(array('success' => 0, 'msg' => 'Email tidak terdaftar')));
        }

        $r = $cek_email->row();

        $kode_v = md5($username.date('YmdHis'));

        $pass = substr($kode_v, 0, 4);

        $m->update_data('user', array('password' => md5($pass), 'kode_verifikasi' => $kode_v), array('email' => $username));

        die(json_encode(array('success' => 1, 'kode_v' => $pass)));
    }

    public function new_pass_post()
    {
        header('Access-Control-Allow-Origin: http://localhost:3000');
        //header('Access-Control-Allow-Origin: https://tabunganasa.com');
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Methods: POST');

        $kode = $this->post('kode');
        $pass = $this->post('pass');

        $m = $this->Model;

        $cek_kode = $m->get_data('', 'user', null, array('password' => md5($kode)));

        if($cek_kode->num_rows() == 0){
            die(json_encode(array('success' => 0, 'msg' => 'Kode Tidak Cocok')));
        }

        if($kode != substr($cek_kode->row()->kode_verifikasi, 0, 4)){
            die(json_encode(array('success' => 0, 'msg' => 'Kode Tidak Valid')));
        }

        $m->update_data('user', array('password' => md5($pass), 'kode_verifikasi' => ''), array('password' => md5($kode)));

        die(json_encode(array('success' => 1)));
    }

    public function users_delete(){
        $id = (int) $this->get('id');

        // Validate the id.
        if ($id <= 0)
        {
            // Set the response and exit
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        // $this->some_model->delete_something($id);
        $message = [
            'id' => $id,
            'message' => 'Deleted the resource'
        ];

        $this->set_response($message, REST_Controller::HTTP_NO_CONTENT); // NO_CONTENT (204) being the HTTP response code
    }

}
