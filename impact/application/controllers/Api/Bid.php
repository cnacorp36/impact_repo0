<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Bid extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
        $this->load->model('Model');
    }

    public function accept_bid_post(){
        header('Access-Control-Allow-Origin: http://localhost:3000');
        //header('Access-Control-Allow-Origin: https://tabunganasa.com');
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Methods: POST');

        $id_bid = $this->post('id_bid');
        $use_delivery = $this->post('use_delivery');
        $harga_delivery = $this->post('harga_deivery');
        
        $gambar = $this->post('gambar');
        $qty = $this->post('qty');
        $id_petani = $this->post('id_petani');
        $id_produk = $this->Model->get_id('produk');

        $cek_petani = $this->Model->cek_petani($id_petani);

        if($cek_petani->num_rows() > 0){
            if($cek_petani->row()->status == '1'){
                
            }else{
                $message = [
                    'success' => '2',
                    'msg' => 'Akun ini belum diverifikasi'
                ];
                $this->set_response($message, REST_Controller::HTTP_OK); // CREATED (201) being the HTTP response code   
            }
        }else{
            $message = [
                'success' => '0',
                'msg' => 'Anda tidak terdaftar dalam sistem'
            ];
            $this->set_response($message, REST_Controller::HTTP_OK); // CREATED (201) being the HTTP response code   
        }
    }
}
