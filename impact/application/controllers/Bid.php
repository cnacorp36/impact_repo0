<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Bid extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
        $this->load->model('Model');
    }

    public function cc_get(){
        $m = $this->Model;
        $join0 = array(0 => 'u_petani up-up.id_user=u.id_user');
        
        $q0 = $m->get_data('up.*', 'user u', $join0, array('u.email' => 'cnacorp37@gmail.com'))->row()->id_petani;
        
        $q1 = $m->get_data('', 'kebun', null, array('id_petani' => $q0))->row()->id_kebun;
        
        $q2 = $m->get_data('', 'produk', null, array('id_kebun' => $q1))->row();
        ?>
        <pre>
            <?php print_r($q2);?>
        </pre>
        <?php
        
    }

    public function get_deal_pengiriman_post(){
      header('Access-Control-Allow-Origin: http://localhost:3000');
      //header('Access-Control-Allow-Origin: https://tabunganasa.com');
      header('Access-Control-Allow-Headers: *');
      header('Access-Control-Allow-Methods: POST');
    
      $username = $this->post('username');

      if($username == ''){// || $token == ''){
        die(json_encode(array('success' => 0, 'msg' => 'Invalid Request')));
        // $message = ['success' => 0, 'msg' => 'Invalid Request'];
        
        // $this->set_response($message, REST_Controller::HTTP_BAD_REQUEST);
      }else{
        $cek = $this->Model->get_from_query("select up.* from user u left join u_petani up on up.id_user=u.id_user where (u.email='".$username."' or u.hp='".$username."')");// and u.token='".$token."'");

        if($cek->num_rows() > 0){
          $join_accept1 = array(
            0 => 'produk p-p.id_produk=pod.id_produk',
            1 => 'kebun k-k.id_kebun=p.id_kebun',
            2 => 'delivery_order do-do.id_po=pod.id_purchase_order',
            3 => 'delivery_order_detail dod-dod.id_do=do.id_do and dod.id_produk=pod.id_produk',
            4 => 'pembayaran inv-inv.id_po=pod.id_purchase_order',
            5 => 'purchase_order po-po.id_po=pod.id_purchase_order',
            6 => 'bid_detail bd-bd.id_bid=po.id_bid and bd.id_produk=pod.id_produk'
          );

          $where_accept11 = array(
            'k.id_petani' => $cek->row()->id_petani,
            'inv.is_child' => '0',
            'inv.status_bayar' => '2',
            'bd.status' => '6'
          );

          $where_accept111 = array(
            0 => "dod.status_pengiriman='1' or dod.status_pengiriman='0' or dod.status_pengiriman='2' or dod.status_pengiriman='3' or do.id_do is null"
          );
          $q_accept11 = $this->Model->get_data('
            dod.*,inv.is_child, inv.status_bayar, p.gambar, pod.*, inv.tgl_expired, inv.tgl_expired_proses_do, bd.satuan',
            'purchase_order_detail pod', $join_accept1, $where_accept11, '', '', 0, 0, $where_accept111, '', 'bd.tgl_edit desc');

          $accept = array();

          foreach ($q_accept11->result() as $key) {
            $key->hargaa = number_format($key->harga, 2);
            $key->tgl_expired = date('d M Y (H:i:s)', strtotime($key->tgl_expired));
            $key->qty1 = $key->qty;
            $key->total1 = number_format(($key->harga * $key->qty), 2);
            $key->satuan = $key->satuan == '0' ? 'Kg' : ($key->satuan == '1' ? 'Ton' : 'Janjang');
            if($key->id_do == null){
              $key->status_accept = '2';
            }else{
              if($key->status_pengiriman == '3'){
                $key->status_accept = '4';
              }else{

                $key->status_accept = '3';
              }
              //cek status_pengiriman
            }

            $accept[count($accept)] = $key;
          }

          die(json_encode(array('success' => 1, 'data' => $accept)));
        }else{
          die(json_encode(array('success' => 0, 'msg' => 'Invalid Token')));
        }
      }
    }

    public function get_deal_post(){
      header('Access-Control-Allow-Origin: http://localhost:3000');
      //header('Access-Control-Allow-Origin: https://tabunganasa.com');
      header('Access-Control-Allow-Headers: *');
      header('Access-Control-Allow-Methods: POST');
    
      $username = $this->post('username');
      $status = $this->post('status');

      if($username == ''){// || $token == ''){
        die(json_encode(array('success' => 0, 'msg' => 'Invalid Request')));
        // $message = ['success' => 0, 'msg' => 'Invalid Request'];
        
        // $this->set_response($message, REST_Controller::HTTP_BAD_REQUEST);
      }else{
        $cek = $this->Model->get_from_query("select up.* from user u left join u_petani up on up.id_user=u.id_user where (u.email='".$username."' or u.hp='".$username."')");// and u.token='".$token."'");

        if($cek->num_rows() > 0){
          $join = array(
            0 => 'produk p-p.id_produk=bd.id_produk',
            1 => 'kebun k-k.id_kebun=p.id_kebun',
            2 => 'bid b-b.id_bid=bd.id_bid',
            3 => 'u_perusahaan uc-uc.id_perusahaan=b.id_perusahaan',
            4 => 'user u-u.id_user=uc.id_user'
          );
          //$where1[0] = "bd.status!='0' && bd.status!='6'";
          if($status == '1'){
            //pending
            $where1[0] = "bd.status='1'";
          }else if($status == '2'){
            //accepted
            $where1[0] = "bd.status='3' or bd.status='5' or bd.status='6'";
          }else if($status == '3'){
            //canceled
            $where1[0] = "bd.status='2' or bd.status='4'";
          }else{
            //$where1[0] = "bd.status!='0' or bd.status!='6'";
          }

          $where1[0] = "(b.status='0' and (bd.status!='0' or bd.status!='6')) or (b.status='1' and (bd.status='2' or bd.status='4'))";
          
          $q_bid = $this->Model->get_data('bd.*, p.gambar, p.qty as tersisa, bd.satuan', 'bid_detail bd', $join, array('u.status' => '1', 'k.id_petani' => $cek->row()->id_petani), '', 'bd.tgl_edit desc', 0, 0, $where1);
            
          $pending = array();
          $cancel = array();
          $accept = array();

          foreach ($q_bid->result() as $key) {
            $key->total = number_format(($key->harga * $key->qty), 2);
            $key->harga1 = $key->harga;
            $key->harga = number_format($key->harga, 2);
            $key->tgl_edit = date('d M Y', strtotime($key->tgl_edit));
            $key->status_accept = '0';
            $key->satuan = $key->satuan == '0' ? 'Kg' : ($key->satuan == '1' ? 'Ton' : 'Janjang');

            if($key->status == '1'){
              $pending[count($pending)] = $key;
            }else if($key->status == '2' || $key->status == '4'){
              $key->keterangan = $key->status == '2' ? 'Pembeli Membatalkan Pesanan Ini' : 'Anda Membatalkan Pesanan Ini';
              $cancel[count($cancel)] = $key;
            }else if($key->status != '0'){
              $key->total1 = number_format(($key->harga1 * $key->qty1), 2);
              $key->harga11 = $key->harga1;
              $key->hargaa = number_format($key->harga1, 2);
              $key->keterangan = 'Menunggu Konfirmasi Pembeli';
              $accept[count($accept)] = $key;
            }
          }

          $join_accept1 = array(
            0 => 'produk p-p.id_produk=pod.id_produk',
            1 => 'kebun k-k.id_kebun=p.id_kebun',
            2 => 'delivery_order do-do.id_po=pod.id_purchase_order',
            3 => 'delivery_order_detail dod-dod.id_do=do.id_do and dod.id_produk=pod.id_produk',
            4 => 'pembayaran inv-inv.id_po=pod.id_purchase_order',
            5 => 'purchase_order po-po.id_po=pod.id_purchase_order',
            6 => 'bid_detail bd-bd.id_bid=po.id_bid and bd.id_produk=pod.id_produk'
          );

          $where_accept11 = array(
            'k.id_petani' => $cek->row()->id_petani,
            'inv.is_child' => '0',
            'inv.status_bayar' => '2',
            'bd.status' => '6'
          );

          $where_accept111 = array(
            0 => "dod.status_pengiriman='1' or dod.status_pengiriman='0' or dod.status_pengiriman='2' or dod.status_pengiriman='3' or do.id_do is null"
          );
          $q_accept11 = $this->Model->get_data('
            dod.*,inv.is_child, inv.status_bayar, p.gambar, pod.*, inv.tgl_expired, inv.tgl_expired_proses_do, bd.satuan',
            'purchase_order_detail pod', $join_accept1, $where_accept11, '', '', 0, 0, $where_accept111);

          $total_pengiriman = $q_accept11->num_rows();

          $join_accept0 = array(
            0 => 'produk p-p.id_produk=pod.id_produk',
            1 => 'kebun k-k.id_kebun=p.id_kebun',
            2 => 'pembayaran inv-inv.id_po=pod.id_purchase_order',
            3 => 'purchase_order po-po.id_po=pod.id_purchase_order',
            4 => 'bid_detail bd-bd.id_bid=po.id_bid and bd.id_produk=pod.id_produk'
          );

          $where_accept = array(
            'k.id_petani' => $cek->row()->id_petani,
            'inv.is_child' => '0',
            'bd.status' => '6'
          );

          $where_accept_[0] = "inv.status_bayar='0' or inv.status_bayar='3'";

          $q_accept = $this->Model->get_data('
            inv.is_child, inv.status_bayar, p.gambar, pod.*, inv.tgl_expired, bd.satuan, bd.id_bid, bd.qty1, p.qty as p_qty',
            'purchase_order_detail pod', $join_accept0, $where_accept, '', '', 0, 0, $where_accept_);

          $cnt_pengiriman = 0;
          foreach ($q_accept->result() as $key) {
            $tgl_now = date('YmdHis');
            if($key->status_bayar == '0'){
              $tgl_expired = date('YmdHis', strtotime($key->tgl_expired));

              $tgl_expired = intval($tgl_expired);
              $tgl_now = intval($tgl_now);

              if($tgl_now >= $tgl_expired){
                $tgl_expired0 = new DateTime(date('Y-m-d H:i:s', strtotime($key->tgl_expired)));
                $tgl_expired0->modify('+1 day');
                //die(json_encode(array('asd' => $tgl_expired0->format('YmdHis'), 'cc' => $tgl_now)));
                $tgl_expired0 = $tgl_expired0->format('YmdHis');
                $tgl_expired0 = intval($tgl_expired0);
                
                $qty_baru = $key->p_qty + $key->qty1;
                $this->Model->update_data('produk', array('qty' => $qty_baru), array('id_produk' => $key->id_produk));

                if($tgl_now >= $tgl_expired0){
                  $this->Model->update_data('bid_detail', array('status' => '5'), array('id_bid' => $key->id_bid, 'status' => '6'));

                  $this->Model->update_data('bid', array('status' => '0'), array('id_bid' => $key->id_bid));

                  $this->Model->delete_data1('purchase_order', array('id_po' => $key->id_purchase_order));
                  $this->Model->delete_data1('purchase_order_detail', array('id_purchase_order' => $key->id_purchase_order));
                  $this->Model->delete_data1('pembayaran', array('id_po' => $key->id_purchase_order));

                  $key->status_accept = '6';
                }else{
                  $this->Model->update_data('pembayaran', array('status_bayar' => '3'), array('id_po' => $key->id_purchase_order));
                  $key->status_accept = '5';
                }
              }else{
                $key->status_accept = '1';
              }
            }else{
              $tgl_expired0 = new DateTime(date('Y-m-d H:i:s', strtotime($key->tgl_expired)));
              $tgl_expired0->modify('+1 day');
              $tgl_expired0 = $tgl_expired0->format('YmdHis');
              //die(json_encode(array('success' => 0, 'msg' => $tgl_expired0-$tgl_now)));
              if($tgl_now >= $tgl_expired0){
                $qty_baru = $key->p_qty + $key->qty1;
                $this->Model->update_data('produk', array('qty' => $qty_baru), array('id_produk' => $key->id_produk));

                $this->Model->update_data('bid_detail', array('status' => '5'), array('id_bid' => $key->id_bid, 'status' => '6'));

                $this->Model->update_data('bid', array('status' => '0'), array('id_bid' => $key->id_bid));

                $this->Model->delete_data1('purchase_order', array('id_po' => $key->id_purchase_order));
                $this->Model->delete_data1('purchase_order_detail', array('id_purchase_order' => $key->id_purchase_order));
                $this->Model->delete_data1('pembayaran', array('id_po' => $key->id_purchase_order));
                $key->status_accept = '6';
              }else{
                $key->status_accept = '5';
              }
            }
            
            $key->hargaa = number_format($key->harga, 2);
            $key->tgl_expired = date('d M Y (H:i:s)', strtotime($key->tgl_expired));
            $key->qty1 = $key->qty;
            $key->total1 = number_format(($key->harga * $key->qty), 2);
            $key->satuan = $key->satuan == '0' ? 'Kg' : ($key->satuan == '1' ? 'Ton' : 'Janjang');
            
            // if($key->id_do == null){
            //   if($key->status_bayar == '2'){
            //       $key->status_accept = '2';
            //   }else if($key->status_bayar == '0'){
                
            //   }
              
            //   //cek status_bayar
            // }else{
            //   if($key->status_pengiriman == '3'){
            //     $key->status_accept = '4';
            //   }else{

            //     $key->status_accept = '3';
            //   }
            //   //cek status_pengiriman
            // }

            //die(json_encode(array('dd' => $key->status_accept)));

            if($key->status_accept != '6'){
              $accept[count($accept)] = $key;
            }
          }

          $join_accept = array(
            0 => 'purchase_order po-po.id_po=pod.id_purchase_order',
            1 => 'produk p-p.id_produk=pod.id_produk',
            2 => 'kebun k-k.id_kebun=p.id_kebun',
            3 => 'delivery_order do-do.id_po=po.id_po',
            4 => 'delivery_order_detail dod-dod.id_do=do.id_do and dod.id_produk=pod.id_produk',
            5 => 'pembayaran inv-inv.id_po=po.id_po',
            6 => 'bid_detail bd-bd.id_bid=po.id_bid and bd.id_produk=pod.id_produk'
          );

          // $where_accept = array(
          //   'k.id_petani' => $cek->row()->id_petani
          // );
          // $q_accept = $this->Model->get_data('
          //   do.id_do, inv.status_bayar, p.gambar, pod.*, inv.tgl_expired, inv.tgl_expired_proses_do',
          //   'purchase_order_detail pod', $join_accept, $where_accept, '', '', 0, 0, array(0 => 'dod.status_pengiriman < 2 or dod.status_pengiriman is null'));
          // foreach ($q_accept->result() as $key) {
          //   $key->hargaa = number_format($key->harga, 2);
          //   $key->tgl_expired = date('d M Y (H:i:s)', strtotime($key->tgl_expired));
          //   $key->tgl_expired_proses_do = date('d M Y (H:i:s)', strtotime($key->tgl_expired_proses_do));
          //   $key->qty1 = $key->qty;
          //   $key->total1 = number_format(($key->harga * $key->qty), 2);
              
          //   if($key->id_do == null){
          //     if($key->status_bayar == '2'){
          //         $key->status_accept = '2';
          //     }else if($key->status_bayar == '0'){
          //       $key->status_accept = '1';
          //     }
              
          //     //cek status_bayar
          //   }else{
          //       $key->status_accept = '3';
          //     //cek status_pengiriman
          //   }

          //   $accept[count($accept)] = $key;
          // }
          
          $where_accept['dod.status_pengiriman'] = '2';
          $where_accept['inv.status_bayar'] = '0';
          $q_accept1 = $this->Model->get_data('
            do.id_do, inv.status_bayar, p.gambar, pod.*, po.tgl_add, bd.satuan',
            'purchase_order_detail pod', $join_accept, $where_accept, 'do.id_do, pod.id_produk');
            
          foreach ($q_accept1->result() as $key) {
            $key->hargaa = number_format($key->harga, 2);
            $key->tgl_add = date('d M Y', strtotime($key->tgl_add));
            $key->qty1 = $key->qty;
            $key->total1 = number_format(($key->harga * $key->qty), 2);
            $key->satuan = $key->satuan == '0' ? 'Kg' : ($key->satuan == '1' ? 'Ton' : 'Janjang');
              
            $key->status_accept = '5';

            $accept[count($accept)] = $key;
          }

          $join_po = array(
              0 => 'delivery_order do-do.id_do=dod.id_do',
              1 => 'produk pr-pr.id_produk=dod.id_produk',
              2 => 'kebun k-k.id_kebun=pr.id_kebun',
              3 => 'purchase_order_detail pod-pod.id_purchase_order=do.id_po and pod.id_produk=dod.id_produk'
          );

          $where_po = array(
              'k.id_petani' => $cek->row()->id_petani,
              'dod.status_pengiriman' => '4'
          );
          $q_po_history = $this->Model->get_data('do.id_po, dod.id_produk, dod.qty_realisasi, dod.ket_company, dod.tgl_delivery,
          pod.harga, pod.qty, pod.harga_delivery, pod.jarak, k.satuan, dod.id',
          'delivery_order_detail dod', $join_po, $where_po, '', 'dod.tgl_delivery desc');


            
            
          die(json_encode(array(
            'success' => 1,
            'pending' => $pending,
            'cancel' => $cancel,
            'accept' => $accept,
            'total_pengiriman' => $total_pengiriman,
            'total_history' => $q_po_history->num_rows(),
            'asd' => $q_accept->num_rows()
          )));

          // $message = [
          //   'success' => 1,
          //   'pending' => $pending,
          //   'cancel' => $cancel,
          //   'accept' => $accept
          // ];
          // $this->set_response($message, REST_Controller::HTTP_OK); // CREATED (201) being the HTTP response code   
        }else{
          die(json_encode(array('success' => 0, 'msg' => 'Invalid Token')));
          // $message = ['success' => 0, 'msg' => 'Invalid Token'];
            
          // $this->set_response($message, REST_Controller::HTTP_BAD_REQUEST);
        }
      }
    }
    
    public function get_bid_post(){
      header('Access-Control-Allow-Origin: http://localhost:3000');
      //header('Access-Control-Allow-Origin: https://tabunganasa.com');
      header('Access-Control-Allow-Headers: *');
      header('Access-Control-Allow-Methods: POST');
    
        $username = $this->post('username');
        $status = $this->post('status');

      if($username == ''){// || $token == ''){
        die(json_encode(array('success' => 0, 'msg' => 'Invalid Request')));
        // $message = ['success' => 0, 'msg' => 'Invalid Request'];
        
        // $this->set_response($message, REST_Controller::HTTP_BAD_REQUEST);
      }else{
        $cek = $this->Model->get_from_query("select up.* from user u left join u_petani up on up.id_user=u.id_user where (u.email='".$username."' or u.hp='".$username."')");// and u.token='".$token."'");

        if($cek->num_rows() > 0){
          $join = array(
            0 => 'produk p-p.id_produk=bd.id_produk',
            1 => 'kebun k-k.id_kebun=p.id_kebun',
            2 => 'bid b-b.id_bid=bd.id_bid',
            3 => 'u_perusahaan uc-uc.id_perusahaan=b.id_perusahaan',
            4 => 'user u-u.id_user=uc.id_user'
          );
          //$where1[0] = "bd.status!='0' && bd.status!='6'";
          if($status == '1'){
            //pending
            $where1[0] = "bd.status='1'";
          }else if($status == '2'){
            //accepted
            $where1[0] = "bd.status='3' or bd.status='5' or bd.status='6'";
          }else if($status == '3'){
            //canceled
            $where1[0] = "bd.status='2' or bd.status='4'";
          }else{
            $where1[0] = "bd.status!='0' or bd.status!='6'";
          }
          
          $q_bid = $this->Model->get_data('bd.*, p.gambar, p.qty as tersisa, bd.satuan', 'bid_detail bd', $join, array('u.status' => '1', 'k.id_petani' => $cek->row()->id_petani, 'b.status' => '0'), '', 'bd.tgl_edit desc', 0, 0, $where1);
            
          $pending = array();
          $cancel = array();
          $accept = array();

          foreach ($q_bid->result() as $key) {
            $key->total = number_format(($key->harga * $key->qty), 2);
            $key->harga1 = $key->harga;
            $key->harga = number_format($key->harga, 2);
            $key->tgl_edit = date('d M Y', strtotime($key->tgl_edit));
            $key->status_accept = '0';
            $key->satuan = $key->satuan == '0' ? 'Kg' : ($key->satuan == '1' ? 'Ton' : 'Janjang');

            if($key->status == '1'){
              $pending[count($pending)] = $key;
            }else if($key->status == '2' || $key->status == '4'){
              $key->keterangan = $key->status == '2' ? 'Pembeli Membatalkan Pesanan Ini' : 'Anda Membatalkan Pesanan Ini';
              $cancel[count($cancel)] = $key;
            }else if($key->status != '0'){
              $key->total1 = number_format(($key->harga1 * $key->qty1), 2);
              $key->harga11 = $key->harga1;
              $key->hargaa = number_format($key->harga1, 2);
              $key->keterangan = 'Menunggu Konfirmasi Pembeli';
              $accept[count($accept)] = $key;
            }
          }

          $join_accept0 = array(
            0 => 'produk p-p.id_produk=pod.id_produk',
            1 => 'kebun k-k.id_kebun=p.id_kebun',
            2 => 'delivery_order do-do.id_po=pod.id_purchase_order',
            3 => 'delivery_order_detail dod-dod.id_do=do.id_do and dod.id_produk=pod.id_produk',
            4 => 'pembayaran inv-inv.id_po=pod.id_purchase_order',
            5 => 'purchase_order po-po.id_po=pod.id_purchase_order',
            6 => 'bid_detail bd-bd.id_bid=po.id_bid and bd.id_produk=pod.id_produk'
          );

          $where_accept = array(
            'k.id_petani' => $cek->row()->id_petani,
            'inv.is_child' => '0'
          );
          $q_accept = $this->Model->get_data('
            dod.*,inv.is_child, inv.status_bayar, p.gambar, pod.*, inv.tgl_expired, inv.tgl_expired_proses_do, bd.satuan',
            'purchase_order_detail pod', $join_accept0, $where_accept, '', '', 0, 0, array(0 => "dod.status_pengiriman='1' or dod.status_pengiriman='0' or dod.status_pengiriman='2' or dod.status_pengiriman='3' or do.id_do is null"));
          foreach ($q_accept->result() as $key) {
            $key->hargaa = number_format($key->harga, 2);
            $key->tgl_expired = date('d M Y (H:i:s)', strtotime($key->tgl_expired));
            $key->tgl_expired_proses_do = date('d M Y (H:i:s)', strtotime($key->tgl_expired_proses_do));
            $key->qty1 = $key->qty;
            $key->total1 = number_format(($key->harga * $key->qty), 2);
            $key->satuan = $key->satuan == '0' ? 'Kg' : ($key->satuan == '1' ? 'Ton' : 'Janjang');
              
            if($key->id_do == null){
              if($key->status_bayar == '2'){
                  $key->status_accept = '2';
              }else if($key->status_bayar == '0'){
                $key->status_accept = '1';
              }
              
              //cek status_bayar
            }else{
              if($key->status_pengiriman == '3'){
                $key->status_accept = '4';
              }else{

                $key->status_accept = '3';
              }
              //cek status_pengiriman
            }

            $accept[count($accept)] = $key;
          }

          $join_accept = array(
            0 => 'purchase_order po-po.id_po=pod.id_purchase_order',
            1 => 'produk p-p.id_produk=pod.id_produk',
            2 => 'kebun k-k.id_kebun=p.id_kebun',
            3 => 'delivery_order do-do.id_po=po.id_po',
            4 => 'delivery_order_detail dod-dod.id_do=do.id_do and dod.id_produk=pod.id_produk',
            5 => 'pembayaran inv-inv.id_po=po.id_po',
            6 => 'bid_detail bd-bd.id_bid=po.id_bid and bd.id_produk=pod.id_produk'
          );

          // $where_accept = array(
          //   'k.id_petani' => $cek->row()->id_petani
          // );
          // $q_accept = $this->Model->get_data('
          //   do.id_do, inv.status_bayar, p.gambar, pod.*, inv.tgl_expired, inv.tgl_expired_proses_do',
          //   'purchase_order_detail pod', $join_accept, $where_accept, '', '', 0, 0, array(0 => 'dod.status_pengiriman < 2 or dod.status_pengiriman is null'));
          // foreach ($q_accept->result() as $key) {
          //   $key->hargaa = number_format($key->harga, 2);
          //   $key->tgl_expired = date('d M Y (H:i:s)', strtotime($key->tgl_expired));
          //   $key->tgl_expired_proses_do = date('d M Y (H:i:s)', strtotime($key->tgl_expired_proses_do));
          //   $key->qty1 = $key->qty;
          //   $key->total1 = number_format(($key->harga * $key->qty), 2);
              
          //   if($key->id_do == null){
          //     if($key->status_bayar == '2'){
          //         $key->status_accept = '2';
          //     }else if($key->status_bayar == '0'){
          //       $key->status_accept = '1';
          //     }
              
          //     //cek status_bayar
          //   }else{
          //       $key->status_accept = '3';
          //     //cek status_pengiriman
          //   }

          //   $accept[count($accept)] = $key;
          // }
          
          $where_accept['dod.status_pengiriman'] = '2';
          $where_accept['inv.status_bayar'] = '0';
          $q_accept1 = $this->Model->get_data('
            do.id_do, inv.status_bayar, p.gambar, pod.*, po.tgl_add, bd.satuan',
            'purchase_order_detail pod', $join_accept, $where_accept, 'do.id_do, pod.id_produk');
            
          foreach ($q_accept1->result() as $key) {
            $key->hargaa = number_format($key->harga, 2);
            $key->tgl_add = date('d M Y', strtotime($key->tgl_add));
            $key->qty1 = $key->qty;
            $key->total1 = number_format(($key->harga * $key->qty), 2);
            $key->satuan = $key->satuan == '0' ? 'Kg' : ($key->satuan == '1' ? 'Ton' : 'Janjang');
              
            $key->status_accept = '5';

            $accept[count($accept)] = $key;
          }
            
            
          die(json_encode(array(
            'success' => 1,
            'pending' => $pending,
            'cancel' => $cancel,
            'accept' => $accept,
            'asd' => $q_accept->num_rows()
          )));

          // $message = [
          //   'success' => 1,
          //   'pending' => $pending,
          //   'cancel' => $cancel,
          //   'accept' => $accept
          // ];
          // $this->set_response($message, REST_Controller::HTTP_OK); // CREATED (201) being the HTTP response code   
        }else{
          die(json_encode(array('success' => 0, 'msg' => 'Invalid Token')));
          // $message = ['success' => 0, 'msg' => 'Invalid Token'];
            
          // $this->set_response($message, REST_Controller::HTTP_BAD_REQUEST);
        }
      }
    }

    public function accept_bid_post(){
      header('Access-Control-Allow-Origin: http://localhost:3000');
      //header('Access-Control-Allow-Origin: https://tabunganasa.com');
      header('Access-Control-Allow-Headers: *');
      header('Access-Control-Allow-Methods: POST');

      $id_bid = $this->post('id_bid');
      $id_produk = $this->post('id_produk');
      $qty = $this->post('qty');
      $harga = $this->post('harga');

      $cek_qty_harga = $this->Model->get_data('qty, harga', 'bid_detail', null, array('id_bid' => $id_bid, 'id_produk' => $id_produk, 'status' => '1'));

      if($cek_qty_harga->num_rows() > 0){
        if($qty != $cek_qty_harga->row()->qty || $harga != $cek_qty_harga->row()->harga){
          $data = array(
            'status' => '5'
          );
        }else{
          $data = array(
            'status' => '3'
          );
        }
        $data['tgl_edit'] = date('Y/m/d H:i:s');
        $data['qty1'] = $qty;
        $data['harga1'] = $harga;

        $where = array('id_produk' => $id_produk, 'id_bid' => $id_bid, 'status' => '1');
        $this->Model->update_data('bid_detail', $data, $where);

        die(json_encode(array('success' => 1, 'id_bid' => $id_bid, 'id_produk' => $id_produk)));
        // $join = array(
        //   0 => 'produk p-p.id_produk=bd.id_produk',
        //   1 => 'kebun k-k.id_kebun=p.id_kebun',
        //   2 => 'u_petani up_-up_.id_petani=k.id_petani',
        //   3 => 'bid b-b.id_bid=bd.id_bid',
        //   4 => 'u_perusahaan _up-_up.id_perusahaan=b.id_perusahaan',
        //   5 => 'user u-u.id_user=_up.id_user'
        // );
        // $q_bid_detail = $this->Model->get_data('p.gambar, k.alamat, up_.nama, bd.*, k.satuan, u.email, u.id_user, up_.nama', 'bid_detail bd', $join, array('bd.id_produk' => $id_produk, 'bd.id_bid' => $id_bid), '', '', 0, 0, array(0 => "bd.status='3' or bd.status='5'"));

        // if($q_bid_detail->num_rows() > 0){
        //   $row = $q_bid_detail->row();
        //   $satuan = $row->satuan == '0' ? 'Kg' : ($row->satuan == '1' ? 'Ton' : 'Janjang');

        //   $q_setting = $this->Model->get_data('bid_perusahaan', 'setting_expired', null, null, '', '', 1);
        //   $expired = '';

        //   if($q_setting->num_rows() > 0){
        //     if($q_setting->row()->bid_perusahaan > 0){
        //       $date = new DateTime($row->tgl_edit);

        //       $date->modify('+'.$q_setting->row()->bid_perusahaan.' day');
        //       $expired = $date->format('d M Y H:i:s');
        //       //$expired = '<b>nb: Penawaran akan expired setelah <span style="color: #E53956">'..'</span></b>';
        //     }
            
        //   }
        //     $email = str_replace('@', '---', $row->email);
        //     $exp_gambar = explode('/', $row->gambar);
        //     $cnt_exp = count($exp_gambar);
        //     $gambar = $exp_gambar[$cnt_exp - 1];
        //     $gambar = str_replace(' ', '---', $gambar);
        //   die(json_encode(array('success' => 1, 'email' => $email, 'gambar' => $gambar, 'id_produk' => $id_produk,
        //     'nama' => $row->nama, 'alamat' => $row->alamat, 'qty' => $row->qty, 'satuan' => $satuan, 'harga' => $row->harga,
        //     'qty1' => $row->qty1, 'harga1' => $row->harga1, 'id_user' => $row->id_user, 'expired' => $expired
        //   )));
        // }else{
        //   die(json_encode(array('success' => $id_bid)));
        // }
        
        // $message = ['success' => 1];
            
        // $this->set_response($message, REST_Controller::HTTP_OK); // CREATED (201) being the HTTP response code   
      }else{
        die(json_encode(array('success' => 0, 'msg' => 'Invalid Request')));
        // $message = ['success' => 0, 'msg' => 'Invalid Request'];
          
        // $this->set_response($message, REST_Controller::HTTP_BAD_REQUEST);
      }
    }

    public function cancel_bid_post(){
      header('Access-Control-Allow-Origin: http://localhost:3000');
      //header('Access-Control-Allow-Origin: https://tabunganasa.com');
      header('Access-Control-Allow-Headers: *');
      header('Access-Control-Allow-Methods: POST');

      $id_bid = $this->post('id_bid');
      $id_produk = $this->post('id_produk');

      $data['status'] = '4';
      $where = array('id_bid' => $id_bid, 'id_produk' => $id_produk);
      $this->Model->update_data('bid_detail', $data, $where);
      die(json_encode(array('success' => 1)));
    }
}
