<!-- <br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br> -->
<div class="footer-copyright-area" style="margin-top: -100px">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="footer-copy-right">
                        <p>Copyright © 2018 . All rights reserved. Template by <a href="#">KLS</a>.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Footer area-->
    <!-- jquery
        ============================================ -->
    <script src="<?php echo base_url()?>assets/admin/js/vendor/jquery-1.12.4.min.js"></script>
    <!-- bootstrap JS
        ============================================ -->
    <script src="<?php echo base_url()?>assets/admin/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>assets/admin/js/datapicker/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url()?>assets/admin/js/datapicker/datepicker-active.js"></script>
    <script src="<?php echo base_url()?>assets/admin/js/bootstrap-select/bootstrap-select.js"></script>
    <!-- wow JS
        ============================================ -->
    <script src="<?php echo base_url()?>assets/admin/js/wow.min.js"></script>
    <!-- price-slider JS
        ============================================ -->
    <script src="<?php echo base_url()?>assets/admin/js/jquery-price-slider.js"></script>
    <!-- owl.carousel JS
        ============================================ -->
    <script src="<?php echo base_url()?>assets/admin/js/owl.carousel.min.js"></script>
    <!-- scrollUp JS
        ============================================ -->
    <script src="<?php echo base_url()?>assets/admin/js/jquery.scrollUp.min.js"></script>
    <!-- meanmenu JS
        ============================================ -->
    <script src="<?php echo base_url()?>assets/admin/js/meanmenu/jquery.meanmenu.js"></script>
    <!-- counterup JS
        ============================================ -->
    <script src="<?php echo base_url()?>assets/admin/js/counterup/jquery.counterup.min.js"></script>
    <script src="<?php echo base_url()?>assets/admin/js/counterup/waypoints.min.js"></script>
    <script src="<?php echo base_url()?>assets/admin/js/counterup/counterup-active.js"></script>
    <!-- mCustomScrollbar JS
        ============================================ -->
    <script src="<?php echo base_url()?>assets/admin/js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- jvectormap JS
        ============================================ -->
    <script src="<?php echo base_url()?>assets/admin/js/jvectormap/jquery-jvectormap-2.0.2.min.js"></script>
    <script src="<?php echo base_url()?>assets/admin/js/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <script src="<?php echo base_url()?>assets/admin/js/jvectormap/jvectormap-active.js"></script>
    <!-- sparkline JS
        ============================================ -->
    <script src="<?php echo base_url()?>assets/admin/js/sparkline/jquery.sparkline.min.js"></script>
    <script src="<?php echo base_url()?>assets/admin/js/sparkline/sparkline-active.js"></script>
    <!-- sparkline JS
        ============================================ -->
    <script src="<?php echo base_url()?>assets/admin/js/flot/jquery.flot.js"></script>
    <script src="<?php echo base_url()?>assets/admin/js/flot/jquery.flot.resize.js"></script>
    <script src="<?php echo base_url()?>assets/admin/js/flot/curvedLines.js"></script>
    <script src="<?php echo base_url()?>assets/admin/js/flot/flot-active.js"></script>
    <!-- knob JS
        ============================================ -->
    <script src="<?php echo base_url()?>assets/admin/js/knob/jquery.knob.js"></script>
    <script src="<?php echo base_url()?>assets/admin/js/knob/jquery.appear.js"></script>
    <script src="<?php echo base_url()?>assets/admin/js/knob/knob-active.js"></script>
    <!--  wave JS
        ============================================ -->
    <script src="<?php echo base_url()?>assets/admin/js/wave/waves.min.js"></script>
    <script src="<?php echo base_url()?>assets/admin/js/wave/wave-active.js"></script>
    <!--  todo JS
        ============================================ -->
    <script src="<?php echo base_url()?>assets/admin/js/todo/jquery.todo.js"></script>
    <!-- plugins JS
        ============================================ -->
    <script src="<?php echo base_url()?>assets/admin/js/plugins.js"></script>
    <script src="<?php echo base_url()?>assets/admin/js/data-table/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url()?>assets/admin/js/data-table/data-table-act.js"></script>
    <script src="<?php echo base_url()?>assets/admin/js/dialog/sweetalert2.min.js"></script>
    <script src="<?php echo base_url()?>assets/admin/js/dialog/dialog-active.js"></script>
    <!--  Chat JS
        ============================================ -->
    <script src="<?php echo base_url()?>assets/admin/js/chat/moment.min.js"></script>
    <script src="<?php echo base_url()?>assets/admin/js/chat/jquery.chat.js"></script>
    <!-- main JS
        ============================================ -->
    <script src="<?php echo base_url()?>assets/admin/js/main.js"></script>
    <!-- tawk chat JS
        ============================================ -->
    <!-- <script src="<?php echo base_url()?>assets/admin/js/tawk-chat.js"></script> -->

    <script type="text/javascript">
        $('#loginsubmit').submit(function (evt) {
            evt.preventDefault();
            //console.log($('#signed_in_flag').val());
            swal({
                title: 'Mohon Tunggu',
                html: 'Data sedang diproses',
                allowOutsideClick: false,
                showConfirmButton: false,
                onOpen: () => {
                    var count_pabrik = $('#count_pabrik').val();
                    var pabrik = Array();
                    for(var a=0; a<parseInt(count_pabrik); a++){
                        pabrik.push({
                            'alamat': $('#alamat'+a).val(),
                            'luas': $('#luas'+a).val(),
                            'kapasitas': $('#kapasitas'+a).val()
                        });
                    }
                    $.ajax({
                        type: "POST",
                        url: url,
                        dataType: "json",
                        data: {
                            email: $('#email').val(),
                            password: $('#password').val(),
                            sign_me: sign_me
                        },
                        success: function (data) {
                            if(data.success == '1'){
                                swal({
                                    title: 'Login Berhasil',
                                    text: 'Welcome On Board !',
                                    type: 'success'
                                }).then((result) => {
                                    window.location.href = url_redirect;
                                });
                            }else if(data.success == '0'){
                                swal({
                                    title: 'Oops..',
                                    text: data.msg,
                                    type: 'error'
                                });
                            }else if(data.success == '2'){
                                swal({
                                    title: 'Login Berhasil',
                                    text: 'Anda belum melakukan verifikasi email',
                                    type: 'success'
                                }).then((result) => {
                                    window.location.href = '<?=base_url('registrasi/confirm_email')?>';
                                });
                            }
                        },
                        error: function () {
                            swal({
                                title: 'Oops.. Server sedang error.',
                                type: 'error'
                            }).then((result) => {

                            })
                        }
                    })
                },
                onClose: () => {

                }
            });
        });

        $('#registersubmit').submit(function (evt) {
            evt.preventDefault();
            //const swal_ = swal.mixin({});
            swal({
                title: 'Mohon Tunggu',
                html: 'Data sedang diproses',
                showConfirmButton: false,
                allowOutsideClick: false,
                onOpen: () => {
                    var count_pabrik = $('#count_pabrik').val();
                    var pabrik = Array();
                    for(var a=0; a<parseInt(count_pabrik); a++){
                        pabrik.push({
                            'alamat': $('#alamat'+a).val(),
                            'luas': $('#luas'+a).val(),
                            'kapasitas': $('#kapasitas'+a).val()
                        });
                    }
                    $.ajax({
                        type: "POST",
                        url: url,
                        dataType: "json",
                        data: {
                            email: $('#email').val(),
                            password: $('#password').val(),
                            password1: $('#password1').val(),
                            nama: $('#nama').val(),
                            pabrik: pabrik
                        },
                        success: function (data) {
                            if(data.success == '1'){
                                var kode_verifikasi = data.kode_verifikasi;
                                console.log($('#email').val()+' '+kode_verifikasi);
                                $.ajax({
                                    type: "POST",
                                    //url: 'http://192.168.1.12/kelapa_sawit/email_library/confirm_email.php?email='+$('#email').val()+'&kode_v='+kode_verifikasi,
                                    url: 'https://agrinesia.id/email_library/confirm_email.php?email='+$('#email').val()+'&kode_v='+kode_verifikasi,
                                    dataType: "json",
                                    data: {
                                        email: $('#email').val(),
                                        password: $('#password').val(),
                                        password1: $('#password1').val(),
                                        nama: $('#nama').val(),
                                        pabrik: pabrik
                                    },
                                    success: function (data) {
                                    }
                                })
                                swal({
                                    title: 'Registrasi Berhasil',
                                    text: 'Welcome On Board !',
                                    type: 'success'
                                }).then((result) => {
                                    window.location.href = url_redirect;
                                });
                            }else{
                                swal({
                                    title: 'Oops..',
                                    text: data.msg,
                                    type: 'error'
                                });
                            }
                        },
                        error: function () {
                            swal({
                                title: 'Oops.. Server sedang error.',
                                type: 'error'
                            }).then((result) => {

                            })
                        }
                    })
                },
                onClose: () => {

                }
            });
            //return false;
            //window.history.back();
        });
    </script>
</body>

</html>