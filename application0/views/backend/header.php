<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Agrinesia</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
        ============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url()?>assets/images/agrinesia_logo.png">
    <!-- Google Fonts
        ============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
    <!-- Bootstrap CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/admin/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/admin/css/bootstrap-select/bootstrap-select.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/admin/css/datapicker/datepicker3.css">
    <!-- Bootstrap CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/admin/css/font-awesome.min.css">
    <!-- owl.carousel CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/admin/css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/admin/css/owl.theme.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/admin/css/owl.transitions.css">
    <!-- meanmenu CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/admin/css/meanmenu/meanmenu.min.css">
    <!-- animate CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/admin/css/animate.css">
    <!-- normalize CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/admin/css/normalize.css">
    <!-- mCustomScrollbar CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/admin/css/scrollbar/jquery.mCustomScrollbar.min.css">
    <!-- jvectormap CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/admin/css/jvectormap/jquery-jvectormap-2.0.3.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/admin/css/jquery.dataTables.min.css">
    <!-- notika icon CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/admin/css/notika-custom-icon.css">
    <!-- wave CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/admin/css/wave/waves.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/admin/css/dialog/sweetalert2.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/admin/css/dialog/dialog.css">
    <!-- main CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/admin/css/main.css">
    <!-- style CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/admin/css/style.css">
    <!-- responsive CSS
        ============================================ -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/admin/css/responsive.css">
    <!-- modernizr JS
        ============================================ -->
    <script src="<?php echo base_url()?>assets/admin/js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body>