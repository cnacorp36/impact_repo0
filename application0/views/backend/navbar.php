<div class="header-top-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="logo-area">
                        <!-- <a href="#"><img src="<?php echo base_url()?>assets/admin/img/logo/logo.png" alt="" /></a> -->
                        <a href="<?php echo base_url('admin')?>"><h3 style="color: white ; font-family: sans-serif;"><?=$this->M_models->get_data('up_.nama', 'user u', array(0 => 'u_perusahaan up_-up_.id_user=u.id_user'), array('u.email' => $this->cek['email']))->row()->nama?></h3></a>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <div class="header-top-menu">
                        <ul class="nav navbar-nav notika-top-nav">
                           <!--  <li class="nav-item dropdown">
                                <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><span><i class="notika-icon notika-search"></i></span></a>
                                <div role="menu" class="dropdown-menu search-dd animated flipInX">
                                    <div class="search-input">
                                        <i class="notika-icon notika-left-arrow"></i>
                                        <input type="text" />
                                    </div>
                                </div>
                            </li> -->
                            <li><a href="<?php echo base_url('login/logout')?>"></i> Logout</a>
                        </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="mobile-menu-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="mobile-menu">
                        <nav id="dropdown">
                            <ul class="mobile-menu-nav">
                                <li><a data-toggle="collapse" data-target="#Charts" href="<?php echo base_url('admin')?>">Home</a>
                                </li>

                                <?php
                                    $sess_ = $this->session->userdata('impact_sess');
                                    if($sess_['role'] == '2'){

                                ?>
                                <li><a data-toggle="collapse" data-target="#democrou" href="<?php echo base_url('admin/my_company')?>">My Company</a>
                                </li>
                                <li><a data-toggle="collapse" data-target="#demolibra" href="#">Market</a>
                                    <ul id="demolibra" class="collapse dropdown-header-top">
                                        <li><a href="<?php echo base_url('admin/market')?>">Products</a></li>
                                        <li><a href="<?php echo base_url('bid')?>">My Bids</a></li>
                                    </ul>
                                </li>
                                <li><a data-toggle="collapse" data-target="#demodepart" href="#">Deal</a>
                                    <ul id="demodepart" class="collapse dropdown-header-top">
                                        <li><a href="<?php echo base_url('payment')?>">Payment</a></li>
                                        <li><a href="<?php echo base_url('delivery')?>">Delivery Order</a></li>
                                        <li><a href="<?php echo base_url('PO_history')?>">Purchase Order History</a></li>
                                    </ul>
                                </li>
                                <?php
                                    }else{
                                ?>
                                <li><a data-toggle="collapse" data-target="#democrou" href="<?php echo base_url('setting')?>">Settings</a>
                                </li>
                                <li><a data-toggle="collapse" data-target="#demolibra" href="#">Pembayaran</a>
                                    <ul id="demolibra" class="collapse dropdown-header-top">
                                        <li><a href="<?php echo base_url('payment')?>">Pending</a></li>
                                        <li><a href="<?php echo base_url('payment/history')?>">History</a></li>
                                    </ul>
                                </li>
                                <li><a data-toggle="collapse" data-target="#deal__" href="#">Deal</a>
                                    <ul id="deal__" class="collapse dropdown-header-top">
                                        <li><a href="<?php echo base_url('delivery')?>">Delivery</a></li>
                                        <li><a href="<?php echo base_url('delivery/history')?>">History Delivery</a></li>
                                    </ul>
                                </li>
                                <?php
                                    }
                                ?>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Mobile Menu end -->
    <!-- Main Menu area start-->
    <div class="main-menu-area mg-tb-40">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-5 col-md-12 col-md-offset-0">
                    <ul class="nav nav-tabs notika-menu-wrap menu-it-icon-pro">
                        <li class="<?php if($menu == 'home') echo 'active' ?>"><a  href="<?php echo base_url('admin')?>"><i class="notika-icon notika-house"></i> Home</a>
                        </li>
                        <?php
                            if($sess_['role'] == '2'){

                        ?>
                        <li class="<?php if($menu == 'company') echo 'active' ?>"><a href="<?php echo base_url('admin/My_company')?>"><i class="notika-icon notika-map"></i> My Company</a>
                        </li>
                        <li ><a data-toggle="tab" href="#market" ><i class="notika-icon notika-form"></i> Market</a>
                        </li>
                        <li ><a data-toggle="tab" href="#deal"><i class="notika-icon notika-dollar"></i> Deal</a>
                        </li>
                        <li class="<?php if($menu == 'help') echo 'active' ?>"><a href="<?=base_url('admin/help')?>" ><i class="notika-icon notika-support"></i> Help</a>
                        </li>

                        <?php
                            }else{
                                ?>
                        <li class="<?php if($menu == 'setting') echo 'active' ?>"><a href="<?php echo base_url('setting')?>"><i class="notika-icon notika-map"></i> Settings</a>
                        </li>
                        <li ><a data-toggle="tab" href="#payment_admin" ><i class="notika-icon notika-form"></i> Pembayaran</a>
                        </li>
                        <li ><a data-toggle="tab" href="#deal_admin" ><i class="notika-icon notika-form"></i> Deal</a>
                        </li>
                                <?php
                            }
                        ?>
                        <!-- <li><a href="<?php echo base_url('login/logout')?>"><i class="notika-icon notika-travel"></i> Logout</a>
                        </li> -->
                    </ul>
                    <div class="tab-content custom-menu-content">
                        <div id="payment_admin" class="tab-pane notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                                <li><a href="<?php echo base_url('payment')?>">Pending</a>
                                </li>
                                <li><a href="<?php echo base_url('payment/history')?>">History</a>
                                </li>
                            </ul>
                        </div>
                        <div id="deal_admin" class="tab-pane notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                                <li><a href="<?php echo base_url('delivery')?>">Delivery</a>
                                </li>
                                <li><a href="<?php echo base_url('delivery/history')?>">History Delivery</a>
                                </li>
                            </ul>
                        </div>
                        <div id="market" class="tab-pane notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                                <li><a href="<?php echo base_url('admin/market')?>">Products</a>
                                </li>
                                <li><a href="<?php echo base_url('bid')?>">My Bids</a>
                                </li>
                            </ul>
                        </div>
                        <div id="deal" class="tab-pane notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                                <li><a href="<?php echo base_url('payment')?>">Payment</a>
                                </li>
                                <li><a href="<?php echo base_url('delivery')?>">Delivery Order</a>
                                </li>
                                <li><a href="<?php echo base_url('PO_history')?>">Purchase Order History</a>
                                </li>
                                
                            </ul>
                        </div>
                        <div id="master" class="tab-pane notika-tab-menu-bg animated flipInX">
                            <ul class="notika-main-menu-dropdown">
                                <li><a href="<?php echo base_url('master')?>">Category Sawit</a>
                                </li>
                                <li><a href="<?php echo base_url('harga_pengirim')?>">Harga Pengiriman</a>
                                </li>
                                <li><a href="<?php echo base_url('master/user')?>">User</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>