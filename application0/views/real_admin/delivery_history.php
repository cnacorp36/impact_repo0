<?php $this->load->view('backend/header');?>
<?php $this->load->view('backend/navbar');?>
<div class="breadcomb-area">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="breadcomb-list">
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="breadcomb-wp">
                  <div class="breadcomb-icon">
                    <i class="notika-icon notika-credit-card"></i>
                  </div>
                  <div class="breadcomb-ctn">
                    <h2>DELIVERY</h2>
                    <p>KELAPA SAWIT</span></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<div class="normal-table-area">
        <div class="container">
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="normal-table-list mg-t-30">
                        <div class="basic-tb-hd">
                            <h2 style="text-align: center;">Data Pengiriman</h2>
                            <!-- <p>Add Classes (<code>.table-striped</code>) to any table row within the tbody</p> -->
                        </div>
                        <div class="bsc-tbl-st">
                            
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>ID SURAT JALAN</th>
                                        <th>TUJUAN PENGIRIMAN</th>
                                        <th>PERMINTAAN</th>
                                        <th>TERKIRIM</th>
                                        <th>KETERANGAN</th>
                                        <th>ACTION</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $a = 1;
                                    foreach ($do->result() as $row) {
                                      $satuan = $row->satuan == '0' ? 'Kg' : ($row->satuan == '1' ? 'Ton' : 'Janjang');
                                        ?>
                                    <tr>
                                        <td>
                                            <?php echo $a?>
                                            <input type="hidden" id="id_do<?=$a?>" value="<?=$row->id_do?>">
                                            <input type="hidden" id="id_produk<?=$a?>" value="<?=$row->id_produk?>">
                                            <input type="hidden" id="mobil<?=$a?>" value="<?=$row->mobil?>">
                                            <input type="hidden" id="no_plat<?=$a?>" value="<?=$row->no_plat?>">
                                            <input type="hidden" id="sopir<?=$a?>" value="<?=$row->sopir?>">
                                            <input type="hidden" id="estimasi<?=$a?>" value="<?=$row->estimasi?>">
                                            <input type="hidden" id="status_pengiriman<?=$a?>" value="<?=$row->status_pengiriman?>">
                                            <input type="hidden" id="nama<?=$a?>" value="<?=$row->nama_p?>">
                                            <input type="hidden" id="hp<?=$a?>" value="<?=$row->hp?>">
                                        </td>
                                        <td><?php echo $row->id_sj?></td>
                                        <td><?=$row->alamat?></td>
                                        <td><?=$row->qty?> <?=$satuan?></td>
                                        <td><?=$row->qty_realisasi?> <?=$satuan?></td>
                                        <td><?=$row->ket_company?></td>
                                        <td>
                                          <?php
                                          if($row->status_pengiriman != '0'){
                                            $data_tambahan = '';
                                            if($row->status_pengiriman != '3'){
                                              $data_tambahan = '<p>Estimasi: <b>'.$row->estimasi.' jam</b></p>';
                                            }
                                            
                                            ?>
                                            <button type="button" class="btn btn-default" onclick="show(<?=$a?>, '<?=$data_tambahan?>')">Lihat Detail Pengiriman</button>
                                            <?php
                                            
                                          }
                                          ?>
                                            
                                        </td>
                                    </tr>
                                        <?php
                                        $a++;
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <br>
                    
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">

    function confirm_(index, satuan) {
      var id_do = $('#id_do'+index).val();
      var id_produk = $('#id_produk'+index).val();
      swal({
        title: 'Konfirmasi Pengiriman',
        html: '\n\
        <div>\n\
          <div class="row">\n\
            <p>Quantity produk yang diterima ('+satuan+')</p>\n\
            <input class="form-control" placeholder="Qty yang diterima ('+satuan+')" type="number" id="qty_confirm"/><br>\n\
          </div>\n\
        </div>\n\
        ',
        showConfirmButton: true,
        confirmButtonText: 'Verifikasi Pengiriman',
        onOpen: () => {},
        onClose: () => {}
      }).then((result) => {
        if(result){
          var qty_confirm = $('#qty_confirm').val();
          var ket = $('#ket').val();
          $.ajax({
            type: "POST",
            url: '<?php echo base_url()?>delivery/verifikasi_admin',
            dataType: "json",
            data: {
                qty_confirm: qty_confirm,
                id_do: id_do,
                id_produk: id_produk
            },
            success: function (data) {
              console.log(data);
              if(data.success == 1){
                send_notif(data.token, "Produk telah diterima oleh" + data.perusahaan, "PO_history");

                  swal({
                      title: 'Verifikasi',
                      type: 'success'
                  }).then((result) => {
                      window.location.href='<?php echo base_url()?>delivery/history';
                  })
              }else{
                  swal({
                      title: 'Oops..',
                      text: data.msg,
                      type: 'error'
                  }).then((result) => {

                  })
              }
            },
            error: function () {
                swal({
                    title: 'Oops.. Server sedang error.',
                    type: 'error'
                }).then((result) => {

                })
            }
          })
        }
      })
    }

    function show(index, tambahan) {
        var id_produk = $('#id_produk'+index).val();
        var nama = $('#nama'+index).val();
        var mobil = $('#mobil'+index).val();
        var no_plat = $('#no_plat'+index).val();
        var sopir = $('#sopir'+index).val();
        var hp = $('#hp'+index).val();
        var mobil = $('#mobil'+index).val();
        var estimasi = $('#estimasi'+index).val();
        var status_pengiriman = $('#status_pengiriman'+index).val();
        var show_confirm = false;
        if(status_pengiriman == '3'){
            show_confirm = false;
        }
        swal({
          title: 'Data Pengiriman',
          html: '\n\
          <div>\n\
            <div class="row">\n\
              <p>ID PRODUK: <b>'+id_produk+'</b></p>\n\
              <p>PETANI: <b>'+nama+'</b></p>\n\
              <p>Sopir: <b>'+sopir+'</b></p>\n\
              <p>No. Hp: <b>'+hp+'</b></p>\n\
              <p>Mobil: <b>'+mobil+'</b></p>\n\
              <p>Plat Nomor: <b>'+no_plat+'</b></p>\n\
              '+tambahan+'\n\
            </div>\n\
          </div>\n\
          ',
          showConfirmButton: show_confirm,
          confirmButtonText: 'Verifikasi Pengiriman',
          onOpen: () => {},
          onClose: () => {}
        }).then((result) => {
          if(result){
            swal({
              title: 'Konfirmasi Pengiriman',
              html: '\n\
              <div>\n\
                <div class="row">\n\
                  <p>QTY (kg): </p>\n\
                  <input type="number" class="form-control" id="qty_confirm" placeholder="QTY (Kg)" />\n\
                </div>\n\
              </div>\n\
              ',
              showConfirmButton: true,
              confirmButtonText: 'Submit',
              onOpen: () => {},
              onClose: () => {}
            })
          }
        })
    }
    function total(val,a){
        var harga = $('#harga'+a).val();
        $('#total'+a).html(val * harga );
    };

    function edit(a,id_bid,id_produk){
        var qty = $('#qty'+a).val();
        var request = $.ajax({
          url: '<?php echo base_url()?>admin/update_delivery',
          type: "POST",
          data:{qty : qty , id_bid : id_bid, id_produk : id_produk},
          dataType: "json",
          success: function(result){
            location.reload();
          }
        });
        
    }

    function send_notif(token, msg, target){
        let body;
        body = {
          to: token,
          data: {
            custom_notification: {
              title: "Pengiriman",
              body: msg,
              sound: "default",
              priority: "high",
              show_in_foreground: true,
              targetScreen: target,
              large_icon: 'https://image.freepik.com/free-icon/small-boy-cartoon_318-38077.jpg'
            }
          },
          priority: 10
        };
        $.ajax({
            type: "POST",
            url: 'https://fcm.googleapis.com/fcm/send',
            dataType: "json",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'key=AAAAysvf2c4:APA91bGQyw1452mF0GIxGcb9Rg5yvVXAQ8sIXfR6KEQAiTEopi3Uz0REWQJ8fdv_B4xyi-lNbA74toVEEiMzaUK4NO-hxYCI9rwEkOXG9ptCcd9ZFDb5QkMgsy0g5gAoq1zz_e4E0veW'
            },
            data: JSON.stringify(body),
            success: function (data) {
            },
            error: function () {
            }
        })
    }
    </script>
<?php $this->load->view('backend/footer');?>