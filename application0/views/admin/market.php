<?php $this->load->view('backend/header');?>
<?php $this->load->view('backend/navbar');?>

<?php
    $bid = $this->M_models->get_from_query("
        select p0.nama, p.qty, p.gambar, p.tgl_edit, k.alamat, bd.id_produk, bd.qty as bd_qty, bd.harga, b.id_bid, bd.satuan
        from bid b
        left join bid_detail bd on bd.id_bid=b.id_bid
        left join produk p on p.id_produk=bd.id_produk
        left join kebun k on k.id_kebun=p.id_kebun
        left join u_petani p0 on p0.id_petani=k.id_petani
        left join user u on u.id_user=p0.id_user
        left join u_perusahaan up on up.id_perusahaan=b.id_perusahaan
        left join user u1 on u1.id_user=up.id_user
        where u.status='1' and k.status='1' and p.status='1' and (b.status='0' or b.status='3') and (bd.status='0') and u1.email='".$sess['email']."'
        order by bd.tgl_edit desc
    ");

    $style_ = $bid->num_rows() == 0 ? 'display: none' : '';
?>
<div class="breadcomb-area">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="breadcomb-list">
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="breadcomb-wp">
                  <div class="breadcomb-icon">
                    <i class="notika-icon notika-dollar"></i>
                  </div>
                  <div class="breadcomb-ctn">
                    <h2>MARKET</h2>
                    <p>KEPALA SAWIT</span></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
    <div class="normal-table-area" style="background-color: #E53956; <?=$style_?>" id="bid-form">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="normal-table-list mg-t-30">
                        <div class="basic-tb-hd">
                            <h2>Penawaran Anda</h2>
                            <button type="button" class="btn btn-primary" onclick="submit_bid()">Kirim Penawaran ke Petani</button>

                            <!-- <p>Add Classes (<code>.table-striped</code>) to any table row within the tbody</p> -->
                        </div>
                        <div class="bsc-tbl-st">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Petani</th>
                                        <th>Tersedia</th>
                                        <th>Bid</th>
                                        <th>Harga</th>
                                        <th>Sub Total</th>
                                        <th>Gambar</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php

                                    $i = 1;
                                    foreach ($bid->result() as $a) {
                                        $satuan_ = $a->satuan == '0' ? 'Kg' : ($a->satuan == '1' ? 'Ton' : 'Janjang');
                                    ?>
                                    <tr>
                                        <td><?php echo $i?></td>
                                        <td><p><?php echo $a->nama.'</p>('.$a->alamat.')'?></td>
                                        <td><?php echo number_format($a->qty).' '.$satuan_?></td>
                                        <td id="qty-bid-<?php echo $a->id_produk?>"><?php echo $a->bd_qty.' '.$satuan_?></td>
                                        <td id="qty-harga-<?php echo $a->id_produk?>">Rp. <?php echo number_format($a->harga, 2)?></td>
                                        <td id="qty-sub_total-<?php echo $a->id_produk?>">Rp. <?php echo number_format($a->harga * $a->bd_qty, 2)?></td>
                                        <td><img class="thumbnail" src="<?php echo $a->gambar?>" style="width: 100px"/></td>
                                        <td id="btn_tr<?php echo $i?>">
                                            <button class="btn btn-primary" onclick="edit_bid(<?php echo $i?>, <?php echo $a->qty?>, <?php echo $a->bd_qty?>, <?php echo $a->harga?>, '<?php echo $a->id_produk?>', '<?php echo $a->id_bid?>', '<?=$satuan_?>')" type="button"><i class="fa fa-pencil"></i></button>
                                            <button class="btn btn-danger" onclick="delete_produk('<?php echo $a->id_produk?>', '<?php echo $a->id_bid?>')" type="button"><i class="fa fa-trash"></i></button>
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                    }
                                    ?>
                                    
                                    <tr id="tr_1"></tr>
                                </tbody>
                            </table>
                            <input type="hidden" id="count_bid" value="<?php echo $i?>"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="normal-table-area" >
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="normal-table-list mg-t-30">
                        <div class="basic-tb-hd">
                            <h2>Produk Kelapa Sawit</h2>
                            <!-- <p>Add Classes (<code>.table-striped</code>) to any table row within the tbody</p> -->
                        </div>
                        <div class="bsc-tbl-st">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Petani</th>
                                        <th>Tersedia</th>
                                        <th>Gambar</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 1;

                                    $join = array(
                                        0 => 'kebun k-k.id_kebun=p.id_kebun',
                                        1 => 'u_petani p0-p0.id_petani=k.id_petani',
                                        2 => 'user u-u.id_user=p0.id_user'
                                    );
                                    $count_market = $this->M_models->get_data('p0.nama', 'produk p', $join, array('u.status' => '1', 'k.status' => '1', 'p.status' => '1', 'p.qty >' => 0), '', 'p.id_produk')->num_rows();

                                    $market = $this->M_models->get_data('k.satuan, p.id_produk, p0.nama, p.qty, p.gambar, p.tgl_edit, k.alamat', 'produk p', $join, array('u.status' => '1', 'k.status' => '1', 'p.status' => '1', 'p.qty >' => 0), '', 'p.tgl_add DESC', 20);
                                    foreach ($market->result() as $a) {
                                    ?>
                                    <tr id="tr<?php echo $i?>">
                                        <td><?php echo $i?></td>
                                        <td>
                                            <a href="<?php echo base_url('petani')?>" ><p><?php echo $a->nama.'</p>('.$a->alamat.')'?>
                                            <input type="hidden" id="id_produk<?php echo $i?>" value="<?php echo $a->id_produk?>">
                                        </td>
                                        <td><?php echo number_format($a->qty) ?> <?=($a->satuan == '0' ? 'Kg' : ($a->satuan == '1' ? 'Ton' : 'Janjang'))?><input type="hidden" id="qty_<?php echo $i?>" value="<?php echo $a->qty?>"/></td>
                                        <td><img class="thumbnail" style="width: 200px" src="<?php echo $a->gambar?>"/></td>
                                        <td>
                                            <button type="button" class="btn btn-info" onclick="bid(<?php echo $i?>, <?=$a->satuan?>)">BID</button>
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                    }
                                    ?>
                                    
                                    <tr id="tr_">
                                        <td colspan="5">
                                            <input type="hidden" id="loaded" value="<?php echo ($i-1)?>">
                                            <button style="width: 100%; display: <?php if($count_market > 20){ echo 'block'; }else{ echo 'none'; } ?>" onclick="load_more()" id="load-more" type="button" class="btn btn-primary">Load More</button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var email = '<?php echo $sess["email"]?>';
        function load_more(){
            swal({
                title: 'Mohon Tunggu',
                html: 'Data sedang diproses',
                showConfirmButton: false,
                allowOutsideClick: false,
                onOpen: () => {
                    var loaded = $('#loaded').val();
                    $.ajax({
                        type: "POST",
                        url: '<?php echo base_url()?>admin/load_more_produk',
                        dataType: "json",
                        data: {
                            loaded: loaded
                        },
                        success: function (data) {
                            $('#tr_').before(data.data);
                            $('#loaded').val(data.loaded);
                            if(!data.param_load_more){
                                $('#load-more').hide();
                            }
                            swal.close();
                        },
                        error: function () {
                            swal({
                                title: 'Oops.. Server sedang error.',
                                type: 'error'
                            }).then((result) => {

                            })
                        }
                    })
                },
                onClose: () => {

                }
            });
        }

        function submit_bid() {
            swal({
              title: 'Mengirim Penawaran',
              text: 'Data yang Anda masukkan sudah benar?',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Ya, Lanjutkan!'
            }).then((result) => {
              if (result) {
                $.ajax({
                    type: "POST",
                    url: '<?php echo base_url()?>admin/submit_bid',
                    dataType: "json",
                    data: {
                        email: email
                    },
                    success: function (data) {
                        if(data.success == '1'){
                            console.log(data.token)
                            for(var j=0; j<data.token.length; j++){
                                send_notif(data.token[j], data.perusahaan);
                            }
                            
                            swal({
                                title: 'Kirim permintaan berhasil!',
                                type: 'success'
                            }).then((result) => {
                                window.location.href='<?php echo base_url()?>bid';
                            })
                        }else{
                            swal({
                                title: 'Oops..',
                                text: data.msg,
                                type: 'error'
                            }).then((result) => {

                            })
                        }
                    },
                    error: function () {
                        swal({
                            title: 'Oops.. Server sedang error.',
                            type: 'error'
                        }).then((result) => {

                        })
                    }
                })
              }
            });
        }

        function send_notif(token, perusahaan){
            let body;
            body = {
              to: token,
              data: {
                custom_notification: {
                  title: "Penawaran Produk",
                  body: perusahaan+" memberikan penawaran pada produk Anda",
                  sound: "default",
                  priority: "high",
                  show_in_foreground: true,
                  targetScreen: "Deal0",
                  large_icon: 'https://image.freepik.com/free-icon/small-boy-cartoon_318-38077.jpg'
                }
              },
              priority: 10
            };
            $.ajax({
                type: "POST",
                url: 'https://fcm.googleapis.com/fcm/send',
                dataType: "json",
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'key=AAAAysvf2c4:APA91bGQyw1452mF0GIxGcb9Rg5yvVXAQ8sIXfR6KEQAiTEopi3Uz0REWQJ8fdv_B4xyi-lNbA74toVEEiMzaUK4NO-hxYCI9rwEkOXG9ptCcd9ZFDb5QkMgsy0g5gAoq1zz_e4E0veW'
                },
                data: JSON.stringify(body),
                success: function (data) {
                },
                error: function () {
                }
            })
        }

        function edit_bid(index, tersedia, qty, harga, id_produk, id_bid, satuan) {
            swal({
                title: 'Jumlah Bid ('+satuan+')',
                html: '\n\
                <p>Tersedia: '+tersedia+' '+satuan+'</p>\n\
                <input class="form-control" type="number" onkeyup="check_qty(this.value, '+tersedia+')" id="input_swal" placeholder="Jumlah Bid ('+satuan+')" value="'+qty+'"/><br>\n\
                <input class="form-control" type="number" value="'+harga+'" id="input_swal1" placeholder="Harga Bid (Rp) per '+satuan+'"/>',
                showCancelButton: true,
                cancelButtonColor: '#d33',
                onOpen: () => {
                    
                },
                onClose: () => {

                }
            }).then((result) => {
                if(result.value && $('#input_swal').val() != '' || $('#input_swal1').val() != ''){
                    var qty = $('#input_swal').val();
                    var harga = $('#input_swal1').val();
                    swal({
                        title: 'Mohon Tunggu',
                        html: 'Permintaan sedang diproses',
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        onOpen: () => {
                            $.ajax({
                                type: "POST",
                                url: '<?php echo base_url()?>admin/edit_bid',
                                dataType: "json",
                                data: {
                                    id_bid: id_bid,
                                    id_produk: id_produk,
                                    qty: qty,
                                    harga: harga
                                },
                                success: function (data) {
                                    if(data.success == '1'){
                                        $('#qty-bid-'+id_produk).html(qty+' '+satuan);
                                        $('#qty-harga-'+id_produk).html('Rp. '+data.harga);
                                        $('#btn_tr'+index).html('\n\
                                            <button class="btn btn-primary" onclick="edit_bid('+index+', '+tersedia+', '+qty+', '+harga+', \''+id_produk+'\', \''+id_bid+'\')" type="button"><i class="fa fa-pencil"></i></button>\n\
                                            <button class="btn btn-danger" onclick="delete_produk(\''+id_produk+'\', \''+id_bid+'\')" type="button"><i class="fa fa-trash"></i></button>\n\
                                        ');
                                        swal({
                                            title: 'Ubah data berhasil',
                                            type: 'success'
                                        }).then((result) => {

                                        })
                                    }else{
                                        swal({
                                            title: 'Oops..',
                                            text: data.msg,
                                            type: 'error'
                                        }).then((result) => {

                                        })
                                    }
                                },
                                error: function () {
                                    swal({
                                        title: 'Oops.. Server sedang error.',
                                        type: 'error'
                                    }).then((result) => {

                                    })
                                }
                            })
                        },
                        onClose: () => {

                        }
                    });
                }
            });
        }

        function bid(index, satuan) {
            var tersedia = $('#qty_'+index).val();
            var satuan_ = (satuan == '0' ? 'Kg' : (satuan == '1' ? 'Ton' : 'Janjang'));
            swal({
                title: 'Jumlah Bid ('+satuan_+')',
                html: '\n\
                <p>Tersedia: '+tersedia+' '+satuan_+'</p>\n\
                <input class="form-control" type="number" onkeyup="check_qty(this.value, '+tersedia+')" id="input_swal" placeholder="Jumlah Bid ('+satuan_+')"/><br>\n\
                <input class="form-control" type="number" id="input_swal1" placeholder="Harga Bid (Rp) per '+satuan_+'"/>',
                showCancelButton: true,
                cancelButtonColor: '#d33',
                onOpen: () => {
                    
                },
                onClose: () => {

                }
            }).then((result) => {
                console.log(result);
                if(result && $('#input_swal').val() != '' || $('#input_swal1').val() != ''){
                    var qty = $('#input_swal').val();
                    var harga = $('#input_swal1').val();
                    var id_produk = $('#id_produk'+index).val();
                    swal({
                        title: 'Mohon Tunggu',
                        html: 'Permintaan sedang diproses',
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        onOpen: () => {
                            $.ajax({
                                type: "POST",
                                url: '<?php echo base_url()?>admin/add_bid',
                                dataType: "json",
                                data: {
                                    email: email,
                                    id_produk: id_produk,
                                    qty: qty,
                                    harga: harga,
                                    count_bid: $('#count_bid').val()
                                },
                                success: function (data) {
                                    if(data.success == '1'){
                                        $('#bid-form').show();
                                        $('#tr_1').before(data.data.data);
                                        $('#count_bid').val(data.data.count_bid);
                                        swal.close();
                                        $('html, body').animate({ scrollTop: $('#bid-form').offset().top }, 'slow');
                                    }else if(data.success == '2'){
                                        $('#bid-form').show();
                                        console.log(data);
                                        $('#qty-bid-'+id_produk).html(data.qty);
                                        $('#qty-harga-'+id_produk).html(data.harga);
                                        swal.close();
                                        $('html, body').animate({ scrollTop: $('#bid-form').offset().top }, 'slow');
                                    }else{
                                        swal({
                                            title: 'Oops..',
                                            text: data.msg,
                                            type: 'error'
                                        }).then((result) => {

                                        })
                                    }
                                },
                                error: function () {
                                    swal({
                                        title: 'Oops.. Server sedang error.',
                                        type: 'error'
                                    }).then((result) => {

                                    })
                                }
                            })
                        },
                        onClose: () => {

                        }
                    });
                }
            });
        }

        function check_qty(val, tersedia){
            if(parseInt(tersedia) < parseInt(val)){
                $('#input_swal').val(tersedia);
            }
        }

        function delete_produk(id_produk, id_bid){
            swal({
              title: 'Anda yakin ingin menghapus produk ini?',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
              if (result) {
                $.ajax({
                    type: "POST",
                    url: '<?php echo base_url()?>admin/delete_bid',
                    dataType: "json",
                    data: {
                        id_bid: id_bid,
                        id_produk: id_produk
                    },
                    success: function (data) {
                        if(data.success == '1'){
                            swal({
                                title: 'Hapus data berhasil',
                                type: 'success'
                            }).then((result) => {
                                window.location.href='<?php echo base_url()?>admin/market';
                            })
                        }else{
                            swal({
                                title: 'Oops..',
                                text: data.msg,
                                type: 'error'
                            }).then((result) => {

                            })
                        }
                    },
                    error: function () {
                        swal({
                            title: 'Oops.. Server sedang error.',
                            type: 'error'
                        }).then((result) => {

                        })
                    }
                })
              }
            });
        }
    </script>
<script src="<?php echo base_url('assets/js/package/dist/sweetalert2.all.min.js')?>"></script>
<?php $this->load->view('backend/footer');?>