<?php $this->load->view('backend/header')?>

 <div class="login-content">
        <!-- Login -->

        <div class="nk-block toggled" id="l-login">
            <form class="form-horizontal m-t-20" id="loginsubmit" action="<?php echo base_url('login/aksi_login');?>" method="post">
                <div class="nk-form">
                    <div class="input-group">
                        <span class="input-group-addon nk-ic-st-pro"><i class="notika-icon notika-support"></i></span>
                        <div class="nk-int-st">
                            <input id="email" type="email" name="email" class="form-control" placeholder="Email" required>
                        </div>
                    </div>
                    <div class="input-group mg-t-15">
                        <span class="input-group-addon nk-ic-st-pro"><i class="notika-icon notika-edit"></i></span>
                        <div class="nk-int-st">
                            <input id="password" type="password" name="password"  class="form-control" placeholder="Password" required>
                        </div>
                    </div>
                    <div class="fm-checkbox">
                        <label><input type="checkbox" class="i-checks" <?=($sign_me == '1' ? 'checked' : '')?> onclick="signed_in_flag_click()"> <i></i> Biarkan saya tetap masuk</label>
                        <input type="hidden" id="signed_in_flag">

                    </div>
                    <!-- <button class="btn btn-default float-right" type="submit">Login</button> -->
                    <button href="#l-register" data-ma-action="nk-login-switch" type="submit" data-ma-block="#l-register" class="btn btn-login btn-info btn-float"><i class="notika-icon notika-right-arrow right-arrow-ant"></i></button>
                </div>
            </form>

            <div class="nk-navigation nk-lg-ic">
                <a href="<?php echo base_url('registrasi')?>" data-ma-action="nk-login-switch" data-ma-block="#l-register"><i class="notika-icon notika-plus-symbol"></i> <span>Register</span></a>
                <a href="<?=base_url('login/forgot_password')?>" data-ma-action="nk-login-switch" data-ma-block="#l-forget-password"><i>?</i> <span>Lupa Password</span></a>
            </div>
        </div>

        <!-- Register -->

        <div class="nk-block" id="l-register">
            <div class="nk-form">
                <div class="input-group">
                    <span class="input-group-addon nk-ic-st-pro"><i class="notika-icon notika-support"></i></span>
                    <div class="nk-int-st">
                        <input type="text" class="form-control" placeholder="Username">
                    </div>
                </div>

                <div class="input-group mg-t-15">
                    <span class="input-group-addon nk-ic-st-pro"><i class="notika-icon notika-mail"></i></span>
                    <div class="nk-int-st">
                        <input type="text" class="form-control" placeholder="Email Address">
                    </div>
                </div>

                <div class="input-group mg-t-15">
                    <span class="input-group-addon nk-ic-st-pro"><i class="notika-icon notika-edit"></i></span>
                    <div class="nk-int-st">
                        <input type="password" class="form-control" placeholder="Password">
                    </div>
                </div>

                <a href="#l-login" data-ma-action="nk-login-switch" data-ma-block="#l-login" class="btn btn-login btn-success btn-float"><i class="notika-icon notika-right-arrow"></i></a>
            </div>

            <div class="nk-navigation rg-ic-stl">
                <a href="#" data-ma-action="nk-login-switch" data-ma-block="#l-login"><i class="notika-icon notika-right-arrow"></i> <span>Sign in</span></a>
                <a href="" data-ma-action="nk-login-switch" data-ma-block="#l-forget-password"><i>?</i> <span>Forgot Password</span></a>
            </div>
        </div>

        <!-- Forgot Password -->
        <div class="nk-block" id="l-forget-password">
            <div class="nk-form">
                <p class="text-left">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla eu risus. Curabitur commodo lorem fringilla enim feugiat commodo sed ac lacus.</p>

                <div class="input-group">
                    <span class="input-group-addon nk-ic-st-pro"><i class="notika-icon notika-mail"></i></span>
                    <div class="nk-int-st">
                        <input type="text" class="form-control" placeholder="Email Address">
                    </div>
                </div>

                <a href="#l-login" data-ma-action="nk-login-switch" data-ma-block="#l-login" class="btn btn-login btn-success btn-float"><i class="notika-icon notika-right-arrow"></i></a>
            </div>

            <div class="nk-navigation nk-lg-ic rg-ic-stl">
                <a href="" data-ma-action="nk-login-switch" data-ma-block="#l-login"><i class="notika-icon notika-right-arrow"></i> <span>Sign in</span></a>
                <a href="" data-ma-action="nk-login-switch" data-ma-block="#l-register"><i class="notika-icon notika-plus-symbol"></i> <span>Register</span></a>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var sign_me = '<?=$sign_me?>';
        var url = '<?php echo base_url()?>login/aksi_login';
        var url_redirect = '<?php echo base_url()?>admin';

        function signed_in_flag_click() {
            if(sign_me == '1'){
                sign_me = '0';
            }else{
                sign_me = '1';
            }
        }
    </script>
<script src="<?php echo base_url('assets/js/package/dist/sweetalert2.all.min.js')?>"></script>
<?php $this->load->view('backend/footer_sweet_alert')?>