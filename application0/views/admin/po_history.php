<?php $this->load->view('backend/header');?>
<?php $this->load->view('backend/navbar');?>
<div class="breadcomb-area">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="breadcomb-list">
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="breadcomb-wp">
                  <div class="breadcomb-icon">
                    <i class="notika-icon notika-credit-card"></i>
                  </div>
                  <div class="breadcomb-ctn">
                    <h2>PURCHASE ORDER HISTORY</h2>
                    <p>KELAPA SAWIT</span></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="data-table-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="data-table-list">
                    <div class="basic-tb-hd">
                        <h2>PO History</h2>
                        <!-- <p>It's just that simple. Turn your simple table into a sophisticated data table and offer your users a nice experience and great features without any effort.</p> -->
                    </div>
                    <div class="table-responsive">
                        <table id="data-table-basic" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>ID PO</th>
                                    <th>ID Produk</th>
                                    <th>QTY</th>
                                    <th>Total Harga</th>
                                    <th>Tgl PO</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $q_po = $this->M_models->get_data(
                                        'pod.id_purchase_order, po.tgl_add, dod.qty_realisasi, pod.harga, bd.satuan, pod.id_produk',
                                        'delivery_order_detail dod',
                                        array(
                                            0 => 'delivery_order do-do.id_do=dod.id_do',
                                            1 => 'purchase_order_detail pod-pod.id_purchase_order=do.id_po and pod.id_produk=dod.id_produk',
                                            2 => 'purchase_order po-po.id_po=do.id_po',
                                            3 => 'bid b-b.id_bid=po.id_bid',
                                            4 => 'u_perusahaan up-up.id_perusahaan=b.id_perusahaan',
                                            5 => 'user u-u.id_user=up.id_user',
                                            6 => 'produk pr-pr.id_produk=dod.id_produk',
                                            7 => 'kebun k-k.id_kebun=pr.id_kebun',
                                            8 => 'bid_detail bd-bd.id_bid=b.id_bid and bd.id_produk=dod.id_produk'
                                        ),
                                        array(
                                            'u.email' => $this->cek['email'],
                                            'u.role' => '2',
                                            'dod.status_pengiriman' => '4'
                                        ), '', 'do.tgl_add desc'
                                    );

                                    foreach($q_po->result() as $key){
                                        $satuan = $key->satuan == '0' ? ' Kg' : ($key->satuan == '1' ? ' Ton' : ' Janjang');
                                        ?>
                                <tr>
                                    <td><?=$key->id_purchase_order?></td>
                                    <td><?=$key->id_produk?></td>
                                    <td><?=$key->qty_realisasi.$satuan?></td>
                                    <td>Rp. <?=number_format(($key->qty_realisasi * $key->harga), 2)?></td>
                                    <td><?=date('d M Y', strtotime($key->tgl_add))?></td>
                                    <td><a class="btn btn-default" href="<?=base_url('PO_history/detail/'.$key->id_purchase_order.'/'.$key->id_produk)?>">Detail</a></td>
                                </tr>
                                        <?php
                                    }
                                ?>
                            </tbody>
                        </table>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
    
<?php $this->load->view('backend/footer');?>