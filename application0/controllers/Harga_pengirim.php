<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Harga_pengirim extends CI_Controller {
	function __construct(){
		parent::__construct();
		 $this->load->model('M_models');
		 
		 $cek =  $this->session->userdata('email');
		 if($cek  == ''){
		 	redirect(base_url('login'));
		 }
	}
	public function index()
	{
		// $data['kategori'] = $this->model->categori_sawit();
		$this->load->view('admin/harga_pengirim');
	}
	public function add_harga(){
		$query = $this->db->query('select * from ref_mobil');
		$id_mobil = "MBL-".str_pad($query->num_rows()+1, STR_PAD_LEFT);
		// $mobil = $this->input->post('mobil');
		$harga = $this->input->post('harga');
		$kapasitas_min = $this->input->post('kapasitas_min');
		$kapasitas_max = $this->input->post('kapasitas_max');

		$data = array(
			'id_mobil' => $id_mobil,
			'harga' => $harga,
			'kapasitas_min' => $kapasitas_min,
			'kapasitas_max' => $kapasitas_max
		);
		$this->M_models->insertData('ref_mobil',$data);
		redirect("harga_pengirim");
	}

	public function edit(){
		$where = array('id' => $id);
		$data['mobil'] = $this->M_models->edit_data($where,'ref_jarak')->result();
		$this->load->view('v_edit',$data);
	}
}
