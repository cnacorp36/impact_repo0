<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master extends CI_Controller {
	function __construct(){
		parent::__construct();
		 $this->load->model('M_models');
		 $this->load->model('master_model');
		 
		 $cek =  $this->session->userdata('email');
		 if($cek  == ''){
		 	redirect(base_url('login'));
		 }
	}
	public function index()
	{
		 $data['kategori'] = $this->master_model->categori_sawit();
		$this->load->view('admin/kategori',$data);
	}
	public function add_kategory(){
		$id_kategori = $this->input->post('id_kategori');
		$nama = $this->input->post('nama');

		$data = array(
			'id_kategori' => $id_kategori,
			'nama' => $nama
		);
		$this->M_models->insertData('ref_kategori',$data);
		redirect("master");
	}

	public function user(){
		$data['user'] = $this->master_model->get_user();
		$this->load->view('admin/user',$data);
	}
	public function add_user(){
		$query = $this->db->query('select * from user');
		$id_user = "USR-".str_pad($query->num_rows()+1, STR_PAD_LEFT);
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$role = $this->input->post('role');
		$status  = $this->input->post('status');

		$data = array(
			'id_user' => $id_user,
			'email' => $email,
			'password' =>$password,
			'role' =>$role,
			'status' => $status
		);
		$this->M_models->insertData('user',$data);
		redirect(base_url('master/user'));
	}
}
