<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Email_handler extends CI_Controller {
	public $cek = null;

	function __construct(){
		parent::__construct();		
		 $this->load->model('M_models');
		 
		 $this->load->library('email');
	}

	public function proses_bid($id_user){
		$q_user = $this->M_models->get_data('email, role', 'user', null, array('id_user' => $id_user));

		if($q_user->num_rows() > 0){
			$data_session = array(
				'email' => $q_user->row()->email,
				'role' => $q_user->row()->role
			);

			$this->session->set_userdata('impact_sess', $data_session);

			redirect(base_url('bid'));
		}else{
			redirect(base_url());
		}
		
	}

	public function send_email_konfirmasi_pembayaran($id_pembayaran, $id_user){
		header('Access-Control-Allow-Origin: http://localhost:3000');
    //header('Access-Control-Allow-Origin: https://tabunganasa.com');
    header('Access-Control-Allow-Headers: *');
		header('Access-Control-Allow-Methods: GET');
		
		$join = array(
			0 => 'bank b-b.id_bank=p.id_bank_tujuan',
			1 => 'bank b1-b1.id_bank=p.id_bank_asal'
		);
		$q_pembayaran = $this->M_models->get_data('p.*, b.*, b1.nama_bank as n0, b1.norek as n1, b1.atas_nama as n2', 'pembayaran p', $join, array('p.id_pembayaran' => $id_pembayaran));

		if($q_pembayaran->num_rows() > 0){
			$q_user = $this->M_models->get_data('', 'user', null, array('id_user' => $id_user));

			if($q_user->num_rows() > 0){
				$row = $q_pembayaran->row();
				$text = '
				<table border="0" cellpadding="0" cellspacing="0" style="margin-bottom: 24px">
					<tbody>
						<tr>
							<td align="center">
								<img style="width: 50%;" src="http://impact1.tabunganasa.com/impact/assets/images/banner/banner1.png"/>
							</td>
						</tr>
					</tbody>
				</table>
				<p>Tagihan untuk ID pembayaran <span style="color: #E53956">'.$id_pembayaran.',</span></p>
				
				
				<table border="0" cellpadding="0" cellspacing="0" class="btn btn-primary" style="margin-bottom: 24px">
					<tbody>
						<tr>
							<td align="left" colspan="2">
								<b>Bank Pengirim</b>
							</td>
						</tr>
						<tr>
							<td align="left" style="width: 40%">Nama Bank</td>
							<td align="left" style="width: 60%">: '.$row->n0.'</td>
						</tr>
						<tr>
							<td align="left" style="width: 40%">No Rekening</td>
							<td align="left" style="width: 60%">: '.$row->n1.'</td>
						</tr>
						<tr>
							<td align="left" style="width: 40%">Atas Nama</td>
							<td align="left" style="width: 60%">: '.$row->n2.'</td>
						</tr>
						<tr>
							<td align="left" colspan="2">
								<b>Bank Penerima</b>
							</td>
						</tr>
						<tr>
							<td align="left" style="width: 40%">Nama Bank</td>
							<td align="left" style="width: 60%">: '.$row->nama_bank.'</td>
						</tr>
						<tr>
							<td align="left" style="width: 40%">No Rekening</td>
							<td align="left" style="width: 60%">: '.$row->norek.'</td>
						</tr>
						<tr>
							<td align="left" style="width: 40%">Atas Nama</td>
							<td align="left" style="width: 60%">: '.$row->atas_nama.'</td>
						</tr>
						<tr>
							<td align="left" style="width: 40%"><b>Total Pembayaran</b></td>
							<td align="left" style="width: 60%">: <span style="color: #E53956">Rp. '.number_format(($row->total_bayar+$row->kode_unik), 2).'</span></td>
						</tr>
						<tr>
							<td align="left" style="width: 40%"><b>Expired</b></td>
							<td align="left" style="width: 60%">: <span style="color: #E53956">'.date('d m Y (H:i:s)', strtotime($row->tgl_expired)).'</span></td>
						</tr>
					</tbody>
				</table>

				<b>Pembayaran telah dikonfirmasi. Produk yang Anda pesan akan segera diproses.</b>
				';
				
				$this->send_mail('Konfirmasi Pembayaran', $q_user->row()->email, 'Konfirmasi Pembayaran', $text);
			}
		}
	}

	public function send_email_pembayaran($id_pembayaran){
		header('Access-Control-Allow-Origin: http://localhost:3000');
    //header('Access-Control-Allow-Origin: https://tabunganasa.com');
    header('Access-Control-Allow-Headers: *');
		header('Access-Control-Allow-Methods: GET');

		//$id_pembayaran = 'pay_19_01_23_000001';
		$id_pembayaran = str_replace(' ', '', $id_pembayaran);
		$q_pembayaran = $this->M_models->get_data('', 'pembayaran p', array(0 => 'bank b-b.id_bank=p.id_bank_tujuan'), array('p.id_pembayaran' => $id_pembayaran));

		if($q_pembayaran->num_rows() > 0){
			$expired = date('d M Y H:i:s', strtotime($q_pembayaran->row()->tgl_expired));
			$text = '
			<table border="0" cellpadding="0" cellspacing="0" style="margin-bottom: 24px">
				<tbody>
					<tr>
						<td align="center">
							<img style="width: 50%;" src="http://impact1.tabunganasa.com/impact/assets/images/banner/banner1.png"/>
						</td>
					</tr>
				</tbody>
			</table>
			<p>Tagihan untuk ID pembayaran <span style="color: #E53956">'.$id_pembayaran.',</span></p>
			
			
			<table border="0" cellpadding="0" cellspacing="0" class="btn btn-primary" style="margin-bottom: 24px">
				<tbody>
					<tr>
						<td align="left" colspan="2">
							<b>Bank Tujuan</b>
						</td>
					</tr>
					<tr>
						<td align="left" style="width: 40%">Nama Bank Tujuan</td>
						<td align="left" style="width: 60%">: '.$q_pembayaran->row()->nama_bank.'</td>
					</tr>
					<tr>
						<td align="left" style="width: 40%">No Rekening</td>
						<td align="left" style="width: 60%">: '.$q_pembayaran->row()->norek.'</td>
					</tr>
					<tr>
						<td align="left" style="width: 40%">Atas Nama</td>
						<td align="left" style="width: 60%">: '.$q_pembayaran->row()->atas_nama.'</td>
					</tr>
					<tr>
						<td align="left" style="width: 40%"><b>Total Pembayaran</b></td>
						<td align="left" style="width: 60%">: <span style="color: #E53956">Rp. '.number_format(($q_pembayaran->row()->total_bayar + $q_pembayaran->row()->kode_unik), 2).'</span></td>
					</tr>
					<tr>
						<td align="left" style="width: 40%"><b>Expired</b></td>
						<td align="left" style="width: 60%">: <span style="color: #E53956">'.$expired.'</span></td>
					</tr>
				</tbody>
			</table>

			<b>nb: Pengiriman tidak akan diproses sebelum pembayaran dilakukan</b>
			';

			$email =  $this->session->userdata('impact_sess')['email'];
			//$email = 'ainulyaqin444@gmail.com';
			$this->send_mail('Invoice Pembayaran', $email, 'Invoice Pembayaran', $text);
		}
	}

	public function send_email2($id_po, $id_produk){
		header('Access-Control-Allow-Origin: http://localhost:3000');
    //header('Access-Control-Allow-Origin: https://tabunganasa.com');
    header('Access-Control-Allow-Headers: *');
		header('Access-Control-Allow-Methods: GET');
		
		$id_po = str_replace(' ', '', $id_po);
		$id_produk = str_replace(' ', '', $id_produk);

		$join = array(
			0 => 'delivery_order_detail dod-dod.id_do=do.id_do',
			1 => 'purchase_order po-po.id_po=do.id_po',
			2 => 'u_perusahaan up_-up_.id_perusahaan=po.id_perusahaan',
			3 => 'user u-u.id_user=up_.id_user'
		);
		$where = array('do.id_po' => $id_po, 'dod.id_produk' => $id_produk);
		$q = $this->M_models->get_data('', 'delivery_order do', $join, $where);

		if($q->num_rows() > 0){
			$r = $q->row();

			$text = '
				<table border="0" cellpadding="0" cellspacing="0" style="margin-bottom: 24px">
          <tbody>
            <tr>
              <td align="center">
                <img style="width: 50%;" src="http://impact1.tabunganasa.com/impact/assets/images/banner/banner1.png"/>
              </td>
            </tr>
          </tbody>
        </table>
        <p>Pengiriman sedang diproses</p>                          
        
        <table border="0" cellpadding="0" cellspacing="0" class="btn btn-primary" style="margin-bottom: 24px">
          <tbody>
            <tr>
              <td align="left" style="width: 40%">ID Surat Jalan</td>
              <td align="left" style="width: 60%">: '.$r->id_sj.'</td>
            </tr>
            <tr>
              <td align="left" style="width: 40%">ID Produk</td>
              <td align="left" style="width: 60%">: '.$id_produk.'</td>
            </tr>
            <tr>
              <td align="left" style="width: 40%">Sopir</td>
              <td align="left" style="width: 60%">: '.$r->sopir.'</td>
            </tr>
            <tr>
              <td align="left" style="width: 40%">No hp</td>
              <td align="left" style="width: 60%">: '.$r->hp.'</td>
            </tr>
            <tr>
              <td align="left" style="width: 40%">Mobil</td>
              <td align="left" style="width: 60%">: '.$r->mobil.'</td>
            </tr>
            <tr>
              <td align="left" style="width: 40%">Plat Mobil</td>
              <td align="left" style="width: 60%">: '.$r->no_plat.'</td>
            </tr>
          </tbody>
        </table>

        <b>nb: Estimasi pengiriman '.$r->estimasi.' Jam</b>
			';
        echo $text;
			//$this->send_mail('Pengiriman', $r->email, 'Pengiriman', $text);
		}
	}

	public function send_email1($id_bid, $id_produk){
		header('Access-Control-Allow-Origin: http://localhost:3000');
    //header('Access-Control-Allow-Origin: https://tabunganasa.com');
    header('Access-Control-Allow-Headers: *');
    header('Access-Control-Allow-Methods: GET');

		// $id_bid = $this->input->post('id_bid');//'bid_19_01_14_000001';//
		// $id_produk = $this->input->post('id_produk');//'prod_18_12_26_000004';//

		//die(json_encode(array('success' => 'ccc'.$id_bid.$id_produk)));

		$id_bid = str_replace(' ', '', $id_bid);
		$id_produk = str_replace(' ', '', $id_produk);
		$join = array(
			0 => 'produk p-p.id_produk=bd.id_produk',
			1 => 'kebun k-k.id_kebun=p.id_kebun',
			2 => 'u_petani up_-up_.id_petani=k.id_petani',
			3 => 'bid b-b.id_bid=bd.id_bid',
			4 => 'u_perusahaan _up-_up.id_perusahaan=b.id_perusahaan',
			5 => 'user u-u.id_user=_up.id_user'
		);
		$q_bid_detail = $this->M_models->get_data('p.gambar, k.alamat, up_.nama, bd.*, k.satuan, u.email, u.id_user, up_.nama', 'bid_detail bd', $join, array('bd.id_produk' => $id_produk, 'bd.id_bid' => $id_bid), '', '', 0, 0, array(0 => "bd.status='4'"));

		if($q_bid_detail->num_rows() > 0){
			$row = $q_bid_detail->row();
			//die(json_encode(array('success' => $row->gambar)));
			$satuan = $row->satuan == '0' ? 'Kg' : ($row->satuan == '1' ? 'Ton' : 'Janjang');

			$q_setting = $this->M_models->get_data('bid_perusahaan', 'setting_expired', null, null, '', '', 1);
			$expired = '';

			if($q_setting->num_rows() > 0){
				if($q_setting->row()->bid_perusahaan > 0){
					$date = new DateTime($row->tgl_edit);

					$date->modify('+'.$q_setting->row()->bid_perusahaan.' day');
					$expired = '<b>nb: Penawaran akan expired setelah <span style="color: #E53956">'.$date->format('d M Y H:i:s').'</span></b>';
				}
				
			}
			$text = '
		<table border="0" cellpadding="0" cellspacing="0" style="margin-bottom: 24px">
	        <tbody>
	          <tr>
	            <td align="center">
	              <img style="width: 50%;" src="'.$row->gambar.'"/>
	            </td>
	          </tr>
	        </tbody>
	      </table>		                          
	      
	      <table border="0" cellpadding="0" cellspacing="0" class="btn btn-primary" style="margin-bottom: 24px">
	        <tbody>
	          <tr>
	            <td align="left" colspan="2">
	              <b>Detail Produk</b>
	            </td>
	          </tr>
	          <tr>
	            <td align="left" style="width: 40%">ID Produk</td>
	            <td align="left" style="width: 60%">: '.$id_produk.'</td>
	          </tr>
	          <tr>
	            <td align="left" style="width: 40%">Petani</td>
	            <td align="left" style="width: 60%">: '.$row->nama.'</td>
	          </tr>
	          <tr>
	            <td align="left" style="width: 40%">Alamat Kebun</td>
	            <td align="left" style="width: 60%">: '.$row->alamat.'</td>
	          </tr>
	          <tr>
	            <td align="left" colspan="2">
	              <b>Penawaran Anda</b>
	            </td>
	          </tr>
	          <tr>
	            <td align="left" style="width: 40%">QTY</td>
	            <td align="left" style="width: 60%">: '.$row->qty.' '.$satuan.'</td>
	          </tr>
	          <tr>
	            <td align="left" style="width: 40%">Harga per '.$satuan.'</td>
	            <td align="left" style="width: 60%">: <span style="color: #E53956">Rp. '.number_format($row->harga, 2).'</span></td>
	          </tr>
	          <tr>
	            <td align="center" colspan="2">
	              <table border="0" cellpadding="0" cellspacing="0">
	                <tbody>
	                  <tr>
	                    <td> <a href="http://impact1.tabunganasa.com/email_handler/proses_bid/'.$row->id_user.'" target="_blank">Lihat Bid</a> </td>
	                  </tr>
	                </tbody>
	              </table>
	            </td>
	          </tr>
	        </tbody>
	      </table>
			';

			$this->send_mail('Penawaran Ditolak', $row->email, 'Penawaran Ditolak', $text);
		}else{
			die(json_encode(array('success' => $id_bid)));
		}
	}

	public function send_email($id_bid, $id_produk){

		header('Access-Control-Allow-Origin: http://localhost:3000');
    //header('Access-Control-Allow-Origin: https://tabunganasa.com');
    header('Access-Control-Allow-Headers: *');
    header('Access-Control-Allow-Methods: GET');

		// $id_bid = $this->input->post('id_bid');//'bid_19_01_14_000001';//
		// $id_produk = $this->input->post('id_produk');//'prod_18_12_26_000004';//

		//die(json_encode(array('success' => $id_bid.$id_produk)));

		$id_bid = str_replace(' ', '', $id_bid);
		$id_produk = str_replace(' ', '', $id_produk);
		$join = array(
			0 => 'produk p-p.id_produk=bd.id_produk',
			1 => 'kebun k-k.id_kebun=p.id_kebun',
			2 => 'u_petani up_-up_.id_petani=k.id_petani',
			3 => 'bid b-b.id_bid=bd.id_bid',
			4 => 'u_perusahaan _up-_up.id_perusahaan=b.id_perusahaan',
			5 => 'user u-u.id_user=_up.id_user'
		);
		$q_bid_detail = $this->M_models->get_data('p.gambar, k.alamat, up_.nama, bd.*, k.satuan, u.email, u.id_user, up_.nama', 'bid_detail bd', $join, array('bd.id_produk' => $id_produk, 'bd.id_bid' => $id_bid), '', '', 0, 0, array(0 => "bd.status='3' or bd.status='5'"));

		if($q_bid_detail->num_rows() > 0){
			$row = $q_bid_detail->row();
			$satuan = $row->satuan == '0' ? 'Kg' : ($row->satuan == '1' ? 'Ton' : 'Janjang');

			$q_setting = $this->M_models->get_data('bid_perusahaan', 'setting_expired', null, null, '', '', 1);
			$expired = '';

			if($q_setting->num_rows() > 0){
				if($q_setting->row()->bid_perusahaan > 0){
					$date = new DateTime($row->tgl_edit);

					$date->modify('+'.$q_setting->row()->bid_perusahaan.' day');
					$expired = '<b>nb: Penawaran akan expired setelah <span style="color: #E53956">'.$date->format('d M Y H:i:s').'</span></b>';
				}
				
			}
			$text = '
				<table border="0" cellpadding="0" cellspacing="0" style="margin-bottom: 24px">
	        <tbody>
	          <tr>
	            <td align="center">
	              <img style="width: 50%;" src="'.$row->gambar.'"/>
	            </td>
	          </tr>
	        </tbody>
	      </table>		                          
	      
	      <table border="0" cellpadding="0" cellspacing="0" class="btn btn-primary" style="margin-bottom: 24px">
	        <tbody>
	          <tr>
	            <td align="left" colspan="2">
	              <b>Detail Produk</b>
	            </td>
	          </tr>
	          <tr>
	            <td align="left" style="width: 40%">ID Produk</td>
	            <td align="left" style="width: 60%">: '.$id_produk.'</td>
	          </tr>
	          <tr>
	            <td align="left" style="width: 40%">Petani</td>
	            <td align="left" style="width: 60%">: '.$row->nama.'</td>
	          </tr>
	          <tr>
	            <td align="left" style="width: 40%">Alamat Kebun</td>
	            <td align="left" style="width: 60%">: '.$row->alamat.'</td>
	          </tr>
	          <tr>
	            <td align="left" colspan="2">
	              <b>Penawaran Anda</b>
	            </td>
	          </tr>
	          <tr>
	            <td align="left" style="width: 40%">QTY</td>
	            <td align="left" style="width: 60%">: '.$row->qty.' '.$satuan.'</td>
	          </tr>
	          <tr>
	            <td align="left" style="width: 40%">Harga per '.$satuan.'</td>
	            <td align="left" style="width: 60%">: <span style="color: #E53956">Rp. '.number_format($row->harga, 2).'</span></td>
	          </tr>
	          <tr>
	            <td align="left" colspan="2">
	              <b>Penawaran '.$row->nama.' (petani)</b>
	            </td>
	          </tr>
	          <tr>
	            <td align="left" style="width: 40%">QTY</td>
	            <td align="left" style="width: 60%">: '.$row->qty1.' '.$satuan.'</td>
	          </tr>
	          <tr>
	            <td align="left" style="width: 40%">Harga per '.$satuan.'</td>
	            <td align="left" style="width: 60%">: <span style="color: #E53956">Rp. '.number_format($row->harga1, 2).'</span></td>
	          </tr>
	          <tr>
	            <td align="center" colspan="2">
	              <table border="0" cellpadding="0" cellspacing="0">
	                <tbody>
	                  <tr>
	                    <td> <a href="http://impact1.tabunganasa.com/email_handler/proses_bid/'.$row->id_user.'" target="_blank">Proses Penawaran</a> </td>
	                  </tr>
	                </tbody>
	              </table>
	            </td>
	          </tr>
	        </tbody>
	      </table>
	      '.$expired.'
			';

			$this->send_mail('Penawaran Diterima', $row->email, 'Penawaran Diterima', $text);
		}else{
			die(json_encode(array('success' => $id_bid)));
		}
	}
	
	public function testing(){
	    $this->send_mail('title', 'ainulyaqin444@gmail.com', 'subject');
	}

	public function send_mail($title='', $tujuan='', $subject='', $text_='') {
    //$nama = 'Ainul Yaqin';
    $tujuan = 'ainulyaqin444@gmail.com';
    $config['protocol']    = 'smtp';
    $config['smtp_host']    = 'smtp.sendgrid.net';
    //$config['smtp_host']    = 'ssl://smtp.gmail.com';
    //$config['smtp_port']    = '587';
    //$config['smtp_port']    = '465';
    $config['smtp_port']    = '25';
    $config['smtp_timeout'] = '10';
    $config['smtp_user']    = 'apikey';
		//$config['smtp_pass']    = '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';
		$config['smtp_pass']    = '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';
		//echo $tujuan.'<br>';
		//echo $config['smtp_pass'];
    $config['charset']    = 'utf-8';
    $config['newline']    = "\r\n";
    $config['crlf'] = "\r\n";
    $config['mailtype'] = 'text'; // or html
    $config['validation'] = TRUE; // bool whether to validate email or not      

    $this->email->initialize($config);
    $this->email->set_mailtype("html");
    $this->email->from('support@agrinesia.id', 'Agrinesia.id');
    //$this->email->to('cnacorp36@gmail.com');
    $this->email->to($tujuan);

    $this->email->subject($subject);
    
    $text = '
      <!doctype html>
		  <html>
		    <head>
		      <meta name="viewport" content="width=device-width" />
		      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		      <title>IMPACT</title>
		      <style>
		        /* -------------------------------------
		            GLOBAL RESETS
		        ------------------------------------- */
		        img {
		          border: none;
		          -ms-interpolation-mode: bicubic;
		          max-width: 100%; }

		        body {
		          background-color: #f6f6f6;
		          font-family: sans-serif;
		          -webkit-font-smoothing: antialiased;
		          font-size: 14px;
		          line-height: 1.4;
		          margin: 0;
		          padding: 0;
		          -ms-text-size-adjust: 100%;
		          -webkit-text-size-adjust: 100%; }

		        table {
		          border-collapse: separate;
		          mso-table-lspace: 0pt;
		          mso-table-rspace: 0pt;
		          width: 100%; }
		          table td {
		            font-family: sans-serif;
		            font-size: 14px;
		            vertical-align: top; }

		        /* -------------------------------------
		            BODY & CONTAINER
		        ------------------------------------- */

		        .body {
		          background-color: #f6f6f6;
		          width: 100%; }

		        /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
		        .container {
		          display: block;
		          Margin: 0 auto !important;
		          /* makes it centered */
		          max-width: 580px;
		          padding: 10px;
		          width: 580px; }

		        /* This should also be a block element, so that it will fill 100% of the .container */
		        .content {
		          box-sizing: border-box;
		          display: block;
		          Margin: 0 auto;
		          max-width: 580px;
		          padding: 10px; }

		        /* -------------------------------------
		            HEADER, FOOTER, MAIN
		        ------------------------------------- */
		        .main {
		          background: #ffffff;
		          border-radius: 3px;
		          width: 100%; }

		        .wrapper {
		          box-sizing: border-box;
		          padding: 20px; }

		        .content-block {
		          padding-bottom: 10px;
		          padding-top: 10px;
		        }

		        .footer {
		          clear: both;
		          Margin-top: 10px;
		          text-align: center;
		          width: 100%; }
		          .footer td,
		          .footer p,
		          .footer span,
		          .footer a {
		            color: #999999;
		            font-size: 12px;
		            text-align: center; }

		        /* -------------------------------------
		            TYPOGRAPHY
		        ------------------------------------- */
		        h1,
		        h2,
		        h3,
		        h4 {
		          color: #000000;
		          font-family: sans-serif;
		          font-weight: 400;
		          line-height: 1.4;
		          margin: 0;
		          margin-bottom: 30px; }

		        h1 {
		          font-size: 35px;
		          font-weight: 300;
		          text-align: center;
		          text-transform: capitalize; }

		        p,
		        ul,
		        ol {
		          font-family: sans-serif;
		          font-size: 14px;
		          font-weight: normal;
		          margin: 0;
		          margin-bottom: 15px; }
		          p li,
		          ul li,
		          ol li {
		            list-style-position: inside;
		            margin-left: 5px; }

		        a {
		          color: #3498db;
		          text-decoration: underline; }

		        /* -------------------------------------
		            BUTTONS
		        ------------------------------------- */
		        .btn {
		          box-sizing: border-box;
		          width: 100%; }
		          .btn > tbody > tr > td {
		            padding-bottom: 15px; }
		          .btn table {
		            width: auto; }
		          .btn table td {
		            background-color: #ffffff;
		            border-radius: 5px;
		            text-align: center; }
		          .btn a {
		            background-color: #ffffff;
		            border: solid 1px #3498db;
		            border-radius: 5px;
		            box-sizing: border-box;
		            color: #3498db;
		            cursor: pointer;
		            display: inline-block;
		            font-size: 14px;
		            font-weight: bold;
		            margin: 0;
		            padding: 12px 25px;
		            text-decoration: none;
		            text-transform: capitalize; }

		        .btn-primary table td {
		          background-color: #E53956; }

		        .btn-primary a {
		          background-color: #E53956;
		          border-color: #E53956;
		          color: #ffffff; }

		        .btn-primary1 table td {
		          background-color: #34ADFF; }

		        .btn-primary1 a {
		          background-color: #34ADFF;
		          border-color: #34ADFF;
		          color: #ffffff; }

		        .btn-primary2 table td {
		          background-color: #D17F2B; }

		        .btn-primary2 a {
		          background-color: #D17F2B;
		          border-color: #D17F2B;
		          color: #ffffff; }
		          

		        /* -------------------------------------
		            OTHER STYLES THAT MIGHT BE USEFUL
		        ------------------------------------- */
		        .last {
		          margin-bottom: 0; }

		        .first {
		          margin-top: 0; }

		        .align-center {
		          text-align: center; }

		        .align-right {
		          text-align: right; }

		        .align-left {
		          text-align: left; }

		        .clear {
		          clear: both; }

		        .mt0 {
		          margin-top: 0; }

		        .mb0 {
		          margin-bottom: 0; }

		        .preheader {
		          color: transparent;
		          display: none;
		          height: 0;
		          max-height: 0;
		          max-width: 0;
		          opacity: 0;
		          overflow: hidden;
		          mso-hide: all;
		          visibility: hidden;
		          width: 0; }

		        .powered-by a {
		          text-decoration: none; }

		        hr {
		          border: 0;
		          border-bottom: 1px solid #f6f6f6;
		          Margin: 20px 0; }

		        /* -------------------------------------
		            RESPONSIVE AND MOBILE FRIENDLY STYLES
		        ------------------------------------- */
		        @media only screen and (max-width: 620px) {
		          table[class=body] h1 {
		            font-size: 28px !important;
		            margin-bottom: 10px !important; }
		          table[class=body] p,
		          table[class=body] ul,
		          table[class=body] ol,
		          table[class=body] td,
		          table[class=body] span,
		          table[class=body] a {
		            font-size: 16px !important; }
		          table[class=body] .wrapper,
		          table[class=body] .article {
		            padding: 10px !important; }
		          table[class=body] .content {
		            padding: 0 !important; }
		          table[class=body] .container {
		            padding: 0 !important;
		            width: 100% !important; }
		          table[class=body] .main {
		            border-left-width: 0 !important;
		            border-radius: 0 !important;
		            border-right-width: 0 !important; }
		          table[class=body] .btn table {
		            width: 100% !important; }
		          table[class=body] .btn a {
		            width: 100% !important; }
		          table[class=body] .img-responsive {
		            height: auto !important;
		            max-width: 100% !important;
		            width: auto !important; }}

		        /* -------------------------------------
		            PRESERVE THESE STYLES IN THE HEAD
		        ------------------------------------- */
		        @media all {
		          .ExternalClass {
		            width: 100%; }
		          .ExternalClass,
		          .ExternalClass p,
		          .ExternalClass span,
		          .ExternalClass font,
		          .ExternalClass td,
		          .ExternalClass div {
		            line-height: 100%; }
		          .apple-link a {
		            color: inherit !important;
		            font-family: inherit !important;
		            font-size: inherit !important;
		            font-weight: inherit !important;
		            line-height: inherit !important;
		            text-decoration: none !important; }
		          .btn-primary table td:hover {
		            background-color: #34495e !important; }
		          .btn-primary a:hover {
		            background-color: #34495e !important;
		            border-color: #34495e !important; }
		          .btn-primary1 table td:hover {
		            background-color: #34495e !important; }
		          .btn-primary1 a:hover {
		            background-color: #34495e !important;
		            border-color: #34495e !important; }
		          .btn-primary2 table td:hover {
		            background-color: #34495e !important; }
		          .btn-primary2 a:hover {
		            background-color: #34495e !important;
		            border-color: #34495e !important; } }

		      </style>
		    </head>
		    <body class="">
		      <table border="0" cellpadding="0" cellspacing="0" class="body">
		        <tr>
		          <td>&nbsp;</td>
		          <td class="container">
		            <div class="content">

		              <!-- START CENTERED WHITE CONTAINER -->
		              <span class="preheader">'.$title.'</span>
		              <table class="main">

		                <!-- START MAIN CONTENT AREA -->
		                <tr>
		                  <td class="wrapper">
		                    <table border="0" cellpadding="0" cellspacing="0">
		                      <tr>
		                        <td>
		                          <table border="0" cellpadding="0" cellspacing="0" style="margin-bottom: 0px">
		                            <tbody>
		                              <tr>
		                                <td align="center">
		                                  <h2>'.$title.'</h2>
		                                </td>
		                              </tr>
		                            </tbody>
		                          </table>
		                        </td>
		                      </tr>
		                      <tr>
		                        <td>
		                          '.$text_.'
		                          
		                        </td>
		                      </tr>
		                    </table>
		                  </td>
		                </tr>

		              <!-- END MAIN CONTENT AREA -->
		              </table>

		              <!-- START FOOTER -->
		              <div class="footer">
		                <table border="0" cellpadding="0" cellspacing="0">
		                  <tr>
		                    <td class="content-block">
		                      <span class="apple-link">Impact, Alamat</span>
		                      <!-- <br> Don\'t like these emails? <a href="http://i.imgur.com/CScmqnj.gif">Unsubscribe</a>. -->
		                    </td>
		                  </tr>
		                  <tr>
		                    <td class="content-block powered-by">
		                      Powered by <a href="http://htmlemail.io">HTMLemail</a>.
		                    </td>
		                  </tr>
		                </table>
		              </div>
		              <!-- END FOOTER -->

		            <!-- END CENTERED WHITE CONTAINER -->
		            </div>
		          </td>
		          <td>&nbsp;</td>
		        </tr>
		      </table>
		    </body>
		  </html>

    ';
    //echo $text;
    $this->email->message($text);  

    $this->email->send();
    
    echo $this->email->print_debugger();
  }
}
