<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Po_history extends CI_Controller {
	public $cek = null;

	function __construct(){
		parent::__construct();		
		$this->load->model('M_models');
		$this->cek =  $this->session->userdata('impact_sess');
		if(!$this->cek){
			redirect(base_url('login'));
		}
	}

	public function index(){
		$data['menu'] = 'Purchase Order History';
		$this->load->view('admin/po_history', $data);
	}

	public function detail($id_po, $id_produk){
		$m = $this->M_models;
		$data['m'] = $m;
		$data['menu'] = 'Purchase Order History Detail';

		$join = array(
			0 => 'delivery_order do-do.id_po=pod.id_purchase_order',
			1 => 'delivery_order_detail dod-dod.id_do=do.id_do and dod.id_produk=pod.id_produk',
			2 => 'purchase_order po-po.id_po=pod.id_purchase_order',
			3 => 'bid_detail bd-bd.id_bid=po.id_bid and bd.id_produk=pod.id_produk'
		);

		$where = array(
            'pod.id_purchase_order' => $id_po,
            'pod.id_produk' => $id_produk
        );
		
		$q = $m->get_data('pod.*, dod.*, bd.satuan', 'purchase_order_detail pod', $join, $where);
        if($q->num_rows() > 0){
            $r = $q->row();
            $data_po = array(
                'id_po' => $r->id_purchase_order,
                'id_produk' => $r->id_produk
            );

            $data_do = array(
                'id_sj' => $r->id_sj,
                'mobil' => $r->mobil,
                'no_plat' => $r->no_plat,
                'jarak' => $r->jarak,
                'qty_realisasi' => number_format($r->qty_realisasi).($r->satuan == '0' ? ' Kg' : ($r->satuan == '1' ? ' Ton' : ' Janjang')),
                'sopir' => $r->sopir,
                'estimasi' => $r->estimasi,
                'hp' => $r->hp,
                'ket_company' => $r->ket_company,
                'tgl_delivery' => date('d/M/Y H:i:s', strtotime($r->tgl_delivery))
            );

            $q1 = $m->get_data(
                'p.*, b.nama_bank as nb0, b1.nama_bank as nb1, b.atas_nama as an0, b1.atas_nama as an1',
                'pembayaran p',
                array(
                    0 => 'bank b-b.id_bank=p.id_bank_tujuan',
                    1 => 'user u-u.id_user=b.id_user',
                    2 => 'bank b1-b1.id_bank=p.id_bank_asal',
                    3 => 'user u1-u1.id_user=b1.id_user'
                ),
                array(
                    'p.id_po' => $r->id_purchase_order,
                    'u1.email' => $this->session->userdata('impact_sess')['email']
                ),
                '',
                'p.tgl_add desc'
            );

            if($q1->num_rows() > 0){
                $q1->row()->total_bayar = 'Rp. '.number_format($q1->row()->total_bayar + $q1->row()->kode_unik);
                $q1->row()->tgl_trf = date('d/M/Y H:i:s', strtotime($q1->row()->tgl_trf));
                $q1->row()->status_bayar = $q1->row()->status_bayar == '0' ? '<label class="label label-danger">Belum Bayar</label>' : '<label class="label label-success">Sudah Bayar</label>';
            }

            $pembayaran = $q1->num_rows() > 0 ? $q1->row() : null;

            $q11 = $m->get_data(
                'p.*, b.nama_bank as nb0, b1.nama_bank as nb1, b.atas_nama as an0, b1.atas_nama as an1',
                'pembayaran p',
                array(
                    0 => 'bank b-b.id_bank=p.id_bank_tujuan',
                    1 => 'user u-u.id_user=b.id_user',
                    2 => 'bank b1-b1.id_bank=p.id_bank_asal',
                    3 => 'user u1-u1.id_user=b1.id_user'
                ),
                array(
                    'p.id_po' => $r->id_purchase_order,
                    'u.email' => $this->session->userdata('impact_sess')['email']
                ),
                '',
                'p.tgl_add desc'
            );

            if($q1->num_rows() > 0){
                $q11->row()->total_bayar = 'Rp. '.number_format($q11->row()->total_bayar + $q11->row()->kode_unik);
                $q11->row()->tgl_trf = date('d/M/Y H:i:s', strtotime($q11->row()->tgl_trf));
                $q11->row()->status_bayar = $q11->row()->status_bayar == '0' ? '<label class="label label-danger">Belum Bayar</label>' : '<label class="label label-success">Sudah Bayar</label>';
            }

            $pembayaran1 = $q11->num_rows() > 0 ? $q11->row() : null;

            $data['data_po'] = $data_po;
            $data['data_do'] = $data_do;
            $data['pay'] = $pembayaran;
            $data['pay1'] = $pembayaran1;
            
            $this->load->view('admin/po_history_detail', $data);
        }else{
        	redirect(base_url('PO_history'));
        }
		
	}
}