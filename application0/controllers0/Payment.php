<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends CI_Controller {
	public $cek = null;

	function __construct(){
		parent::__construct();		
		$this->load->model('M_models');
		$this->cek =  $this->session->userdata('impact_sess');
		if(!$this->cek){
			redirect(base_url('login'));
		}
		$this->load->library('email');
	}
	public function index()
	{
		//$this->load->model('market_model');
		// $join = array(
		// 	0 => 'pembayaran p-p.id_po=po.id_po',
		// 	1 => 'u_perusahaan up-up.id_perusahaan=po.id_perusahaan',
		// 	2 => 'user u-u.id_user=up.id_user'
		// );
		// $where = array(
		// 	'p.status' => '0',
		// 	'u.email' => $this->cek['email']
		// );
		// $q_po = $this->M_models->get_data('po.*, p.total_bayar, p.kode_unik', 'purchase_order po', $join, $where);
		// $asd['po'] = $q_po;
		if($this->cek['role'] == '2'){
			$join0 = array(
				0 => 'bank b-b.id_bank=p.id_bank_tujuan',
				1 => 'purchase_order po-po.id_po=p.id_po',
				2 => 'u_perusahaan up-up.id_perusahaan=po.id_perusahaan',
				3 => 'user u-u.id_user=up.id_user'
			);
			$q_payment = $this->M_models->get_data('p.*, b.*', 'pembayaran p', $join0, array('u.email' => $this->cek['email'], 'p.is_child' => '0'), '', 'p.tgl_add asc', 0, 0, array(0 => "p.status_bayar != '2'"));

			$asd['payment'] = $q_payment;
			$asd['menu'] = 'payment';
			$this->load->view('admin/payment',$asd);
		}else{
			$join0 = array(
				0 => 'bank b-b.id_bank=p.id_bank_tujuan',
				1 => 'purchase_order po-po.id_po=p.id_po',
				2 => 'u_perusahaan up-up.id_perusahaan=po.id_perusahaan',
				3 => 'user u-u.id_user=up.id_user',
				4 => 'bank b1-b1.id_bank=p.id_bank_asal'
			);
			$q_payment = $this->M_models->get_data('p.*, b.*, u.id_user, b1.nama_bank as nama_bank1, b1.norek as norek1, b1.atas_nama as atas_nama1', 'pembayaran p', $join0, array('p.status_bayar' => '0'), '', 'p.tgl_add asc');

			$asd['payment'] = $q_payment;
			$asd['menu'] = 'payment';
			$this->load->view('real_admin/payment',$asd);
		}
		
	}

	public function ubah_pembayaran_admin($id_pembayaran, $id_user){
		if($this->cek['role'] != '2'){
			$asd['menu'] = 'payment';
			$asd['id_pembayaran'] = $id_pembayaran;
			$asd['id_user'] = $id_user;
			$this->load->view('real_admin/payment_edit', $asd);
		}
	}

	public function history(){
		$join0 = array(
			0 => 'bank b-b.id_bank=p.id_bank_tujuan',
			1 => 'purchase_order po-po.id_po=p.id_po',
			2 => 'u_perusahaan up-up.id_perusahaan=po.id_perusahaan',
			3 => 'user u-u.id_user=up.id_user',
			4 => 'bank b1-b1.id_bank=p.id_bank_asal'
		);
		$q_payment = $this->M_models->get_data('p.*, b.*, u.id_user, b1.nama_bank as nama_bank1, b1.norek as norek1, b1.atas_nama as atas_nama1', 'pembayaran p', $join0, array('p.status_bayar' => '2'), '', 'p.tgl_add asc');

		$asd['payment'] = $q_payment;
		$asd['menu'] = 'payment';
		$this->load->view('real_admin/history_payment',$asd);
	}

	public function update_payment(){
		$selected_bank = $this->input->post('selected_bank');
		$bank_pengirim = $this->input->post('bank_pengirim');
		$norek = $this->input->post('norek');
		$an_ = $this->input->post('an_');
		$id_user = $this->input->post('id_user');
		$status_bayar = $this->input->post('status_bayar');
		$id_pembayaran = $this->input->post('id_pembayaran');

		if($selected_bank == '000'){
			$id_bank = $this->M_models->get_id('bank');
			$data_bank = array(
				'id_bank' => $id_bank,
				'nama_bank' => $bank_pengirim,
				'norek' => $norek,
				'atas_nama' => $an_,
				'status' => '1',
				'id_user' => $id_user,
				'tgl_add' => date('Y/m/d H:i:s')
			);

			$this->M_models->insertData('bank', $data_bank);
		}else{
			$q_bank_ = $this->M_models->get_data('atas_nama', 'bank', null, array('id_bank' => $selected_bank));
			if($q_bank_->num_rows() > 0){
				$an_ = $q_bank_->row()->atas_nama;
			}
			$id_bank = $selected_bank;
		}

		$tgl_expired_proses_do = '240';

		$q_setting = $this->M_models->get_data('proses_pengiriman', 'setting_expired');

		if($q_setting->num_rows() > 0){
			$tgl_expired_proses_do = $q_setting->row()->proses_pengiriman;
		}

		$tgl_expired_proses_do = date('Y/m/d H:i:s', strtotime('+'.$tgl_expired_proses_do.' hour'));

		$data_pembayaran = array(
			'status_bayar' => $status_bayar,
			'id_bank_asal' => $id_bank,
			'atas_nama' => $an_,
			'tgl_trf' => date('Y/m/d H:i:s'),
			'tgl_expired_proses_do' => $tgl_expired_proses_do
		);

		$where_pembayaran['id_pembayaran'] = $id_pembayaran;

		$this->M_models->updateData('pembayaran', $data_pembayaran, $where_pembayaran);

		$q_user = $this->M_models->get_data('', 'user u', array(0 => 'u_perusahaan up-up.id_user=u.id_user'), array('u.id_user' => $id_user));

		$join_token = array(
			0 => 'purchase_order_detail pod-pod.id_purchase_order=p.id_po',
			1 => 'produk prod-prod.id_produk=pod.id_produk',
			2 => 'kebun k-k.id_kebun=prod.id_kebun',
			3 => 'u_petani up-up.id_petani=k.id_petani',
			4 => 'user u-u.id_user=up.id_user'
		);
		$q_token = $this->M_models->get_data('u.token', 'pembayaran p', $join_token, array('p.id_pembayaran' => $id_pembayaran));

		$token = array();
		foreach($q_token->result() as $key){
			$token[count($token)] = $key->token;
		}

		die(json_encode(array('success' => 1, 'token' => $token, 'nama_perusahaan' => $q_user->row()->nama)));
	}

	public function update_bank_tujuan(){
		$id_bank_tujuan = $this->input->post('id_bank_tujuan');
		$id_pembayaran = $this->input->post('id_pembayaran');
		// $id_pembayaran = 'pay_19_01_01_000002';
		// $id_bank_tujuan = 'bank_18_11_09_000003';
		$data['id_bank_tujuan'] = $id_bank_tujuan;
		$where['id_pembayaran'] = $id_pembayaran;

		$this->M_models->updateData('pembayaran', $data, $where);

		echo json_encode(array('success' => 1));

	}

	public function admin_setting(){
		$bid_perusahaan = $this->input->post('bid_perusahaan');
		$bid_petani = $this->input->post('bid_petani');
		$pembayaran = $this->input->post('pembayaran');
		$proses_pengiriman = $this->input->post('proses_pengiriman');
		$konfirmasi_pengiriman = $this->input->post('konfirmasi_pengiriman');

		if($bid_perusahaan != ''){
			$data['bid_perusahaan'] = $bid_perusahaan;
		}

		if($bid_petani != ''){
			$data['bid_petani'] = $bid_petani;
		}

		if($pembayaran != ''){
			$data['pembayaran'] = $pembayaran;
		}

		if($proses_pengiriman != ''){
			$data['proses_pengiriman'] = $proses_pengiriman;
		}

		if($konfirmasi_pengiriman != ''){
			$data['konfirmasi_pengiriman'] = $konfirmasi_pengiriman;
		}

		$cek_setting = $this->M_models->get_data('', 'setting_expired');
		if($cek_setting->num_rows() > 0){
			$where['id'] = $cek_setting->row()->id;
			$this->M_models->updateData('setting_expired', $data, $where);
		}else{
			$this->M_models->insertData('setting_expired', $data);
		}

		die(json_encode(array('success' => 1)));
	}

	public function insert_payment(){
		$total_bayar = $this->input->post('total_bayar');
		$id_bank_tujuan = $this->input->post('id_bank_tujuan');
		$kode_unik = $this->input->post('kode_unik');
		$data_pembayaran = $this->input->post('data_pembayaran');
		$id_pembayaran = $this->M_models->get_id('pembayaran');

		$data_pembayaran1 = array(
			'id_pembayaran' => $id_pembayaran,
			'status_bayar' => '0',
			'total_bayar' => $total_bayar,
			'id_bank_tujuan' => $id_bank_tujuan,
			'id_bank_asal' => '',
			'atas_nama' => '',
			'tgl_trf' => date('Y/m/d H:i:s'),
			'keterangan' => '',
			'sisa_hutang' => $total_bayar,
			'kode_unik' => $kode_unik,
			'tgl_add' => date('Y/m/d H:i:s')
		);

		$this->M_models->insertData('pembayaran', $data_pembayaran1);

		for($a=0; $a<count($data_pembayaran); $a++){
			$data_pembayaran_detail = array(
				'id_pembayaran' => $id_pembayaran,
				'id_po' => $data_pembayaran[$a]['id_po'],
				'id_produk' => $data_pembayaran[$a]['id_produk']
			);

			$this->M_models->insertData('pembayaran_detail', $data_pembayaran_detail);

			$data_po = array('status_bayar' => '1');
			$where_po = array('id_purchase_order' => $data_pembayaran[$a]['id_po'], 'id_produk' => $data_pembayaran[$a]['id_produk']);

			$this->M_models->updateData('purchase_order_detail', $data_po, $where_po);
		}
		die(json_encode(array('success' => 1)));
	}

	public function load_bank(){
		$q_bank = $this->M_models->get_data('b.*', 'bank b', array(0 => 'user u-u.id_user=b.id_user'), array('b.status' => '1', 'u.role' => '0'), '', 'b.nama_bank asc');

		$data_bank = '';
		foreach ($q_bank->result() as $key) {
			$data_bank .= '<input type="radio" name="id_bank" onclick="change_bank(this.value)" value="'.$key->id_bank.'"><b style="margin-left: 12px">'.$key->nama_bank.'</b><p style="margin-left: 24px;">'.$key->norek.'</p><p style="margin-left: 24px; margin-top: -8px">'.$key->atas_nama.'</p><br>';
		}

		die(json_encode(array('success' => 1, 'data_bank' => $data_bank)));
	}

	public function add_new_delivery($id_produk){
		$q = $this->M_models->get_from_query("select k.harga from produk p left join sawit_kelas k on k.id=p.id_kelas where p.id_produk='".$id_produk."'");
		$harga = $q->row()->harga;

		$email = $this->session->userdata('email');
		$abc = $this->M_models->get_from_query("select a.*,b.id_perusahaan from user a left join u_perusahaan b on b.id_user = a.id_user where a.email = '".$email."' ");
		$id_perusahaan = $abc->row()->id_perusahaan;
		$peru = $this->M_models->get_from_query("select b.* from bid a left join bid_detail b on b.id_bid=a.id_bid where id_perusahaan ='".$id_perusahaan."' and a.status = '0'  ");
		if($peru->num_rows()>0){
			$insert = true;

			foreach ($peru->result() as $key) {
				if($id_produk == $key->id_produk){
					$insert = false;
				}
			}

			if($insert){
				$id_bid = $peru->row()->id_bid;
				$bid_detail = array(
					'id_bid' => $id_bid,
					'id_produk' => $id_produk,
					'qty' => 0,
					'harga' => $harga,
					'tgl_add' => date('Y-m-d H:i:s'),
					'tgl_edit' => date('Y-m-d H:i:s'),
					'status' => '0'
				);
				$this->M_models->insertData('bid_detail',$bid_detail);
			}
			
			redirect(base_url("admin/delivery"));
		}else{
			$id_bid = $this->M_models->get_id('bid');
			$bid_array = array(
				'id_bid' => $id_bid,
				'id_perusahaan' => $id_perusahaan,
				'tgl_add' => date('Y-m-d H:i:s'),
				'tgl_edit' => date('Y-m-d H:i:s'),
				'status' => '0'
			);
			$this->M_models->insertData('bid',$bid_array);
			$bid_detail_array = array(
				'id_bid' => $id_bid,
				'id_produk' => $id_produk,
				'qty' => 0,
				'harga' => $harga,
				'tgl_add' => date('Y-m-d H:i:s'),
				'tgl_edit' => date('Y-m-d H:i:s'),
				'status' => '0'
			);
			$this->M_models->insertData('bid_detail',$bid_detail_array);
			redirect(base_url("admin/delivery"));
		}
	}

	public function delivery(){
		$this->load->model('market_model');
		$data['bid'] = $this->market_model->get_bid();
		$this->load->view('admin/delivery',$data);
	}
	public function update_delivery(){
		$qty = $this->input->post('qty');
		$id_bid = $this->input->post('id_bid');
		$id_produk = $this->input->post('id_produk');
		$data_edit['qty'] = $qty;
		$where_edit['id_bid'] = $id_bid;
		$where_edit['id_produk'] = $id_produk;
		$this->M_models->update_data('bid_detail',$data_edit,$where_edit);
		//redirect(base_url("admin/delivery"));
		die(json_encode(array('success' => 1)));
	}
	public function payment(){
		$this->load->model('market_model');
		$asd['payment'] = $this->market_model->bid_payment();
		$this->load->view('admin/payment',$asd);
	}
	public function finis(){
		$this->load->view('admin/finis');
	}

	function tambah_data(){
		for($a=0; $a<10; $a++){
			$id_user = $this->M_models->get_id('user');
			$data_user = array(
				'id_user' => $id_user,
				'password' => md5('123'),
				'email' => $a.'@gmail.com',
				'role' => '3',
				'status' => '1'
			);

			$this->M_models->insertData('user', $data_user);

			$id_petani = $this->M_models->get_id('u_petani');
			$data_petani = array(
				'id_petani' => $id_petani,
				'nama' => $a.'nama',
				'id_user' => $id_user,
				'id_kategori' => ''
			);

			$this->M_models->insertData('u_petani', $data_petani);

			for($b=0; $b<10; $b++){
				$id_kebun = $this->M_models->get_id('kebun');

				$data_kebun = array(
					'id_kebun' => $id_kebun,
					'id_petani' => $id_petani,
					'alamat' => $a.'-'.$b.'alamat',
					'luas' => ($a+$b),
					'kapasitas' => ($a+$b),
					'status' => '1'
				);

				$this->M_models->insertData('kebun', $data_kebun);

				for($c=0; $c<10; $c++){
					$id_produk = $this->M_models->get_id('produk');

					$data_produk = array(
						'id_produk' => $id_produk,
						'gambar' => '',
						'qty' => ($a+$b+$c),
						'id_kebun' => $id_kebun,
						'status' => '1',
						'tgl_add' => date('Y/m/d H:i:s'),
						'tgl_edit' => date('Y/m/d H:i:s')
					);

					$this->M_models->insertData('produk', $data_produk);
				}
			}
		}
	}

	public function send_mail0($nama='', $tujuan='', $subject='', $text_='') {
    //$nama = 'Ainul Yaqin';
    $config['protocol']    = 'smtp';
    $config['smtp_host']    = 'smtp.sendgrid.net';
    //$config['smtp_host']    = 'ssl://smtp.gmail.com';
    $config['smtp_port']    = '587';
    //$config['smtp_port']    = '465';
    $config['smtp_timeout'] = '10';
    $config['smtp_user']    = 'apikey';
    $config['smtp_pass']    = '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';
    $config['charset']    = 'utf-8';
    $config['newline']    = "\r\n";
    $config['crlf'] = "\r\n";
    $config['mailtype'] = 'text'; // or html
    $config['validation'] = TRUE; // bool whether to validate email or not      

    $this->email->initialize($config);
    $this->email->set_mailtype("html");
    $this->email->from('support@impact.com', 'Impact.com');
    //$this->email->to('cnacorp36@gmail.com');
    $this->email->to($tujuan);
    //$this->email->to('cnacorp36@gmail.com');

    //$this->email->subject('Salam hangat dari tim TabunganAsa.com');
    $this->email->subject($subject);
    
    $text = '
      <!doctype html>
			  <html>
			    <head>
			      <meta name="viewport" content="width=device-width" />
			      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			      <title>IMPACT</title>
			      <style>
			        /* -------------------------------------
			            GLOBAL RESETS
			        ------------------------------------- */
			        img {
			          border: none;
			          -ms-interpolation-mode: bicubic;
			          max-width: 100%; }

			        body {
			          background-color: #f6f6f6;
			          font-family: sans-serif;
			          -webkit-font-smoothing: antialiased;
			          font-size: 14px;
			          line-height: 1.4;
			          margin: 0;
			          padding: 0;
			          -ms-text-size-adjust: 100%;
			          -webkit-text-size-adjust: 100%; }

			        table {
			          border-collapse: separate;
			          mso-table-lspace: 0pt;
			          mso-table-rspace: 0pt;
			          width: 100%; }
			          table td {
			            font-family: sans-serif;
			            font-size: 14px;
			            vertical-align: top; }

			        /* -------------------------------------
			            BODY & CONTAINER
			        ------------------------------------- */

			        .body {
			          background-color: #f6f6f6;
			          width: 100%; }

			        /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
			        .container {
			          display: block;
			          Margin: 0 auto !important;
			          /* makes it centered */
			          max-width: 580px;
			          padding: 10px;
			          width: 580px; }

			        /* This should also be a block element, so that it will fill 100% of the .container */
			        .content {
			          box-sizing: border-box;
			          display: block;
			          Margin: 0 auto;
			          max-width: 580px;
			          padding: 10px; }

			        /* -------------------------------------
			            HEADER, FOOTER, MAIN
			        ------------------------------------- */
			        .main {
			          background: #ffffff;
			          border-radius: 3px;
			          width: 100%; }

			        .wrapper {
			          box-sizing: border-box;
			          padding: 20px; }

			        .content-block {
			          padding-bottom: 10px;
			          padding-top: 10px;
			        }

			        .footer {
			          clear: both;
			          Margin-top: 10px;
			          text-align: center;
			          width: 100%; }
			          .footer td,
			          .footer p,
			          .footer span,
			          .footer a {
			            color: #999999;
			            font-size: 12px;
			            text-align: center; }

			        /* -------------------------------------
			            TYPOGRAPHY
			        ------------------------------------- */
			        h1,
			        h2,
			        h3,
			        h4 {
			          color: #000000;
			          font-family: sans-serif;
			          font-weight: 400;
			          line-height: 1.4;
			          margin: 0;
			          margin-bottom: 30px; }

			        h1 {
			          font-size: 35px;
			          font-weight: 300;
			          text-align: center;
			          text-transform: capitalize; }

			        p,
			        ul,
			        ol {
			          font-family: sans-serif;
			          font-size: 14px;
			          font-weight: normal;
			          margin: 0;
			          margin-bottom: 15px; }
			          p li,
			          ul li,
			          ol li {
			            list-style-position: inside;
			            margin-left: 5px; }

			        a {
			          color: #3498db;
			          text-decoration: underline; }

			        /* -------------------------------------
			            BUTTONS
			        ------------------------------------- */
			        .btn {
			          box-sizing: border-box;
			          width: 100%; }
			          .btn > tbody > tr > td {
			            padding-bottom: 15px; }
			          .btn table {
			            width: auto; }
			          .btn table td {
			            background-color: #ffffff;
			            border-radius: 5px;
			            text-align: center; }
			          .btn a {
			            background-color: #ffffff;
			            border: solid 1px #3498db;
			            border-radius: 5px;
			            box-sizing: border-box;
			            color: #3498db;
			            cursor: pointer;
			            display: inline-block;
			            font-size: 14px;
			            font-weight: bold;
			            margin: 0;
			            padding: 12px 25px;
			            text-decoration: none;
			            text-transform: capitalize; }

			        .btn-primary table td {
			          background-color: #E53956; }

			        .btn-primary a {
			          background-color: #E53956;
			          border-color: #E53956;
			          color: #ffffff; }

			        .btn-primary1 table td {
			          background-color: #34ADFF; }

			        .btn-primary1 a {
			          background-color: #34ADFF;
			          border-color: #34ADFF;
			          color: #ffffff; }

			        .btn-primary2 table td {
			          background-color: #D17F2B; }

			        .btn-primary2 a {
			          background-color: #D17F2B;
			          border-color: #D17F2B;
			          color: #ffffff; }
			          

			        /* -------------------------------------
			            OTHER STYLES THAT MIGHT BE USEFUL
			        ------------------------------------- */
			        .last {
			          margin-bottom: 0; }

			        .first {
			          margin-top: 0; }

			        .align-center {
			          text-align: center; }

			        .align-right {
			          text-align: right; }

			        .align-left {
			          text-align: left; }

			        .clear {
			          clear: both; }

			        .mt0 {
			          margin-top: 0; }

			        .mb0 {
			          margin-bottom: 0; }

			        .preheader {
			          color: transparent;
			          display: none;
			          height: 0;
			          max-height: 0;
			          max-width: 0;
			          opacity: 0;
			          overflow: hidden;
			          mso-hide: all;
			          visibility: hidden;
			          width: 0; }

			        .powered-by a {
			          text-decoration: none; }

			        hr {
			          border: 0;
			          border-bottom: 1px solid #f6f6f6;
			          Margin: 20px 0; }

			        /* -------------------------------------
			            RESPONSIVE AND MOBILE FRIENDLY STYLES
			        ------------------------------------- */
			        @media only screen and (max-width: 620px) {
			          table[class=body] h1 {
			            font-size: 28px !important;
			            margin-bottom: 10px !important; }
			          table[class=body] p,
			          table[class=body] ul,
			          table[class=body] ol,
			          table[class=body] td,
			          table[class=body] span,
			          table[class=body] a {
			            font-size: 16px !important; }
			          table[class=body] .wrapper,
			          table[class=body] .article {
			            padding: 10px !important; }
			          table[class=body] .content {
			            padding: 0 !important; }
			          table[class=body] .container {
			            padding: 0 !important;
			            width: 100% !important; }
			          table[class=body] .main {
			            border-left-width: 0 !important;
			            border-radius: 0 !important;
			            border-right-width: 0 !important; }
			          table[class=body] .btn table {
			            width: 100% !important; }
			          table[class=body] .btn a {
			            width: 100% !important; }
			          table[class=body] .img-responsive {
			            height: auto !important;
			            max-width: 100% !important;
			            width: auto !important; }}

			        /* -------------------------------------
			            PRESERVE THESE STYLES IN THE HEAD
			        ------------------------------------- */
			        @media all {
			          .ExternalClass {
			            width: 100%; }
			          .ExternalClass,
			          .ExternalClass p,
			          .ExternalClass span,
			          .ExternalClass font,
			          .ExternalClass td,
			          .ExternalClass div {
			            line-height: 100%; }
			          .apple-link a {
			            color: inherit !important;
			            font-family: inherit !important;
			            font-size: inherit !important;
			            font-weight: inherit !important;
			            line-height: inherit !important;
			            text-decoration: none !important; }
			          .btn-primary table td:hover {
			            background-color: #34495e !important; }
			          .btn-primary a:hover {
			            background-color: #34495e !important;
			            border-color: #34495e !important; }
			          .btn-primary1 table td:hover {
			            background-color: #34495e !important; }
			          .btn-primary1 a:hover {
			            background-color: #34495e !important;
			            border-color: #34495e !important; }
			          .btn-primary2 table td:hover {
			            background-color: #34495e !important; }
			          .btn-primary2 a:hover {
			            background-color: #34495e !important;
			            border-color: #34495e !important; } }

			      </style>
			    </head>
			    <body class="">
			      <table border="0" cellpadding="0" cellspacing="0" class="body">
			        <tr>
			          <td>&nbsp;</td>
			          <td class="container">
			            <div class="content">

			              <!-- START CENTERED WHITE CONTAINER -->
			              <span class="preheader">Invoice Pembayaran</span>
			              <table class="main">

			                <!-- START MAIN CONTENT AREA -->
			                <tr>
			                  <td class="wrapper">
			                    <table border="0" cellpadding="0" cellspacing="0">
			                      <tr>
			                        <td>
			                          <table border="0" cellpadding="0" cellspacing="0" style="margin-bottom: 24px">
			                            <tbody>
			                              <tr>
			                                <td align="center">
			                                  <img style="width: 50%;" src="http://impact1.tabunganasa.com/impact/assets/images/banner/banner1.png"/>
			                                </td>
			                              </tr>
			                            </tbody>
			                          </table>
			                          '.$text_.'
			                          
			                        </td>
			                      </tr>
			                    </table>
			                  </td>
			                </tr>

			              <!-- END MAIN CONTENT AREA -->
			              </table>

			              <!-- START FOOTER -->
			              <div class="footer">
			                <table border="0" cellpadding="0" cellspacing="0">
			                  <tr>
			                    <td class="content-block">
			                      <span class="apple-link">Impact, Alamat</span>
			                      <!-- <br> Don\'t like these emails? <a href="http://i.imgur.com/CScmqnj.gif">Unsubscribe</a>. -->
			                    </td>
			                  </tr>
			                  <tr>
			                    <td class="content-block powered-by">
			                      Powered by <a href="http://htmlemail.io">HTMLemail</a>.
			                    </td>
			                  </tr>
			                </table>
			              </div>
			              <!-- END FOOTER -->

			            <!-- END CENTERED WHITE CONTAINER -->
			            </div>
			          </td>
			          <td>&nbsp;</td>
			        </tr>
			      </table>
			    </body>
			  </html>

    ';
    //echo $text;
    $this->email->message($text);  

    $this->email->send();
    
    //echo $this->email->print_debugger();
  }
}
