<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends CI_Controller {
	public $cek = null;

	function __construct(){
		parent::__construct();		
		$this->load->model('M_models');
		$this->cek =  $this->session->userdata('impact_sess');
		if(!$this->cek){
			redirect(base_url('login'));
		}
		$this->load->library('email');
	}
	public function index()
	{
		//$this->load->model('market_model');
		// $join = array(
		// 	0 => 'pembayaran p-p.id_po=po.id_po',
		// 	1 => 'u_perusahaan up-up.id_perusahaan=po.id_perusahaan',
		// 	2 => 'user u-u.id_user=up.id_user'
		// );
		// $where = array(
		// 	'p.status' => '0',
		// 	'u.email' => $this->cek['email']
		// );
		// $q_po = $this->M_models->get_data('po.*, p.total_bayar, p.kode_unik', 'purchase_order po', $join, $where);
		// $asd['po'] = $q_po;
		if($this->cek['role'] == '2'){
			$join0 = array(
				0 => 'bank b-b.id_bank=p.id_bank_tujuan',
				1 => 'purchase_order po-po.id_po=p.id_po',
				2 => 'u_perusahaan up-up.id_perusahaan=po.id_perusahaan',
				3 => 'user u-u.id_user=up.id_user'
			);
			$q_payment = $this->M_models->get_data('p.*, b.*', 'pembayaran p', $join0, array('u.email' => $this->cek['email']), '', 'p.tgl_add asc');

			$asd['payment'] = $q_payment;
			$asd['menu'] = 'payment';
			$this->load->view('admin/payment',$asd);
		}else{
			$join0 = array(
				0 => 'bank b-b.id_bank=p.id_bank_tujuan',
				1 => 'purchase_order po-po.id_po=p.id_po',
				2 => 'u_perusahaan up-up.id_perusahaan=po.id_perusahaan',
				3 => 'user u-u.id_user=up.id_user'
			);
			$q_payment = $this->M_models->get_data('p.*, b.*', 'pembayaran p', $join0, array('p.status_bayar' => '0'), '', 'p.tgl_add asc');

			$asd['payment'] = $q_payment;
			$asd['menu'] = 'payment';
			$this->load->view('real_admin/payment',$asd);
		}
		
	}

	public function update_bank_tujuan(){
		$id_bank_tujuan = $this->input->post('id_bank_tujuan');
		$id_pembayaran = $this->input->post('id_pembayaran');
		
		$data['id_bank_tujuan'] = $id_bank_tujuan;
		$where['id_pembayaran'] = $id_pembayaran;

		$this->M_models->updateData('pembayaran', $data, $where);

		die(json_encode(array('success' => 1)));
	}

	public function insert_payment(){
		$total_bayar = $this->input->post('total_bayar');
		$id_bank_tujuan = $this->input->post('id_bank_tujuan');
		$kode_unik = $this->input->post('kode_unik');
		$data_pembayaran = $this->input->post('data_pembayaran');
		$id_pembayaran = $this->M_models->get_id('pembayaran');

		$data_pembayaran1 = array(
			'id_pembayaran' => $id_pembayaran,
			'status_bayar' => '0',
			'total_bayar' => $total_bayar,
			'id_bank_tujuan' => $id_bank_tujuan,
			'id_bank_asal' => '',
			'atas_nama' => '',
			'tgl_trf' => date('Y/m/d H:i:s'),
			'keterangan' => '',
			'sisa_hutang' => $total_bayar,
			'kode_unik' => $kode_unik,
			'tgl_add' => date('Y/m/d H:i:s')
		);

		$this->M_models->insertData('pembayaran', $data_pembayaran1);

		for($a=0; $a<count($data_pembayaran); $a++){
			$data_pembayaran_detail = array(
				'id_pembayaran' => $id_pembayaran,
				'id_po' => $data_pembayaran[$a]['id_po'],
				'id_produk' => $data_pembayaran[$a]['id_produk']
			);

			$this->M_models->insertData('pembayaran_detail', $data_pembayaran_detail);

			$data_po = array('status_bayar' => '1');
			$where_po = array('id_purchase_order' => $data_pembayaran[$a]['id_po'], 'id_produk' => $data_pembayaran[$a]['id_produk']);

			$this->M_models->updateData('purchase_order_detail', $data_po, $where_po);
		}
		die(json_encode(array('success' => 1)));
	}

	public function load_bank(){
		$q_bank = $this->M_models->get_data('b.*', 'bank b', array(0 => 'user u-u.id_user=b.id_user'), array('b.status' => '1', 'u.role' => '0'), '', 'b.nama_bank asc');

		$data_bank = '';
		foreach ($q_bank->result() as $key) {
			$data_bank .= '<input type="radio" name="id_bank" onclick="change_bank(this.value)" value="'.$key->id_bank.'"><b style="margin-left: 12px">'.$key->nama_bank.'</b><p style="margin-left: 24px;">'.$key->norek.'</p><p style="margin-left: 24px; margin-top: -8px">'.$key->atas_nama.'</p><br>';
		}

		die(json_encode(array('success' => 1, 'data_bank' => $data_bank)));
	}
	public function my_company(){
		//$this->load->model('master_model');
		//$data['company'] = $this->master_model->company();
		//$data['company'] = $this->M_models->get_data('*', 'produk');
		$data['menu'] = 'company';
		$data['sess'] = $this->cek;
		$this->load->view('admin/my_company',$data);
	}
	public function market(){
		//$this->load->model('market_model');
		//$data['market'] = $this->market_model->market();
		$data['menu'] = 'market';
		$data['sess'] = $this->cek;
		$this->load->view('admin/market',$data);
	}

	public function submit_bid(){
		$email = $this->input->post('email');

		$join0[0][0] = 'u_perusahaan p';
		$join0[0][1] = 'p.id_user=u.id_user';
		$cek_email = $this->M_models->get_data('p.id_perusahaan', 'user u', $join0, array('u.status' => '1', 'u.role' => '2', 'u.email' => $email));
		if($cek_email->num_rows() == 0){
			die(json_encode(array('success' => 0, 'msg' => 'Invalid Request')));
		}

		//die(json_encode(array('success' => 0, 'msg' => $cek_email->row()->id_perusahaan)));

		$get_bid = $this->M_models->get_from_query("
			select b.id_bid, bd.id_produk
			from bid b
			left join bid_detail bd on bd.id_bid=b.id_bid
			where b.id_perusahaan='".$cek_email->row()->id_perusahaan."'
			and b.status='0' and bd.status='0'
		");

		foreach ($get_bid->result() as $key) {
			$data['status'] = '1';
			$where['id_bid'] = $key->id_bid;
			$where['id_produk'] = $key->id_produk;

			$this->M_models->updateData('bid_detail', $data, $where);
		}

		die(json_encode(array('success' => 1)));
	}

	public function delete_bid(){
		$id_bid = $this->input->post('id_bid');
		$id_produk = $this->input->post('id_produk');

		$cek_status_bid = $this->M_models->get_data('status', 'bid_detail', null, array('id_bid' => $id_bid, 'id_produk' => $id_produk));

		if($cek_status_bid->num_rows() == 0){
			die(json_encode(array('success' => 0, 'msg' => 'Invalid Request')));
		}

		if($cek_status_bid->row()->status == '0' || $cek_status_bid->row()->status == '5'){
			$data_where['id_bid'] = $id_bid;
			$data_where['id_produk'] = $id_produk;

			$this->M_models->deleteData('bid_detail', $data_where);

			if($this->M_models->get_data('id_bid', 'bid_detail', null, array('id_bid' => $id_bid))->num_rows() == 0){
				$data_where1['id_bid'] = $id_bid;
				$this->M_models->deleteData('bid', $data_where1);
			}
		}else if($cek_status_bid->row()->status == '1'){
			$data['status'] = '2';
			$data_where['id_bid'] = $id_bid;
			$data_where['id_produk'] = $id_produk;

			$this->M_models->updateData('bid_detail', $data, $data_where);
		}else{
			die(json_encode(array('success' => 0, 'msg' => 'Invalid Status')));
		}
		die(json_encode(array('success' => 1)));
	}

	public function edit_bid(){
		$id_bid = $this->input->post('id_bid');
		$id_produk = $this->input->post('id_produk');
		$qty = $this->input->post('qty');
		$harga = $this->input->post('harga');

		$data = array(
			'qty' => $qty,
			'harga' => $harga
		);

		$where = array(
			'id_bid' => $id_bid,
			'id_produk' => $id_produk
		);

		$this->M_models->updateData('bid_detail', $data, $where);
		die(json_encode(array('success' => 1, 'harga' => number_format($harga, 2))));
	}

	public function add_bid(){
		//die(json_encode(array('success' => 0, 'msg' => $this->input->post('harga'))));
		$email = $this->input->post('email');

		$join0[0][0] = 'u_perusahaan p';
		$join0[0][1] = 'p.id_user=u.id_user';
		$cek_email = $this->M_models->get_data('p.id_perusahaan', 'user u', $join0, array('u.status' => '1', 'u.role' => '2', 'u.email' => $email));
		if($cek_email->num_rows() == 0){
			die(json_encode(array('success' => 0, 'msg' => 'Invalid Request')));
		}
		$id_produk = $this->input->post('id_produk');
		$qty = $this->input->post('qty');
		$harga = $this->input->post('harga');

		// $join2[0] = array(
		// 	0 => 'bid b',
		// 	1 => 'b.id_bid=bd.id_bid'
		// );
		//$cek_produk = $this->M_models->get_data('b.id_bid', 'bid_detail bd', $join2, array('bd.id_produk' => $id_produk, 'b.id_perusahaan' => $cek_email->row()->id_perusahaan, 'b.status !=' => '0'));
		$cek_produk = $this->M_models->get_from_query("
			select b.id_bid, bd.status from bid_detail bd left join bid b on b.id_bid=bd.id_bid where bd.id_produk='".$id_produk."' and b.id_perusahaan='".$cek_email->row()->id_perusahaan."' and b.status != '1'
		");

		if($cek_produk->num_rows() > 0){

			$data_edit1['status'] = '0';
			$data_edit1['tgl_edit'] = date('Y/m/d H:i:s');
			$where_edit1['id_bid'] = $cek_produk->row()->id_bid;
			$this->M_models->updateData('bid', $data_edit1, $where_edit1);

			if($cek_produk->row()->status == '0' || $cek_produk->row()->status == '2' || $cek_produk->row()->status == '4' || $cek_produk->row()->status == '5'){

				$data_edit = array(
					'qty' => $qty,
					'harga' => $harga,
					'tgl_edit' => date('Y/m/d H:i:s'),
					'status' => '0'
				);

				$where_edit = array(
					'id_bid' => $cek_produk->row()->id_bid,
					'id_produk' => $id_produk
				);

				$this->M_models->updateData('bid_detail', $data_edit, $where_edit);
			}
			
			if($cek_produk->row()->status == '2' || $cek_produk->row()->status == '4' || $cek_produk->row()->status == '5'){
				$count_bid = $this->input->post('count_bid');
		
				$join1[0] = array(
					0 => 'kebun k',
					1 => 'k.id_kebun=p.id_kebun'
				);
				$join1[1] = array(
					0 => 'u_petani up',
					1 => 'up.id_petani=k.id_petani'
				);
				$q_return = $this->M_models->get_data('p.id_produk, up.nama, k.alamat, p.qty, p.gambar', 'produk p', $join1, array('p.id_produk' => $id_produk))->row();

				$data['data'] = '
					<tr>
						<td>'.$count_bid.'</td>
						<td><p>'.$q_return->nama.'</p>('.$q_return->alamat.')</td>
						<td>'.$q_return->qty.' Kg</td>
						<td id="qty-bid-'.$q_return->id_produk.'">'.$qty.' Kg</td>
						<td id="qty-harga-'.$q_return->id_produk.'">Rp. '.number_format($harga, 2).'</td>
						<td><img class="thumbnail" src="'.$q_return->gambar.'" style="width: 100px"/></td>
						<td id="btn_tr'.$count_bid.'">
		                    <button class="btn btn-primary" onclick="edit_bid('.$count_bid.', '.$q_return->qty.', '.$qty.', '.$harga.', \''.$id_produk.'\', \''.$cek_produk->row()->id_bid.'\')" type="button"><i class="fa fa-pencil"></i></button>
		                    <button class="btn btn-danger" onclick="delete_produk(\''.$id_produk.'\', \''.$cek_produk->row()->id_bid.'\')" type="button"><i class="fa fa-trash"></i></button>
		                </td>
					</tr>
				';
				$data['count_bid'] = $count_bid+1;

				die(json_encode(array('success' => 1, 'data' => $data)));
			}else if($cek_produk->row()->status == '0'){
				die(json_encode(array('success' => 2, 'qty' => $qty.' Kg', 'harga' => 'Rp. '.number_format($harga, 2))));
			}else{
				die(json_encode(array('success' => 0, 'msg' => 'Silahkan selesaikan transaksi Anda di menu "my_bid" sebelum bid lagi')));
			}
		}

		$cek_bid = $this->M_models->get_data('id_bid', 'bid', null, array('id_perusahaan' => $cek_email->row() ->id_perusahaan, 'status' => '0'));

		if($cek_bid->num_rows() > 0){
			$id_bid = $cek_bid->row()->id_bid;
		}else{
			$id_bid = $this->M_models->get_id('bid');
			$data_bid = array(
				'id_bid' => $id_bid,
				'id_pabrik' => '',
				'status' => '0',
				'tgl_add' => date('Y/m/d H:i:s'),
				'tgl_edit' => date('Y/m/d H:i:s'),
				'use_deliveries' => '0',
				'id_perusahaan' => $cek_email->row()->id_perusahaan
			);

			$this->M_models->insertData('bid', $data_bid);
		}

		

		$data_bid_detail = array(
			'id_bid' => $id_bid,
			'id_produk' => $id_produk,
			'qty' => $qty,
			'harga' => $harga,
			'tgl_add' => date('Y/m/d H:i:s'),
			'tgl_edit' => date('Y/m/d H:i:s'),
			'status' => '0',
			'harga_delivery' => 0,
			'jarak' => 0
		);

		$this->M_models->insertData('bid_detail', $data_bid_detail);

		//get data produk
		$count_bid = $this->input->post('count_bid');
		
		$join1[0] = array(
			0 => 'kebun k',
			1 => 'k.id_kebun=p.id_kebun'
		);
		$join1[1] = array(
			0 => 'u_petani up',
			1 => 'up.id_petani=k.id_petani'
		);
		$q_return = $this->M_models->get_data('p.id_produk, up.nama, k.alamat, p.qty, p.gambar', 'produk p', $join1, array('p.id_produk' => $id_produk))->row();

		$data['data'] = '
			<tr>
				<td>'.$count_bid.'</td>
				<td><p>'.$q_return->nama.'</p>('.$q_return->alamat.')</td>
				<td>'.$q_return->qty.' Kg</td>
				<td id="qty-bid-'.$q_return->id_produk.'">'.$qty.' Kg</td>
				<td id="qty-harga-'.$q_return->id_produk.'">Rp. '.number_format($harga, 2).'</td>
				<td><img class="thumbnail" src="'.$q_return->gambar.'" style="width: 100px"/></td>
				<td id="btn_tr'.$count_bid.'">
                    <button class="btn btn-primary" onclick="edit_bid('.$count_bid.', '.$q_return->qty.', '.$qty.', '.$harga.', \''.$id_produk.'\', \''.$id_bid.'\')" type="button"><i class="fa fa-pencil"></i></button>
                    <button class="btn btn-danger" onclick="delete_produk(\''.$id_produk.'\', \''.$id_bid.'\')" type="button"><i class="fa fa-trash"></i></button>
                </td>
			</tr>
		';
		$data['count_bid'] = $count_bid+1;

		die(json_encode(array('success' => 1, 'data' => $data)));
	}

	public function load_more_produk(){
		$loaded = $this->input->post('loaded');

		$i = $loaded + 1;
        $join[0] = array(
            0 => 'kebun k',
            1 => 'k.id_kebun=p.id_kebun'
        );
        $join[1] = array(
            0 => 'u_petani p0',
            1 => 'p0.id_petani=k.id_petani'
        );
        $join[2] = array(
            0 => 'user u',
            1 => 'u.id_user=p0.id_user'
        );

        $count_market = $this->M_models->get_data('p0.nama', 'produk p', $join, array('u.status' => '1', 'k.status' => '1', 'p.status' => '1', 'p.qty >' => 0), '', 'p.id_produk')->num_rows();

        $market = $this->M_models->get_data('p.id_produk, p0.nama, p.qty, p.gambar, p.tgl_edit, k.alamat', 'produk p', $join, array('u.status' => '1', 'k.status' => '1', 'p.status' => '1', 'p.qty >' => 0), '', 'p.id_produk', 20, $loaded);

        $data = '';
        foreach ($market->result() as $a) {
        	$data .= '
        		<tr id="tr'.$i.'">
                    <td>'.$i.'</td>
                    <td>
                        <a href="'.base_url().'petani" ><p><'.$a->nama.'</p>('.$a->alamat.')
                        <input type="hidden" id="id_produk'.$i.'" value="'.$a->id_produk.'">
                    </td>
                    <td>'.$a->qty.' Kg<input type="hidden" id="qty_'.$i.'" value="'.$a->qty.'"/></td>
                    <td><img class="thumbnail" src="'.$a->gambar.'"/></td>
                    <td>
                        <button type="button" class="btn btn-info" onclick="bid('.$i.')">BID</button>
                    </td>
                </tr>
        	';
        	$i++;
        }

        die(json_encode(array('data' => $data, 'loaded' => ($i-1), 'now_loaded' => $market->num_rows(), 'param_load_more' => ($count_market > ($loaded + 20)))));
	}

	public function add_new_delivery($id_produk){
		$q = $this->M_models->get_from_query("select k.harga from produk p left join sawit_kelas k on k.id=p.id_kelas where p.id_produk='".$id_produk."'");
		$harga = $q->row()->harga;

		$email = $this->session->userdata('email');
		$abc = $this->M_models->get_from_query("select a.*,b.id_perusahaan from user a left join u_perusahaan b on b.id_user = a.id_user where a.email = '".$email."' ");
		$id_perusahaan = $abc->row()->id_perusahaan;
		$peru = $this->M_models->get_from_query("select b.* from bid a left join bid_detail b on b.id_bid=a.id_bid where id_perusahaan ='".$id_perusahaan."' and a.status = '0'  ");
		if($peru->num_rows()>0){
			$insert = true;

			foreach ($peru->result() as $key) {
				if($id_produk == $key->id_produk){
					$insert = false;
				}
			}

			if($insert){
				$id_bid = $peru->row()->id_bid;
				$bid_detail = array(
					'id_bid' => $id_bid,
					'id_produk' => $id_produk,
					'qty' => 0,
					'harga' => $harga,
					'tgl_add' => date('Y-m-d H:i:s'),
					'tgl_edit' => date('Y-m-d H:i:s'),
					'status' => '0'
				);
				$this->M_models->insertData('bid_detail',$bid_detail);
			}
			
			redirect(base_url("admin/delivery"));
		}else{
			$id_bid = $this->M_models->get_id('bid');
			$bid_array = array(
				'id_bid' => $id_bid,
				'id_perusahaan' => $id_perusahaan,
				'tgl_add' => date('Y-m-d H:i:s'),
				'tgl_edit' => date('Y-m-d H:i:s'),
				'status' => '0'
			);
			$this->M_models->insertData('bid',$bid_array);
			$bid_detail_array = array(
				'id_bid' => $id_bid,
				'id_produk' => $id_produk,
				'qty' => 0,
				'harga' => $harga,
				'tgl_add' => date('Y-m-d H:i:s'),
				'tgl_edit' => date('Y-m-d H:i:s'),
				'status' => '0'
			);
			$this->M_models->insertData('bid_detail',$bid_detail_array);
			redirect(base_url("admin/delivery"));
		}
	}

	public function delivery(){
		$this->load->model('market_model');
		$data['bid'] = $this->market_model->get_bid();
		$this->load->view('admin/delivery',$data);
	}
	public function update_delivery(){
		$qty = $this->input->post('qty');
		$id_bid = $this->input->post('id_bid');
		$id_produk = $this->input->post('id_produk');
		$data_edit['qty'] = $qty;
		$where_edit['id_bid'] = $id_bid;
		$where_edit['id_produk'] = $id_produk;
		$this->M_models->update_data('bid_detail',$data_edit,$where_edit);
		//redirect(base_url("admin/delivery"));
		die(json_encode(array('success' => 1)));
	}
	public function payment(){
		$this->load->model('market_model');
		$asd['payment'] = $this->market_model->bid_payment();
		$this->load->view('admin/payment',$asd);
	}
	public function finis(){
		$this->load->view('admin/finis');
	}

	function tambah_data(){
		for($a=0; $a<10; $a++){
			$id_user = $this->M_models->get_id('user');
			$data_user = array(
				'id_user' => $id_user,
				'password' => md5('123'),
				'email' => $a.'@gmail.com',
				'role' => '3',
				'status' => '1'
			);

			$this->M_models->insertData('user', $data_user);

			$id_petani = $this->M_models->get_id('u_petani');
			$data_petani = array(
				'id_petani' => $id_petani,
				'nama' => $a.'nama',
				'id_user' => $id_user,
				'id_kategori' => ''
			);

			$this->M_models->insertData('u_petani', $data_petani);

			for($b=0; $b<10; $b++){
				$id_kebun = $this->M_models->get_id('kebun');

				$data_kebun = array(
					'id_kebun' => $id_kebun,
					'id_petani' => $id_petani,
					'alamat' => $a.'-'.$b.'alamat',
					'luas' => ($a+$b),
					'kapasitas' => ($a+$b),
					'status' => '1'
				);

				$this->M_models->insertData('kebun', $data_kebun);

				for($c=0; $c<10; $c++){
					$id_produk = $this->M_models->get_id('produk');

					$data_produk = array(
						'id_produk' => $id_produk,
						'gambar' => '',
						'qty' => ($a+$b+$c),
						'id_kebun' => $id_kebun,
						'status' => '1',
						'tgl_add' => date('Y/m/d H:i:s'),
						'tgl_edit' => date('Y/m/d H:i:s')
					);

					$this->M_models->insertData('produk', $data_produk);
				}
			}
		}
	}

	public function send_mail1($nama='', $tujuan='', $subject='', $text='') {
    $nama = 'Ainul Yaqin';
    $config['protocol']    = 'smtp';
    $config['smtp_host']    = 'smtp.sendgrid.net';
    //$config['smtp_host']    = 'ssl://smtp.gmail.com';
    $config['smtp_port']    = '587';
    //$config['smtp_port']    = '465';
    $config['smtp_timeout'] = '10';
    $config['smtp_user']    = 'apikey';
    $config['smtp_pass']    = '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';
    $config['charset']    = 'utf-8';
    $config['newline']    = "\r\n";
    $config['crlf'] = "\r\n";
    $config['mailtype'] = 'text'; // or html
    $config['validation'] = TRUE; // bool whether to validate email or not      

    $this->email->initialize($config);
    $this->email->set_mailtype("html");
    $this->email->from('support@tabunganasa.com', 'TabunganAsa.com');
    $this->email->to('cnacorp36@gmail.com');
    //$this->email->to('cnacorp36@gmail.com');

    $this->email->subject('Salam hangat dari tim TabunganAsa.com');
    
    $text = '
      <!doctype html>
        <html>
          <head>
            <meta name="viewport" content="width=device-width" />
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>Tabungan-Asa.com</title>
            <style>
              /* -------------------------------------
                  GLOBAL RESETS
              ------------------------------------- */
              img {
                border: none;
                -ms-interpolation-mode: bicubic;
                max-width: 100%; }

              body {
                background-color: #f6f6f6;
                font-family: sans-serif;
                -webkit-font-smoothing: antialiased;
                font-size: 14px;
                line-height: 1.4;
                margin: 0;
                padding: 0;
                -ms-text-size-adjust: 100%;
                -webkit-text-size-adjust: 100%; }

              table {
                border-collapse: separate;
                mso-table-lspace: 0pt;
                mso-table-rspace: 0pt;
                width: 100%; }
                table td {
                  font-family: sans-serif;
                  font-size: 14px;
                  vertical-align: top; }

              /* -------------------------------------
                  BODY & CONTAINER
              ------------------------------------- */

              .body {
                background-color: #f6f6f6;
                width: 100%; }

              /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
              .container {
                display: block;
                Margin: 0 auto !important;
                /* makes it centered */
                max-width: 580px;
                padding: 10px;
                width: 580px; }

              /* This should also be a block element, so that it will fill 100% of the .container */
              .content {
                box-sizing: border-box;
                display: block;
                Margin: 0 auto;
                max-width: 580px;
                padding: 10px; }

              /* -------------------------------------
                  HEADER, FOOTER, MAIN
              ------------------------------------- */
              .main {
                background: #ffffff;
                border-radius: 3px;
                width: 100%; }

              .wrapper {
                box-sizing: border-box;
                padding: 20px; }

              .content-block {
                padding-bottom: 10px;
                padding-top: 10px;
              }

              .footer {
                clear: both;
                Margin-top: 10px;
                text-align: center;
                width: 100%; }
                .footer td,
                .footer p,
                .footer span,
                .footer a {
                  color: #999999;
                  font-size: 12px;
                  text-align: center; }

              /* -------------------------------------
                  TYPOGRAPHY
              ------------------------------------- */
              h1,
              h2,
              h3,
              h4 {
                color: #000000;
                font-family: sans-serif;
                font-weight: 400;
                line-height: 1.4;
                margin: 0;
                margin-bottom: 30px; }

              h1 {
                font-size: 35px;
                font-weight: 300;
                text-align: center;
                text-transform: capitalize; }

              p,
              ul,
              ol {
                font-family: sans-serif;
                font-size: 14px;
                font-weight: normal;
                margin: 0;
                margin-bottom: 15px; }
                p li,
                ul li,
                ol li {
                  list-style-position: inside;
                  margin-left: 5px; }

              a {
                color: #3498db;
                text-decoration: underline; }

              /* -------------------------------------
                  BUTTONS
              ------------------------------------- */
              .btn {
                box-sizing: border-box;
                width: 100%; }
                .btn > tbody > tr > td {
                  padding-bottom: 15px; }
                .btn table {
                  width: auto; }
                .btn table td {
                  background-color: #ffffff;
                  border-radius: 5px;
                  text-align: center; }
                .btn a {
                  background-color: #ffffff;
                  border: solid 1px #3498db;
                  border-radius: 5px;
                  box-sizing: border-box;
                  color: #3498db;
                  cursor: pointer;
                  display: inline-block;
                  font-size: 14px;
                  font-weight: bold;
                  margin: 0;
                  padding: 12px 25px;
                  text-decoration: none;
                  text-transform: capitalize; }

              .btn-primary table td {
                background-color: #E53956; }

              .btn-primary a {
                background-color: #E53956;
                border-color: #E53956;
                color: #ffffff; }

              .btn-primary1 table td {
                background-color: #34ADFF; }

              .btn-primary1 a {
                background-color: #34ADFF;
                border-color: #34ADFF;
                color: #ffffff; }

              .btn-primary2 table td {
                background-color: #D17F2B; }

              .btn-primary2 a {
                background-color: #D17F2B;
                border-color: #D17F2B;
                color: #ffffff; }
                

              /* -------------------------------------
                  OTHER STYLES THAT MIGHT BE USEFUL
              ------------------------------------- */
              .last {
                margin-bottom: 0; }

              .first {
                margin-top: 0; }

              .align-center {
                text-align: center; }

              .align-right {
                text-align: right; }

              .align-left {
                text-align: left; }

              .clear {
                clear: both; }

              .mt0 {
                margin-top: 0; }

              .mb0 {
                margin-bottom: 0; }

              .preheader {
                color: transparent;
                display: none;
                height: 0;
                max-height: 0;
                max-width: 0;
                opacity: 0;
                overflow: hidden;
                mso-hide: all;
                visibility: hidden;
                width: 0; }

              .powered-by a {
                text-decoration: none; }

              hr {
                border: 0;
                border-bottom: 1px solid #f6f6f6;
                Margin: 20px 0; }

              /* -------------------------------------
                  RESPONSIVE AND MOBILE FRIENDLY STYLES
              ------------------------------------- */
              @media only screen and (max-width: 620px) {
                table[class=body] h1 {
                  font-size: 28px !important;
                  margin-bottom: 10px !important; }
                table[class=body] p,
                table[class=body] ul,
                table[class=body] ol,
                table[class=body] td,
                table[class=body] span,
                table[class=body] a {
                  font-size: 16px !important; }
                table[class=body] .wrapper,
                table[class=body] .article {
                  padding: 10px !important; }
                table[class=body] .content {
                  padding: 0 !important; }
                table[class=body] .container {
                  padding: 0 !important;
                  width: 100% !important; }
                table[class=body] .main {
                  border-left-width: 0 !important;
                  border-radius: 0 !important;
                  border-right-width: 0 !important; }
                table[class=body] .btn table {
                  width: 100% !important; }
                table[class=body] .btn a {
                  width: 100% !important; }
                table[class=body] .img-responsive {
                  height: auto !important;
                  max-width: 100% !important;
                  width: auto !important; }}

              /* -------------------------------------
                  PRESERVE THESE STYLES IN THE HEAD
              ------------------------------------- */
              @media all {
                .ExternalClass {
                  width: 100%; }
                .ExternalClass,
                .ExternalClass p,
                .ExternalClass span,
                .ExternalClass font,
                .ExternalClass td,
                .ExternalClass div {
                  line-height: 100%; }
                .apple-link a {
                  color: inherit !important;
                  font-family: inherit !important;
                  font-size: inherit !important;
                  font-weight: inherit !important;
                  line-height: inherit !important;
                  text-decoration: none !important; }
                .btn-primary table td:hover {
                  background-color: #34495e !important; }
                .btn-primary a:hover {
                  background-color: #34495e !important;
                  border-color: #34495e !important; }
                .btn-primary1 table td:hover {
                  background-color: #34495e !important; }
                .btn-primary1 a:hover {
                  background-color: #34495e !important;
                  border-color: #34495e !important; }
                .btn-primary2 table td:hover {
                  background-color: #34495e !important; }
                .btn-primary2 a:hover {
                  background-color: #34495e !important;
                  border-color: #34495e !important; } }

              .flex-container {
                display: flex;
                flex-wrap: nowrap;
                background-color: DodgerBlue;
              }

              .flex-container > table {
                background-color: #f1f1f1;
                width: 100px;
                margin: 10px;
                text-align: center;
                line-height: 75px;
                font-size: 30px;
              }
            </style>
          </head>
          <body class="">
            <table border="0" cellpadding="0" cellspacing="0" class="body">
              <tr>
                <td>&nbsp;</td>
                <td class="container">
                  <div class="content">

                    <!-- START CENTERED WHITE CONTAINER -->
                    <span class="preheader">Pesan dari TabunganAsa.com</span>
                    <table class="main">

                      <!-- START MAIN CONTENT AREA -->
                      <tr>
                        <td class="wrapper">
                          <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                              <td>
                                <table border="0" cellpadding="0" cellspacing="0" style="margin-bottom: 24px">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <img style="width: 50%;" src="https://www.tabunganasa.com/t_back_end/assets/images/logo_a3.png"/>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                                <p style="text-align: center; font-size: 18px"><b style="color: #E53956">Welcome on board !</b></p><br>
                                <p>Anda menerima pesan ini karena telah registrasi di TabunganAsa.com.</p>
                                <p>Tim TabunganAsa.com baru saja memverifikasi pendaftaran Komunitas <br><b style="color: #E53956">EduChild Initiative Community</b></p>
                                <p>TabunganAsa.com berkomitmen untuk membantu memaksimalkan value dari tiap kegiatan komunitas melalui fitur yang tersedia</p><br>
                                
                                <table border="0" cellpadding="0" cellspacing="0" style="margin-bottom: 24px; box-sizing: border-box; width: 100%; text-align: center; ">
                                  <tbody>
                                    <tr>
                                      <td align="left">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                          <tbody>
                                            <tr>
                                              <td> 
                                                <img style="width: 100%;" src="https://www.tabunganasa.com/t_back_end/assets/images/brand_design/community_page.jpg"/> 
                                              </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>

                                <table border="0" cellpadding="0" cellspacing="0" class="btn btn-primary" style="margin-bottom: 24px; margin-top: -12px">
                                  <tbody>
                                    <tr>
                                      <td align="left">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                          <tbody>
                                            <tr>
                                              <td> <a href="https://www.tabunganasa.com/community-page?community=EduChildInitiativeCommunity" target="_blank">Lihat Community Page</a> </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>

                                <table border="0" cellpadding="0" cellspacing="0" style="margin-bottom: 24px; box-sizing: border-box; width: 100%; text-align: center; ">
                                  <tbody>
                                    <tr>
                                      <td align="left">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                          <tbody>
                                            <tr>
                                              <td> 
                                                <img style="width: 100%;" src="https://www.tabunganasa.com/t_back_end/assets/images/brand_design/proyek_asa.jpg"/> 
                                              </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>

                                <table border="0" cellpadding="0" cellspacing="0" class="btn btn-primary" style="margin-bottom: 24px; margin-top: -12px">
                                  <tbody>
                                    <tr>
                                      <td align="left">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                          <tbody>
                                            <tr>
                                              <td> <a href="https://www.tabunganasa.com/halaman-galang-dana" target="_blank">Buat Proyek Asa</a> </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>

                                <table border="0" cellpadding="0" cellspacing="0" style="margin-bottom: 24px; box-sizing: border-box; width: 100%; text-align: center; ">
                                  <tbody>
                                    <tr>
                                      <td align="left">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                          <tbody>
                                            <tr>
                                              <td> 
                                                <img style="width: 100%;" src="https://www.tabunganasa.com/t_back_end/assets/images/brand_design/event.jpg"/> 
                                              </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>

                                <table border="0" cellpadding="0" cellspacing="0" class="btn btn-primary" style="margin-bottom: 24px; margin-top: -12px">
                                  <tbody>
                                    <tr>
                                      <td align="left">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                          <tbody>
                                            <tr>
                                              <td> <a href="https://www.tabunganasa.com/events/create" target="_blank">Buat Event</a> </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>

                                <table border="0" cellpadding="0" cellspacing="0" style="margin-bottom: 24px; box-sizing: border-box; width: 100%; text-align: center; ">
                                  <tbody>
                                    <tr>
                                      <td align="left">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                          <tbody>
                                            <tr>
                                              <td> 
                                                <img style="width: 100%;" src="https://www.tabunganasa.com/t_back_end/assets/images/brand_design/publikasi.jpg"/> 
                                              </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>

                                <table border="0" cellpadding="0" cellspacing="0" class="btn btn-primary" style="margin-bottom: 24px; margin-top: -12px">
                                  <tbody>
                                    <tr>
                                      <td align="left">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                          <tbody>
                                            <tr>
                                              <td> <a href="https://www.tabunganasa.com/guide/publishing" target="_blank">Pelajari cara publish tulisan</a> </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>

                                <p>Kakak punya saran fitur keren ?. Silahkan beritahu kami melalui social media kami :) </p>
                                <p>Kind Regards,</p>
                                <p>Tim TabunganAsa.com</p><br>
                                <table border="0" cellpadding="0" cellspacing="0" style="margin-bottom: 24px">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <p><h3>Temukan kami di</h3></p>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>

                                <table border="0" cellpadding="0" cellspacing="0" style="margin-bottom: 24px">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <a href="https://web.facebook.com/TabunganAsaIndonesia/" target="_blank"><img style="width: 58px" src="https://www.tabunganasa.com/t_back_end/assets/images/umum/fb.png"/></a>
                                      </td>
                                      <td align="center">
                                        <a href="https://www.instagram.com/tabungan_asa/?hl=id" target="_blank"><img style="width: 58px" src="https://www.tabunganasa.com/t_back_end/assets/images/umum/ig.png"/></a>
                                      </td>
                                      <td align="center">
                                        <a href="https://api.whatsapp.com/send?phone=6281382792182&text=Hai" target="_blank"><img style="width: 58px" src="https://www.tabunganasa.com/t_back_end/assets/images/umum/wa.png"/></a>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                                <p>Terima kasih sebelumnya. Kontribusi yang telah kakak berikan akan sangat membantu tim TabunganAsa.com :)</p>
                                
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>

                    <!-- END MAIN CONTENT AREA -->
                    </table>

                    <!-- START FOOTER -->
                    <div class="footer">
                      <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                          <td class="content-block">
                            <span class="apple-link">Tabungan Asa, Jl. Perintis Kemerdekaan Km.6</span>
                            <!-- <br> Don\'t like these emails? <a href="http://i.imgur.com/CScmqnj.gif">Unsubscribe</a>. -->
                          </td>
                        </tr>
                        <tr>
                          <td class="content-block powered-by">
                            Powered by <a href="http://htmlemail.io">HTMLemail</a>.
                          </td>
                        </tr>
                      </table>
                    </div>
                    <!-- END FOOTER -->

                  <!-- END CENTERED WHITE CONTAINER -->
                  </div>
                </td>
                <td>&nbsp;</td>
              </tr>
            </table>
          </body>
        </html>

    ';
    $this->email->message($text);  

    $this->email->send();
    
    //echo $this->email->print_debugger();
  }
}
