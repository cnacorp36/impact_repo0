<?php $this->load->view('backend/header')?>
<?php $this->load->view('backend/navbar')?>
<div class="notika-status-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="normal-table-list mg-t-30">
                        <div class="basic-tb-hd">
                            <h2 style="text-align: center;">KATEGORI SAWIT</h2>
                            <!-- <p>Tables with borders on all possible sides of the Table and Cells (<code>.table-bordered</code>).</p> -->
                            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModalone"><i class="fa fa-plus"> Add New</i></button>
                        </div>
                        <div class="bsc-tbl-bdr">
                            <?php 
                            if($kategori != false){
                            ?>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nama Kategori</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        foreach ($kategori as $key) {
                                            ?>
                                            <tr>
                                        <td><?php echo $key->id_kategori?></td>
                                        <td><?php echo $key->nama?></td>
                                        <td>
                                            <button class="btn btn-default"><i class="fa fa-edit"> edit</i></button>
                                            <button class="btn btn-default"><i class="fa fa-trash"> hapus</i></button>
                                        </td>
                                    </tr
                                            <?php
                                        }
                                    ?>
                                    >
                                    
                                </tbody>
                            </table>
                            <?php
                            }
                            ?>
                            
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal -->
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="modals-single">
                            <div class="modals-default-cl">
                                <div class="modal fade" id="myModalone" role="dialog">
                                    <div class="modal-dialog modals-default">
                                        <div class="modal-content">
                                            <form class="form-horizontal" action="<?php echo base_url('master/add_kategory')?>" method="post">
                                    <div class="modal-body">
                                    <div class="cmp-tb-hd cmp-int-hd">
                                        <h2>Category Sawit</h2>
                                    </div>
                                    <div class="form-example-int form-horizental">
                                        <div class="form-group">
                                            
                                            <div class="row">
                                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-12">
                                                    <label class="hrzn-fm">Name Category</label>
                                                </div>
                                                <div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
                                                    <div class="nk-int-st">
                                                        <input type="text" name="nama" class="form-control input-sm" placeholder="Name Category">
                                                    </div>
                                                </div>
                                            </div>
                                        
                                            </div>
                                        </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-default">Save</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <!-- End Modal -->
        </div>
    </div>
<?php $this->load->view('backend/footer')?>
<!-- <script>
        $(function(){
            // Success Type
            $('#ts-success').on('click', function() {
                toastr.success('Have fun storming the castle!', 'Miracle Max Says');
            });

            // Success Type
            $('#ts-info').on('click', function() {
                toastr.info('We do have the Kapua suite available.', 'Turtle Bay Resort');
            });

            // Success Type
            $('#ts-warning').on('click', function() {
                toastr.warning('My name is Inigo Montoya. You killed my father, prepare to die!');
            });

            // Success Type
            $('#ts-error').on('click', function() {
                toastr.error('I do not think that word means what you think it means.', 'Inconceivable!');
            });
        });
    </script> -->