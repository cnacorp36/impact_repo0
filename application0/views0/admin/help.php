 <?php $this->load->view('backend/header')?>
<?php $this->load->view('backend/navbar')?>
 <div class="breadcomb-area">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="breadcomb-list">
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="breadcomb-wp">
                  <div class="breadcomb-icon">
                    <i class="notika-icon notika-map"></i>
                  </div>
                  <div class="breadcomb-ctn">
                    <h2>HELP</h2>
                    <p>KEPALA SAWIT</span></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
 <div class="contact-area">
        <div class="container">
            <div class="row">   
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <h3 style="text-align: center;">MY COMPANY</h3>
                    <div class="contact-list">
                        <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                                        <!-- <label class="hrzn-fm">Nama Kebun/PT :</label> -->
                                    </div>
                                    <div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
                                        <div class="nk-int-st">
                                            <input type="text" name="nama" class="form-control" readonly="readonly" placeholder="Nama Kebun/PT" value="PT IWEN SAWIT">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                                        <!-- <label class="hrzn-fm">Alamat :</label> -->
                                    </div>
                                    <div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
                                        <div class="nk-int-st">
                                            <input type="text" name="alamat" readonly="readonly" class="form-control" placeholder="Alamat" value="jambi indonesia">
                                        </div>
                                    </div>
                                </div>
                            </div>
                             <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                                        <!-- <label class="hrzn-fm"> No Telp :</label> -->
                                    </div>
                                    <div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
                                        <div class="nk-int-st">
                                            <input type="text" name="no_tlp" class="form-control input-sm" placeholder="No Telp">
                                        </div>
                                    </div>
                                </div>
                            </div>
                             <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                                        <!-- <label class="hrzn-fm"> Email :</label> -->
                                    </div>
                                    <div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
                                        <div class="nk-int-st">
                                            <input type="text" name="email" class="form-control input-sm" placeholder="Email">
                                        </div>
                                    </div>
                                </div>
                            </div>
                             <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                                        <!-- <label class="hrzn-fm">Pesan :</label> -->
                                    </div>
                                    <div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
                                        <div class="nk-int-st">
                                            <input type="text" name="pesan" class="form-control input-sm" placeholder="Luas" value="50">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                                        <!-- <label class="hrzn-fm">Pesan :</label> -->
                                    </div>
                                    <div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
                                        <div class="nk-int-st">
                                            <textarea type="textarea" name="pesan" class="form-control input-sm" placeholder="Pesan"></textarea> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div><!-- end from -->
                </div>
                <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12">
                    <h3 style="text-align: center;">KONTAK KAMI</h3>
                    <div class="contact-list">
                        <form class="form-horizontal" action="<?php echo base_url('petani/add_new_kontak')?>" method="post">
                        <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                                        <!-- <label class="hrzn-fm">Nama Kebun/PT :</label> -->
                                    </div>
                                    <div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
                                        <div class="nk-int-st">
                                            <input type="text" name="nama" class="form-control input-sm" placeholder="Nama Kebun/PT">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                                        <!-- <label class="hrzn-fm">Alamat :</label> -->
                                    </div>
                                    <div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
                                        <div class="nk-int-st">
                                            <input type="text" name="alamat" class="form-control input-sm" placeholder="Alamat">
                                        </div>
                                    </div>
                                </div>
                            </div>
                             <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                                        <!-- <label class="hrzn-fm"> No Telp :</label> -->
                                    </div>
                                    <div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
                                        <div class="nk-int-st">
                                            <input type="text" name="no_tlp" class="form-control input-sm" placeholder="No Telp">
                                        </div>
                                    </div>
                                </div>
                            </div>
                             <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                                        <!-- <label class="hrzn-fm"> Email :</label> -->
                                    </div>
                                    <div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
                                        <div class="nk-int-st">
                                            <input type="text" name="email" class="form-control input-sm" placeholder="Email">
                                        </div>
                                    </div>
                                </div>
                            </div>
                             <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                                        <!-- <label class="hrzn-fm">Pesan :</label> -->
                                    </div>
                                    <div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
                                        <div class="nk-int-st">
                                            <textarea type="textarea" name="pesan" class="form-control input-sm" placeholder="Pesan"></textarea> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                            <button class="btn btn-info" type="submit">SEND MESSAGE</button>
                        </div>
                        </form>
                    </div><!-- end from -->
                </div>
            </div>
        </div>                
    </div>
    <br>
<?php $this->load->view('backend/footer')?>
