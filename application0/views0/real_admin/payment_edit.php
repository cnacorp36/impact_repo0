<?php $this->load->view('backend/header');?>
<?php $this->load->view('backend/navbar');?>
  <div class="breadcomb-area">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="breadcomb-list">
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="breadcomb-wp">
                  <div class="breadcomb-icon">
                    <i class="notika-icon notika-dollar"></i>
                  </div>
                  <div class="breadcomb-ctn">
                    <h2>PAYMENT</h2>
                    <p>KEPALA SAWIT</span></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="normal-table-area">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="normal-table-list mg-t-30">
            <div class="basic-tb-hd">
              <h2 style="text-align: center;">UBAH TAGIHAN</h2>
              
            </div>
            <div class="bsc-tbl-st">
                <div class="row">
                    <div class="col-md-4">
                        <p>Rekening Pengirim</p>
                    </div>
                    <div class="col-md-8">
                        <?php
                            $q_bank_ = $this->M_models->get_data('', 'bank b', null, array('b.id_user' => $id_user), '', 'nama_bank asc');

                            foreach($q_bank_->result() as $key){
                                ?>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-1">
                                    <input type="radio" name="id_bank" onclick="change_bank(this.value)" value="<?=$key->id_bank?>">
                                </div>
                                <div class="col-md-11">
                                    <b style="margin-left: 0px"><?=$key->nama_bank?></b>
                                    <p style="margin-left: 0px; margin-top: 18px"><?=$key->norek?></p>
                                    <p style="margin-left: 0px;"><?=$key->atas_nama?></p>
                                </div>
                            </div>
                        </div>
                                <?php
                            }
                        ?>

                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-1">
                                    <input type="radio" name="id_bank" onclick="change_bank(this.value)" value="000">
                                </div>
                                <div class="col-md-2">
                                    <p>Bank Pengirim</p>
                                </div>
                                <div class="col-md-9">
                                    <input style="margin-top: -8px" type="text" id="bank_pengirim" readonly class="form-control" placeholder="Bank Pengirim">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-2">
                                    <p style="margin-top: 8px">No. Rek</p>
                                </div>
                                <div class="col-md-9">
                                    <input type="number" id="norek" readonly class="form-control" placeholder="Nomor Rekening">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-2">
                                    <p style="margin-top: 8px">Atas Nama</p>
                                </div>
                                <div class="col-md-9">
                                    <input type="text" id="an_" readonly class="form-control" placeholder="Atas Nama">
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4">
                        <p>Status Transfer</p>
                    </div>
                    <div class="col-md-8">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-1">
                                    <input type="radio" name="status_bayar" id="status_bayar0" onclick="ubah_status_bayar(this.value)" value="0">
                                </div>
                                <div class="col-md-11">
                                    <p style="margin-top: 0px">Menunggu Pembayaran</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-1">
                                    <input type="radio" name="status_bayar" id="status_bayar2" value="2" onclick="ubah_status_bayar(this.value)">
                                </div>
                                <div class="col-md-11">
                                    <p style="margin-top: 0px">Pembayaran Selesai</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <center>
                        <button class="btn btn-success" type="button" onclick="save()"><i class="fa fa-save"></i> SIMPAN</button>
                    </center>
                </div>
            </div>
          </div>
          <input type="hidden" name="selected_bank" id="selected_bank">
          <input type="hidden" id="status_bayar">
          <br>
        </div>
      </div>
    </div>
  </div>

  <?php
    $q_pembayaran = $this->M_models->get_data('', 'pembayaran', null, array('id_pembayaran' => $id_pembayaran));
    
    $status_pembayaran = '0';
    if($q_pembayaran->num_rows() > 0){
        $status_pembayaran = $q_pembayaran->row()->status_bayar;
    }
  ?>
  <script type="text/javascript">

    function ubah_status_bayar(val){
        $('#status_bayar').val(val);
    }

    function change_bank(val) {
      if(val != ''){
        $('#selected_bank').val(val);
      }
      if(val == '000'){
        $('#bank_pengirim').prop('readonly', false);
        $('#norek').prop('readonly', false);
        $('#an_').prop('readonly', false);
      }else{
        $('#bank_pengirim').prop('readonly', true);
        $('#norek').prop('readonly', true);
        $('#an_').prop('readonly', true);
      }
    }

    function save() {
      swal({
        title: 'Mohon Tunggu',
        showConfirmButton: false,
        onOpen: () => {
          $.ajax({
            type: "POST",
            url: '<?php echo base_url()?>payment/update_payment',
            dataType: "json",
            data: {
                selected_bank: $('#selected_bank').val(),
                bank_pengirim: $('#bank_pengirim').val(),
                norek: $('#norek').val(),
                an_: $('#an_').val(),
                id_user: '<?=$id_user?>',
                status_bayar: $('#status_bayar').val(),
                id_pembayaran: '<?=$id_pembayaran?>'
            },
            success: function (data) {
              if(data.success == '1'){
                if($('#status_bayar').val() == '2'){
                    for(var c=0; c<data.token.length; c++){
                        send_notif(data.token[c], data.nama_perusahaan+" telah melakukan pembayaran pada produk Anda", "Deal3");
                    }

                    $.ajax({
                        type: "POST",
                        url: '<?php echo base_url()?>email_handler/send_email_konfirmasi_pembayaran/<?=$id_pembayaran?>/<?=$id_user?>',
                        dataType: "json",
                        data: {
                        },
                        success: function (data) {
                        },
                        error: function () {
                        }
                    })
                }
                swal({
                    title: 'Ubah data pembayaran berhasil!',
                    type: 'success'
                }).then((result) => {
                    window.location.href='<?php echo base_url()?>payment';
                })
              }else{
                swal({
                    title: 'Oops..',
                    text: data.msg,
                    type: 'error'
                }).then((result) => {

                })
              }
            },
            error: function () {
              swal({
                  title: 'Oops.. Server sedang error.',
                  type: 'error'
              }).then((result) => {

              })
            }
          })
        },
        onClose: () => {

        }
      })
    }

    function send_notif(token, msg, target){
        let body;
        body = {
            to: token,
            data: {
            custom_notification: {
                title: "Konfirmasi Pembayaran",
                body: msg,
                sound: "default",
                priority: "high",
                show_in_foreground: true,
                targetScreen: target,
                large_icon: 'https://image.freepik.com/free-icon/small-boy-cartoon_318-38077.jpg'
            }
            },
            priority: 10
        };
        $.ajax({
            type: "POST",
            url: 'https://fcm.googleapis.com/fcm/send',
            dataType: "json",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'key=AAAAysvf2c4:APA91bGQyw1452mF0GIxGcb9Rg5yvVXAQ8sIXfR6KEQAiTEopi3Uz0REWQJ8fdv_B4xyi-lNbA74toVEEiMzaUK4NO-hxYCI9rwEkOXG9ptCcd9ZFDb5QkMgsy0g5gAoq1zz_e4E0veW'
            },
            data: JSON.stringify(body),
            success: function (data) {
            },
            error: function () {
            }
        })
    }
  </script>
<?php $this->load->view('backend/footer');?>    
<script>
    $('#status_bayar<?=$status_pembayaran?>').click();
</script>