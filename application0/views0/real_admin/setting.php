<?php $this->load->view('backend/header');?>
<?php $this->load->view('backend/navbar');?>
  <div class="breadcomb-area">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="breadcomb-list">
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="breadcomb-wp">
                  <div class="breadcomb-icon">
                    <i class="notika-icon notika-dollar"></i>
                  </div>
                  <div class="breadcomb-ctn">
                    <h2>SETTING</h2>
                    <p>KEPALA SAWIT</span></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="normal-table-area">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="normal-table-list mg-t-30">
            <?php
              $data0 = $this->M_models->get_data('', 'setting_expired');

              if($data0->num_rows() > 0){
                $row0 = $data0->row();
                ?>
            <div class="basic-tb-hd">
              <h2 style="text-align: center;">Expired (Jam)</h2>
              
            </div>
            <div class="bsc-tbl-st">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>Bid Perusahaan</th>
                    <th>Bid Petani</th>
                    <th>Pembayaran</th>
                    <th>Proses Pengiriman</th>
                    <th>Konfirmasi Pengiriman</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th><span id="bid_perusahaan_"><p id="bid_perusahaan_p"><?=$row0->bid_perusahaan?></p> <button onclick="bid_perusahaan()"><i class="fa fa-edit"></i> edit</button></span>
                      <span id="bid_perusahaan_0" style="display: none">
                        <input type="number" value="<?=$row0->bid_perusahaan?>" id="bid_perusahaan"/>
                        <button onclick="bid_perusahaan_()"><i class="fa fa-save"></i> save</button>
                      </span>
                    </th>
                    <th><span id="bid_petani_"><p id="bid_petani_p"><?=$row0->bid_petani?></p> <button onclick="bid_petani()">  <i class="fa fa-edit"></i> edit</button></span>
                      <span id="bid_petani_0" style="display: none">
                        <input type="number" value="<?=$row0->bid_petani?>" id="bid_petani"/>
                        <button onclick="bid_petani_()"><i class="fa fa-save"></i> save</button>
                      </span>
                    </th>
                    <th><span id="pembayaran_"><p id="pembayaran_p"><?=$row0->pembayaran?></p> <button onclick="pembayaran()">  <i class="fa fa-edit"></i> edit</button></span>
                      <span id="pembayaran_0" style="display: none">
                        <input type="number" value="<?=$row0->pembayaran?>" id="pembayaran"/>
                        <button onclick="pembayaran_()"><i class="fa fa-save"></i> save</button>
                      </span>
                    </th>
                    <th><span id="proses_pengiriman_"><p id="proses_pengiriman_p"><?=$row0->proses_pengiriman?></p> <button onclick="proses_pengiriman()">  <i class="fa fa-edit"></i> edit</button></span>
                      <span id="proses_pengiriman_0" style="display: none">
                        <input type="number" value="<?=$row0->proses_pengiriman?>" id="proses_pengiriman"/>
                        <button onclick="proses_pengiriman_()"><i class="fa fa-save"></i> save</button>
                      </span>
                    </th>
                    <th><span id="konfirmasi_pengiriman_"><p id="konfirmasi_pengiriman_p"><?=$row0->konfirmasi_pengiriman?></p> <button onclick="konfirmasi_pengiriman()">  <i class="fa fa-edit"></i> edit</button></span>
                      <span id="konfirmasi_pengiriman_0" style="display: none">
                        <input type="number" value="<?=$row0->konfirmasi_pengiriman?>" id="konfirmasi_pengiriman"/>
                        <button onclick="konfirmasi_pengiriman_()"><i class="fa fa-save"></i> save</button>
                      </span>
                    </th>
                  </tr>
                </tbody>
              </table>
            </div>
                <?php
              }
            ?>
          </div>
          
          <br>
        </div>
      </div>
    </div>
  </div>

  
  <script type="text/javascript">
    function konfirmasi_pengiriman() {
      $('#konfirmasi_pengiriman_0').show();
      $('#konfirmasi_pengiriman_').hide();
    }

    function konfirmasi_pengiriman_() {
      var konfirmasi_pengiriman = $('#konfirmasi_pengiriman').val();
      $.ajax({
        type: "POST",
        url: '<?php echo base_url()?>payment/admin_setting',
        dataType: "json",
        data: {
          konfirmasi_pengiriman: konfirmasi_pengiriman
        },
        success: function (data) {
          if(data.success == '1'){
            $('#konfirmasi_pengiriman_p').html(konfirmasi_pengiriman)
            $('#konfirmasi_pengiriman_0').hide();
            $('#konfirmasi_pengiriman_').show();
          }else{
            swal({
                title: 'Oops..',
                text: data.msg,
                type: 'error'
            }).then((result) => {

            })
          }
        },
        error: function () {
          swal({
              title: 'Oops.. Server sedang error.',
              type: 'error'
          }).then((result) => {

          })
        }
      })
    }

    function proses_pengiriman() {
      $('#proses_pengiriman_0').show();
      $('#proses_pengiriman_').hide();
    }

    function proses_pengiriman_() {
      var proses_pengiriman = $('#proses_pengiriman').val();
      $.ajax({
        type: "POST",
        url: '<?php echo base_url()?>payment/admin_setting',
        dataType: "json",
        data: {
          proses_pengiriman: proses_pengiriman
        },
        success: function (data) {
          if(data.success == '1'){
            $('#proses_pengiriman_p').html(proses_pengiriman)
            $('#proses_pengiriman_0').hide();
            $('#proses_pengiriman_').show();
          }else{
            swal({
                title: 'Oops..',
                text: data.msg,
                type: 'error'
            }).then((result) => {

            })
          }
        },
        error: function () {
          swal({
              title: 'Oops.. Server sedang error.',
              type: 'error'
          }).then((result) => {

          })
        }
      })
    }

    function pembayaran() {
      $('#pembayaran_0').show();
      $('#pembayaran_').hide();
    }

    function pembayaran_() {
      var pembayaran = $('#pembayaran').val();
      $.ajax({
        type: "POST",
        url: '<?php echo base_url()?>payment/admin_setting',
        dataType: "json",
        data: {
          pembayaran: pembayaran
        },
        success: function (data) {
          if(data.success == '1'){
            $('#pembayaran_p').html(pembayaran)
            $('#pembayaran_0').hide();
            $('#pembayaran_').show();
          }else{
            swal({
                title: 'Oops..',
                text: data.msg,
                type: 'error'
            }).then((result) => {

            })
          }
        },
        error: function () {
          swal({
              title: 'Oops.. Server sedang error.',
              type: 'error'
          }).then((result) => {

          })
        }
      })
    }

    function bid_petani() {
      $('#bid_petani_0').show();
      $('#bid_petani_').hide();
    }

    function bid_petani_() {
      var bid_petani = $('#bid_petani').val();
      $.ajax({
        type: "POST",
        url: '<?php echo base_url()?>payment/admin_setting',
        dataType: "json",
        data: {
          bid_petani: bid_petani
        },
        success: function (data) {
          if(data.success == '1'){
            $('#bid_petani_p').html(bid_petani)
            $('#bid_petani_0').hide();
            $('#bid_petani_').show();
          }else{
            swal({
                title: 'Oops..',
                text: data.msg,
                type: 'error'
            }).then((result) => {

            })
          }
        },
        error: function () {
          swal({
              title: 'Oops.. Server sedang error.',
              type: 'error'
          }).then((result) => {

          })
        }
      })
    }

    function bid_perusahaan() {
      $('#bid_perusahaan_0').show();
      $('#bid_perusahaan_').hide();
    }

    function bid_perusahaan_() {
      var bid_perusahaan = $('#bid_perusahaan').val();
      $.ajax({
        type: "POST",
        url: '<?php echo base_url()?>payment/admin_setting',
        dataType: "json",
        data: {
          bid_perusahaan: bid_perusahaan
        },
        success: function (data) {
          if(data.success == '1'){
            $('#bid_perusahaan_p').html(bid_perusahaan)
            $('#bid_perusahaan_0').hide();
            $('#bid_perusahaan_').show();
          }else{
            swal({
                title: 'Oops..',
                text: data.msg,
                type: 'error'
            }).then((result) => {

            })
          }
        },
        error: function () {
          swal({
              title: 'Oops.. Server sedang error.',
              type: 'error'
          }).then((result) => {

          })
        }
      })
    }
    function change_check(index, val){
      //console.log(val);
      if(val == '0'){
        $('#check'+index).val('1');
        var harga = $('#harga'+index).val();
        var total_ = $('#total_').val();
        //$('#total').html('Rp. '+(parseInt(total_) + parseInt(harga)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
        $('#total_').val(parseInt(total_) + parseInt(harga));
      }else{
        $('#check'+index).val('0');
        var harga = $('#harga'+index).val();
        var total_ = $('#total_').val();
        //$('#total').html('Rp. '+(parseInt(total_) - parseInt(harga)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
        $('#total_').val(parseInt(total_) - parseInt(harga));
      }
    }

    function change_bank(val) {
      if(val != ''){
        $('#selected_bank').val(val);
      }
    }

    function update_bank_tujuan(id_pembayaran) {
      swal({
        title: 'Mohon Tunggu',
        showConfirmButton: false,
        onOpen: () => {
          $.ajax({
            type: "POST",
            url: '<?php echo base_url()?>payment/load_bank',
            dataType: "json",
            data: {
            },
            success: function (data) {
              if(data.success == '1'){
                swal({
                  html: '\n\
                  <div>\n\
                    <div class="row">\n\
                      <p>Pilih Rekening Tujuan</p>'+data.data_bank+'\n\
                      <input type="hidden" id="selected_bank">\n\
                    </div>\n\
                  </div>\n\
                  ',
                  showCancelButton: true,
                  cancelButtonColor: '#d33',
                  onOpen: () => {},
                  onClose: () => {}
                }).then((result) => {
                  if(result){
                    var id_bank_tujuan = $('#selected_bank').val();
                    if(id_bank_tujuan != ''){
                      $.ajax({
                        type: "POST",
                        url: '<?php echo base_url()?>payment/update_bank_tujuan',
                        dataType: "json",
                        data: {
                          id_bank_tujuan: id_bank_tujuan,
                          id_pembayaran: id_pembayaran
                        },
                        success: function (data) {
                          if(data.success == '1'){
                            swal({
                                title: 'Kirim permintaan berhasil!',
                                type: 'success'
                            }).then((result) => {
                                window.location.href='<?php echo base_url()?>payment';
                            })
                          }else{
                            swal({
                                title: 'Oops..',
                                text: data.msg,
                                type: 'error'
                            }).then((result) => {

                            })
                          }
                          //swal.close();
                        },
                        error: function () {
                          swal({
                              title: 'Oops.. Server sedang error.',
                              type: 'error'
                          }).then((result) => {

                          })
                        }
                      })
                    }
                  }
                })
              }else{
                swal({
                    title: 'Oops..',
                    text: data.msg,
                    type: 'error'
                }).then((result) => {

                })
              }
            },
            error: function () {
              swal({
                  title: 'Oops.. Server sedang error.',
                  type: 'error'
              }).then((result) => {

              })
            }
          })
        },
        onClose: () => {

        }
      })
    }
  </script>
<?php $this->load->view('backend/footer');?>    